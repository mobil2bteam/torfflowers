package com.mobil2b.renatganiev.torfflowers.ControlView.Slider;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Slider.ObjSliderItem;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerLink;
import com.mobil2b.renatganiev.torfflowers.R;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.List;


public class CustomPagerAdapter extends PagerAdapter implements IconPagerAdapter {

    private Context mContext;
    private List<ObjSliderItem> itemList;
    private int itemsOnTheScreen    =   1;
    private ClickListenerLink onItemClickLictener;

    public CustomPagerAdapter(Context context, List<ObjSliderItem> itemList, int itemsOnTheScreen) {
        mContext                = context;
        this.itemList           = itemList;
        this.itemsOnTheScreen   = itemsOnTheScreen;
    }
    private int ICON;

    public void setIcon(int icon){
        this.ICON = icon;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_slider, collection, false);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickLictener != null){
                    onItemClickLictener.onClick(v, position, getItem(position).link, false);
                }
            }
        });
        ImageView image = (ImageView)layout.findViewById(R.id.slider_item_image) ;
        if (getItem(position).image != null) {
            if (getItem(position).image.url != null) {
                if (!getItem(position).image.url.equals("")) {
                    Picasso.with(mContext).load(getItem(position).image.url).into(image);
                }
            }
        }

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getIconResId(int index) {
        return ICON;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }
    private ObjSliderItem getItem(int position){
        return itemList.get(position);
    }

    @Override
    public float getPageWidth(int position) {
        return 1f/itemsOnTheScreen;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void setOnItemClickLictener(ClickListenerLink onItemClickLictener) {
        this.onItemClickLictener = onItemClickLictener;
    }
}
