package com.mobil2b.renatganiev.torfflowers.ControlView.MenuInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.MenuInfo.ObjMenuInfo;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 10.10.15.
 */
public class MenuInfoView extends RelativeLayout implements View.OnClickListener {

    Context             mContext;
    TextView txtValue;
    LinearLayout layout_change_city;
    ClickListenerMenuInfo _wrappedOnClickListener;

    ObjMenuInfo value;

    public MenuInfoView(Context context) {
        super(context);
        super.setOnClickListener(this);

        mContext    = context;
        initComponent();
    }

    public MenuInfoView(Context context, ObjMenuInfo v) {
        super(context);
        super.setOnClickListener(this);

        mContext    = context;
        value = v;
        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_menu_info_view, this);

        txtValue                = (TextView) findViewById(R.id.txtValue);
        layout_change_city      = (LinearLayout) findViewById(R.id.layout_change_city);

        layout_change_city.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_wrappedOnClickListener != null) {
                    _wrappedOnClickListener.onClick(view, value);
                }
            }
        });

        updateView();
    }

    private void updateView() {
        if (value != null) {
            txtValue.setText(value.name);
            txtValue.setVisibility(VISIBLE);
        } else {
            txtValue.setVisibility(GONE);
        }
    }

    public ObjMenuInfo getValue() {
        return value;
    }

    public void setValue(ObjMenuInfo v) {
        value = v;
        updateView();
    }

    public void clear() {
        value = null;
        updateView();
    }

    @Override
    public void onClick(View view) {
        if (_wrappedOnClickListener != null)
            _wrappedOnClickListener.onClick(view, value);
    }

    public void setOnClickListener(ClickListenerMenuInfo l) {
        _wrappedOnClickListener = l;
    }

}