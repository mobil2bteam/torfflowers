package com.mobil2b.renatganiev.torfflowers.Class.Authorization;


/**
 * Created by renatganiev on 20.07.14.
 */
public interface CallbackUserExist {
    // Пользователь авторизовался
    public  void onStart();
    public  void onSuccess(boolean exist);
    public  void onError(String message, int error_code);
}
