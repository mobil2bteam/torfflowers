package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterCommentList;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterEmptyRecyclerView;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Comment.ObjComment;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Comment.ObjCommentItem;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.R;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_company_comment extends Fragment implements SwipeRefreshLayout.OnRefreshListener  {

    public static final String TAG = "fragment_app_company_comment";
    public static final int LAYOUT = R.layout.fragment_app_company_comment;
    View rootView;
    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;


    private SwipeRefreshLayout mSwipeRefreshLayout;

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    AdapterCommentList mAdapterSearch;

    ModuleObjects moduleObjects;
    Toolbar toolbar;

    TextView txtCompanyName, txtCompanyRating, txtMessage;
    Button btnCommentAdd, btnClose, btnAddComment;
    LinearLayout layout_rating;
    SlidingUpPanelLayout sliding_layout;
    EditText userComment;
    ProgressBar progressBar3;
    RatingBar ratingBar;

    ObjComment objComment;
    ArrayList<ObjCommentItem> objCommentItemsArrayList = new ArrayList<>();

    public static fragment_app_company_comment getInstance(Bundle args) {
        fragment_app_company_comment fragment = new fragment_app_company_comment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        if (arg != null) {}
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        mSwipeRefreshLayout     = (SwipeRefreshLayout)  rootView.findViewById(R.id.swipe_container);
        sliding_layout          = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);
        txtCompanyName          = (TextView)            rootView.findViewById(R.id.txtCompanyName);
        txtCompanyRating        = (TextView)            rootView.findViewById(R.id.txtCompanyRating);
        txtMessage              = (TextView)            rootView.findViewById(R.id.txtMessage);
        btnCommentAdd           = (Button)              rootView.findViewById(R.id.btnCommentAdd);
        btnClose                = (Button)              rootView.findViewById(R.id.btnClose);
        btnAddComment           = (Button)              rootView.findViewById(R.id.btnAddComment);
        layout_rating           = (LinearLayout)        rootView.findViewById(R.id.layout_rating);
        userComment             = (EditText)            rootView.findViewById(R.id.userComment);
        progressBar3            = (ProgressBar)         rootView.findViewById(R.id.progressBar3);
        ratingBar               = (RatingBar)           rootView.findViewById(R.id.ratingBar);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        moduleObjects = new ModuleObjects(getActivity(), callBackSendDate);

        sliding_layout.addPanelSlideListener(panelSlideListener);
        sliding_layout.setSaveEnabled(false);
        sliding_layout.setScrollableView(userComment);

        initToolbar();
        intiRecyclerView();
        loadData();

        btnCommentAdd.setOnClickListener(view -> {
            sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
            userComment.requestFocus();
        });

        btnClose.setOnClickListener(view -> sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED));


        btnAddComment.setOnClickListener(view -> {
            if (userComment.getText().toString().trim().length() > 0) {
                ModuleObjects moduleObjects1 = new ModuleObjects(getActivity(), new CallBackSendDate() {
                    @Override
                    public void onSend(String txt, ObjLink linkURL) {
                        btnAddComment.setVisibility(View.GONE);
                        progressBar3.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onReturnAnswer(final String result, boolean cache, long cache_datetime, ObjLink linkURL) {
                        try {
                            JSONObject json = new JSONObject(result);
                            if ((json.has("review")) || (json.has("comment"))) {
                                // here you check the value of getActivity() and break up if needed
                                if(getActivity() == null) return;
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnAddComment.setVisibility(View.VISIBLE);
                                        progressBar3.setVisibility(View.GONE);
                                        sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                                        loadData();

                                    }
                                });
                            } else {
                                if(getActivity() == null) return;
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnAddComment.setVisibility(View.VISIBLE);
                                        progressBar3.setVisibility(View.GONE);
                                        sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                                    }
                                });
                            }
                        } catch (JSONException e) {

                        }


                    }

                    @Override
                    public void onError(final String text, int code_error, ObjLink linkURL) {
                        // here you check the value of getActivity() and break up if needed
                        if(getActivity() == null) return;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnAddComment.setVisibility(View.VISIBLE);
                                progressBar3.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onHttpError(final String text, int code_error, ObjLink linkURL) {
                        // here you check the value of getActivity() and break up if needed
                        if(getActivity() == null) return;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnAddComment.setVisibility(View.VISIBLE);
                                progressBar3.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

                moduleObjects1.setCompanyCommentAdd(true, userComment.getText().toString(), ratingBar.getRating(), User.getCurrentSession());
            }
        });

        return rootView;
    }

    SlidingUpPanelLayout.PanelSlideListener  panelSlideListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {

        }

        @Override
        public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
            if (newState.equals(SlidingUpPanelLayout.PanelState.ANCHORED)) {}
            if (newState.equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {}
            if (newState.equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {}
        }
    };

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        AppBarLayout app_bar = (AppBarLayout) rootView.findViewById(R.id.app_bar);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(0));

            app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    //some other code here
                    ViewCompat.setElevation(appBarLayout, Helps.dpToPx(2));
                }
            });

        }
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        toolbar.setTitle("Отзывы");

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_menu));
            } else {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_menu));
            }
        }
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView   = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    void loadData() {
        moduleObjects.getCompanyCommentList(true, User.getCurrentSession());
    }

    void refreshAllData() {
        objCommentItemsArrayList.clear();
        if (objComment != null) {
            if (objComment.user_comment != null) {
                objCommentItemsArrayList.add(objComment.user_comment);
                btnCommentAdd.setVisibility(View.GONE);
                txtMessage.setVisibility(View.GONE);
            } else {
                if (User.isLoggedIn()) {
                    btnCommentAdd.setVisibility(View.VISIBLE);
                    txtMessage.setVisibility(View.GONE);
                } else {
                    btnCommentAdd.setVisibility(View.GONE);
                    txtMessage.setVisibility(View.VISIBLE);
                }
            }
            if (objComment.all_comments != null) {
                objCommentItemsArrayList.addAll(objComment.all_comments);
            }

            txtCompanyName.setText(objComment.name);
            txtCompanyRating.setText(String.valueOf(objComment.rating));
            if (Double.valueOf(objComment.rating) < 3) {
                txtCompanyRating.setBackgroundResource(R.color.colorTextRating2);
            }
            if ((Double.valueOf(objComment.rating) >= 3) && (Double.valueOf(objComment.rating) < 4)) {
                txtCompanyRating.setBackgroundResource(R.color.colorTextRating3);
            }
            if (Double.valueOf(objComment.rating) >= 4) {
                txtCompanyRating.setBackgroundResource(R.color.colorTextRating4);
            }

        } else {
            if (User.isLoggedIn()) {
                btnCommentAdd.setVisibility(View.VISIBLE);
                txtMessage.setVisibility(View.GONE);
            } else {
                btnCommentAdd.setVisibility(View.GONE);
                txtMessage.setVisibility(View.VISIBLE);
            }
            if (PublicData.SETTINGS != null) {
                if (PublicData.SETTINGS.company_info != null) {
                    txtCompanyName.setText(PublicData.SETTINGS.company_info.name);
                }
            }
            txtCompanyRating.setText("0.0");
            txtCompanyRating.setBackgroundResource(R.color.colorTextRating3);
        }
        layout_rating.setVisibility(View.VISIBLE);

        if (objCommentItemsArrayList.size() > 0) {
            mAdapterSearch = new AdapterCommentList(getActivity(), objCommentItemsArrayList);
            mRecyclerView.setAdapter(mAdapterSearch);
            mAdapterSearch.notifyDataSetChanged();
        } else {
            AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_message), R.drawable.ic_icon_message_empty);
            LinearLayoutManager managerLinear = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(managerLinear);
            mRecyclerView.setAdapter(mEmptyAdapter);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (sliding_layout.getPanelState() != SlidingUpPanelLayout.PanelState.COLLAPSED) {
                        sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });

    }

    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }

        @Override
        public void onReturnAnswer(final String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    String rezData = OpenBlocks.getOpenData(result);
                    if (rezData != null) {
                        if (rezData.equals(PublicData.QUESTION_TYPE_COMMENT_LIST)) {
                            objComment = new ObjComment(result);
                            refreshAllData();
                        }
                    } else {
                        refreshAllData();
                    }
                }
            });
        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    @Override
    public void onRefresh() {
        loadData();
    }
}
