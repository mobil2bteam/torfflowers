package com.mobil2b.renatganiev.torfflowers;

import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjProperties;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjPrice;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProduct;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBBag;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBFavorites;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBWatch;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.ControlView.PropertyValueView.PropertyValueView;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class product_item extends AppCompatActivity {

    Bundle arg;

    int product_id;
    ObjProduct objProduct;
    ModuleObjects moduleObjects;

    Boolean rised = false;

    View view1, view2;

    TextView txtDescription, txtName, txtDescriptionShort, txtPrice, txtBagIndicator;
    LinearLayout layout_property, layout_information, layout_price_select;

    ImageButton btnAddBag;

    TextView txtCount;

    ImageView favoriteBG, imageView, buttonBG;

    //

    ImageButton plus, minus;

    ObjLink objLink = null;

    ObjPrice objPrice;

    ImageButton buttonFavorite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_item);

        txtDescription      = findViewById(R.id.txtDescription);
        txtDescriptionShort = findViewById(R.id.txtDescriptionShort);
        txtName             = findViewById(R.id.txtName);
        txtPrice            = findViewById(R.id.txtPrice);
        txtBagIndicator     = findViewById(R.id.txtBagIndicator);
        layout_property     = findViewById(R.id.layout_property);
        layout_information  = findViewById(R.id.layout_information);
        layout_price_select = findViewById(R.id.layout_price_select);

        buttonFavorite = findViewById(R.id.btn_favorit);
        favoriteBG = findViewById(R.id.btn_bg_favorit);


        plus = findViewById(R.id.btn_plus);
        minus = findViewById(R.id.btn_minus);

        txtCount = findViewById(R.id.count);

        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);

        btnAddBag           = findViewById(R.id.btnAddBag);
        buttonBG            = findViewById(R.id.btn_bg);

        imageView       = findViewById(R.id.imageView);
        imageView.setVisibility(View.GONE);

        //txtDescription.setText(Html.fromHtml(PublicData.SETTINGS.company_info.description_long));

        arg = getIntent().getExtras();
        if (arg != null) {
            if (arg.containsKey("id")) {
                int id = arg.getInt("id");
                if (id != 0) {
                    product_id = id;
                    loadProductItem(id);
                }
            }
            if (arg.containsKey("link")) {
                objLink = (ObjLink) arg.getSerializable("link");
                loadProductItem(0);
            }
        }
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar(String title) {
        Toolbar toolbarTop = findViewById(R.id.main_toolbar);
        //TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        //mTitle.setText(title);

        toolbarTop.setTitle(title);

        toolbarTop.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        Drawable drawable = ContextCompat.getDrawable(product_item.this, R.drawable.ic_toolbar_icon_back_24dp);
        toolbarTop.setNavigationIcon(drawable);
        //setSupportActionBar(toolbarTop);
        setSupportActionBar(toolbarTop);

        //setHasOptionsMenu(true);

        toolbarTop.setNavigationOnClickListener(v -> finish());
    }


    void loadProductItem(int id) {
        moduleObjects = new ModuleObjects(product_item.this, new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
                product_item.this.runOnUiThread(new Runnable() {
                    public void run() {
                        PublicData.showDialog(product_item.this);
                    }
                });
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
                try {
                    JSONObject json = new JSONObject(result);
                    String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                    if (typeDate.equals(PublicData.QUESTION_TYPE_PRODUCT)) {
                        objProduct = new ObjProduct(result);
                        product_item.this.runOnUiThread(new Runnable() {
                            public void run() {
                                DBWatch watch = new DBWatch(product_item.this);
                                watch.add(objProduct);
                                watch.close();
                                PublicData.closeDialog();
                                initToolbar("Каталог");
                                updateView();
                            }
                        });
                    }
                } catch (JSONException e) {
                }
            }

            @Override
            public void onError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    product_item.this.runOnUiThread(new Runnable() {
                        public void run() {
                            PublicData.closeDialog();
                            Toast.makeText(product_item.this, text, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    product_item.this.runOnUiThread(new Runnable() {
                        public void run() {
                            PublicData.closeDialog();
                            Toast.makeText(product_item.this, getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

            @Override
            public void onHttpError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    product_item.this.runOnUiThread(new Runnable() {
                        public void run() {
                            PublicData.closeDialog();
                            Toast.makeText(product_item.this, text, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    product_item.this.runOnUiThread(new Runnable() {
                        public void run() {
                            PublicData.closeDialog();
                            Toast.makeText(product_item.this, getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        if (id != 0) {
            moduleObjects.getProduct(false, id, User.getCurrentSession());
        }
        if (objLink != null) {
            moduleObjects.getDataToLink(false, objLink, User.getCurrentSession());
        }
    }

    void updateView() {
        if (objProduct != null) {

            txtName.setText(objProduct.name);
            if (Helps.isNotEmpty(objProduct.short_description)) {
                txtDescriptionShort.setText(objProduct.short_description);
                txtDescriptionShort.setVisibility(View.VISIBLE);
            } else {
                txtDescriptionShort.setVisibility(View.GONE);
            }
            if (Helps.isNotEmpty(objProduct.description)) {
                txtDescription.setText(Html.fromHtml(objProduct.description));
                txtDescription.setVisibility(View.VISIBLE);
            } else {
                txtDescription.setVisibility(View.GONE);
            }

            if (objProduct.images != null) {
                if (objProduct.images.size() > 0) {
                    if (objProduct.images.get(0) != null) {
                        if (Helps.isNotEmpty(objProduct.images.get(0).url)) {
                            //Picasso.with(mContext).load(getItem(position).image.url).into(viewHolder0.imgImage);

                            DisplayMetrics metrics = Helps.getDisplayWH(product_item.this);
                            if (metrics != null) {
                                if (metrics.widthPixels != 0) {
                                    float c = (float) objProduct.images.get(0).heigth / (float) objProduct.images.get(0).width;
                                    Picasso.with(product_item.this)
                                            .load(objProduct.images.get(0).url)
                                            .resize(metrics.widthPixels, (int)(((float)metrics.widthPixels) * c))
                                            .into(imageView);
                                } else {
                                    Picasso.with(product_item.this).load(objProduct.images.get(0).url).into(imageView);
                                }
                            } else {
                                Picasso.with(product_item.this).load(objProduct.images.get(0).url).into(imageView);
                            }

                            imageView.setVisibility(View.VISIBLE);

                        }
                    }
                }
            }


            if (objProduct.properties != null) {
                if (objProduct.properties.size() > 0) {
                    layout_property.removeAllViews();
                    for (ObjProperties objProperties : objProduct.properties) {
                        PropertyValueView propertyValueView = new PropertyValueView(product_item.this, objProperties);
                        layout_property.addView(propertyValueView);
                    }
                } else {
                    //layout_property.setVisibility(View.GONE);
                }
            } else {
                //layout_property.setVisibility(View.GONE);
            }




            if (objProduct.prices != null) {
                if (objProduct.prices.size() > 0) {

                    layout_price_select.setVisibility(View.VISIBLE);
                    layout_information.setVisibility(View.GONE);

                    objPrice = objProduct.prices.get(0);
                    initIndicator(objPrice);


                    final DBBag[] bag = {new DBBag(product_item.this)};

                    if(bag[0].getCount(objPrice.id) > 0){
                    txtCount.setText(String.valueOf(bag[0].getCount(objPrice.id)));
                    buttonBG.setImageDrawable(getResources().getDrawable(R.drawable.shape));
                    btnAddBag.setVisibility(View.GONE);
                    view1.setVisibility(View.VISIBLE);
                    view2.setVisibility(View.VISIBLE);
                    txtCount.setVisibility(View.VISIBLE);
                    plus.setVisibility(View.VISIBLE);
                    minus.setVisibility(View.VISIBLE);
                    }


                    checkFavorit();


                    buttonFavorite.setOnClickListener(view -> {
                        DBFavorites favorites1 = new DBFavorites(product_item.this);
                        if (favorites1.exist(objProduct.id)) {
                            favorites1.delete(objProduct.id);
                            favorites1.close();
                            checkFavorit();
                        } else {
                            favorites1.add(objProduct.id);
                            favorites1.close();
                            checkFavorit();
                        }
                    });



                    btnAddBag.setOnClickListener(view -> {
                        if (objPrice != null) {
                            bag[0] = new DBBag(product_item.this);

                            int count = bag[0].getCount(objPrice.id) + 1;

                            Toast.makeText(product_item.this, "Товар добавлен в корзину!",
                                    Toast.LENGTH_SHORT).show();

                            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(buttonBG, "alpha",0, 1);
                            objectAnimator.setDuration(400);
                            objectAnimator.setStartDelay(10);
                            objectAnimator.start();

                            if(rised) {
                                buttonBG.setImageDrawable(getResources().getDrawable(R.drawable.shape_circle));
                                btnAddBag.setVisibility(View.VISIBLE);
                                view1.setVisibility(View.GONE);
                                view2.setVisibility(View.GONE);
                                txtCount.setVisibility(View.GONE);
                                plus.setVisibility(View.GONE);
                                minus.setVisibility(View.GONE);
                            } else {
                                buttonBG.setImageDrawable(getResources().getDrawable(R.drawable.shape));
                                btnAddBag.setVisibility(View.GONE);
                                view1.setVisibility(View.VISIBLE);
                                view2.setVisibility(View.VISIBLE);
                                txtCount.setVisibility(View.VISIBLE);
                                plus.setVisibility(View.VISIBLE);
                                minus.setVisibility(View.VISIBLE);
                            }

                            rised = !rised;

                            bag[0].add(objProduct.id, objPrice.id, count);
                                if (bag[0].exist(objPrice.id)) {
                                    initIndicator(objPrice);
                                }
                            txtCount.setText(String.valueOf(bag[0].getCount(objPrice.id)));
                            bag[0].close();
                        }
                    });


                    minus.setOnClickListener(view -> {

                        if(Integer.valueOf(txtCount.getText().toString()) < 2){

                            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(buttonBG, "alpha",0, 1);
                            objectAnimator.setDuration(400);
                            objectAnimator.setStartDelay(10);
                            objectAnimator.start();

                            buttonBG.setImageDrawable(getResources().getDrawable(R.drawable.shape_circle));
                            btnAddBag.setVisibility(View.VISIBLE);
                            view1.setVisibility(View.GONE);
                            view2.setVisibility(View.GONE);
                            txtCount.setVisibility(View.GONE);
                            plus.setVisibility(View.GONE);
                            minus.setVisibility(View.GONE);
                            rised = false;
                            bag[0] = new DBBag(product_item.this);

                            int count = bag[0].getCount(objPrice.id) - 1 ;
                            bag[0].add(objProduct.id, objPrice.id, count);

                            txtCount.setText(String.valueOf(count));



                        } else {

                            bag[0] = new DBBag(product_item.this);
                            int count = bag[0].getCount(objPrice.id) -1 ;
                            bag[0].add(objProduct.id, objPrice.id, count);
                            txtCount.setText(String.valueOf(count));
                        }

                        if (bag[0].exist(objPrice.id)) {
                            initIndicator(objPrice);
                        }
                        bag[0].close();

                    });



                    plus.setOnClickListener(view -> {

                        bag[0] = new DBBag(product_item.this);

                        int count = bag[0].getCount(objPrice.id) + 1;
                        bag[0].add(objProduct.id, objPrice.id, count);
                        if (bag[0].exist(objPrice.id)) {
                            initIndicator(objPrice);
                        }
                        txtCount.setText(String.valueOf(count));
                        bag[0].close();});

                } else {
                    layout_price_select.setVisibility(View.GONE);
                    layout_information.setVisibility(View.VISIBLE);
                }
            } else {
                layout_price_select.setVisibility(View.GONE);
                layout_information.setVisibility(View.VISIBLE);
            }

        }
        //PublicData.closeDialog();
    }

    void initIndicator(ObjPrice price) {
        if (price != null) {

            String p = String.valueOf(price.price).concat(" р.");
            txtPrice.setText(p);

            DBBag bag = new DBBag(product_item.this);
            int count = bag.getCount(price.id);
            double sum = price.price * count;
            txtBagIndicator.setText("В корзине ".concat(String.valueOf(count).concat(" шт. на сумму: ".concat(String.valueOf(sum).concat(" р.")))));
            bag.close();
        }
    }


    void checkFavorit(){
        DBFavorites favorites = new DBFavorites(product_item.this);
        if (favorites.exist(objProduct.id)) {
            favoriteBG.setImageDrawable(getResources().getDrawable(R.drawable.shape_circle));
        } else {
            favoriteBG.setImageDrawable(getResources().getDrawable(R.drawable.shape_circle_unchecked));
        }
        favorites.close();
    }



    @Override
    protected void onStop() {
        super.onStop();
        if (moduleObjects != null) {
            moduleObjects.cancelAllRequest();
        }
    }
}
