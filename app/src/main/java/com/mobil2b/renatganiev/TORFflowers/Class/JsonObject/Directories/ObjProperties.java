package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjProperties extends ObjBaseDirectories {

    public int type_id;
    public boolean filter;
    public boolean filter_main;
    public String value;
    public int value_id;
    public int count_value;


    public ObjProperties() {
        init();
    }
    public ObjProperties(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjProperties(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("properties")) {
                JSONObject jsonMain = json.getJSONObject("properties");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                super.id = json.getInt("id");
            }
            if (json.has("name")) {
                super.name = json.getString("name");
            }
            if (json.has("typeId")) {
                type_id = json.getInt("typeId");
            }
            if (json.has("value_id")) {
                value_id = json.getInt("value_id");
            }
            if (json.has("value")) {
                value = json.getString("value");
            }
            if (json.has("count_value")) {
                count_value = json.getInt("count_value");
            }
            if (json.has("filter")) {
                filter = json.getBoolean("filter");
            }
            if (json.has("filter_main")) {
                filter_main = json.getBoolean("filter_main");
            }
        } catch (JSONException e) {
        }
    }

}
