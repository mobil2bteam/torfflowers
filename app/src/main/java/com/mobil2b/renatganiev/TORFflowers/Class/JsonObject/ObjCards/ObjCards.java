package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjCards;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCards {

    public static String TYPE_CARD_BALL = "ball";
    public static String TYPE_CARD_DISCOUNT = "discount";

    public int     id;
    public String number, type;
    public int balance, cashback, max_percent, relation;
    public double totalCard;
    public int receive_balls;   // Начислить баллы
    public int taken_balls;     // Списать баллы

    public ObjCards() {
        init();
    }
    public ObjCards(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjCards(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("cards")) {
                JSONObject jsonMain = json.getJSONObject("cards");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("balance")) {
                balance = json.getInt("balance");
            }
            if (json.has("cashback")) {
                cashback = json.getInt("cashback");
            }
            if (json.has("max_percent")) {
                max_percent = json.getInt("max_percent");
            }
            if (json.has("relation")) {
                relation = json.getInt("relation");
            }
            if (json.has("receive_balls")) {
                receive_balls = json.getInt("receive_balls");
            }
            if (json.has("taken_balls")) {
                taken_balls = json.getInt("taken_balls");
            }
            if (json.has("totalCard")) {
                totalCard = json.getDouble("totalCard");
            }
            if (json.has("number")) {
                number = json.getString("number");
            }
            if (json.has("type")) {
                type = json.getString("type");
            }
        } catch (JSONException e) {
        }
    }

}
