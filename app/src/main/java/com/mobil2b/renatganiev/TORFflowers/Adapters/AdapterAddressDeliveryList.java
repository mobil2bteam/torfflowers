package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.DeliveryAddress.ObjDeliveryAddress;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerDeliveryAddress;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterAddressDeliveryList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private ArrayList<ObjDeliveryAddress> mList;
    String mCity;

    ClickListenerDeliveryAddress clickListenerDelivery;

    public AdapterAddressDeliveryList(Context context, ArrayList<ObjDeliveryAddress> list, String city) {
        this.mContext = context;
        this.mList = list;
        this.mCity = city;
    }

    public void setOnClickListenerDelivery(ClickListenerDeliveryAddress onClickListener){
        this.clickListenerDelivery = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_delivery_address, parent, false);

        return new ViewHolderProduct(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ViewHolderProduct viewHolder0 = (ViewHolderProduct) holder;
        final ObjDeliveryAddress obj = getItem(position);
        obj.city = this.mCity;

        // НАИМЕНОВАНИЕ
        viewHolder0.txtAddress.setText(obj.getAddress());

        viewHolder0.txtComment.setVisibility(View.GONE);
        if (obj.comment != null) {
            if (!obj.comment.equals("")) {
                viewHolder0.txtComment.setText(obj.comment);
                viewHolder0.txtComment.setVisibility(View.VISIBLE);
            }
        }

        viewHolder0.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListenerDelivery != null) {
                    clickListenerDelivery.onRemove(view, obj);
                }
            }
        });

        viewHolder0.setClickListener(clickListenerDelivery);
    }


    private ObjDeliveryAddress getItem(int position){
        return mList.get(position);
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderProduct extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtAddress, txtComment;
        ImageView imgDelete;

        ClickListenerDeliveryAddress clickListenerDelivery;

        public ViewHolderProduct(View itemView) {
            super(itemView);

            txtAddress          = (TextView) itemView.findViewById(R.id.txtAddress);
            txtComment          = (TextView) itemView.findViewById(R.id.txtComment);
            imgDelete           = (ImageView) itemView.findViewById(R.id.imgDelete);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListenerDelivery != null) {
                clickListenerDelivery.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerDeliveryAddress clickListener){
            this.clickListenerDelivery = clickListener;
        }
    }
}