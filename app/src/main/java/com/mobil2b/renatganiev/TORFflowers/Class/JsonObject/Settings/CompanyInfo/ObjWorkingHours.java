package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjWorkingHours {
    public String day, hours_start, hours_finish;
    public boolean free_day;

    public ObjWorkingHours() {
        init();
    }
    public ObjWorkingHours(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjWorkingHours(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("day")) {
                this.day = json.getString("day");
            }
            if (json.has("hours_start")) {
                this.hours_start = json.getString("hours_start");
            }
            if (json.has("hours_finish")) {
                this.hours_finish = json.getString("hours_finish");
            }
            if (json.has("free_day")) {
                this.free_day = json.getBoolean("free_day");
            }

        } catch (JSONException e) {
        }
    }
}
