package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerSort;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjSort;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterSort extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private ArrayList<ObjSort> mList;

    ClickListenerSort onClickListener;

    public AdapterSort(Context context, ArrayList<ObjSort> list) {
        this.mContext = context;
        this.mList = list;
    }


    public void setOnClickListener(ClickListenerSort onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view3 = inflater.inflate(R.layout.item_sort, parent, false);
        return new ViewHolderOrders(view3);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderOrders viewHolder0 = (ViewHolderOrders) holder;

        viewHolder0.txtSort.setText(getItem(position).name);

        if (getItem(position).check) {
            viewHolder0.imgCheck.setVisibility(View.VISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder0.layout_body.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
                viewHolder0.txtSort.setTextColor(mContext.getResources().getColor(R.color.colorBackground));
            } else {
                viewHolder0.layout_body.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorPrimary, null));
                viewHolder0.txtSort.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorBackground, null));
            }
        } else {
            viewHolder0.imgCheck.setVisibility(View.INVISIBLE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder0.layout_body.setBackgroundColor(mContext.getResources().getColor(R.color.colorBackground));
                viewHolder0.txtSort.setTextColor(mContext.getResources().getColor(R.color.colorTextPrimary));
            } else {
                viewHolder0.layout_body.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorBackground, null));
                viewHolder0.txtSort.setTextColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorTextPrimary, null));
            }
        }

        viewHolder0.setClickListener(onClickListener);
    }



    private ObjSort getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderOrders extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtSort;
        ImageView imgCheck;
        LinearLayout layout_body;
        ClickListenerSort clickListener;

        public ViewHolderOrders(View itemView) {
            super(itemView);

            txtSort             = (TextView)itemView.findViewById(R.id.txtSort);
            imgCheck            = (ImageView)itemView.findViewById(R.id.imgCheck);
            layout_body         = (LinearLayout)itemView.findViewById(R.id.layout_body);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            for (ObjSort objSort : mList) {
                objSort.check = false;
            }
            getItem(getAdapterPosition()).check = true;

            if (clickListener != null){
                clickListener.onClick(v, getItem(getAdapterPosition()));
                notifyDataSetChanged();
            }
        }
        public void setClickListener(ClickListenerSort clickListener){
            this.clickListener = clickListener;
        }

    }

}