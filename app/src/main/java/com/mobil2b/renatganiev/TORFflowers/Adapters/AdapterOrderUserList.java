package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order.ObjOrderUserList;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.ControlView.ItemImageView.ItemImageView;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerOrderUserList;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterOrderUserList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private List<ObjOrderUserList> mList;

    public AdapterOrderUserList(Context context, ArrayList<ObjOrderUserList> list) {
        this.mContext = context;
        this.mList = list;
    }

    ClickListenerOrderUserList onClickListener;
    public void setOnClickListener(ClickListenerOrderUserList onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view3 = inflater.inflate(R.layout.item_order_list, parent, false);
        return new ViewHolderOrders(view3);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderOrders viewHolder0 = (ViewHolderOrders) holder;

        viewHolder0.txtNumber.setText("Заказ №".concat(String.valueOf(getItem(position).number)));
        viewHolder0.txtStatus.setText(getItem(position).status);
        viewHolder0.txtDelivery.setText(getItem(position).deliveryText);
        viewHolder0.txtPayment.setText(getItem(position).paymentText);
        if (getItem(position).products != null) {
            int count = getItem(position).products.size();
            viewHolder0.txtCount.setText(String.valueOf(count).concat(" шт."));
        }
        viewHolder0.txtTotal.setText(String.valueOf(getItem(position).total).concat(" р."));
        if (getItem(position).isPayment) {
            viewHolder0.txtIsPayment.setText("Оплачен");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder0.txtIsPayment.setTextColor(mContext.getResources().getColorStateList(R.color.colorPrimaryDark));
            } else {
                viewHolder0.txtIsPayment.setTextColor(ResourcesCompat.getColorStateList(mContext.getResources(), R.color.colorPrimaryDark, null));
            }
        } else {
            viewHolder0.txtIsPayment.setText("Не оплачен");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder0.txtIsPayment.setTextColor(mContext.getResources().getColorStateList(R.color.colorTextDiscountPrice));
            } else {
                viewHolder0.txtIsPayment.setTextColor(ResourcesCompat.getColorStateList(mContext.getResources(), R.color.colorTextDiscountPrice, null));
            }
        }
        viewHolder0.txtDate.setText(getItem(position).createdAt);

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(Helps.dpToPx(50), Helps.dpToPx(50));
        param.setMargins(Helps.dpToPx(3), Helps.dpToPx(3), Helps.dpToPx(3), Helps.dpToPx(3));

        viewHolder0.layout_image.removeAllViews();
        for (ObjImage img : getItem(position).products) {
            ObjProductMin objProduct = new ObjProductMin();
            objProduct.image = img;
            final ItemImageView imageView = new ItemImageView(mContext, objProduct);
            imageView.setLayoutParams(param);
            viewHolder0.layout_image.addView(imageView);
        }

        viewHolder0.setClickListener(onClickListener);
    }



    private ObjOrderUserList getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderOrders extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtNumber, txtStatus, txtDelivery, txtPayment, txtCount, txtTotal, txtIsPayment, txtDate;
        LinearLayout layout_image;
        ClickListenerOrderUserList clickListener;

        public ViewHolderOrders(View itemView) {
            super(itemView);

            txtNumber               = (TextView)itemView.findViewById(R.id.txtNumber);
            txtStatus               = (TextView)itemView.findViewById(R.id.txtStatus);
            txtDelivery             = (TextView)itemView.findViewById(R.id.txtDelivery);
            txtPayment              = (TextView)itemView.findViewById(R.id.txtPayment);
            txtCount                = (TextView)itemView.findViewById(R.id.txtCount);
            txtTotal                = (TextView)itemView.findViewById(R.id.txtTotal);
            txtIsPayment            = (TextView)itemView.findViewById(R.id.txtIsPayment);
            txtDate                 = (TextView)itemView.findViewById(R.id.txtDate);
            layout_image            = (LinearLayout)itemView.findViewById(R.id.layout_image);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null){
                clickListener.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerOrderUserList clickListener){
            this.clickListener = clickListener;
        }

    }

}