package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Comment.ObjCommentItem;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.List;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterCommentList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ObjCommentItem> mList;

    public AdapterCommentList(Context context, List<ObjCommentItem> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_user_comment, parent, false);

        return new ViewHolderComment(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderComment viewHolder0 = (ViewHolderComment) holder;
        final ObjCommentItem obj = getItem(position);

        // НАИМЕНОВАНИЕ
        if (obj.user != null) {
            viewHolder0.txtUserName.setText(obj.user.user_name);
            viewHolder0.txtUserCity.setText(obj.user.city_name);
        }
        if (Double.valueOf(obj.rating) < 3) {
            viewHolder0.txtUserRating.setBackgroundResource(R.color.colorTextRating2);
        }
        if ((Double.valueOf(obj.rating) >= 3) && (Double.valueOf(obj.rating) < 4)) {
            viewHolder0.txtUserRating.setBackgroundResource(R.color.colorTextRating3);
        }
        if (Double.valueOf(obj.rating) >= 4) {
            viewHolder0.txtUserRating.setBackgroundResource(R.color.colorTextRating4);
        }
        viewHolder0.txtUserRating.setText(String.valueOf(obj.rating));

        viewHolder0.txtUserComment.setText(String.valueOf(obj.text));
        viewHolder0.txtDate.setText(String.valueOf(obj.date));
    }


    private ObjCommentItem getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderComment extends RecyclerView.ViewHolder  {

        TextView txtUserName, txtUserCity, txtUserComment, txtUserRating, txtDate;

        public ViewHolderComment(View itemView) {
            super(itemView);

            txtUserName     = (TextView)itemView.findViewById(R.id.txtUserName);
            txtUserCity     = (TextView)itemView.findViewById(R.id.txtUserCity);
            txtUserComment  = (TextView)itemView.findViewById(R.id.txtUserComment);
            txtUserRating   = (TextView)itemView.findViewById(R.id.txtUserRating);
            txtDate         = (TextView)itemView.findViewById(R.id.txtDate);
        }
    }
}