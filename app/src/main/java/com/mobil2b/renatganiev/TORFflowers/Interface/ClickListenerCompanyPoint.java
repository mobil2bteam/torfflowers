package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerCompanyPoint {
    void onClick(View view, ObjCompanyPoint objCompanyPoint);
}
