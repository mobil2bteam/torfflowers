package com.mobil2b.renatganiev.torfflowers.ControlView.Slider;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Slider.ObjSlider;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerLink;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.IconPageIndicator;
import com.viewpagerindicator.PageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


public class SliderView extends RelativeLayout {
    private Context mContext;
    private float density = getResources().getDisplayMetrics().density;
    private int dpPad = (int) (5*density + 0.5f);

    RelativeLayout.LayoutParams indicatorParams = new RelativeLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT);

    PageIndicator mPageIndicator;
    private CustomViewPager customViewPager;
    private int circleFillColor = Color.YELLOW;
    private int circleStrokeColor = Color.BLACK;
    private int circleStrokeWidth = 3;
    private int circleRadius = 7;
    private int swipeSpeed = 5000;
    private int itemsOnTheScreen=1;
    private int pagerItemMargin = (int) (0*density);
    Handler handler = new Handler();

    private ObjSlider mObjSlider;
    int currentPage=0;
    int NUM_PAGES;

    ClickListenerLink onClickListener;


    Runnable swipe = new Runnable() {
        public void run() {
            if (currentPage == NUM_PAGES - 1) {
                currentPage = 0;
            }
            customViewPager.setCurrentItem(currentPage++, true);
        }
    };




    public void setOnClickListener(ClickListenerLink onClickListener){
        this.onClickListener = onClickListener;
        if (customViewPager != null) {
            customViewPager.setOnItemClickListener(onClickListener);
        }
    }

    public SliderView(Context context, String json) {
        super(context);
        this.mContext = context;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonObject != null && jsonObject.has("slider")) {
            mObjSlider = new ObjSlider();
            try {
                mObjSlider.loadJsonObject(new JSONObject(json).getJSONObject("slider"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            init();
        }


    }
    public SliderView(Context context, ObjSlider objSlider) {
        super(context);
        this.mContext = context;
        this.mObjSlider = objSlider;
        init();
    }
    public void setMargin(int left,int top,int right,int bottom){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
        params.topMargin = (int) (top*density);
        params.leftMargin = (int) (left*density);
        params.rightMargin = (int) (right*density);
        params.bottomMargin = (int) (bottom*density);
        setLayoutParams(params);
    }
    public void setSize(int height, int width){
        getLayoutParams().height = toDp(height);
        getLayoutParams().width = toDp(width);
    }
    private void init(){

        NUM_PAGES = mObjSlider.items.size()+1;
        customViewPager = new CustomViewPager(mContext,mObjSlider.items,itemsOnTheScreen);
        customViewPager.setOnItemClickListener(onClickListener);


        setLayoutParams(new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        //indicatorParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        customViewPager.setId(ViewIdGenerator.generateViewId());
        indicatorParams.addRule(RelativeLayout.ALIGN_BOTTOM,customViewPager.getId());
        indicatorParams.addRule(RelativeLayout.CENTER_HORIZONTAL);


        customViewPager.setPageMargin(pagerItemMargin);
        customViewPager.setPagingEnabled(mObjSlider.manual);

        addView(customViewPager);

        if(mObjSlider.speed != 0){
            swipeSpeed=mObjSlider.speed;
        }
        if(mObjSlider.status){
            setCircle();
        }
        if(mObjSlider.auto){
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(swipe);
                }
            }, 100, swipeSpeed);
        }
    }


    public void setCircleFillColor(int fillColor, int pageColor){
        this.circleFillColor = fillColor;
        if(mPageIndicator != null && mPageIndicator instanceof CirclePageIndicator) {
            ((CirclePageIndicator) mPageIndicator).setFillColor(circleFillColor);
            ((CirclePageIndicator) mPageIndicator).setPageColor(pageColor);
        }
    }
    public void setCircleStrokeColor(int color){
        this.circleStrokeColor = color;
        if(mPageIndicator != null && mPageIndicator instanceof CirclePageIndicator) {
            ((CirclePageIndicator) mPageIndicator).setStrokeColor(circleStrokeColor);
        }
    }
    public void setCircleStrokeWidth(int width){
        this.circleStrokeWidth = width;
        if(mPageIndicator != null && mPageIndicator instanceof CirclePageIndicator) {
            ((CirclePageIndicator) mPageIndicator).setStrokeWidth(circleStrokeWidth * density);
        }
    }
    public void setCircleRadius(int radius){
        this.circleRadius = radius;
        if(mPageIndicator != null && mPageIndicator instanceof CirclePageIndicator) {
            ((CirclePageIndicator) mPageIndicator).setRadius(circleRadius * density);
        }
    }
    public void setIndicatorBackgroundColor(int color){
        if(mPageIndicator != null) {
            ((View) mPageIndicator).setBackgroundColor(color);
        }
    }

    public void setIndicatorBackgroundResource(int resId){
        if(mPageIndicator != null) {
            ((View) mPageIndicator).setBackgroundResource(resId);
        }
    }
    public void setCusomIndicator(int icon){
        IconPageIndicator iconPageIndicator = new IconPageIndicator(mContext);
        mPageIndicator = iconPageIndicator;

        iconPageIndicator.setLayoutParams(indicatorParams);
        customViewPager.setIcon(icon);
        iconPageIndicator.setViewPager(customViewPager);
        iconPageIndicator.setPadding(dpPad,dpPad,dpPad,dpPad);
        addView(iconPageIndicator);
    }
    private void setCircle(){
        CirclePageIndicator circlePageIndicator = new CirclePageIndicator(mContext);
        mPageIndicator = circlePageIndicator;

        circlePageIndicator.setLayoutParams(indicatorParams);
        circlePageIndicator.setViewPager(customViewPager);
        circlePageIndicator.setFillColor(circleFillColor);
        circlePageIndicator.setStrokeColor(circleStrokeColor);
        circlePageIndicator.setStrokeWidth(circleStrokeWidth*density);
        circlePageIndicator.setRadius(circleRadius * density);
        circlePageIndicator.setPadding(dpPad, dpPad, dpPad ,dpPad);
        circlePageIndicator.setCentered(false);
        addView(circlePageIndicator);
    }
    public CustomViewPager getPager(){
        return customViewPager;
    }


    public void setSliderPadding(int left,int top,int right,int bottom){
        setPadding(toDp(left),
                toDp(top),
                toDp(right),
                toDp(bottom));
    }
    public void setIndicatorMargin(int left,int top,int right,int bottom){
        indicatorParams.topMargin = toDp(top);
        indicatorParams.leftMargin = toDp(left);
        indicatorParams.rightMargin = toDp(right);
        indicatorParams.bottomMargin = toDp(bottom);
        if(mPageIndicator != null) {
            ((View) mPageIndicator).setLayoutParams(indicatorParams);
        }
    }
    public void setPagerMargin(int left,int top,int right,int bottom){
        RelativeLayout.LayoutParams params = (LayoutParams) customViewPager.getLayoutParams();
        params.topMargin = toDp(top);
        params.leftMargin = toDp(left);
        params.rightMargin = toDp(right);
        params.bottomMargin = toDp(bottom);
        if(customViewPager != null) {
            customViewPager.setLayoutParams(params);
        }
    }
    public void setPagerPadding(int left,int top,int right,int bottom){
        if(customViewPager != null){
            customViewPager.setPadding(toDp(left),
                    toDp(top),
                    toDp(right),
                    toDp(bottom));
        }
    }
    public void setIndicatorPadding(int left,int top,int right,int bottom){
        if(mPageIndicator != null){
            ((View) mPageIndicator).setPadding(toDp(left),
                    toDp(top),
                    toDp(right),
                    toDp(bottom));
        }
    }
    private int toDp(int i){
        return  (int) (i*density + 0.5f);
    }

}
