package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjPropertiesType extends ObjBaseDirectories {

    public String value;


    public ObjPropertiesType() {
        init();
    }
    public ObjPropertiesType(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjPropertiesType(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("properties")) {
                JSONObject jsonMain = json.getJSONObject("properties");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                super.id = json.getInt("id");
            }
            if (json.has("name")) {
                super.name = json.getString("name");
            }
            if (json.has("value")) {
                value = json.getString("value");
            }
        } catch (JSONException e) {
        }
    }

}
