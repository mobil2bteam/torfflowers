package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog.ObjCatalog;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBrand;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjFlag;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjProperties;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjProduct {

    public int      id;
    public int      rating;
    public int      count_comment;
    public String   name = "";
    public String   code = "";
    public String   article = "";
    public String   short_description = "";
    public String   description = "";
    public String   url = "";
    public ArrayList<ObjImage> images;
    public ArrayList<ObjFlag>  flags;
    public ArrayList<ObjPrice> prices;
    public ArrayList<ObjProperties> properties;
    public ObjBrand brand;
    public ArrayList<ObjCatalog> catalog;
    public boolean flag_favorites;
    public boolean flag_bag;

    public ObjProduct() {
        init();
    }
    public ObjProduct(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjProduct(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("product")) {
                JSONObject jsonMain = json.getJSONObject("product");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        flags       = new ArrayList<>();
        prices      = new ArrayList<>();
        images      = new ArrayList<>();
        properties  = new ArrayList<>();
        catalog     = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("rating")) {
                rating = json.getInt("rating");
            }
            if (json.has("count_comment")) {
                count_comment = json.getInt("count_comment");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("code")) {
                code = json.getString("code");
            }
            if (json.has("article")) {
                article = json.getString("article");
            }
            if (json.has("short_description")) {
                short_description = json.getString("short_description");
            }
            if (json.has("description")) {
                description = json.getString("description");
            }
            if (json.has("url")) {
                url = json.getString("url");
            }
            if (json.has("brand")) {
                brand = new ObjBrand(json.getJSONObject("brand"));
            }
            if (json.has("images")) {
                for (int i = 0; i < json.getJSONArray("images").length(); i++) {
                    ObjImage it = new ObjImage(json.getJSONArray("images").getJSONObject(i));
                    images.add(it);
                }
            }
            if (json.has("prices")) {
                for (int i = 0; i < json.getJSONArray("prices").length(); i++) {
                    ObjPrice it = new ObjPrice(json.getJSONArray("prices").getJSONObject(i));
                    prices.add(it);
                }
            }
            if (json.has("flags")) {
                for (int i = 0; i < json.getJSONArray("flags").length(); i++) {
                    ObjFlag it = new ObjFlag(json.getJSONArray("flags").getJSONObject(i));
                    flags.add(it);
                }
            }
            if (json.has("catalog")) {
                for (int i = 0; i < json.getJSONArray("catalog").length(); i++) {
                    ObjCatalog it = new ObjCatalog(json.getJSONArray("catalog").getJSONObject(i));
                    catalog.add(it);
                }
            }
        } catch (JSONException e) {
        }
    }

}
