package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCity {

    public int     id,     country_id;
    public String name;
    public boolean metro;
    public ObjCountry country;

    public ObjCity() {
        init();
    }
    public ObjCity(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjCity(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("city")) {
                JSONObject jsonMain = json.getJSONObject("city");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("country_id")) {
                country_id = json.getInt("country_id");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("metro")) {
                metro = json.getBoolean("metro");
            }
        } catch (JSONException e) {
        }
    }

}
