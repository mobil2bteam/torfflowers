package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBaseDirectories;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjFilter;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.SelectFilter.ClickListenerFilterGroup;
import com.mobil2b.renatganiev.torfflowers.ControlView.SelectFilter.FilterGroupView;

import java.util.ArrayList;


/**
 * TODO: Активити с фильтрами.
 * */

public class filter extends AppCompatActivity {


    LinearLayout layout_filter, layout_sex, layout_price;
    RadioButton radioButtonMan, radioButtonWoman, radioButtonClear;
    EditText editPriceFrom, editPriceTo;
    Switch switchSale;
    Button btnFilterOk;
    private Toolbar toolbar;

    int REQUEST_COUNTRY     = 101;
    int REQUEST_BRAND       = 102;
    int REQUEST_SIZE        = 103;
    int REQUEST_SEASON      = 104;
    int REQUEST_COLOR       = 105;
    int REQUEST_MATERIAL    = 106;
    int REQUEST_FLAGS       = 107;
    int REQUEST_AGE         = 108;

    ObjFilter objFilter = null;
    FilterGroupView filterGroupViewCountry;
    FilterGroupView filterGroupViewBrand;
    FilterGroupView filterGroupViewSize;
    FilterGroupView filterGroupViewSeason;
    FilterGroupView filterGroupViewColor;
    FilterGroupView filterGroupViewMaterial;
    FilterGroupView filterGroupViewFlag;
    FilterGroupView filterGroupViewAge;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        layout_filter   = (LinearLayout)findViewById(R.id.layout_filter);
        layout_sex      = (LinearLayout)findViewById(R.id.layout_sex);
        layout_price    = (LinearLayout)findViewById(R.id.layout_price);

        radioButtonMan = (RadioButton) findViewById(R.id.radioButtonMan);
        radioButtonWoman = (RadioButton) findViewById(R.id.radioButtonWoman);
        radioButtonClear = (RadioButton) findViewById(R.id.radioButtonClear);

        editPriceFrom   = (EditText) findViewById(R.id.editPriceFrom);
        editPriceTo     = (EditText) findViewById(R.id.editPriceTo);
        switchSale      = (Switch)  findViewById(R.id.switchSale);
        btnFilterOk     = (Button)  findViewById(R.id.btnFilterOk);

        initToolbar();


        Intent data = getIntent();
        if (data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                if (bundle.containsKey("filter")) {
                    objFilter = (ObjFilter)bundle.getSerializable("filter");
                }
            }
        }

        if (PublicData.SETTINGS != null) {
            if (PublicData.SETTINGS.filters != null) {

                if (PublicData.SETTINGS.filters.sex) {
                    layout_sex.setVisibility(View.VISIBLE);
                } else {
                    layout_sex.setVisibility(View.GONE);
                }

                if (PublicData.SETTINGS.filters.price) {

                    editPriceFrom.setText(String.valueOf(objFilter.price_from));
                    editPriceTo.setText(String.valueOf(objFilter.price_to));
                    if (objFilter.sale != 0) {
                        switchSale.setChecked(true);
                    } else {
                        switchSale.setChecked(false);
                    }

                    layout_price.setVisibility(View.VISIBLE);

                } else {
                    layout_price.setVisibility(View.GONE);
                }


                if (PublicData.SETTINGS.filters.country) {
                    if (PublicData.DIRECTORIES != null) {
                        if (PublicData.DIRECTORIES.country_manufacture != null) {

                            filterGroupViewCountry = new FilterGroupView(filter.this, "Страна производитель", REQUEST_COUNTRY, clickListenerFilterGroup);
                            if ((objFilter != null) && (objFilter.country_manufacture_id != null)) {
                                filterGroupViewCountry.setData(PublicData.DIRECTORIES.country_manufacture, objFilter.country_manufacture_id);
                            } else {
                                filterGroupViewCountry.setData(PublicData.DIRECTORIES.country_manufacture, null);
                            }
                            layout_filter.addView(filterGroupViewCountry);
                        }
                    }
                }

                if (PublicData.SETTINGS.filters.brand) {
                    if (PublicData.DIRECTORIES != null) {
                        if (PublicData.DIRECTORIES.brand != null) {
                            filterGroupViewBrand = new FilterGroupView(filter.this, "Бренд", REQUEST_BRAND, clickListenerFilterGroup);
                            if ((objFilter != null) && (objFilter.brand_id != null)) {
                                filterGroupViewBrand.setData(PublicData.DIRECTORIES.brand, objFilter.brand_id);
                            } else {
                                filterGroupViewBrand.setData(PublicData.DIRECTORIES.brand, null);
                            }

                            layout_filter.addView(filterGroupViewBrand);
                        }
                    }
                }

                if (PublicData.SETTINGS.filters.size) {
                    if (PublicData.DIRECTORIES != null) {
                        if (PublicData.DIRECTORIES.size != null) {
                            filterGroupViewSize = new FilterGroupView(filter.this, "Размер", REQUEST_SIZE, clickListenerFilterGroup);
                            if ((objFilter != null) && (objFilter.size_id != null)) {
                                filterGroupViewSize.setData(PublicData.DIRECTORIES.size, objFilter.size_id);
                            } else {
                                filterGroupViewSize.setData(PublicData.DIRECTORIES.size, null);
                            }
                            layout_filter.addView(filterGroupViewSize);
                        }
                    }
                }

                if (PublicData.SETTINGS.filters.season) {
                    if (PublicData.DIRECTORIES != null) {
                        if (PublicData.DIRECTORIES.season != null) {
                            filterGroupViewSeason = new FilterGroupView(filter.this, "Сезон", REQUEST_SEASON, clickListenerFilterGroup);
                            if ((objFilter != null) && (objFilter.season_id != null)) {
                                filterGroupViewSeason.setData(PublicData.DIRECTORIES.season, objFilter.season_id);
                            } else {
                                filterGroupViewSeason.setData(PublicData.DIRECTORIES.season, null);
                            }
                            layout_filter.addView(filterGroupViewSeason);
                        }
                    }
                }

                if (PublicData.SETTINGS.filters.color) {
                    if (PublicData.DIRECTORIES != null) {
                        if (PublicData.DIRECTORIES.color != null) {
                            filterGroupViewColor = new FilterGroupView(filter.this, "Цвет", REQUEST_COLOR, clickListenerFilterGroup);
                            if ((objFilter != null) && (objFilter.color_id != null)) {
                                filterGroupViewColor.setData(PublicData.DIRECTORIES.color, objFilter.color_id);
                            } else {
                                filterGroupViewColor.setData(PublicData.DIRECTORIES.color, null);
                            }

                            layout_filter.addView(filterGroupViewColor);

                        }
                    }
                }

                if (PublicData.SETTINGS.filters.material) {
                    if (PublicData.DIRECTORIES != null) {
                        if (PublicData.DIRECTORIES.material != null) {
                            filterGroupViewMaterial = new FilterGroupView(filter.this, "Материал", REQUEST_MATERIAL, clickListenerFilterGroup);
                            if ((objFilter != null) && (objFilter.material_id != null)) {
                                filterGroupViewMaterial.setData(PublicData.DIRECTORIES.material, objFilter.material_id);
                            } else {
                                filterGroupViewMaterial.setData(PublicData.DIRECTORIES.material, null);
                            }
                            layout_filter.addView(filterGroupViewMaterial);
                        }
                    }
                }

                if (PublicData.SETTINGS.filters.flags) {
                    if (PublicData.DIRECTORIES != null) {
                        if (PublicData.DIRECTORIES.flags != null) {
                            filterGroupViewFlag = new FilterGroupView(filter.this, "Метки", REQUEST_FLAGS, clickListenerFilterGroup);
                            if ((objFilter != null) && (objFilter.flag_id != null)) {
                                filterGroupViewFlag.setData(PublicData.DIRECTORIES.flags, objFilter.flag_id);
                            } else {
                                filterGroupViewFlag.setData(PublicData.DIRECTORIES.flags, null);
                            }
                            layout_filter.addView(filterGroupViewFlag);
                        }
                    }
                }

                if (PublicData.SETTINGS.filters.age) {
                    if (PublicData.DIRECTORIES != null) {
                        if (PublicData.DIRECTORIES.age != null) {
                            filterGroupViewAge = new FilterGroupView(filter.this, "Возраст", REQUEST_AGE, clickListenerFilterGroup);
                            if ((objFilter != null) && (objFilter.age_id != null)) {
                                filterGroupViewAge.setData(PublicData.DIRECTORIES.age, objFilter.age_id);
                            } else {
                                filterGroupViewAge.setData(PublicData.DIRECTORIES.age, null);
                            }
                            layout_filter.addView(filterGroupViewAge);
                        }
                    }
                }

            }
        }


        btnFilterOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (objFilter == null) {
                    objFilter = new ObjFilter();
                }

                if (filterGroupViewCountry != null) {
                    if (filterGroupViewCountry.getSelectId() != null) {
                        objFilter.country_manufacture_id = filterGroupViewCountry.getSelectId();
                    } else {
                        objFilter.country_manufacture_id = null;
                    }
                } else {
                    objFilter.country_manufacture_id = null;
                }

                if (filterGroupViewBrand != null) {
                    if (filterGroupViewBrand.getSelectId() != null) {
                        objFilter.brand_id = filterGroupViewBrand.getSelectId();
                    } else {
                        objFilter.brand_id = null;
                    }
                } else {
                    objFilter.brand_id = null;
                }

                if (filterGroupViewSize != null) {
                    if (filterGroupViewSize.getSelectId() != null) {
                        objFilter.size_id = filterGroupViewSize.getSelectId();
                    } else {
                        objFilter.size_id = null;
                    }
                } else {
                    objFilter.size_id = null;
                }

                if (filterGroupViewSeason != null) {
                    if (filterGroupViewSeason.getSelectId() != null) {
                        objFilter.season_id = filterGroupViewSeason.getSelectId();
                    } else {
                        objFilter.season_id = null;
                    }
                } else {
                    objFilter.season_id = null;
                }

                if (filterGroupViewColor != null) {
                    if (filterGroupViewColor.getSelectId() != null) {
                        objFilter.color_id = filterGroupViewColor.getSelectId();
                    } else {
                        objFilter.color_id = null;
                    }
                } else {
                    objFilter.color_id = null;
                }

                if (filterGroupViewMaterial != null) {
                    if (filterGroupViewMaterial.getSelectId() != null) {
                        objFilter.material_id = filterGroupViewMaterial.getSelectId();
                    } else {
                        objFilter.material_id = null;
                    }
                } else {
                    objFilter.material_id = null;
                }

                if (filterGroupViewFlag != null) {
                    if (filterGroupViewFlag.getSelectId() != null) {
                        objFilter.flag_id = filterGroupViewFlag.getSelectId();
                    } else {
                        objFilter.flag_id = null;
                    }
                } else {
                    objFilter.flag_id = null;
                }

                if (filterGroupViewAge != null) {
                    if (filterGroupViewAge.getSelectId() != null) {
                        objFilter.age_id = filterGroupViewAge.getSelectId();
                    } else {
                        objFilter.age_id = null;
                    }
                } else {
                    objFilter.age_id = null;
                }


                editPriceFrom.setText(editPriceFrom.getText().toString().replaceFirst ("^0*", ""));
                editPriceTo.setText(editPriceTo.getText().toString().replaceFirst ("^0*", ""));
                if (!editPriceFrom.getText().toString().equals("")) {
                    objFilter.price_from = Integer.decode(editPriceFrom.getText().toString());
                } else {
                    objFilter.price_from = 0;
                }

                if (!editPriceTo.getText().toString().equals("")) {
                    objFilter.price_to = Integer.decode(editPriceTo.getText().toString());
                } else {
                    objFilter.price_to = 0;
                }

                if (radioButtonClear.isChecked()) {
                    objFilter.sex_id = 0;
                }
                if (radioButtonMan.isChecked()) {
                    objFilter.sex_id = 1;
                }
                if (radioButtonWoman.isChecked()) {
                    objFilter.sex_id = 2;
                }
                if (switchSale.isChecked()) {
                    objFilter.sale = 1;
                } else {
                    objFilter.sale = 0;
                }

                objFilter.page = 1;

                Intent intentResult = new Intent();
                Bundle bundel = new Bundle();
                bundel.putSerializable("filter", objFilter);
                intentResult.putExtras(bundel);
                setResult(RESULT_OK, intentResult);
                finish();
            }
        });


    }


    ClickListenerFilterGroup clickListenerFilterGroup = new ClickListenerFilterGroup() {
        @Override
        public void onClick(View view, ArrayList<? extends ObjBaseDirectories> objBaseDirectories, int request_code, String name) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("filterList", objBaseDirectories);
            bundle.putString("name", name);
            Intent intent = new Intent(filter.this, filter_list.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, request_code);
        }
    };


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle(getString(R.string.str_caption_filter));
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);

        //if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        //    toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_toolbar_icon_back_black_24dp));
        //} else {
        //    toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_black_24dp));
        //}

        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                setResult(RESULT_OK);
                finish();

            }
        });*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.filter_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_clear) {
            //updateUserData();
            if (filterGroupViewCountry  != null) filterGroupViewCountry.setSelectArrayId(null);
            if (filterGroupViewBrand    != null) filterGroupViewBrand.setSelectArrayId(null);
            if (filterGroupViewSize     != null) filterGroupViewSize.setSelectArrayId(null);
            if (filterGroupViewSeason   != null) filterGroupViewSeason.setSelectArrayId(null);
            if (filterGroupViewColor    != null) filterGroupViewColor.setSelectArrayId(null);
            if (filterGroupViewMaterial != null) filterGroupViewMaterial.setSelectArrayId(null);
            if (filterGroupViewFlag     != null) filterGroupViewFlag.setSelectArrayId(null);
            if (filterGroupViewAge      != null) filterGroupViewAge.setSelectArrayId(null);

            radioButtonClear.setChecked(true);
            editPriceFrom.setText("");
            editPriceTo.setText("");
            switchSale.setChecked(false);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (data != null) {
                Bundle bundle = data.getExtras();
                if (bundle.containsKey("listId")) {
                    ArrayList<Integer> result = bundle.getIntegerArrayList("listId");
                    if (result != null) {

                        if (requestCode == REQUEST_COUNTRY) {
                            if (filterGroupViewCountry  != null) filterGroupViewCountry.setSelectArrayId(result);
                        }
                        if (requestCode == REQUEST_BRAND) {
                            if (filterGroupViewBrand  != null) filterGroupViewBrand.setSelectArrayId(result);
                        }
                        if (requestCode == REQUEST_SIZE) {
                            if (filterGroupViewSize  != null) filterGroupViewSize.setSelectArrayId(result);
                        }
                        if (requestCode == REQUEST_SEASON) {
                            if (filterGroupViewSeason  != null) filterGroupViewSeason.setSelectArrayId(result);
                        }
                        if (requestCode == REQUEST_COLOR) {
                            if (filterGroupViewColor  != null) filterGroupViewColor.setSelectArrayId(result);
                        }
                        if (requestCode == REQUEST_MATERIAL) {
                            if (filterGroupViewMaterial  != null) filterGroupViewMaterial.setSelectArrayId(result);
                        }
                        if (requestCode == REQUEST_FLAGS) {
                            if (filterGroupViewFlag  != null) filterGroupViewFlag.setSelectArrayId(result);
                        }
                        if (requestCode == REQUEST_AGE) {
                            if (filterGroupViewAge  != null) filterGroupViewAge.setSelectArrayId(result);
                        }
                    }
                }
            }

        }

    }


}


