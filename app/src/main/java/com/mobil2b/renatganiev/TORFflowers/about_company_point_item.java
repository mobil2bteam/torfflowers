package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.ControlView.SocialLinkView.SocialLinkView;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjSocialLinkItem;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.Slider.SliderView;
import com.mobil2b.renatganiev.torfflowers.ControlView.SocialLinkView.ClickListenerSocialLink;

public class about_company_point_item extends AppCompatActivity {

    boolean select_type = false;
    int point_id;
    MenuItem menuSelect;

    ObjCompanyPoint objCompanyPoint;

    TextView txtClubName, txtClubAddress, txtSummDelivery, txtSummFreeDelivery;
    Button btnWeb, btnPhone, btnSelect;

    NestedScrollView scrollView;
    AppBarLayout mainAppbar;
    CoordinatorLayout coordinatorLayout;

    LinearLayout layout_slider, layout_social_link, layout_description,
            layout_works, layout_check_cache, layout_summ_delivery, layout_summ_free_delivery,
            layout_select;

    int PERMISSION_REQUEST_CODE = 347678;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_company_point_item);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                if (bundle.containsKey("select_type")) {
                    select_type = bundle.getBoolean("select_type");
                }
                if (bundle.containsKey("point_id")) {
                    point_id = bundle.getInt("point_id");
                }
            }
        }

        if (point_id != 0) {
            if (PublicData.SETTINGS != null) {
                if (PublicData.SETTINGS.company_info != null) {
                    if (PublicData.SETTINGS.company_info.points != null) {
                        for (ObjCompanyPoint objPoint : PublicData.SETTINGS.company_info.points) {
                            if (objPoint.id == point_id) {
                                objCompanyPoint = objPoint;
                                break;
                            }
                        }
                    }
                }
            }
        }

        txtClubName                     = (TextView)    findViewById(R.id.txtClubName);
        txtClubAddress                  = (TextView)    findViewById(R.id.txtClubAddress);
        txtSummDelivery                 = (TextView)    findViewById(R.id.txtSummDelivery);
        txtSummFreeDelivery             = (TextView)    findViewById(R.id.txtSummFreeDelivery);

        scrollView                      = (NestedScrollView) findViewById(R.id.scrollView);
        mainAppbar                      = (AppBarLayout) findViewById(R.id.mainAppbar);
        coordinatorLayout               = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        layout_slider                   = (LinearLayout) findViewById(R.id.layout_slider);
        layout_social_link              = (LinearLayout) findViewById(R.id.layout_social_link);
        layout_description              = (LinearLayout) findViewById(R.id.layout_description);
        layout_works                    = (LinearLayout) findViewById(R.id.layout_works);
        layout_check_cache              = (LinearLayout) findViewById(R.id.layout_check_cache);
        layout_summ_delivery            = (LinearLayout) findViewById(R.id.layout_summ_delivery);
        layout_summ_free_delivery       = (LinearLayout) findViewById(R.id.layout_summ_free_delivery);
        layout_select                   = (LinearLayout) findViewById(R.id.layout_select);

        if (!select_type) {
            layout_select.setVisibility(View.GONE);
        } else {
            layout_select.setVisibility(View.VISIBLE);
        }

        btnWeb                  = (Button) findViewById(R.id.btnWeb);
        btnPhone                = (Button) findViewById(R.id.btnPhone);
        btnSelect               = (Button) findViewById(R.id.btnSelect);


        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("point_id", objCompanyPoint.id);
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        initToolbar();
        updateView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*getMenuInflater().inflate(R.menu.menu_select, menu);

        menuSelect = menu.findItem(R.id.menu_select);

        if (select_type) {
            menuSelect.setVisible(true);
        } else {
            menuSelect.setVisible(false);
        }*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_select) {
            //updateUserData();

        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_toolbar_icon_close_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_close_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                setResult(RESULT_OK);
                finish();

            }
        });
    }

    private void updateView() {
        if (objCompanyPoint != null) {

            if (objCompanyPoint.slider != null) {
                SliderView sliderView = new SliderView(about_company_point_item.this, objCompanyPoint.slider);
                sliderView.setCircleFillColor(ContextCompat.getColor(about_company_point_item.this, R.color.colorAccent), Color.WHITE);
                sliderView.setCircleRadius(3);
                sliderView.setCircleStrokeColor(Color.WHITE);
                sliderView.setCircleStrokeWidth(1);
                sliderView.setPagerMargin(0, 0, 0, 0);

                layout_slider.removeAllViews();
                layout_slider.addView(sliderView);
                layout_slider.setVisibility(View.VISIBLE);
            }

            txtClubName.setText(objCompanyPoint.name);

            txtClubAddress.setText("");
            btnPhone.setVisibility(View.GONE);
            btnWeb.setVisibility(View.GONE);
            if (objCompanyPoint.contacts != null) {
                if (objCompanyPoint.contacts.address != null) {
                    txtClubAddress.setText(objCompanyPoint.contacts.address.city.concat(" ").concat(objCompanyPoint.contacts.address.street));
                }

                if (objCompanyPoint.contacts.phone != null) {
                    btnPhone.setVisibility(View.VISIBLE);
                    btnPhone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callPhone(objCompanyPoint.contacts.phone);
                        }
                    });
                }
            }

            if (objCompanyPoint.social_link != null) {
                if (Helps.isNotEmpty(objCompanyPoint.social_link.web)) {
                    btnWeb.setVisibility(View.VISIBLE);
                    btnWeb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(objCompanyPoint.social_link.web));
                            startActivity(browserIntent);
                        }
                    });
                }
            }

            if (Helps.isNotEmpty(objCompanyPoint.description_long)) {
                layout_description.setVisibility(View.VISIBLE);

                layout_description.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(about_company_point_item.this, about_company_description.class);
                        intent.putExtra("description", objCompanyPoint.description_long);
                        intent.putExtra("name", objCompanyPoint.name);
                        startActivity(intent);
                    }
                });

            } else {
                layout_description.setVisibility(View.GONE);
            }

            if (objCompanyPoint.working_hours != null) {
                layout_works.setVisibility(View.VISIBLE);

                layout_works.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(about_company_point_item.this, about_company_work.class);
                        intent.putExtra("point_id", objCompanyPoint.id);
                        startActivity(intent);
                    }
                });

            } else {
                layout_works.setVisibility(View.GONE);
            }


            if (objCompanyPoint.check_point_pickup) {
                //if (objCompanyPoint.)
                layout_summ_delivery.setVisibility(View.VISIBLE);
                layout_summ_free_delivery.setVisibility(View.VISIBLE);
            } else {
                layout_summ_delivery.setVisibility(View.GONE);
                layout_summ_free_delivery.setVisibility(View.GONE);
            }


            if (objCompanyPoint.summ_delivery != 0) {
                txtSummDelivery.setText(String.valueOf(objCompanyPoint.summ_delivery).concat(" р."));
                layout_summ_delivery.setVisibility(View.VISIBLE);
            } else {
                layout_summ_delivery.setVisibility(View.GONE);
            }

            if (objCompanyPoint.summ_free_delivery != 0) {
                txtSummFreeDelivery.setText(String.valueOf(objCompanyPoint.summ_free_delivery).concat(" р."));
                layout_summ_free_delivery.setVisibility(View.VISIBLE);
            } else {
                layout_summ_free_delivery.setVisibility(View.GONE);
            }

            if (objCompanyPoint.check_point_cache) {
                layout_check_cache.setVisibility(View.VISIBLE);
            } else {
                layout_check_cache.setVisibility(View.GONE);
            }

            if (PublicData.HOME.flag_show_social_link) {

                int count_social_link = 0;

                if (objCompanyPoint != null) {
                    if (objCompanyPoint.social_link != null) {
                        for (ObjSocialLinkItem objSocialLinkItem : objCompanyPoint.social_link.socialLinkItems) {
                            if (Helps.isNotEmpty(objSocialLinkItem.url)) {
                                count_social_link++;
                            }
                        }
                        int count_line = 3;
                        if (count_social_link == 2) count_line = 2;
                        if (count_social_link == 3) count_line = 3;
                        if (count_social_link == 4) count_line = 2;
                        if (count_social_link == 5) count_line = 3;
                        if (count_social_link == 6) count_line = 3;

                        SocialLinkView socialLinkView = new SocialLinkView(about_company_point_item.this, objCompanyPoint.social_link, count_line,
                                new ClickListenerSocialLink() {
                                    @Override
                                    public void onClick(View view, ObjSocialLinkItem objSocialLink) {
                                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(objSocialLink.url));
                                        startActivity(browserIntent);
                                    }
                                });

                        layout_social_link.removeAllViews();
                        layout_social_link.addView(socialLinkView);
                    }
                }

                if (count_social_link > 0) {
                    layout_social_link.setVisibility(View.VISIBLE);
                }
            }


            mainAppbar.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.VISIBLE);

        }
    }

    void callPhone(String callPhoneStr) {
        if (ActivityCompat.checkSelfPermission(about_company_point_item.this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent myIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + callPhoneStr));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(myIntent);
            return;
        } else {
            PublicData.callPhoneStr = callPhoneStr;
            ActivityCompat.requestPermissions(about_company_point_item.this, new String[] { android.Manifest.permission.CALL_PHONE }, PublicData.CALL_PHONE_NUMBER);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callPhone(PublicData.callPhoneStr);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
