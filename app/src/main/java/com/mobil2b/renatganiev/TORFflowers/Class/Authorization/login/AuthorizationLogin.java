package com.mobil2b.renatganiev.torfflowers.Class.Authorization.login;

import android.content.Context;
import android.content.SharedPreferences;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;


/**
 * Created by renatganiev on 02.04.16.
 */
public class AuthorizationLogin {

    public static String TYPE = "login";

    private Context mContext;

    private static String SETTINGS          = "MYSETTINGS";
    private static String TOKEN_ACCESS      = "accessToken";

    //public CallbackSession callbackDateForLogin;


    public AuthorizationLogin(Context ctx) {
        mContext = ctx;
    }

    public void setContext(Context ctx) {
        mContext    = ctx;
    }

    // Проверяем есть ли сессия на телефоне
    public boolean wakeUpSession() {

        if (mContext != null) {
            SharedPreferences mSettings;
            mSettings = mContext.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);

            if (mSettings.contains(TOKEN_ACCESS)) {
                String token = mSettings.getString(TOKEN_ACCESS, "");
                if (!token.equals("")) {
                    return true;
                }
            }
        }
        return false;
    }

    public DataSession getDateForLogin() {
        if (mContext != null) {
            if (wakeUpSession()) {

                SharedPreferences mSettings;
                mSettings = mContext.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);

                DataSession date = new DataSession();
                date.token    = mSettings.getString(TOKEN_ACCESS, "");
                //callbackDateForLogin.onReturnSession(date);
                return date;
            }
        }
        //callbackDateForLogin.onDontSession("", 0);
        return null;
    }

    public void onLogout() {
        if (mContext != null) {
            SharedPreferences mSettings;
            mSettings = mContext.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = mSettings.edit();
            editor.remove(TOKEN_ACCESS);

            editor.apply();
        }
    }

    // Получаем сессию. Данная сессия не подтверждена сервером.
    // и может быть не активной.
    public DataSession getSession() {

        if (mContext != null) {
            if (wakeUpSession()) {
                SharedPreferences mSettings;
                mSettings = mContext.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);

                DataSession date = new DataSession();

                date.token    = mSettings.getString(TOKEN_ACCESS, "");
                return date;
            }
        }
        return null;
    }


    public void setSession(DataSession session) {
        if (session == null) {
            SharedPreferences mSettings;
            mSettings = mContext.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = mSettings.edit();

            editor.putString(TOKEN_ACCESS,      "");
            editor.apply();
        } else {

            if (mContext != null) {
                if (!session.token.equals("")) {
                    SharedPreferences mSettings;
                    mSettings = mContext.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = mSettings.edit();
                    if (session.token == null)    session.token     = "";

                    editor.putString(TOKEN_ACCESS,      session.token);
                    editor.apply();
                }
            }
        }
    }


}
