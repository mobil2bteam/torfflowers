package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


/**
 * Created by renatganiev on 14.10.15.
 */
public class DBBag {

    private ContentValues cv;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private Context mContext;
    private String TABLE = "bag";

    public DBBag(Context context) {
        // создаем объект для данных
        cv          = new ContentValues();
        // создаем объект для создания и управления версиями БД
        dbHelper    = new DBHelper(context);
        // подключаемся к БД
        db          = dbHelper.getWritableDatabase();
        mContext    = context;
    }

    // ДОБАВЛЯЕМ
    public long add(int product_id, int offer_id, int offer_count) {
        cv          = new ContentValues();
        cv.put("product_id",            product_id);
        cv.put("offer_id",              offer_id);
        cv.put("offer_count",           offer_count);

        long rowID;
        if (exist(offer_id)) {
            rowID = db.update(TABLE, cv, "offer_id = ?", new String[]{String.valueOf(offer_id)});
        } else {
            rowID = db.insert(TABLE, null, cv);
        }

        return rowID;
    }

    // ПОЛУЧАЕМ
    public ArrayList<Integer> getOfferList() {
        String query    = "SELECT * FROM "+TABLE;
        Cursor cursor1  = db.rawQuery(query, null);

        ArrayList<Integer> listResult = new ArrayList<>();
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {
                listResult.add(cursor1.getInt(cursor1.getColumnIndex("offer_id")));
            }
        }
        return listResult;
    }

    // ПОЛУЧАЕМ
    public String getOfferString() {
        String query    = "SELECT * FROM "+TABLE;
        Cursor cursor1  = db.rawQuery(query, null);

        String listResult = "";
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {
                if (listResult.length() == 0) {
                    listResult = listResult.concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("offer_id"))));
                } else {
                    listResult = listResult.concat("-").concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("offer_id"))));
                }
            }
        }
        return listResult;
    }


    // ПОЛУЧАЕМ
    public String getOfferStringCount() {
        String query    = "SELECT * FROM "+TABLE;
        Cursor cursor1  = db.rawQuery(query, null);

        String listResult = "";
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {
                if (listResult.length() == 0) {
                    listResult = listResult.concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("offer_id")))).concat("-".concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("offer_count")))));
                } else {
                    listResult = listResult.concat(";").concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("offer_id")))).concat("-".concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("offer_count")))));
                }
            }
        }
        return listResult;
    }

    // ПРОВЕРЯЕМ
    public boolean exist(int offer_id) {
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE offer_id = ").concat(String.valueOf(offer_id));
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            return true;
        }
        return false;
    }

    // ПРОВЕРЯЕМ КОЛИЧЕСТВО
    public int getCount(int offer_id) {
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE offer_id = ").concat(String.valueOf(offer_id));
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {
                return cursor1.getInt(cursor1.getColumnIndex("offer_count"));
            }
        }
        return 0;
    }

    // УДАЛЯЕМ
    public boolean deleteOffer(int offer_id) {
        int clearCount = db.delete(TABLE, "offer_id = ".concat(String.valueOf(offer_id)), null);
        if (clearCount > 0) {
            return true;
        } else {
            return false;
        }
    }

    // УДАЛЯЕМ
    public boolean deleteProduct(int product_id) {
        int clearCount = db.delete(TABLE, "product_id = ".concat(String.valueOf(product_id)), null);
        if (clearCount > 0) {
            return true;
        } else {
            return false;
        }
    }

    // ОЧИСТИТЬ
    public int clear() {
        int clearCount = db.delete(TABLE, null, null);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE + "'");
        return clearCount;
    }

    public void close() {
        db.close();
    }

}
