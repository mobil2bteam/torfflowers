package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjContacts {
    public String phone, email;
    public ObjAddress address;


    public ObjContacts() {
        init();
    }
    public ObjContacts(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjContacts(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("phone")) {
                this.phone = json.getString("phone");
            }
            if (json.has("email")) {
                this.email = json.getString("email");
            }
            if (json.has("address")) {
                JSONObject addressJson = json.getJSONObject("address");
                address = new ObjAddress(addressJson);
            }
        } catch (JSONException e) {
        }
    }
}
