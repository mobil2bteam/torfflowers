package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjPhones {
    public String name, phone;

    public ObjPhones() {
        init();
    }
    public ObjPhones(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjPhones(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("phones")) {
                JSONObject jsonMain = json.getJSONObject("phones");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("phone")) {
                phone = json.getString("phone");
            }
        } catch (JSONException e) {
        }
    }

}
