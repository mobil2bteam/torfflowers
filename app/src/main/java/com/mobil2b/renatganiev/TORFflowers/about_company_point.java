package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Fragments.fragment_app_city_list;

import java.util.ArrayList;


public class about_company_point extends AppCompatActivity {

    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;

    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_company_point);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        fragmentmanager = getSupportFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction = callbackAction;
        fragmentControl.drawer = drawer;


        ArrayList<Integer> city_id = new ArrayList<>();
        if (PublicData.SETTINGS != null) {
            if (PublicData.SETTINGS.company_info != null) {
                if (PublicData.SETTINGS.company_info.points != null) {
                    for (ObjCompanyPoint objCompanyPoint : PublicData.SETTINGS.company_info.points) {
                        if ((objCompanyPoint.check_point_sale) || (objCompanyPoint.check_point_office)) {
                            if (objCompanyPoint.contacts != null) {
                                if (objCompanyPoint.contacts.address != null) {
                                    if (!city_id.contains(objCompanyPoint.contacts.address.city_id)) {
                                        city_id.add(objCompanyPoint.contacts.address.city_id);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (city_id.size() == 1) {
            Bundle bundle = new Bundle();
            bundle.putInt("city_id", city_id.get(0));
            bundle.putBoolean("flag_delivery_point", false);
            fragmentControl.showFragmentMapList(true, null, R.id.layout_container, bundle);
        } else {
            Bundle bundle = new Bundle();
            bundle.putBoolean("select_city_company_point", true);
            fragmentControl.showFragmentCityList(true, null, R.id.layout_container, bundle);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DeliveryCityView.REZULT_REQUEST_SELECT_CITY) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle.containsKey("city_id")) {
                        bundle.putBoolean("flag_delivery_point", false);
                        fragmentControl.showFragmentMapList(false, null, R.id.layout_container, bundle);
                    }
                }
            }
        }
    }

    CallbackAction callbackAction = new CallbackAction() {
        @Override
        public void onAction(String tag, int type, ObjLink data) {

        }

        @Override
        public void onAction(String tag, int type, Bundle data) {
            if (tag.equals(fragment_app_city_list.TAG)) {
                if (type == PublicData.SELECT_CITY) {
                    if (data.containsKey("city_id")) {
                        int city_id = data.getInt("city_id");
                        data.putBoolean("flag_delivery_point", false);
                        fragmentControl.showFragmentMapList(false, null, R.id.layout_container, data);
                    }
                }
            }
        }
    };

}
