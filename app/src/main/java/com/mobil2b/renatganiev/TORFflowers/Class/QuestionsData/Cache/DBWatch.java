package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProduct;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;

import java.util.ArrayList;


/**
 * Created by renatganiev on 14.10.15.
 */
public class DBWatch {

    private ContentValues cv;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private Context mContext;
    private String TABLE = "watch";

    public DBWatch(Context context) {
        // создаем объект для данных
        cv          = new ContentValues();
        // создаем объект для создания и управления версиями БД
        dbHelper    = new DBHelper(context);
        // подключаемся к БД
        db          = dbHelper.getWritableDatabase();
        mContext    = context;
    }

    // ДОБАВЛЯЕМ
    public long add(ObjProduct product) {

        if (exist(product.id)) {
            delete(product.id);
        }

        cv          = new ContentValues();
        cv.put("product_id",         product.id);
        cv.put("name",         product.name);
        if (product.images != null) {
            if (product.images.size() > 0) {
                cv.put("image",         product.images.get(0).url);
            }
        }
        long rowID = db.insert(TABLE, null, cv);
        return rowID;
    }

    // ПОЛУЧАЕМ
    public ArrayList<ObjProductMin> getList() {
        cv          = new ContentValues();
        String query    = "SELECT * FROM "+TABLE+" ORDER BY id DESC LIMIT 15";
        Cursor cursor1  = db.rawQuery(query, null);

        ArrayList<ObjProductMin> listResult = new ArrayList<>();
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {

                ObjProductMin p = new ObjProductMin();
                p.id = cursor1.getInt(cursor1.getColumnIndex("product_id"));
                p.name = cursor1.getString(cursor1.getColumnIndex("name"));
                p.image = new ObjImage();
                p.image.url = cursor1.getString(cursor1.getColumnIndex("image"));

                listResult.add(p);
            }
        }
        return listResult;
    }

    // ПОЛУЧАЕМ
    public String getString() {
        cv          = new ContentValues();
        String query    = "SELECT * FROM "+TABLE+" ORDER BY id DESC LIMIT 15";
        Cursor cursor1  = db.rawQuery(query, null);

        String listResult = "";
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {
                if (listResult.length() == 0) {
                    listResult = listResult.concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("product_id"))));
                } else {
                    listResult = listResult.concat("-").concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("product_id"))));
                }
            }
        }
        return listResult;
    }

    // ПРОВЕРЯЕМ
    public boolean exist(int id) {
        cv          = new ContentValues();
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE product_id = ").concat(String.valueOf(id));
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            return true;
        }
        return false;
    }

    // УДАЛЯЕМ
    public boolean delete(int id) {
        int clearCount = db.delete(TABLE, "product_id = ".concat(String.valueOf(id)), null);
        if (clearCount > 0) {
            return true;
        } else {
            return false;
        }
    }


    // ОЧИСТИТЬ
    public int clear() {
        int clearCount = db.delete(TABLE, null, null);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE + "'");
        return clearCount;
    }

    public void close() {
        db.close();
    }

}
