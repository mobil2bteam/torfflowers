package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterEmptyRecyclerView;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterNews;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.News.ObjGroupNews;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.News.ObjNewsItems;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerNews;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.news_item;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_news_list extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_news_list";
    public static final int LAYOUT = R.layout.fragment_app_news_list;
    View rootView;

    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;


    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    LinearLayoutManager staggeredGridLayoutManager;
    private AdapterNews mAdapterSearch;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public static fragment_app_news_list getInstance(Bundle args) {
        fragment_app_news_list fragment = new fragment_app_news_list();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.

        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        /*txtCaption              = (TextView)        rootView.findViewById(R.id.txtCaption);
        imgHead                 = (ImageView)       rootView.findViewById(R.id.imgHead);*/

        mSwipeRefreshLayout     = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        //initHead();
        intiRecyclerView();
        if (PublicData.NEWS != null) {
            if (PublicData.NEWS.items != null) {
                if (PublicData.NEWS.items.size() != 0) {
                    refrashAllDate();
                } else {
                    empty();
                }
            } else {
                empty();
            }
        } else {
            empty();
        }
        initToolbar();
        loadNews(false);

        return rootView;
    }

    void empty() {
        AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_favorite), R.drawable.ic_icon_news_empty);
        LinearLayoutManager managerLinear = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(managerLinear);
        mRecyclerView.setAdapter(mEmptyAdapter);
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }
        toolbar.setTitle(getString(R.string.app_name));
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_menu));
            } else {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_menu));
            }
        }

    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // В виде списка
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void refrashAllDate() {
        mAdapterSearch = new AdapterNews(getActivity(), PublicData.NEWS.items);
        mRecyclerView.setAdapter(mAdapterSearch);
        mAdapterSearch.notifyDataSetChanged();

        mAdapterSearch.setOnClickListener(new ClickListenerNews() {
                                              @Override
                                              public void onClick(View view, ObjNewsItems objNewsItems) {
                                                  Intent intent = new Intent(getActivity(), news_item.class);
                                                  intent.putExtra("id", objNewsItems.id);
                                                  startActivity(intent);
                                              }
                                          });
    }


    void loadNews(boolean cache) {
        ModuleObjects moduleObjects = new ModuleObjects(getActivity(), new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
                mSwipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {

                try {

                    JSONObject json = new JSONObject(result);
                    String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                    if (typeDate.equals(PublicData.QUESTION_TYPE_NEWS_LIST)) {

                        if (json.has("group_news")) {
                            JSONObject jsonHome = json.getJSONObject("group_news");
                            PublicData.NEWS         = new ObjGroupNews(jsonHome);
                            // here you check the value of getActivity() and break up if needed
                            if(getActivity() == null) return;
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    mSwipeRefreshLayout.setRefreshing(false);
                                    if (PublicData.NEWS != null) {
                                        if (PublicData.NEWS.items != null) {
                                            if (PublicData.NEWS.items.size() > 0) {
                                                refrashAllDate();
                                            } else {
                                                empty();
                                            }
                                        } else {
                                            empty();
                                        }
                                    } else {
                                        empty();
                                    }
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                }


            }

            @Override
            public void onError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }

            }

            @Override
            public void onHttpError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            }
        });

        moduleObjects.getNewsList(cache);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRefresh() {
        loadNews(true);
    }


}
