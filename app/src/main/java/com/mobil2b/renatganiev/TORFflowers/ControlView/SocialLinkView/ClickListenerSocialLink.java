package com.mobil2b.renatganiev.torfflowers.ControlView.SocialLinkView;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjSocialLinkItem;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerSocialLink {
    void onClick(View view, ObjSocialLinkItem objSocialLink);
}
