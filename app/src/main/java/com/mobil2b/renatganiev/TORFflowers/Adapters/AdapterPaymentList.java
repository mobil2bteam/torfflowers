package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Payment.ObjPayment;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerPayment;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterPaymentList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private ArrayList<ObjPayment> mList;

    ClickListenerPayment clickListenerDelivery;

    public AdapterPaymentList(Context context, ArrayList<ObjPayment> list) {
        this.mContext = context;
        this.mList = list;
    }

    public void setOnClickListenerPayment(ClickListenerPayment onClickListener){
        this.clickListenerDelivery = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_payment, parent, false);

        return new ViewHolderProduct(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ViewHolderProduct viewHolder0 = (ViewHolderProduct) holder;
        final ObjPayment obj = getItem(position);

        // НАИМЕНОВАНИЕ
        viewHolder0.txtName.setText(obj.name);

        if (getItem(position).check) {
            viewHolder0.imgCheck.setVisibility(View.VISIBLE);
            viewHolder0.txtName.setTypeface(null, Typeface.BOLD);
        } else {
            viewHolder0.imgCheck.setVisibility(View.INVISIBLE);
            viewHolder0.txtName.setTypeface(null, Typeface.NORMAL);
        }

        viewHolder0.setClickListener(clickListenerDelivery);
    }


    private ObjPayment getItem(int position){
        return mList.get(position);
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderProduct extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtName;
        ImageView imgCheck;

        ClickListenerPayment clickListenerDelivery;

        public ViewHolderProduct(View itemView) {
            super(itemView);

            txtName             = (TextView) itemView.findViewById(R.id.txtName);
            imgCheck            = (ImageView) itemView.findViewById(R.id.imgCheck);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            for (ObjPayment objPayment : mList) {
                objPayment.check = false;
            }
            getItem(getAdapterPosition()).check = true;
            if (clickListenerDelivery != null) {
                clickListenerDelivery.onClick(v, getItem(getAdapterPosition()));
                notifyDataSetChanged();
            }
        }
        public void setClickListener(ClickListenerPayment clickListener){
            this.clickListenerDelivery = clickListener;
        }
    }
}