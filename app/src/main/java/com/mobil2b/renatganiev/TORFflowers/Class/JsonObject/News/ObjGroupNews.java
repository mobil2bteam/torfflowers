package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.News;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjGroupNews {

    public String title;
    public String view;
    public int count_items;
    public ArrayList<ObjNewsItems> items;




    public ObjGroupNews() {
        init();
    }
    public ObjGroupNews(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjGroupNews(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("news")) {
                JSONObject jsonMain = json.getJSONObject("news");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        items = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {

            if (json.has("count_items")) {
                count_items = json.getInt("count_items");
            }
            if (json.has("title")) {
                title = json.getString("title");
            }
            if (json.has("view")) {
                view = json.getString("view");
            }

            for (int i =0;i<json.getJSONArray("items").length();i++){
                ObjNewsItems it = new ObjNewsItems(json.getJSONArray("items").getJSONObject(i));
                items.add(it);
            }
        } catch (JSONException e) {
        }
    }

}
