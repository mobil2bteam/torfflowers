package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjAge extends ObjBaseDirectories {

    //public int      id;
    //public String   name = "";

    public ObjAge() {
        init();
    }
    public ObjAge(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjAge(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("age")) {
                JSONObject jsonMain = json.getJSONObject("age");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                super.id = json.getInt("id");
            }
            if (json.has("name")) {
                super.name = json.getString("name");
            }
        } catch (JSONException e) {
        }
    }

}
