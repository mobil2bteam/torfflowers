package com.mobil2b.renatganiev.torfflowers;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order.ObjOrder;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBBag;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterCartList;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class order_item extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    int order_id;
    ObjOrder objOrder;

    TextView txtNumber, txtStatus, txtDelivery, txtPayment, txtCount, txtIsPayment, txtDate,
            txtTotalNotSale, txtDeliverySum, txtSumSale, txtTotal, txtAddressDelivery, txtDeliveryDate;
    Button btnReapid, btnPayment;

    RecyclerView mRecyclerView;
    LinearLayoutManager manager;
    AdapterCartList mAdapterSearch;

    LinearLayout layout_order_info, layout_total, layout_delivery_date;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    ModuleObjects moduleObjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item);

        txtNumber       = (TextView) findViewById(R.id.txtNumber);
        txtStatus       = (TextView) findViewById(R.id.txtStatus);
        txtDelivery     = (TextView) findViewById(R.id.txtDelivery);
        txtPayment      = (TextView) findViewById(R.id.txtPayment);
        txtCount        = (TextView) findViewById(R.id.txtCount);
        txtIsPayment    = (TextView) findViewById(R.id.txtIsPayment);
        txtDate         = (TextView) findViewById(R.id.txtDate);
        txtAddressDelivery= (TextView) findViewById(R.id.txtAddressDelivery);
        txtDeliveryDate = (TextView) findViewById(R.id.txtDeliveryDate);

        txtTotalNotSale = (TextView) findViewById(R.id.txtTotalNotSale);
        txtDeliverySum  = (TextView) findViewById(R.id.txtDeliverySum);
        txtSumSale      = (TextView) findViewById(R.id.txtSumSale);
        txtTotal        = (TextView) findViewById(R.id.txtTotal);

        btnReapid       = (Button) findViewById(R.id.btnReapid);
        btnPayment      = (Button) findViewById(R.id.btnPayment);

        layout_order_info       = (LinearLayout) findViewById(R.id.layout_order_info);
        layout_total            = (LinearLayout) findViewById(R.id.layout_total);
        layout_delivery_date    = (LinearLayout) findViewById(R.id.layout_delivery_date);

        btnPayment.setVisibility(View.GONE);

        layout_order_info.setVisibility(View.GONE);
        layout_total.setVisibility(View.GONE);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        initToolbar();
        intiRecyclerView();


        btnReapid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                DBBag bag = new DBBag(order_item.this);
                                bag.clear();
                                bag.close();
                                addToBag();
                                openBag();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                addToBag();
                                openBag();
                                break;
                        }
                    }
                };

                DBBag bag = new DBBag(order_item.this);
                ArrayList<Integer> list = bag.getOfferList();
                bag.close();
                if (list.size() > 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(order_item.this);
                    builder.setMessage("Очистить корзину перед тем как добавить товар из заказа?").setPositiveButton("Да", dialogClickListener)
                            .setNegativeButton("Нет", dialogClickListener).show();
                } else {
                    addToBag();
                    openBag();
                }
            }
        });


        moduleObjects = new ModuleObjects(order_item.this, callBackSendDate);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (bundle.containsKey("order_id")) {
                order_id = bundle.getInt("order_id");
                moduleObjects.getUserOrderItem(true, order_id, User.getCurrentSession());
            }
        }

    }

    void addToBag() {
        if (objOrder != null) {
            if (objOrder.products != null) {
                if (objOrder.products.size() > 0) {
                    DBBag bag = new DBBag(order_item.this);
                    for (ObjProductMin objProductMin : objOrder.products) {
                        if (objProductMin.prices != null) {
                            if (objProductMin.prices.size() > 0) {
                                int count_bag = bag.getCount(objProductMin.prices.get(0).id);
                                count_bag = count_bag + objProductMin.prices.get(0).count;
                                bag.add(objProductMin.id, objProductMin.prices.get(0).id, count_bag);
                            }
                        }
                    }
                    bag.close();
                }
            }
        }
    }

    void openBag() {
        Intent intent = new Intent(order_item.this, bag.class);
        startActivity(intent);
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle("Заказ");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_toolbar_icon_back_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(true);
        manager = new LinearLayoutManager(order_item.this);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onRefresh() {
        moduleObjects.getUserOrderItem(true, order_id, User.getCurrentSession());
    }


    void updateView() {
        if (objOrder != null) {

            txtNumber.setText("Заказ №".concat(String.valueOf(objOrder.number)));
            if (objOrder.status != null) {
                txtStatus.setText(objOrder.status.status);
            }

            if (objOrder.delivery != null) {
                txtDelivery.setText(objOrder.delivery.deliveryText);

                layout_delivery_date.setVisibility(View.GONE);
                if (Helps.isNotEmpty(objOrder.delivery.deliveryDate)) {
                    if (objOrder.delivery.deliveryDate.length() > 10) {
                        String deliveryDate = objOrder.delivery.deliveryDate.substring(0,10).trim();
                        if (!deliveryDate.equals("01.01.1970")) {
                            txtDeliveryDate.setText(objOrder.delivery.deliveryDate.substring(0,10));
                            layout_delivery_date.setVisibility(View.VISIBLE);
                        }
                    }
                }

                if (objOrder.delivery.deliveryType.equals(PublicData.DELIVERY_TYPE_PICKUP)) {
                    txtAddressDelivery.setText(objOrder.delivery.addressPoint);
                } else {
                    if (objOrder.contact_user != null) {
                        txtAddressDelivery.setText(objOrder.contact_user.cityText.concat(" ".concat(objOrder.contact_user.address)));
                    }
                }
            }

            if (objOrder.payment != null) {
                txtPayment.setText(objOrder.payment.paymentText);

                if (objOrder.payment.paymentType.equals(PublicData.PAYMENT_TYPE_NONE_CASH)) {
                    if (objOrder.isPayment) {
                        btnPayment.setVisibility(View.GONE);
                    } else {
                        btnPayment.setVisibility(View.VISIBLE);
                    }
                }
            }

            if (objOrder.isPayment) {
                txtIsPayment.setText("Оплачен");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    txtIsPayment.setTextColor(getResources().getColorStateList(R.color.colorPrimaryDark));
                } else {
                    txtIsPayment.setTextColor(ResourcesCompat.getColorStateList(getResources(), R.color.colorPrimaryDark, null));
                }
            } else {
                txtIsPayment.setText("Не оплачен");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    txtIsPayment.setTextColor(getResources().getColorStateList(R.color.colorTextDiscountPrice));
                } else {
                    txtIsPayment.setTextColor(ResourcesCompat.getColorStateList(getResources(), R.color.colorTextDiscountPrice, null));
                }
            }

            txtDate.setText(objOrder.createdAt);


            txtTotalNotSale.setText(String.valueOf(objOrder.totalNotSale).concat(" р."));
            txtDeliverySum.setText(String.valueOf(objOrder.sumDelivery).concat(" р."));
            txtSumSale.setText(String.valueOf(objOrder.totalSumSale).concat(" р."));
            txtTotal.setText(String.valueOf(objOrder.total).concat(" р."));

            if (objOrder.products != null) {
                txtCount.setText(String.valueOf(objOrder.products.size()).concat(" шт."));
                mAdapterSearch = new AdapterCartList(order_item.this, objOrder.products, true);
                mRecyclerView.setAdapter(mAdapterSearch);
                mAdapterSearch.notifyDataSetChanged();
            }

            layout_order_info.setVisibility(View.VISIBLE);
            layout_total.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        moduleObjects.cancelAllRequest();
    }

    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {

            try {

                JSONObject json = new JSONObject(result);
                String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                if (typeDate.equals(PublicData.QUESTION_TYPE_ORDER)) {

                    objOrder = new ObjOrder(result);

                    order_item.this.runOnUiThread(new Runnable() {
                            public void run() {
                                mSwipeRefreshLayout.setRefreshing(false);
                                updateView();
                    }
                        });

                }
            } catch (JSONException e) {
                // here you check the value of getActivity() and break up if needed
                order_item.this.runOnUiThread(new Runnable() {
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }

            order_item.this.runOnUiThread(new Runnable() {
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            if (!text.equals("")) {
                // here you check the value of getActivity() and break up if needed
                order_item.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(order_item.this, text, Toast.LENGTH_SHORT).show();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            } else {
                // here you check the value of getActivity() and break up if needed
                order_item.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(order_item.this, getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }

        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            if (!text.equals("")) {
                // here you check the value of getActivity() and break up if needed
                order_item.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(order_item.this, text, Toast.LENGTH_SHORT).show();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            } else {
                // here you check the value of getActivity() and break up if needed
                order_item.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(order_item.this, getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        }
    };
}
