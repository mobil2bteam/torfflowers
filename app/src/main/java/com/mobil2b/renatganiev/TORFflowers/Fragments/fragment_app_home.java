package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjFilter;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBWatch;
import com.mobil2b.renatganiev.torfflowers.ControlView.Menu.MenuView;
import com.mobil2b.renatganiev.torfflowers.ControlView.SocialLinkView.SocialLinkView;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog.ObjCatalogItems;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.GroupView.ObjGroupView;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Home.ObjHome;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Menu.ObjMenuItem;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjSocialLinkItem;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.ControlView.GroupProduct.ProductView;
import com.mobil2b.renatganiev.torfflowers.ControlView.Menu.ClickListenerMenu;
import com.mobil2b.renatganiev.torfflowers.ControlView.Slider.SliderView;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerLink;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.city_select;
import com.mobil2b.renatganiev.torfflowers.news_item;
import com.mobil2b.renatganiev.torfflowers.product_item;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_home extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_home";
    public static final int LAYOUT = R.layout.fragment_app_home;
    View rootView;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    LinearLayout layout_main, layout_slider, layout_menu, layout_group_view, layout_select_city, layout_change_city, layout_social_link,
                layout_group_view_watched;
    TextView txtCitySelect;

    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;

    SliderView sliderView;
    MenuView menuView;
    DeliveryCityView deliveryCityView;

    public static fragment_app_home getInstance(Bundle args) {
        fragment_app_home fragment = new fragment_app_home();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать
                if (arg.containsKey("NOTIFICATION_MESSAGE")) {
                    showNotificationMessage(arg.getString("NOTIFICATION_MESSAGE"));
                }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        mSwipeRefreshLayout     = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        layout_main             = rootView.findViewById(R.id.layout_main);
        layout_slider           = rootView.findViewById(R.id.layout_slider);
        layout_menu             = rootView.findViewById(R.id.layout_menu);
        layout_group_view       = rootView.findViewById(R.id.layout_group_view);
        layout_select_city      = rootView.findViewById(R.id.layout_select_city);
        layout_change_city      = rootView.findViewById(R.id.layout_change_city);
        layout_social_link      = rootView.findViewById(R.id.layout_social_link);
        layout_group_view_watched= rootView.findViewById(R.id.layout_group_view_watched);

        txtCitySelect           = rootView.findViewById(R.id.txtCitySelect);

        initToolbar();
        updateView();

        return rootView;
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        toolbar.setTitle(getString(R.string.app_name));

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_menu));
            } else {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_menu));
            }
        }

    }

    void updateView() {

        layout_slider.setVisibility(View.GONE);
        layout_menu.setVisibility(View.GONE);
        layout_group_view.setVisibility(View.GONE);
        layout_select_city.setVisibility(View.GONE);
        layout_social_link.setVisibility(View.GONE);


        if (PublicData.HOME != null) {

            /***
             *
             * СЛАЙДЕР
             */

            if (PublicData.HOME.slider != null) {
                if (PublicData.HOME.slider.items != null) {
                    if (PublicData.HOME.slider.items.size() != 0) {
                        sliderView      = new SliderView(getActivity(), PublicData.HOME.slider);
                        sliderView.setCircleFillColor(ContextCompat.getColor(getActivity(), R.color.colorAccent), Color.WHITE);
                        sliderView.setCircleRadius(3);
                        sliderView.setCircleStrokeColor(Color.WHITE);
                        sliderView.setCircleStrokeWidth(1);
                        sliderView.setPagerMargin(0, 0, 0, 0);

                        layout_slider.removeAllViews();
                        layout_slider.addView(sliderView);
                        layout_slider.setVisibility(View.VISIBLE);

                        sliderView.setOnClickListener(new ClickListenerLink() {
                            @Override
                            public void onClick(View view, int position, ObjLink link, boolean isLongClick) {
                                if (link != null) {
                                    openLink(link);
                                } else {
                                    //Toast.makeText(getContext(), "LINK пустой", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    }
                }

            }


            /***
             *
             * МЕНЮ
             */

            if (PublicData.HOME.menu != null) {

                menuView = new MenuView(getActivity(), PublicData.HOME.menu, new ClickListenerMenu() {
                    @Override
                    public void onClick(View view, ObjMenuItem objMenuItem) {
                        openLink(objMenuItem.link);
                    }
                });
                layout_menu.removeAllViews();
                layout_menu.addView(menuView);
                layout_menu.setVisibility(View.VISIBLE);
            }

            /***
             *
             * ПОДБОРКА ТОВАРОВ
             */

            if (PublicData.HOME.group_view != null) {
                if (PublicData.HOME.group_view.size() > 0) {
                    layout_group_view.removeAllViews();
                    for (ObjGroupView groupView : PublicData.HOME.group_view) {
                        if (groupView.items.size() > 0) {
                            ProductView productView = new ProductView(getContext(), groupView);
                            productView.setOnItemClickListener(new ClickListenerLink() {
                                @Override
                                public void onClick(View view, int position, ObjLink link, boolean isLongClick) {
                                    if (link != null) {
                                        openLink(link);
                                    } else {
                                        //Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            layout_group_view.addView(productView);
                        }
                    }
                    layout_group_view.setVisibility(View.VISIBLE);
                }
            }

            /***
             *
             * ГОРОД ДОСТАВКИ
             */

            if (PublicData.HOME.flag_show_city_delivery) {
                deliveryCityView = new DeliveryCityView(getContext(), PublicData.SETTINGS);
                deliveryCityView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getActivity(), city_select.class);
                        startActivityForResult(i, deliveryCityView.REZULT_REQUEST_SELECT_CITY);
                    }
                });
                layout_select_city.removeAllViews();
                layout_select_city.addView(deliveryCityView);
                layout_select_city.setVisibility(View.VISIBLE);
            }

            /***
             *
             * СКОЦИАЛЬНЫЕ КНОПКИ
             */

            if (PublicData.HOME.flag_show_social_link) {

                int count_social_link = 0;
                if (PublicData.SETTINGS != null) {
                    if (PublicData.SETTINGS.company_info != null) {
                        if (PublicData.SETTINGS.company_info.social_link != null) {

                            for (ObjSocialLinkItem objSocialLinkItem : PublicData.SETTINGS.company_info.social_link.socialLinkItems) {
                                if (Helps.isNotEmpty(objSocialLinkItem.url)) {
                                    count_social_link++;
                                }
                            }

                            int count_line = 3;
                            if (count_social_link == 2) count_line = 2;
                            if (count_social_link == 3) count_line = 3;
                            if (count_social_link == 4) count_line = 2;
                            if (count_social_link == 5) count_line = 3;
                            if (count_social_link == 6) count_line = 3;

                            SocialLinkView socialLinkView = new SocialLinkView(getActivity(), PublicData.SETTINGS.company_info.social_link, count_line,
                                    (view, objSocialLink) -> {
                                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(objSocialLink.url));
                                        startActivity(browserIntent);
                                    });

                            layout_social_link.removeAllViews();
                            layout_social_link.addView(socialLinkView);
                        }
                    }
                }

                if (count_social_link > 0) {
                    layout_social_link.setVisibility(View.VISIBLE);
                }


                /***
                 *
                 * ПОДБОРКА ВЫ СМОТРЕЛИ
                 */

                if (PublicData.HOME != null) {
                    if (PublicData.HOME.flag_show_watched) {
                        DBWatch watch = new DBWatch(getContext());
                        ArrayList<ObjProductMin> list_product = watch.getList();
                        watch.close();
                        if (list_product.size() > 0) {
                            //moduleObjects.getUpdateListProduct(false, list_id);

                            layout_group_view_watched.removeAllViews();
                            if (list_product.size() != 0) {
                                ObjGroupView objGroupView = new ObjGroupView();
                                objGroupView.button_more = false;
                                objGroupView.link = null;
                                objGroupView.name = "Вы недавно смотрели";
                                objGroupView.count_items_display = 2f;
                                objGroupView.items.addAll(list_product);


                                ProductView productView = new ProductView(getContext(), objGroupView);
                                layout_group_view_watched.addView(productView);
                            }

                        }
                    }
                }


            }
        }

        mSwipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        deliveryCityView.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRefresh() {
        ModuleObjects moduleObjects = new ModuleObjects(getActivity(), new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
                mSwipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
                try {

                    JSONObject json = new JSONObject(result);
                    String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                    if (typeDate.equals(PublicData.QUESTION_TYPE_HOME)) {

                        if (json.has("home")) {
                            JSONObject jsonHome = json.getJSONObject("home");
                            PublicData.HOME         = new ObjHome(jsonHome);
                            // here you check the value of getActivity() and break up if needed
                            if(getActivity() == null) return;
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    if (PublicData.objCity != null) {
                                        updateView();
                                        mSwipeRefreshLayout.setRefreshing(false);
                                    }
                                }
                            });

                        }
                    }
                } catch (JSONException e) {
                }
            }

            @Override
            public void onError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            }

            @Override
            public void onHttpError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            }
        });
        moduleObjects.getHome(true);
    }


    void openLink(ObjLink link) {
        String urlName = OpenBlocks.getLinkURL(link);

        if (Helps.isNotEmpty(link.url_link)) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.url_link));
                startActivity(browserIntent);
            } catch (Exception e) {

            }
            return;
        }

        if (urlName.equals(PublicData.QUESTION_TYPE_CATALOG)) {
            /***
             *
             * ПЕРЕХОД К КАТАЛОГУ
             */
            if (link.param != null) {
                if (link.param.size() != 0) {
                    for (ObjParam objParam : link.param) {
                        if (objParam.name.equals("catalog_id")) {

                            int catalog_id = Integer.decode(objParam.value);
                            boolean catalog_open = true;

                            if ((PublicData.DIRECTORIES != null) && ((PublicData.DIRECTORIES.catalog != null))) {
                                ObjCatalogItems objCatalogItems = PublicData.DIRECTORIES.catalog.getCatalogItems(catalog_id, PublicData.DIRECTORIES.catalog.items);
                                if (objCatalogItems != null) {
                                    catalog_open = !objCatalogItems.search;
                                }
                            }

                            Bundle bundle = new Bundle();

                            if (catalog_open) {
                                bundle.putInt("catalog_id", catalog_id);
                                callbackActionSuper.onAction(TAG, PublicData.SELECT_CATALOG, bundle);
                            } else {
                                ObjFilter objFilter = new ObjFilter();
                                objFilter.catalog_id = catalog_id;
                                bundle.putSerializable("filter", objFilter);
                                callbackActionSuper.onAction(TAG, PublicData.SELECT_FILTER, bundle);
                            }
                        }
                    }
                }
            }
        }
        /***
         *
         * ПЕРЕХОД К СПИСКУ ТОВАРОВ
         */
        if (urlName.equals(PublicData.QUESTION_TYPE_PRODUCTS)) {
            if (link != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("link", link);
                callbackActionSuper.onAction(TAG, PublicData.SELECT_FILTER, bundle);
            }
        }

        /***
         *
         * ПЕРЕХОД К КОНКРЕТНОМУ ТОВАРУ
         */
        if (urlName.equals(PublicData.QUESTION_TYPE_PRODUCT)) {
            if (link != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("link", link);
                Intent intent = new Intent(getActivity(), product_item.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }

        /***
         *
         * ПЕРЕХОД К НОВОСТИ ИЛИ АКЦИИ
         */
        if ((urlName.equals(PublicData.QUESTION_TYPE_NEWS_ITEM)) ||
            (link.name.equals(PublicData.QUESTION_TYPE_NEWS))) {
            if (link != null) {

                if (link.name.equals(PublicData.QUESTION_TYPE_NEWS)) {
                    link.name = PublicData.QUESTION_TYPE_NEWS_ITEM;
                    if (link.param != null) {
                        for (ObjParam objParam : link.param) {
                            if (objParam.name.equals("id")) {
                                objParam.name = "news_id";
                            }
                        }
                    }
                }

                Bundle bundle = new Bundle();
                bundle.putSerializable("link", link);
                Intent intent = new Intent(getActivity(), news_item.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }
    }


    void showNotificationMessage(String messageBody) {

        //Toast.makeText(getActivity(), messageBody, Toast.LENGTH_LONG).show();

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(messageBody).setPositiveButton("Ok", dialogClickListener).show();
    }

}
