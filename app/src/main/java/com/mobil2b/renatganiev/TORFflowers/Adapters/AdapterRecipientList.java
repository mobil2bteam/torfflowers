package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjRecipient.ObjRecipient;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerRecipient;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterRecipientList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private ArrayList<ObjRecipient> mList;

    ClickListenerRecipient clickListenerDelivery;

    public AdapterRecipientList(Context context, ArrayList<ObjRecipient> list) {
        this.mContext = context;
        this.mList = list;
    }

    public void setOnClickListenerDelivery(ClickListenerRecipient onClickListener){
        this.clickListenerDelivery = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_recipient, parent, false);

        return new ViewHolderProduct(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ViewHolderProduct viewHolder0 = (ViewHolderProduct) holder;
        final ObjRecipient obj = getItem(position);

        // НАИМЕНОВАНИЕ
        viewHolder0.txtName.setText(obj.name);
        viewHolder0.txtPhone.setText(obj.phone);

        viewHolder0.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListenerDelivery != null) {
                    clickListenerDelivery.onRemove(view, obj);
                }
            }
        });

        viewHolder0.setClickListener(clickListenerDelivery);
    }


    private ObjRecipient getItem(int position){
        return mList.get(position);
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderProduct extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtName, txtPhone;
        ImageView imgDelete;

        ClickListenerRecipient clickListenerDelivery;

        public ViewHolderProduct(View itemView) {
            super(itemView);

            txtName             = (TextView) itemView.findViewById(R.id.txtName);
            txtPhone            = (TextView) itemView.findViewById(R.id.txtPhone);
            imgDelete           = (ImageView) itemView.findViewById(R.id.imgDelete);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListenerDelivery != null) {
                clickListenerDelivery.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerRecipient clickListener){
            this.clickListenerDelivery = clickListener;
        }
    }
}