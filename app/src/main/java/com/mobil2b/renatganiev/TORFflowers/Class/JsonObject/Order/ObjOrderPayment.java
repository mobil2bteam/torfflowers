package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjOrderPayment {


    public int      paymentId;
    public String   paymentText;
    public String   paymentType;

    public ObjOrderPayment() {
        init();
    }
    public ObjOrderPayment(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjOrderPayment(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("payment")) {
                JSONObject jsonMain = json.getJSONObject("payment");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("paymentId")) {
                paymentId = json.getInt("paymentId");
            }
            if (json.has("paymentText")) {
                paymentText = json.getString("paymentText");
            }
            if (json.has("paymentType")) {
                paymentType = json.getString("paymentType");
            }

        } catch (JSONException e) {
        }
    }

}
