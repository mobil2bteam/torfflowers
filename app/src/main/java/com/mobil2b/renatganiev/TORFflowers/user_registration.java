package com.mobil2b.renatganiev.torfflowers;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackAuthorization;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Fragments.User.Registration.fragment_app_user_registration;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;

public class user_registration extends AppCompatActivity {

    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_registration);

        fragmentmanager = getSupportFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction = callbackAction;

        User.init(user_registration.this);
        User.callbackAuthorization = callbackAuthorization;
        fragmentControl.showFragmentUserRegistration(true, null, R.id.layout_container, null);
    }

    CallbackAction callbackAction = new CallbackAction() {
        @Override
        public void onAction(String tag, int type, ObjLink data) {

        }

        @Override
        public void onAction(String tag, int type, Bundle data) {
            if (tag.equals(fragment_app_user_registration.TAG)) {
                if (type == PublicData.USER_REGISTRATION_CLICK) {
                    if (data != null) {
                        if (data.containsKey("userEmail")) {
                            if (data.containsKey("userName")) {
                                if (data.containsKey("userFirstName")) {
                                    if (data.containsKey("userSecondName")) {
                                        if (data.containsKey("userPassword")) {
                                            userRegistration(data);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (type == PublicData.USER_LOGIN_CLICK) {
                    fragmentControl.showFragmentUserLogin(false, null, R.id.layout_container, null);
                }
            }
        }
    };



    void userRegistration(Bundle data) {

        if (data != null) {
            if (data.containsKey("userEmail")) {
                if (data.containsKey("userName")) {
                    if (data.containsKey("userFirstName")) {
                        if (data.containsKey("userSecondName")) {
                            if (data.containsKey("userPassword")) {
                                DataSession dataSession = new DataSession();
                                if (PublicData.objCity != null) {
                                    dataSession.city_id = PublicData.objCity.id;
                                }
                                dataSession.email = data.getString("userEmail");
                                dataSession.login = data.getString("userEmail");
                                dataSession.password = data.getString("userPassword");
                                dataSession.user_name = data.getString("userName");
                                dataSession.first_name = data.getString("userFirstName");
                                dataSession.second_name = data.getString("userSecondName");
                                dataSession.phone = data.getString("phone");

                                User.onRegistrationUser(dataSession, callbackAuthorization);
                            }
                        }
                    }
                }
            }
        }
    }


    CallbackAuthorization callbackAuthorization = new CallbackAuthorization() {
        @Override
        public void onStart(String text) {
            user_registration.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.showDialog(user_registration.this);
                }
            });
        }

        @Override
        public void onReturnSession(DataSession result) {
        }

        @Override
        public void onDontSession(final String text, int error_code) {
            user_registration.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    Toast.makeText(user_registration.this, text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onSuccess(DataSession result, int type) {
            user_registration.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                }
            });
            if ((type == User.USER_REGISTRATION) || (type == User.USER_PASSWORD)) {
                setResult(RESULT_OK);
                finish();
            }
        }

        @Override
        public void onError(DataSession result, final String message, int error_code, int type) {
            user_registration.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    Toast.makeText(user_registration.this, message, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onExit() {

        }
    };
}
