package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjGeo {

    public double lat, lng;


    public ObjGeo() {
        init();
    }
    public ObjGeo(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjGeo(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("lat")) {
                this.lat = json.getDouble("lat");
            }
            if (json.has("lng")) {
                this.lng = json.getDouble("lng");
            }
        } catch (JSONException e) {
        }
    }
}
