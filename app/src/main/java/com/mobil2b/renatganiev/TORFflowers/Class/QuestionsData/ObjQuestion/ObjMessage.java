package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 25.04.16.
 */
public class ObjMessage {

    public String title;
    public String text;
    public int  code;

    public ObjMessage() {}
    public ObjMessage(String tit, String txt) {
        this.title = tit;
        this.text = txt;
    }
    public ObjMessage(JSONObject json) {
        loadJSONObject(json);
    }
    public ObjMessage(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("message")) {
                JSONObject msgJsdon = json.getJSONObject("message");
                if (msgJsdon.has("title")) {
                    this.title = msgJsdon.getString("title");
                }
                if (msgJsdon.has("text")) {
                    this.text = msgJsdon.getString("text");
                }
                if (msgJsdon.has("code")) {
                    this.code = msgJsdon.getInt("code");
                }
            }

        } catch (JSONException e) {
        }
    }
}
