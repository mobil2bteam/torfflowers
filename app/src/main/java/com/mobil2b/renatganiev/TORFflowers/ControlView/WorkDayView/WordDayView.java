package com.mobil2b.renatganiev.torfflowers.ControlView.WorkDayView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjWorkingHours;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 10.10.15.
 */
public class WordDayView extends LinearLayout implements View.OnClickListener {

    Context                 mContext;

    TextView                txtCaption, txtValue;
    ObjWorkingHours         objWork;


    public WordDayView(Context context, ObjWorkingHours obj) {
        super(context);

        mContext        = context;
        objWork         = obj;
        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_work, this);

        txtCaption             = (TextView) findViewById(R.id.txtCaption);
        txtValue             = (TextView) findViewById(R.id.txtValue);

        txtCaption.setTypeface(PublicData.tf_custom_font_400);
        txtValue.setTypeface(PublicData.tf_custom_font_400);

        txtCaption.setText(objWork.day);
        if (objWork.free_day) {
            txtValue.setText("Выходной");
        } else {
            txtValue.setText(objWork.hours_start.concat(" - ").concat(objWork.hours_finish));
        }
    }

    public ObjWorkingHours getObject() {
        return objWork;
    }

    @Override
    public void onClick(View v) {

    }

}