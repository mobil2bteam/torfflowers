package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBrand;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCatalogFastFilter;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjFilter implements Serializable {

    public int                  catalog_id;
    public ArrayList<Integer>   brand_id;
    public ArrayList<Integer>   season_id;
    public ArrayList<Integer>   age_id;
    public ArrayList<Integer>   size_id;
    public ArrayList<Integer>   flag_id;
    public ArrayList<Integer>   color_id;
    public ArrayList<Integer>   material_id;
    public int                  sex_id;
    public ArrayList<Integer>   country_manufacture_id;
    public int                  price_from;
    public int                  price_to;
    public int                  sale;

    public int                  page;
    public int                  onpage;
    public int                  count_page;
    public int                  result_count;
    public int                  sort_id;
    public String               search;
    public ArrayList<ObjCatalogFastFilter> catalog_list;
    public ArrayList<ObjBrand> brand_list;


    public ObjFilter() {
        init();
    }
    public ObjFilter(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjFilter(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("filter")) {
                JSONObject jsonMain = json.getJSONObject("filter");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        catalog_list    = new ArrayList<>();
        brand_list      = new ArrayList<>();
        size_id         = new ArrayList<>();
        flag_id         = new ArrayList<>();
        color_id        = new ArrayList<>();
        material_id     = new ArrayList<>();
        brand_id                    = new ArrayList<>();
        season_id                   = new ArrayList<>();
        age_id                      = new ArrayList<>();
        country_manufacture_id      = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("catalog_id")) {
                catalog_id = json.getInt("catalog_id");
            }
            if (json.has("season_id")) {
                for (int i = 0; i < json.getJSONArray("season_id").length(); i++){
                    season_id.add(json.getJSONArray("season_id").getInt(i));
                }
            }
            if (json.has("brand_id")) {
                for (int i = 0; i < json.getJSONArray("brand_id").length(); i++){
                    brand_id.add(json.getJSONArray("brand_id").getInt(i));
                }
            }
            if (json.has("age_id")) {
                for (int i = 0; i < json.getJSONArray("age_id").length(); i++){
                    age_id.add(json.getJSONArray("age_id").getInt(i));
                }
            }
            if (json.has("sex_id")) {
                sex_id = json.getInt("sex_id");
            }
            if (json.has("country_manufacture_id")) {
                for (int i = 0; i < json.getJSONArray("country_manufacture_id").length(); i++){
                    country_manufacture_id.add(json.getJSONArray("country_manufacture_id").getInt(i));
                }
            }
            if (json.has("price_from")) {
                price_from = json.getInt("price_from");
            }
            if (json.has("price_to")) {
                price_to = json.getInt("price_to");
            }
            if (json.has("page")) {
                page = json.getInt("page");
            }
            if (json.has("onpage")) {
                onpage = json.getInt("onpage");
            }
            if (json.has("count_page")) {
                count_page = json.getInt("count_page");
            }
            if (json.has("result_count")) {
                result_count = json.getInt("result_count");
            }
            if (json.has("sort_id")) {
                sort_id = json.getInt("sort_id");
            }
            if (json.has("sale")) {
                sale = json.getInt("sale");
            }
            if (json.has("search")) {
                search = json.getString("search");
            }
            if (json.has("size_id")) {
                for (int i = 0; i < json.getJSONArray("size_id").length(); i++){
                    size_id.add(json.getJSONArray("size_id").getInt(i));
                }
            }
            if (json.has("flag_id")) {
                for (int i = 0; i < json.getJSONArray("flag_id").length(); i++){
                    flag_id.add(json.getJSONArray("flag_id").getInt(i));
                }
            }
            if (json.has("color_id")) {
                for (int i = 0; i < json.getJSONArray("color_id").length(); i++){
                    color_id.add(json.getJSONArray("color_id").getInt(i));
                }
            }
            if (json.has("material_id")) {
                for (int i = 0; i < json.getJSONArray("material_id").length(); i++){
                    material_id.add(json.getJSONArray("material_id").getInt(i));
                }
            }
            if (json.has("catalog_list")) {
                for (int i = 0; i < json.getJSONArray("catalog_list").length(); i++){
                    ObjCatalogFastFilter it = new ObjCatalogFastFilter(json.getJSONArray("catalog_list").getJSONObject(i));
                    catalog_list.add(it);
                }
            }
            if (json.has("brand_list")) {
                for (int i = 0; i < json.getJSONArray("brand_list").length(); i++){
                    ObjBrand it = new ObjBrand(json.getJSONArray("brand_list").getJSONObject(i));
                    brand_list.add(it);
                }
            }

        } catch (JSONException e) {
        }
    }

    /**
     * Проверяем, возможно ли дальше перелистывать.
     * */
    public boolean existNextPage() {
        if (this.page < this.count_page) {
            return true;
        }
        return false;
    }

    /**
     * Переключаем счетчик страницы.
     * */
    public int getNextPage() {
        if (this.page >= 0) {
            if (this.page < this.count_page) {
                this.page = this.page + 1;
            } else {
                this.page = this.count_page;
            }
        } else {
            this.page = 1;
        }
        return this.page;
    }

    /**
     * Получаем фильтры в виде параметров
     * */
    public ArrayList<ObjParam> getParamList() {
        ArrayList<ObjParam> rez = new ArrayList<>();

        String str_value = "";
        if (size_id != null) {
            if (size_id.size() > 0) {
                str_value = "";
                for (Integer ii : size_id) {
                    if (str_value.length() > 0) {
                        str_value = str_value.concat("-").concat(String.valueOf(ii));
                    } else {
                        str_value = str_value.concat(String.valueOf(ii));
                    }
                }
                if (!str_value.equals("")) {
                    rez.add(new ObjParam("size_id", str_value));
                }
            }
        }

        if (flag_id != null) {
            if (flag_id.size() > 0) {
                str_value = "";
                for (Integer ii : flag_id) {
                    if (str_value.length() > 0) {
                        str_value = str_value.concat("-").concat(String.valueOf(ii));
                    } else {
                        str_value = str_value.concat(String.valueOf(ii));
                    }
                }
                if (!str_value.equals("")) {
                    rez.add(new ObjParam("flag_id", str_value));
                }
            }
        }

        if (color_id != null) {
            if (color_id.size() > 0) {
                str_value = "";
                for (Integer ii : color_id) {
                    if (str_value.length() > 0) {
                        str_value = str_value.concat("-").concat(String.valueOf(ii));
                    } else {
                        str_value = str_value.concat(String.valueOf(ii));
                    }
                }
                if (!str_value.equals("")) {
                    rez.add(new ObjParam("color_id", str_value));
                }
            }
        }

        if (material_id != null) {
            if (material_id.size() > 0) {
                str_value = "";
                for (Integer ii : material_id) {
                    if (str_value.length() > 0) {
                        str_value = str_value.concat("-").concat(String.valueOf(ii));
                    } else {
                        str_value = str_value.concat(String.valueOf(ii));
                    }
                }
                if (!str_value.equals("")) {
                    rez.add(new ObjParam("material_id", str_value));
                }
            }
        }

        if (season_id != null) {
            if (season_id.size() > 0) {
                str_value = "";
                for (Integer ii : season_id) {
                    if (str_value.length() > 0) {
                        str_value = str_value.concat("-").concat(String.valueOf(ii));
                    } else {
                        str_value = str_value.concat(String.valueOf(ii));
                    }
                }
                if (!str_value.equals("")) {
                    rez.add(new ObjParam("season_id", str_value));
                }
            }
        }

        if (brand_id != null) {
            if (brand_id.size() > 0) {
                str_value = "";
                for (Integer ii : brand_id) {
                    if (str_value.length() > 0) {
                        str_value = str_value.concat("-").concat(String.valueOf(ii));
                    } else {
                        str_value = str_value.concat(String.valueOf(ii));
                    }
                }
                if (!str_value.equals("")) {
                    rez.add(new ObjParam("brand_id", str_value));
                }
            }
        }
        if (age_id != null) {
            if (age_id.size() > 0) {
                str_value = "";
                for (Integer ii : age_id) {
                    if (str_value.length() > 0) {
                        str_value = str_value.concat("-").concat(String.valueOf(ii));
                    } else {
                        str_value = str_value.concat(String.valueOf(ii));
                    }
                }
                if (!str_value.equals("")) {
                    rez.add(new ObjParam("age_id", str_value));
                }
            }
        }
        if (country_manufacture_id != null) {
            if (country_manufacture_id.size() > 0) {
                str_value = "";
                for (Integer ii : country_manufacture_id) {
                    if (str_value.length() > 0) {
                        str_value = str_value.concat("-").concat(String.valueOf(ii));
                    } else {
                        str_value = str_value.concat(String.valueOf(ii));
                    }
                }
                if (!str_value.equals("")) {
                    rez.add(new ObjParam("country_manufacture_id", str_value));
                }
            }
        }
        if (this.catalog_id != 0) {
            rez.add(new ObjParam("catalog_id", String.valueOf(this.catalog_id)));
        }


        if (this.sex_id != 0) {
            rez.add(new ObjParam("sex_id", String.valueOf(this.sex_id)));
        }
        if (this.price_from != 0) {
            rez.add(new ObjParam("price_from", String.valueOf(this.price_from)));
        }
        if (this.price_to != 0) {
            rez.add(new ObjParam("price_to", String.valueOf(this.price_to)));
        }
        if (this.page != 0) {
            rez.add(new ObjParam("page", String.valueOf(this.page)));
        }
        if (this.onpage != 0) {
            rez.add(new ObjParam("onpage", String.valueOf(this.onpage)));
        }
        if (this.sort_id != 0) {
            rez.add(new ObjParam("sort_id", String.valueOf(this.sort_id)));
        }
        if (this.sale != 0) {
            rez.add(new ObjParam("sale", String.valueOf(this.sale)));
        }
        if ((this.search != null) && (!this.search.equals(""))) {
            rez.add(new ObjParam("search", this.search));
        }

        return rez;
    }

}
