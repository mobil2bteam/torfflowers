package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjRecipient.ObjRecipient;

import java.util.ArrayList;


/**
 * Created by renatganiev on 14.10.15.
 */
public class DBRecipientHistory {

    private ContentValues cv;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private Context mContext;
    private String TABLE = "recipient_history";

    public DBRecipientHistory(Context context) {
        // создаем объект для данных
        cv          = new ContentValues();
        // создаем объект для создания и управления версиями БД
        dbHelper    = new DBHelper(context);
        // подключаемся к БД
        db          = dbHelper.getWritableDatabase();
        mContext    = context;
    }

    // ДОБАВЛЯЕМ
    public long add(ObjRecipient value) {

        if (exist(value)) return 0;

        cv          = new ContentValues();
        cv.put("name",   value.name);
        cv.put("phone",  value.phone);

        long rowID = db.insert(TABLE, null, cv);
        return rowID;
    }

    // ПОЛУЧАЕМ
    public ArrayList<ObjRecipient> getList() {
        String query    = "";
        query    = "SELECT * FROM "+TABLE+" ORDER BY id DESC";
        Cursor cursor1  = db.rawQuery(query, null);

        ArrayList<ObjRecipient> listResult = new ArrayList<>();
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {

                ObjRecipient objRecipient = new ObjRecipient();
                objRecipient.id     = cursor1.getInt(cursor1.getColumnIndex("id"));
                objRecipient.name   = cursor1.getString(cursor1.getColumnIndex("name"));
                objRecipient.phone  = cursor1.getString(cursor1.getColumnIndex("phone"));

                listResult.add(objRecipient);
            }
        }
        return listResult;
    }


    // ПОЛУЧАЕМ
    public ObjRecipient getRecipient(int recipient_id) {
        String query    = "SELECT * FROM "+TABLE+" WHERE id = "+String.valueOf(recipient_id)+" ORDER BY id DESC";

        Cursor cursor1  = db.rawQuery(query, null);

        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {

                ObjRecipient objRecipient = new ObjRecipient();
                objRecipient.id     = cursor1.getInt(cursor1.getColumnIndex("id"));
                objRecipient.name   = cursor1.getString(cursor1.getColumnIndex("name"));
                objRecipient.phone  = cursor1.getString(cursor1.getColumnIndex("phone"));

                return objRecipient;
            }
        }
        return null;
    }

    // ПРОВЕРЯЕМ
    public boolean exist(ObjRecipient value) {
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE phone = '"+value.phone+"'");
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // ПОЛУЧАЕМ ДАННЫЕ
    public ObjRecipient getRecipientToObj(ObjRecipient value) {
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE phone = '"+value.phone+"'");
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {

                ObjRecipient objRecipient = new ObjRecipient();
                objRecipient.id     = cursor1.getInt(cursor1.getColumnIndex("id"));
                objRecipient.name   = cursor1.getString(cursor1.getColumnIndex("name"));
                objRecipient.phone  = cursor1.getString(cursor1.getColumnIndex("phone"));

                return objRecipient;
            }
        }
        return null;
    }

    // УДАЛЯЕМ
    public boolean delete(ObjRecipient value) {
        int clearCount = db.delete(TABLE, "id = '".concat(String.valueOf(value.id)).concat("'"), null);
        if (clearCount > 0) {
            return true;
        } else {
            return false;
        }
    }


    // ОЧИСТИТЬ
    public int clear() {
        int clearCount = db.delete(TABLE, null, null);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE + "'");
        return clearCount;
    }

    public void close() {
        db.close();
    }

}
