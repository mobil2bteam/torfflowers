package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.ControlView.IntroSlider.MyIntro;
import com.mobil2b.renatganiev.torfflowers.Fragments.fragment_app_city_list;


/**
 * TODO: Активити для загрузки предварительных значений для проекта.
 * */

public class loading extends AppCompatActivity {

    ModuleObjects moduleObjects;
    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;
    SharedPreferences getSharedPreferences;

    public int REZULT_REQUEST_INTRO_SLIDER                  = 500;

    String NOTIFICATION_MESSAGE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("NOTIFICATION_MESSAGE")) {
                NOTIFICATION_MESSAGE = extras.getString("NOTIFICATION_MESSAGE");
            }
        }

        fragmentmanager = getSupportFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction          = new CallbackAction() {
            @Override
            public void onAction(String tag, int type, ObjLink data) {
            }
            @Override
            public void onAction(String tag, int type, Bundle data) {
                if (tag.equals(fragment_app_city_list.TAG)) {
                    if (type == PublicData.SELECT_CITY) {
                        if (data.containsKey("city_id")) {
                            int city_id = data.getInt("city_id");
                            setCitySelect(city_id);
                        }
                    }
                }
            }
        };

        getSharedPreferences    = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        String token = null;
        User.init(getApplicationContext());
        if (User.isLoggedIn()) {
            token = User.getToken();
        }
        moduleObjects = new ModuleObjects(loading.this, callBackSendDate);
        moduleObjects.getFirstQuestions(true, null, token);
    }

    @Override
    public void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        // ---> extras is always null when app is not running or when app is in background
        if (extras != null) {
            if (extras.containsKey("NOTIFICATION_MESSAGE")) {
                NOTIFICATION_MESSAGE = extras.getString("NOTIFICATION_MESSAGE");
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REZULT_REQUEST_INTRO_SLIDER) {
            if (resultCode == RESULT_OK) {
                setFirstStart(false);
                fragmentControl.showFragmentCityList(true, null, R.id.layout_container, null);
            } else {
                setFirstStart(false);
                fragmentControl.showFragmentCityList(true, null, R.id.layout_container, null);
            }
        }
    }


    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {

        }

        @Override
        public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            String typeDate = OpenBlocks.getOpenData(result);

            if (typeDate == null) return;

            if (typeDate.equals(PublicData.QUESTION_TYPE_URL)) {
                // Предустанавливаем все URL для работы с пользовательскими URL
                User.setURLLogin(PublicData.getURL(PublicData.QUESTION_TYPE_USER_LOGIN));
                User.setURLRegistration(PublicData.getURL(PublicData.QUESTION_TYPE_USER_REGISTRATION));
                User.setURLEdit(PublicData.getURL(PublicData.QUESTION_TYPE_USER_UPDATE));
                User.setURLPasswordChange(PublicData.getURL(PublicData.QUESTION_TYPE_USER_PASSWORD_CHANGE));
                User.setURLPasswordMemory(PublicData.getURL(PublicData.QUESTION_TYPE_USER_PASSWORD_MEMORY));
                User.setURLExist(PublicData.getURL(PublicData.QUESTION_TYPE_USER_EXIST));

                if (OpenBlocks.getExistData(result, PublicData.QUESTION_TYPE_USER_LOGIN)) {
                    DataSession dataSession = new DataSession(result, getApplicationContext());
                    User.setCurrentSession(dataSession);
                }

                // Предустановка города. Проверяем все настройки.
                if (PublicData.SETTINGS.select_start_city) {
                    if (PublicData.SETTINGS.change_city) {
                        if (isFirstStart()) {
                            //startIntro();
                            fragmentControl.showFragmentCityList(true, null, R.id.layout_container, null);
                        } else {
                            setCity();
                        }
                    } else {
                        setCity();
                    }
                } else {
                    setCity();
                }
            }
        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            if (!text.equals("")) {
                loading.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(loading.this, text, Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                loading.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(loading.this, getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            if (!text.equals("")) {
                loading.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(loading.this, text, Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                loading.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(loading.this, getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };


    void startMain() {
        Intent intent = new Intent(loading.this, main.class);
        if (Helps.isNotEmpty(NOTIFICATION_MESSAGE)) {
            intent.putExtra("NOTIFICATION_MESSAGE", NOTIFICATION_MESSAGE);
        }
        startActivity(intent);
        finish();
    }

    void startIntro() {
        Intent i = new Intent(loading.this, MyIntro.class);
        startActivityForResult(i, REZULT_REQUEST_INTRO_SLIDER);
    }

    void setCity() {
        int city_id = getCitySelect();
        if (city_id == 0) {
            if (PublicData.SETTINGS.default_city != 0) {
                setCitySelect(PublicData.SETTINGS.default_city);
            }
        } else {
            setCitySelect(city_id);
        }
    }


    boolean isFirstStart() {
        return getSharedPreferences.getBoolean("firstStart", true);
    }
    int getCitySelect() {
        return getSharedPreferences.getInt("city_id", 0);
    }
    void setCitySelect(int city_id) {
        SharedPreferences.Editor e = getSharedPreferences.edit();
        e.putInt("city_id", city_id);
        e.apply();
        setFirstStart(false);

        if (PublicData.DIRECTORIES != null) {
            if (PublicData.DIRECTORIES.city != null) {
                for (ObjCity objCity : PublicData.DIRECTORIES.city) {
                    if (objCity.id == city_id) {
                        PublicData.objCity = objCity;

                        loading.this.runOnUiThread(new Runnable() {
                            public void run() {
                                if (PublicData.objCity != null) {
                                    Toast.makeText(loading.this, PublicData.objCity.name, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        break;
                    }
                }
            }
        }
        startMain();
    }
    void setFirstStart(boolean start) {
        SharedPreferences.Editor e = getSharedPreferences.edit();
        e.putBoolean("firstStart", start);
        e.apply();
    }

}
