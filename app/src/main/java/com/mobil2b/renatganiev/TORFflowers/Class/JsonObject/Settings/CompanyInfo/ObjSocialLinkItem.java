package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import android.graphics.Bitmap;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjSocialLinkItem {
    public String url;
    public Bitmap icon_bmp;

    public ObjSocialLinkItem() {
    }
    public ObjSocialLinkItem(String link, Bitmap bmp) {
        url         = link;
        icon_bmp    = bmp;
    }
}
