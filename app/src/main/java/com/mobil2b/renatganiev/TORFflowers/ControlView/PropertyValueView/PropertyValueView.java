package com.mobil2b.renatganiev.torfflowers.ControlView.PropertyValueView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjProperties;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 10.10.15.
 */
public class PropertyValueView extends RelativeLayout {

    Context             mContext;
    TextView txtText, txtValue;
    LinearLayout layout_1, layout_2, layout_3, layout_4, layout_5, layout_6, layout_7, layout_8, layout_9;
    ObjProperties value;

    public PropertyValueView(Context context) {
        super(context);
        mContext    = context;
        initComponent();
    }

    public PropertyValueView(Context context, ObjProperties v) {
        super(context);
        mContext    = context;
        value = v;
        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_property_value, this);

        txtText    = (TextView) findViewById(R.id.txtText);
        txtValue    = (TextView) findViewById(R.id.txtValue);
        layout_1    = (LinearLayout) findViewById(R.id.layout_1);
        layout_2    = (LinearLayout) findViewById(R.id.layout_2);
        layout_3    = (LinearLayout) findViewById(R.id.layout_3);
        layout_4    = (LinearLayout) findViewById(R.id.layout_4);
        layout_5    = (LinearLayout) findViewById(R.id.layout_5);
        layout_6    = (LinearLayout) findViewById(R.id.layout_6);
        layout_7    = (LinearLayout) findViewById(R.id.layout_7);
        layout_8    = (LinearLayout) findViewById(R.id.layout_8);
        layout_9    = (LinearLayout) findViewById(R.id.layout_9);

        updateView();
    }

    private void updateView() {
        if (value != null) {
            txtText.setText(value.name);
            txtText.setVisibility(VISIBLE);

            boolean val = false;

            if (value.value.equals("0")) {

            }
            if (value.value.equals("1")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                val = true;
            }
            if (value.value.equals("2")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                setDrawable(layout_2, R.drawable.button_all_points);
                val = true;
            }
            if (value.value.equals("3")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                setDrawable(layout_2, R.drawable.button_all_points);
                setDrawable(layout_3, R.drawable.button_all_points);
                val = true;
            }
            if (value.value.equals("4")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                setDrawable(layout_2, R.drawable.button_all_points);
                setDrawable(layout_3, R.drawable.button_all_points);
                setDrawable(layout_4, R.drawable.button_all_points);
                val = true;
            }
            if (value.value.equals("5")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                setDrawable(layout_2, R.drawable.button_all_points);
                setDrawable(layout_3, R.drawable.button_all_points);
                setDrawable(layout_4, R.drawable.button_all_points);
                setDrawable(layout_5, R.drawable.button_all_points);
                val = true;
            }
            if (value.value.equals("6")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                setDrawable(layout_2, R.drawable.button_all_points);
                setDrawable(layout_3, R.drawable.button_all_points);
                setDrawable(layout_4, R.drawable.button_all_points);
                setDrawable(layout_5, R.drawable.button_all_points);
                setDrawable(layout_6, R.drawable.button_all_points);
                val = true;
            }
            if (value.value.equals("7")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                setDrawable(layout_2, R.drawable.button_all_points);
                setDrawable(layout_3, R.drawable.button_all_points);
                setDrawable(layout_4, R.drawable.button_all_points);
                setDrawable(layout_5, R.drawable.button_all_points);
                setDrawable(layout_6, R.drawable.button_all_points);
                setDrawable(layout_7, R.drawable.button_all_points);
                val = true;
            }
            if (value.value.equals("8")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                setDrawable(layout_2, R.drawable.button_all_points);
                setDrawable(layout_3, R.drawable.button_all_points);
                setDrawable(layout_4, R.drawable.button_all_points);
                setDrawable(layout_5, R.drawable.button_all_points);
                setDrawable(layout_6, R.drawable.button_all_points);
                setDrawable(layout_7, R.drawable.button_all_points);
                setDrawable(layout_8, R.drawable.button_all_points);
                val = true;
            }
            if (value.value.equals("9")) {
                setDrawable(layout_1, R.drawable.button_all_points);
                setDrawable(layout_2, R.drawable.button_all_points);
                setDrawable(layout_3, R.drawable.button_all_points);
                setDrawable(layout_4, R.drawable.button_all_points);
                setDrawable(layout_5, R.drawable.button_all_points);
                setDrawable(layout_6, R.drawable.button_all_points);
                setDrawable(layout_7, R.drawable.button_all_points);
                setDrawable(layout_8, R.drawable.button_all_points);
                setDrawable(layout_9, R.drawable.button_all_points);
                val = true;
            }
            if (!val) {

                layout_1.setVisibility(GONE);
                layout_2.setVisibility(GONE);
                layout_3.setVisibility(GONE);
                layout_4.setVisibility(GONE);
                layout_5.setVisibility(GONE);
                layout_6.setVisibility(GONE);
                layout_7.setVisibility(GONE);
                layout_8.setVisibility(GONE);
                layout_9.setVisibility(GONE);

                txtValue.setText(value.value);
                txtValue.setVisibility(VISIBLE);
            }


        } else {
            txtText.setVisibility(GONE);
            txtValue.setVisibility(GONE);
        }
    }

    void setDrawable(View layout, int resource) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            layout.setBackgroundDrawable( getResources().getDrawable(resource) );
        } else {
            layout.setBackground( getResources().getDrawable(resource));
        }
    }


    public ObjProperties getValue() {
        return value;
    }

    public void setValue(ObjProperties v) {
        value = v;
        updateView();
    }

    public void clear() {
        value = null;
        updateView();
    }

}