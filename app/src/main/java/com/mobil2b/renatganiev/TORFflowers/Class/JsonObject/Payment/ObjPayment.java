package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Payment;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjPayment {

    public int      id;
    public String   name;
    public String   code;
    public String   type;
    public int      sort;
    public boolean  check;

    public ObjPayment() {
        init();
    }
    public ObjPayment(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjPayment(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("delivery")) {
                JSONObject jsonMain = json.getJSONObject("delivery");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("code")) {
                code = json.getString("code");
            }
            if (json.has("sort")) {
                sort = json.getInt("sort");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("type")) {
                type = json.getString("type");
            }
        } catch (JSONException e) {
        }
    }

}
