package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.News;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjNewsItems  {

    public int     id, city_id;
    public String name, text, add_text, date, dateTo, dateFrom, url;
    public boolean promo;
    public ObjImage image, image_big;


    public ObjNewsItems() {
        init();
    }
    public ObjNewsItems(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjNewsItems(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("settings")) {
                JSONObject jsonMain = json.getJSONObject("settings");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("city_id")) {
                city_id = json.getInt("city_id");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("text")) {
                text = json.getString("text");
            }
            if (json.has("add_text")) {
                add_text = json.getString("add_text");
            }
            if (json.has("date")) {
                date = json.getString("date");
            }
            if (json.has("dateTo")) {
                dateTo = json.getString("dateTo");
            }
            if (json.has("dateFrom")) {
                dateFrom = json.getString("dateFrom");
            }
            if (json.has("promo")) {
                promo = json.getBoolean("promo");
            }
            if (json.has("image")) {
                image = new ObjImage(json.getJSONObject("image"));
            }
            if (json.has("image_big")) {
                image_big = new ObjImage(json.getJSONObject("image_big"));
            }
            if (json.has("url")) {
                url = json.getString("url");
            }
        } catch (JSONException e) {
        }
    }

}
