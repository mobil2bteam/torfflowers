package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Delivery.ObjDelivery;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerDelivery {
    void onClick(View view, ObjDelivery objDelivery);
}
