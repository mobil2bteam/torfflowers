package com.mobil2b.renatganiev.torfflowers.ControlView.SocialLinkView;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjSocialLink;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjSocialLinkItem;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 10.10.15.
 */
public class SocialLinkView extends RelativeLayout {

    Context             mContext;

    LinearLayout layout_body;
    HorizontalScrollView horizontalScroll;
    ObjSocialLink objSocialLink;
    ClickListenerSocialLink clickListenerSocialLink;
    DisplayMetrics metrics;
    int count_item;

    public SocialLinkView(Activity context, ObjSocialLink obj, int count_line, ClickListenerSocialLink listener) {
        super(context);

        mContext                    = context;
        objSocialLink               = obj;
        metrics                     = Helps.getDisplayWH(context);
        clickListenerSocialLink     = listener;
        count_item                  = count_line;

        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_social_link_menu, this);

        layout_body = (LinearLayout) findViewById(R.id.layout_vertical);

        //param.setMargins(Helps.dpToPx(5), Helps.dpToPx(5), Helps.dpToPx(5), Helps.dpToPx(5));
        LinearLayout linearLayout = null;

        LinearLayout.LayoutParams param         = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        LinearLayout.LayoutParams layout_param  = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        int i = 0;
        int count = 0;
        for (ObjSocialLinkItem objMenuItem : objSocialLink.socialLinkItems) {

            count++;

            if (Helps.isNotEmpty(objMenuItem.url)) {
                SocialItemView menuItemView = new SocialItemView(mContext, objMenuItem, clickListenerSocialLink);

                i++;

                if (i == 1) {
                    linearLayout = new LinearLayout(mContext);
                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                }
                //menuItemView.setWeightSum(1f);
                linearLayout.addView(menuItemView, param);

                if ((i%count_item == 0)) {
                    if (linearLayout != null) {
                        layout_body.addView(linearLayout, layout_param);
                    }
                    i = 0;
                } else {
                    if (count == objSocialLink.socialLinkItems.size())  {
                        if (linearLayout != null) {
                            layout_body.addView(linearLayout, layout_param);
                            i = 0;
                        }
                    }
                }
            }
            if (count == objSocialLink.socialLinkItems.size()) {
                if (i > 0) {
                    if (linearLayout != null) {
                        layout_body.addView(linearLayout, layout_param);
                        i = 0;
                    }
                }
            }

        }

        layout_body.setVisibility(VISIBLE);

    }

    public void setOnClickListener(ClickListenerSocialLink listener) {
        clickListenerSocialLink = listener;
    }

}