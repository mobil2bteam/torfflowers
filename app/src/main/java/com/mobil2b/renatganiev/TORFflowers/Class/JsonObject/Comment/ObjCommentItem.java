package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Comment;


import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;

import org.json.JSONException;
import org.json.JSONObject;

public class ObjCommentItem {
    public int id;
    public String text;
    public String rating;
    public String date;
    public DataSession user;

    public ObjCommentItem() {}
    public ObjCommentItem(JSONObject json) {
        loadJsonObject(json);
    }
    public ObjCommentItem(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            loadJsonObject(json);
        } catch (JSONException e) {
        }
    }

    public void loadJsonObject(JSONObject json){
        try {
            if(json.has("id")){
                this.id = json.getInt("id");
            }
            if(json.has("rating")){
                this.rating = json.getString("rating");
            }
            if(json.has("text")){
                this.text = json.getString("text");
            }
            if(json.has("date")){
                this.date = json.getString("date");
            }
            if(json.has("user")){
                user = new DataSession(json.getJSONObject("user"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
