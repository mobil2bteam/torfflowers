package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjFilter;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBSearchHistory;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;

public class search extends AppCompatActivity {


    EditText editSearchText;
    Toolbar toolbar;
    ListView listView;
    Button btnSearchHistoryClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        listView = (ListView) findViewById(R.id.listView);
        btnSearchHistoryClear = (Button) findViewById(R.id.btnSearchHistoryClear);

        btnSearchHistoryClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBSearchHistory dbSearchHistory = new DBSearchHistory(search.this);
                dbSearchHistory.clear();
                dbSearchHistory.close();
                refreshData();
            }
        });


        initToolbar();
        refreshData();
    }


    void refreshData() {
        DBSearchHistory dbSearchHistory = new DBSearchHistory(search.this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                R.layout.item_search_history,
                dbSearchHistory.getList() );

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openActivityToSearch((String)adapterView.getItemAtPosition(i));
            }
        });

        listView.setAdapter(arrayAdapter);
        dbSearchHistory.close();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_close) {
            //updateUserData();
            if (editSearchText != null) {
                if (editSearchText.getText().toString().equals("")) {
                    finish();
                } else {
                    editSearchText.setText("");
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_toolbar_icon_back_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_black_24dp));
        }

        editSearchText = toolbar.findViewById(R.id.editSearchText);
        editSearchText.requestFocus();
        //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.showSoftInput(editSearchText, InputMethodManager.SHOW_IMPLICIT);

        editSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                //Toast.makeText(search.this, textView.getText(), Toast.LENGTH_SHORT).show();
                if (textView.getText().toString().trim().length() != 0) {

                    DBSearchHistory dbSearchHistory = new DBSearchHistory(search.this);
                    if (!dbSearchHistory.exist(textView.getText().toString().trim())) {
                        dbSearchHistory.add(textView.getText().toString().trim());
                    } else {
                        dbSearchHistory.delete(textView.getText().toString().trim());
                        dbSearchHistory.add(textView.getText().toString().trim());
                    }
                    dbSearchHistory.close();
                    openActivityToSearch(textView.getText().toString().trim());
                    refreshData();
                }

                return false;
            }
        });


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void openActivityToSearch(String text) {
        ObjFilter objFilter = new ObjFilter();
        objFilter.search = text;
        Bundle bundle = new Bundle();
        bundle.putSerializable("filter", objFilter);
        Intent intent = new Intent(search.this, main.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
