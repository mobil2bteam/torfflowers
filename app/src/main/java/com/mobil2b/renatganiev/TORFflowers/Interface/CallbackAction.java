package com.mobil2b.renatganiev.torfflowers.Interface;


import android.os.Bundle;

import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;


/**
 * Created by renatganiev on 20.07.14.
 */
public interface CallbackAction {
    // Вернули структуру для авторизации на сервере
    public  void onAction(String tag, int type, ObjLink data);
    public  void onAction(String tag, int type, Bundle data);
}
