package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings;

import android.content.Context;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyInfo;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.MenuInfo.ObjMenuInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjSettings {
    public boolean              select_start_city;
    public boolean              change_city;
    public int                  default_city;
    public int                  default_catalog;
    public boolean              flag_closed_type_app;     // ПРИЛОЖЕНИЕ ЗАКРЫТОГО ТИПА
    public boolean              flag_payment_order_after; // ОФОРМЛЕНИЕ ЗАКАЗ ПОСЛЕ ПОДТВЕРЖДЕНИЕ ОПЕРАТОРОМ

    public ObjSettingsFilter    filters;
    public ObjCompanyInfo company_info;
    public ArrayList<ObjMenuInfo> menu_info;
    public ObjMenuInfo partner_info;

    public Context mContext;

    public ObjSettings() {
        init();
    }
    public ObjSettings(JSONObject json, Context context) {
        mContext = context;
        init();
        loadJSONObject(json);
    }
    public ObjSettings(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("settings")) {
                JSONObject jsonMain = json.getJSONObject("settings");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        menu_info = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("flag_closed_type_app")) {
                flag_closed_type_app = json.getBoolean("flag_closed_type_app");
            }
            if (json.has("flag_payment_order_after")) {
                flag_payment_order_after = json.getBoolean("flag_payment_order_after");
            }
            if (json.has("select_start_city")) {
                select_start_city = json.getBoolean("select_start_city");
            }
            if (json.has("select_start_city")) {
                select_start_city = json.getBoolean("select_start_city");
            }
            if (json.has("change_city")) {
                change_city = json.getBoolean("change_city");
            }
            if (json.has("default_city")) {
                default_city = json.getInt("default_city");
            }
            if (json.has("default_catalog")) {
                default_catalog = json.getInt("default_catalog");
            }
            if (json.has("filters")) {
                JSONObject jsonFilters = json.getJSONObject("filters");
                filters = new ObjSettingsFilter(jsonFilters);
            }
            if (json.has("company_info")) {
                JSONObject company_infoJson = json.getJSONObject("company_info");
                company_info = new ObjCompanyInfo(company_infoJson, mContext);
            }
            if(json.has("menu_info")){
                for (int i = 0; i < json.getJSONArray("menu_info").length(); i++){
                    ObjMenuInfo menuinfo = new ObjMenuInfo(json.getJSONArray("menu_info").getJSONObject(i));
                    menu_info.add(menuinfo);
                }
            }
            if (json.has("partner_info")) {
                partner_info = new ObjMenuInfo(json.getJSONObject("partner_info"));
            }
        } catch (JSONException e) {
        }
    }

}
