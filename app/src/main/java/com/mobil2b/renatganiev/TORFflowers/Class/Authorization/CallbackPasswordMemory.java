package com.mobil2b.renatganiev.torfflowers.Class.Authorization;


import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjMessage;

/**
 * Created by renatganiev on 20.07.14.
 */
public interface CallbackPasswordMemory {
    // Старт регистрации, авторизации и т.д.
    public  void onStart();

    // Пользователь авторизовался
    public  void onSuccess(ObjMessage msg);

    // Произошла ошибка
    public  void onError(String message, int error_code);
}
