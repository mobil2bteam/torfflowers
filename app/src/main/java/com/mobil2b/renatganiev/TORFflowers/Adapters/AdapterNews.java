package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.News.ObjNewsItems;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerNews;
import com.mobil2b.renatganiev.torfflowers.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterNews extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private List<ObjNewsItems> mList;

    public AdapterNews(Context context, List<ObjNewsItems> list) {
        this.mContext = context;
        this.mList = list;
    }

    ClickListenerNews onClickListener;
    public void setOnClickListener(ClickListenerNews onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view3 = inflater.inflate(R.layout.item_news, parent, false);
        return new ViewHolderOrders(view3);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderOrders viewHolder0 = (ViewHolderOrders) holder;

        viewHolder0.txtName.setText(getItem(position).name);
        viewHolder0.txtDate.setText(getItem(position).date);
        // ФОТОГРАФИЯ
        if (getItem(position).image != null) {
            if (getItem(position).image.url != null) {
                if (!getItem(position).image.url.equals("")) {
                        DisplayMetrics metrics = Helps.getDisplayWH((Activity)mContext);
                        if (metrics != null) {
                            if (metrics.widthPixels != 0) {
                                float c = (float) getItem(position).image.heigth / (float) getItem(position).image.width;
                                Picasso.with(mContext)
                                        .load(getItem(position).image.url)
                                        .resize(metrics.widthPixels, (int)(((float)metrics.widthPixels) * c))
                                        .into(viewHolder0.imgImage);
                            } else {
                                Picasso.with(mContext).load(getItem(position).image.url).into(viewHolder0.imgImage);
                            }
                        } else {
                            Picasso.with(mContext).load(getItem(position).image.url).into(viewHolder0.imgImage);
                        }
                }
            }
        }

        if (getItem(position).promo) {
            viewHolder0.txtPromo.setVisibility(View.VISIBLE);
        } else {
            viewHolder0.txtPromo.setVisibility(View.GONE);
        }

        viewHolder0.setClickListener(onClickListener);
    }



    private ObjNewsItems getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderOrders extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtName, txtDate, txtPromo;
        ImageView imgImage;
        ClickListenerNews clickListener;

        public ViewHolderOrders(View itemView) {
            super(itemView);

            txtName             = (TextView)itemView.findViewById(R.id.txtName);
            txtPromo            = (TextView)itemView.findViewById(R.id.txtPromo);
            txtDate             = (TextView)itemView.findViewById(R.id.txtDate);
            imgImage            = (ImageView)itemView.findViewById(R.id.imgImage);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null){
                clickListener.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerNews clickListener){
            this.clickListener = clickListener;
        }

    }

}