package com.mobil2b.renatganiev.torfflowers.Firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 11.10.17.
 */
public class MyNotificationHelper extends ContextWrapper {
    private NotificationManager notifManager;
    public static final String CHANNEL_ONE_ID = "ru.duckman.renatganiev.beautygroup.TWO";
    public static final String CHANNEL_ONE_NAME = "Channel TWO";

//Create your notification channels//

    Uri soundUri;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public MyNotificationHelper(Context base) {
        super(base);
        //soundUri = Uri.parse("android.resource://" + base.getPackageName() + "/" + R.raw.cow);
        createChannels();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void createChannels() {

        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ONE_ID, CHANNEL_ONE_NAME, notifManager.IMPORTANCE_HIGH);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.setShowBadge(true);
        notificationChannel.setSound(soundUri, notificationChannel.getAudioAttributes());
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(notificationChannel);

    }

//Create the notification that’ll be posted to Channel One//

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getNotification1(PendingIntent pendingIntent, String title, String body) {

        return new Notification.Builder(getApplicationContext(), CHANNEL_ONE_ID)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.logo_loading)
                .setContentIntent(pendingIntent)
                .setSound(soundUri)
                .setAutoCancel(true);
    }



    public void notify(int id, Notification.Builder notification) {
        getManager().notify(id, notification.build());
    }

//Send your notifications to the NotificationManager system service//

    private NotificationManager getManager() {
        if (notifManager == null) {
            notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notifManager;
    }
}
