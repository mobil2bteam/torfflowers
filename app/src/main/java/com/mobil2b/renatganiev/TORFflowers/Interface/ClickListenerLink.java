package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerLink {
    void onClick(View view, int position, ObjLink link, boolean isLongClick);
}
