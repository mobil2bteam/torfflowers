package com.mobil2b.renatganiev.torfflowers.Class.MarkerManager;


import com.google.android.gms.maps.model.Marker;


/**
 * Created by renatganiev on 20.07.14.
 */
public interface CallbackListenerMarker {
    // Вернули структуру для авторизации на сервере
    public  void onResult(Marker marker);
    public  void onError();
}
