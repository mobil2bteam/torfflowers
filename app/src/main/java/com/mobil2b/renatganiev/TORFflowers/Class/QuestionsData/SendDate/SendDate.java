package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;


/**
 * Created by renatganiev on 20.07.14.
 * Класс для отправки POST и GET запросов
 */
public class SendDate {

    public int SEND_METOD_GET       = 1;    // МЕТОД ОТПРАВКИ - GET
    public int SEND_METOD_POST      = 2;    // МЕТОД ОТПРАВКИ - POST

    public  volatile SendDate sInstance;
    public CallBackSendDate callBackSend;
    public String processing;

    public ObjLink lastLink;

    private OkHttpClient            client;

    public SendDate() {
    }

    public SendDate instance() {
        return sInstance;
    }

    // ИНИЦИАЛИЗАЦИЯ БИБЛИОТЕКИ
    public void initialize(CallBackSendDate callBack) {
        if (sInstance == null) {
            synchronized (SendDate.class) {
                if (sInstance == null) {
                    sInstance = new SendDate();
                }
            }
        }
        callBackSend = callBack;
    }

    // ОТПРАВКА ЗАПРОСА НА СЕРВЕР
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void send(String process, CallBackSendDate callback, String url, int metod, ObjLink linkURL) {
        processing      = process;
        callBackSend    = callback;
        lastLink        = linkURL;

        if (metod == this.SEND_METOD_GET) {
            if (callBackSend != null) callBackSend.onSend(process, linkURL);
            sendGet(url, linkURL);
        }
        if (metod == this.SEND_METOD_POST) {
            if (callBackSend != null) callBackSend.onSend(process, linkURL);
            sendPost(url, linkURL);
        }
    }

    // ОТПРАВКА МЕТОДОМ GET
    private void sendGet(String url, final ObjLink linkURL) {

        client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();

        if (linkURL != null) {
            for (ObjParam objParam : linkURL.param) {
                urlBuilder.addQueryParameter(objParam.name, objParam.value);
            }
        }

        String newUrl = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(newUrl)
                .build();

        Call call = client.newCall(request);
        call.enqueue(mCallback);
    }


    // ОТПРАВКА МЕТОДОМ POST
    private void sendPost(String url, final ObjLink linkURL) {

        client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        FormBody.Builder formBuilder = new FormBody.Builder();

        if (linkURL != null) {
            for (ObjParam objParam : linkURL.param) {
                formBuilder.add(objParam.name, objParam.value);
            }
        }
        RequestBody formBody = formBuilder.build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(mCallback);

    }


    // ВОЗВРАТ РЕЗУЛЬТАТОВ
    Callback mCallback = new Callback() {
        @Override
        public void onFailure(Call call, IOException e) {
            // ЧТО ТО ПОШЛО НЕ ТАК
            Log.d("URL_RESULT_FAILURE", e.getMessage());
            Log.d("URL_RESULT_FAILURE", e.getStackTrace().toString());
            Log.d("URL_RESULT_FAILURE", e.getLocalizedMessage());
            callBackSend.onError(e.getMessage(), 0, lastLink);
        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            if (response.isSuccessful()) {
                String responseStr = response.body().string();

                Log.d("URL_RESULT", responseStr);

                callBackSend.onReturnAnswer(responseStr, false, 0, lastLink);
            } else {

                Log.d("URL_RESULT_ERROR", response.body().string());

                // СЕРВЕР ВЕРНУЛ ЯВНУЮ ОШИБКУ
                callBackSend.onHttpError(processing, response.code(), lastLink);
            }
        }
    };


    public void cancelAllCall() {
        if  (client != null) {
            if  (client.dispatcher() != null) {
                if  (client.dispatcher().queuedCalls() != null) {
                    for(Call call : client.dispatcher().queuedCalls()) {
                        call.cancel();
                    }
                }
                if  (client.dispatcher().runningCalls() != null) {
                    for(Call call : client.dispatcher().runningCalls()) {
                        call.cancel();
                    }
                }
            }
        }
    }

}
