package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.GroupView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ObjGroupView {

    public int      id;
    public int      count_item;
    public float    count_items_display;
    public String   view;
    public String   name;
    public String   text_more;
    public String   description;
    public boolean  button_more;

    public ObjLink  link;

    public String VIEW_HORIZONTAL   = "HORIZONTAL";
    public String VIEW_VAERTICAL    = "VERTICAL";

    public ArrayList<ObjProductMin> items = new ArrayList<>();

    public ObjGroupView() {

    }
    public ObjGroupView(JSONObject json) {
        loadJsonObject(json);
    }
    public ObjGroupView(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            loadJsonObject(json);
        } catch (JSONException e) {
        }
    }

    public void loadJsonObject(JSONObject json){
        try {
            if(json.has("view")){
                this.view = json.getString("view");
            } else {
                this.view = VIEW_HORIZONTAL;
            }
            if(json.has("name")){
                this.name = json.getString("name");
            }
            if(json.has("text_more")){
                this.text_more = json.getString("text_more");
            }
            if(json.has("description")){
                this.description = json.getString("description");
            }
            if(json.has("button_more")){
                this.button_more = json.getBoolean("button_more");
            }
            if(json.has("count_item")){
                this.count_item = json.getInt("count_item");
            }
            if(json.has("count_items_display")){
                this.count_items_display = (float)json.getDouble("count_items_display");
            }

            if(json.has("link")){
                link = new ObjLink(json.getJSONObject("link"));
            }

            if(json.has("items")){
                for (int i =0;i<json.getJSONArray("items").length();i++){
                    ObjProductMin menuItem = new ObjProductMin(json.getJSONArray("items").getJSONObject(i));
                    items.add(menuItem);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
