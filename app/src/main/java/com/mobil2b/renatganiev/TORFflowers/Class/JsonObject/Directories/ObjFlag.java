package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ObjFlag extends ObjBaseDirectories implements Serializable {

    //public int id;
    //public String name;
    public String color;
    public String text_color;

    public ObjFlag() {

    }
    public ObjFlag(JSONObject json) {
        loadJsonObject(json);
    }
    public ObjFlag(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            loadJsonObject(json);
        } catch (JSONException e) {
        }
    }


    public void loadJsonObject(JSONObject json){
        try {
            if(json.has("id")) {
                super.id = json.getInt("id");
            }
            if(json.has("name")) {
                super.name = json.getString("name");
            }
            if(json.has("color")) {
                this.color = json.getString("color");
            }
            if(json.has("text_color")) {
                this.text_color = json.getString("text_color");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
