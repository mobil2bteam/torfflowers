package com.mobil2b.renatganiev.torfflowers;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjRecipient.ObjRecipient;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterRecipientList;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBRecipientHistory;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerRecipient;

import java.util.ArrayList;

public class delivery_recipient_select extends AppCompatActivity {

    AppBarLayout mainAppbar;

    boolean open = true;

    TextInputEditText txtName, txtPhone;
    RecyclerView mRecyclerView;
    LinearLayoutManager manager;
    AdapterRecipientList mAdapterSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_recipient_select);

        txtName     = (TextInputEditText) findViewById(R.id.txtName);
        txtPhone    = (TextInputEditText) findViewById(R.id.txtPhone);

        mainAppbar  = (AppBarLayout)      findViewById(R.id.app_bar);
        mainAppbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    // Fully expanded
                    open = true;
                } else {
                    // Not fully expanded or collapsed
                    open = false;
                }
            }
        });

        initToolbar();
        intiRecyclerView();
        refrashAllDate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_add) {
            //updateUserData();
            if (!open) {
                mainAppbar.setExpanded(true, true);
            } else {
                addNewRecipient();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        AppBarLayout app_bar = (AppBarLayout) findViewById(R.id.app_bar);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(0));

            app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    //some other code here
                    ViewCompat.setElevation(appBarLayout, Helps.dpToPx(2));
                }
            });

        }

        toolbar.setTitle("Получатель");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(delivery_recipient_select.this, R.drawable.ic_toolbar_icon_back_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(true);
        manager = new LinearLayoutManager(delivery_recipient_select.this);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    private void refrashAllDate() {



            DBRecipientHistory addressHistory = new DBRecipientHistory(delivery_recipient_select.this);
            ArrayList<ObjRecipient> list = addressHistory.getList();

            if (list != null) {
                mAdapterSearch = new AdapterRecipientList(delivery_recipient_select.this, list);
                mRecyclerView.setAdapter(mAdapterSearch);
                mAdapterSearch.notifyDataSetChanged();

                mAdapterSearch.setOnClickListenerDelivery(new ClickListenerRecipient() {
                    @Override
                    public void onClick(View view, ObjRecipient objDelivery) {
                        //Toast.makeText(delivery_address_select.this, objDelivery.getAddress(), Toast.LENGTH_SHORT).show();
                        selectAddress(objDelivery);
                    }

                    @Override
                    public void onRemove(View view, final ObjRecipient objDelivery) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(delivery_recipient_select.this);
                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case DialogInterface.BUTTON_POSITIVE:
                                        //Yes button clicked
                                        DBRecipientHistory addressHistory = new DBRecipientHistory(delivery_recipient_select.this);
                                        addressHistory.delete(objDelivery);
                                        addressHistory.close();
                                        refrashAllDate();
                                        break;

                                    case DialogInterface.BUTTON_NEGATIVE:
                                        //No button clicked
                                        break;
                                }
                            }
                        };
                        builder.setMessage("Удалить получателя?").setPositiveButton("Да", dialogClickListener)
                                .setNegativeButton("Нет", dialogClickListener).show();

                    }
                });

                mRecyclerView.setVisibility(View.VISIBLE);
            }
    }


    void addNewRecipient() {
        if (txtName.getText().toString().trim().length() == 0) {
            Helps.errorTextEdit(txtName, delivery_recipient_select.this);
            return;
        }
        if (txtPhone.getText().toString().trim().length() == 0) {
            Helps.errorTextEdit(txtPhone, delivery_recipient_select.this);
            return;
        }

        ObjRecipient objDeliveryAddress = new ObjRecipient();
        objDeliveryAddress.name  = txtName.getText().toString().trim();
        objDeliveryAddress.phone = txtPhone.getText().toString().trim();

        DBRecipientHistory addressHistory = new DBRecipientHistory(delivery_recipient_select.this);
        long id = 0;
        if (addressHistory.exist(objDeliveryAddress)) {
            objDeliveryAddress = addressHistory.getRecipientToObj(objDeliveryAddress);
            id = objDeliveryAddress.id;
        } else {
            id = addressHistory.add(objDeliveryAddress);
            objDeliveryAddress.id = (int)id;
        }
        addressHistory.close();

        if (id != 0) {
            refrashAllDate();
            selectAddress(objDeliveryAddress);
        }

    }

    void selectAddress(ObjRecipient objDeliveryAddress) {
        Intent intent = new Intent();
        intent.putExtra("recipient_id", objDeliveryAddress.id);
        setResult(RESULT_OK, intent);
        finish();
    }
}
