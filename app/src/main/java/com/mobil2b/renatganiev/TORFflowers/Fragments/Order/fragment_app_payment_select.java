package com.mobil2b.renatganiev.torfflowers.Fragments.Order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterPaymentList;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.NewOrder.ObjNewOrder;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order.ObjOrder;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Payment.ObjPayment;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBBag;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.ControlView.ItemImageView.ItemImageView;
import com.mobil2b.renatganiev.torfflowers.Fragments.Bag.fragment_app_bag;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerPayment;
import com.mobil2b.renatganiev.torfflowers.order_new_finish;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.R;

import java.text.SimpleDateFormat;

/**
 * Created by renatganiev on 20.01.18.
 */

public class fragment_app_payment_select extends Fragment {

    public static final String TAG = "fragment_app_payment_select";
    public static final int LAYOUT = R.layout.fragment_app_payment_select;
    View rootView;
    public CallbackAction callbackActionSuper;

    LinearLayout layout_image_list;
    Button btnOk;
    TextView txtTotalNotSale, txtSumSale, txtTotal, txtTotalDelvery;

    RecyclerView mRecyclerView;
    LinearLayoutManager manager;
    AdapterPaymentList mAdapterSearch;

    ModuleObjects moduleObjects;

    ObjNewOrder objNewOrder;
    ObjOrder objOrder;

    public static fragment_app_bag getInstance(Bundle args) {
        fragment_app_bag fragment = new fragment_app_bag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        layout_image_list           = (LinearLayout)            rootView.findViewById(R.id.layout_image_list);
        btnOk                       = (Button)                  rootView.findViewById(R.id.btnOk);
        txtTotalNotSale             = (TextView)                rootView.findViewById(R.id.txtTotalNotSale);
        txtTotalDelvery             = (TextView)                rootView.findViewById(R.id.txtTotalDelvery);
        txtSumSale                  = (TextView)                rootView.findViewById(R.id.txtSumSale);
        txtTotal                    = (TextView)                rootView.findViewById(R.id.txtTotal);


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (objNewOrder != null) {
                    objNewOrder.pre = false;
                    moduleObjects.newOrder(true, objNewOrder, User.getCurrentSession());
                }
            }
        });


        DeliveryCityView deliveryCityView = new DeliveryCityView(getContext(), PublicData.SETTINGS);

        objNewOrder = new ObjNewOrder();
        objNewOrder.address = PublicData.objDeliveryAddress;
        objNewOrder.city_id = deliveryCityView.getCity();
        objNewOrder.comment = PublicData.objComment;
        objNewOrder.contact = PublicData.objRecipient;
        objNewOrder.deliveryId = PublicData.objDelivery;
        if (PublicData.objDelivery.type.equals(PublicData.DELIVERY_TYPE_PICKUP)) {
            if (PublicData.objCompanyPoint != null) {
                objNewOrder.addressPoint = PublicData.objCompanyPoint;
            }
        }
        if (PublicData.objDelivery != null) {
            if (PublicData.objDelivery.payment_list != null) {
                if (PublicData.objDelivery.payment_list.size() > 0) {
                    objNewOrder.paymentId = PublicData.objDelivery.payment_list.get(0);
                }
            }
        }
        if (PublicData.objDeliveryDate != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
            String timestamp = simpleDateFormat.format(PublicData.objDeliveryDate);
            objNewOrder.date = timestamp;
        }
        objNewOrder.promocode = PublicData.objCart.promoCode;
        DBBag bag = new DBBag(getContext());
        objNewOrder.cart = bag.getOfferStringCount();
        objNewOrder.cards = PublicData.objCart.card;
        objNewOrder.pre = true;

        intiRecyclerView();
        refrashAllDate();
        refrashView();

        moduleObjects = new ModuleObjects(getActivity(), callBackSendDate);
        moduleObjects.newOrder(true, objNewOrder, User.getCurrentSession());

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(true);
        manager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void refrashAllDate() {

        if (PublicData.objDelivery != null) {
            if (PublicData.objDelivery.payment_list != null) {
                if (PublicData.objDelivery.payment_list.size() > 0) {
                    PublicData.objDelivery.payment_list.get(0).check = true;
                    mAdapterSearch = new AdapterPaymentList(getActivity(), PublicData.objDelivery.payment_list);
                    mRecyclerView.setAdapter(mAdapterSearch);
                    mAdapterSearch.notifyDataSetChanged();
                    /*
                    * КЛИК НА КАРТОЧКУ
                    * **/
                    mAdapterSearch.setOnClickListenerPayment(new ClickListenerPayment() {
                        @Override
                        public void onClick(View view, ObjPayment objPayment) {
                            //Toast.makeText(getContext(), objPayment.name, Toast.LENGTH_SHORT).show();
                            if (objNewOrder != null) {
                                objNewOrder.paymentId = objPayment;
                            }
                        }
                    });
                }
            }
        }
    }

    private void refrashView() {
        if (objOrder != null) {

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(Helps.dpToPx(50), Helps.dpToPx(50));
            param.setMargins(Helps.dpToPx(3), Helps.dpToPx(3), Helps.dpToPx(3), Helps.dpToPx(3));

            if (objOrder != null) {
                if (objOrder.products != null) {
                    layout_image_list.removeAllViews();
                    for (ObjProductMin objProductMin : objOrder.products) {
                        ItemImageView imageView = new ItemImageView(getContext(), objProductMin);
                        imageView.setLayoutParams(param);
                        layout_image_list.addView(imageView);
                    }
                }
                txtTotalNotSale.setText(String.valueOf(objOrder.totalNotSale).concat(" р."));
                txtTotalDelvery.setText(String.valueOf(objOrder.sumDelivery).concat(" р."));
                txtSumSale.setText(String.valueOf(objOrder.totalSumSale).concat(" р."));
                txtTotal.setText(String.valueOf(objOrder.total).concat(" р."));
            }



        } else {
            if (PublicData.objCart != null) {

                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(Helps.dpToPx(50), Helps.dpToPx(50));
                param.setMargins(Helps.dpToPx(3), Helps.dpToPx(3), Helps.dpToPx(3), Helps.dpToPx(3));

                if (PublicData.objCart != null) {
                    if (PublicData.objCart.products != null) {
                        layout_image_list.removeAllViews();
                        for (ObjProductMin objProductMin : PublicData.objCart.products) {
                            ItemImageView imageView = new ItemImageView(getContext(), objProductMin);
                            imageView.setLayoutParams(param);
                            layout_image_list.addView(imageView);
                        }
                    }
                }

                txtTotalNotSale.setText(String.valueOf(PublicData.objCart.totalNotSale).concat(" р."));
                txtSumSale.setText(String.valueOf(PublicData.objCart.totalSumSale).concat(" р."));
                txtTotal.setText(String.valueOf(PublicData.objCart.total).concat(" р."));
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(getActivity() == null) return;
                    PublicData.showDialog(getActivity());
                }
            });
        }

        @Override
        public void onReturnAnswer(final String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    if(getActivity() == null) return;
                    String rezData = OpenBlocks.getOpenData(result);
                    if (rezData.equals(PublicData.QUESTION_TYPE_ORDER)) {
                        objOrder = new ObjOrder(result);
                        if (objNewOrder.pre) {
                            refrashView();
                        } else {
                            if (getActivity() != null) {
                                DBBag bag = new DBBag(getContext());
                                bag.clear();
                                bag.close();

                                if (objOrder.isPayment) {
                                    Intent intent = new Intent(getActivity(), order_new_finish.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                    return;
                                }

                                if (objOrder.payment.paymentType.equals(PublicData.PAYMENT_TYPE_NONE_CASH)) {
                                    // ЕСЛИ БЕЗНАЛИЧНЫЙ ВИД ОПЛАТЫ


                                } else {
                                    Intent intent = new Intent(getActivity(), order_new_finish.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }

                            }
                        }
                    }
                }
            });
        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    if(getActivity() == null) return;
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    if(getActivity() == null) return;
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };


    public void showResult(String text){
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }
}
