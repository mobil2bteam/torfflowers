package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Delivery;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Payment.ObjPayment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjDelivery {

    public int id;
    public int price;
    public String price_str;
    public int min_price;
    public String add_text;
    public String name;
    public String type;
    public ArrayList<ObjPayment> payment_list;

    public ObjDelivery() {
        init();
    }
    public ObjDelivery(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjDelivery(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("delivery")) {
                JSONObject jsonMain = json.getJSONObject("delivery");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        payment_list = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("price")) {
                price = json.getInt("price");
            }
            if (json.has("min_price")) {
                min_price = json.getInt("min_price");
            }
            if (json.has("add_text")) {
                add_text = json.getString("add_text");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("type")) {
                type = json.getString("type");
            }
            if (json.has("payment_list")) {
                JSONObject jsonObject = json.getJSONObject("payment_list");
                if (jsonObject.has("items")) {
                    for (int i = 0; i < jsonObject.getJSONArray("items").length(); i++) {
                        ObjPayment it = new ObjPayment(jsonObject.getJSONArray("items").getJSONObject(i));
                        payment_list.add(it);
                    }
                }
            }
        } catch (JSONException e) {
        }
    }

}
