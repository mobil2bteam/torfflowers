package com.mobil2b.renatganiev.torfflowers.Class.PublicData;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Window;

/*import com.mobil2b.renatganiev.shop.Class.JsonObjects.ObjCatalog;
import com.mobil2b.renatganiev.shop.Class.JsonObjects.ObjHome;
import com.mobil2b.renatganiev.shop.Class.JsonObjects.ObjNews;
import com.mobil2b.renatganiev.shop.Class.JsonObjects.ObjSettings;*/
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Article.ObjGroupArticles;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.DeliveryAddress.ObjDeliveryAddress;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.News.ObjGroupNews;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjRecipient.ObjRecipient;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjURL;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Cart.ObjCart;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Delivery.ObjDelivery;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjDirectories;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Home.ObjHome;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.ObjSettings;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.Cache;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjOptions;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.Date;


/**
 * Created by renatganiev on 11.10.15.
 */
public class PublicData {

    /**
     * Настройки для кэширования данных, можно отключить во всем прилоежнии любой кэш
     * */
    public static boolean CACHE_QUESTION    = true;  // Включен кэш запросов
    public static String DELIVERY_TYPE_PICKUP    = "pickup";  // САМОВЫВОЗ
    public static String PAYMENT_TYPE_NONE_CASH = "none-cash"; // ОПЛАТА ПО БЕЗНАЛУ


    /**
     * Типы запросов в приложении
     * */
    public static String QUESTION_TYPE_URL                          = "url";                        // Получили список URL

    public static String QUESTION_TYPE_USER_EXIST                   = "url_user_exists";            // Пр
    public static String QUESTION_TYPE_USER_REGISTRATION            = "url_user_registration";      // Регистрация пользователя
    public static String QUESTION_TYPE_USER_LOGIN                   = "url_user_login";             // Авторизация пользователя
    public static String QUESTION_TYPE_USER_UPDATE                  = "url_user_update";            // Изменение данных пользователя
    public static String QUESTION_TYPE_USER_PASSWORD_CHANGE         = "url_user_password_change";   // Изменить пароль
    public static String QUESTION_TYPE_USER_PASSWORD_MEMORY         = "url_user_password_memory";   // Выслать пароль
    public static String QUESTION_TYPE_USER_ORDER_LIST              = "url_user_order_list";        // Список всех заказов пользователя
    public static String QUESTION_TYPE_USER_ORDER_ITEM              = "url_user_order_item";        // Получить заказ пользователя

    public static String QUESTION_TYPE_HOME                         = "url_home";                   // Получили главную
    public static String QUESTION_TYPE_CATALOG                      = "url_catalog";                // Уровень каталога

    public static String QUESTION_TYPE_NEWS_LIST                    = "url_news_list";              // Список новостей
    public static String QUESTION_TYPE_NEWS_ITEM                    = "url_news_item";              // Получить конкретную новость
    public static String QUESTION_TYPE_NEWS                         = "url_news";                   // Получить конкретную новость

    public static String QUESTION_TYPE_ARTICLE_LIST                 = "url_articles_list";          // Список статей
    public static String QUESTION_TYPE_ARTICLE_ITEM                 = "url_articles_item";          // Получить конкретную статью

    public static String QUESTION_TYPE_METRO_LIST                   = "url_metro_list";             // Список метро

    public static String QUESTION_TYPE_PRODUCTS                     = "url_products";               // Список товаров
    public static String QUESTION_TYPE_PRODUCT                      = "url_product";                // Товар
    public static String QUESTION_TYPE_PRODUCT_COMMENTS             = "url_product_comments";       // Комментарии к товару
    public static String QUESTION_TYPE_PRODUCT_COMMENTS_ADD         = "url_product_comments_add";   // Добавить комментарии к товару
    public static String QUESTION_TYPE_GET_PRODUCTS                 = "url_get_products";           // Получить обновленный список товаров

    public static String QUESTION_TYPE_GET_BAG                      = "url_bag";                    // Получить пересчитанную корзину
    public static String QUESTION_TYPE_GET_BAG_DELIVERY             = "url_bag_delivery";           // Получить пересчитанную корзину с доставкой

    public static String QUESTION_TYPE_COMPANY_COMMENT_LIST         = "url_company_comment_list";   // Получить все отзывы о компании
    public static String QUESTION_TYPE_COMPANY_COMMENT_ADD          = "url_company_comment_add";    // Оставить отзыв о компании
    public static String QUESTION_TYPE_INFORMATION                  = "url_information";            // Информационный блок

    public static String QUESTION_TYPE_FEED_PARTNER                 = "url_feed_partner";           // ПОдача заявки на партнерство пользователем.


    public static String QUESTION_TYPE_SETTINGS                     = "url_settings";               // Получить настройки проекта


    public static String QUESTION_TYPE_ORDER                        = "url_order";                  // Оформление заказа
    public static String QUESTION_TYPE_ORDER_PAYMENT                = "url_order_payment";          // Статус заказа


    public static String QUESTION_TYPE_SUCCESS                      = "success";                    // Регистрация пользователя

    public static String QUESTION_TYPE_COMMENT_LIST                 = "url_comment_list";   // Получить все отзывы о компании

    /**
     * Виды активности пользователей в фрагментах
     * */
    public static int USER_REGISTRATION_CLICK                           = 10;
    public static int USER_LOGIN_CLICK                                  = 11;
    public static int USER_ORDER_HISTORY                                = 12;

    public static int SELECT_CITY                                       = 100;
    public static int SELECT_CATALOG                                    = 110;
    public static int SELECT_FILTER                                     = 120;
    public static int SELECT_DELIVERY                                   = 130;
    public static int SELECT_INPUT_DATE                                 = 140;
    public static int SELECT_DELIVERY_POINT                             = 150;


    public static int CALL_PHONE_NUMBER                                 = 764;
    public static int CALL_SEND_EMAIL                                   = 864;
    public static int BAG_ACTION_DELETE_POSITION                        = 500;

    public static int REZULT_REQUEST_USER_REGISTRATION                  = 700;
    public static int REZULT_REQUEST_SELECT_POINT                       = 9090;
    public static int REZULT_REQUEST_REFRESH                            = 8752;


    private static volatile PublicData sInstance;

    public static Context mContext;
    private static Dialog dialog;

    public static ObjOptions                            OPTIONS;
    public static ObjDirectories                        DIRECTORIES;
    public static ObjSettings                           SETTINGS;
    public static ObjHome                               HOME;
    public static ObjGroupNews NEWS;
    public static ObjGroupArticles ARTICLES;

    public static ObjCity                               objCity;

    public static ObjCart                               objCart;
    public static ObjDelivery                           objDelivery;
    public static ObjDeliveryAddress objDeliveryAddress;
    public static ObjRecipient objRecipient;
    public static Date                                  objDeliveryDate;
    public static String                                objComment;
    public static ObjCompanyPoint                       objCompanyPoint;


    public static String                                callPhoneStr;

    public static String custom_font_caption    = "fonts/Oswald_Regular.ttf";
    public static String custom_font            = "fonts/Rubik_Regular.ttf";
    public static String custom_font_bold       = "fonts/Rubik_Bold.ttf";
    public static String custom_font_app        = "fonts/RubikMonoOne_Regular.ttf";


    public static String custom_font_100 = "fonts/Roboto_Thin.ttf";
    public static String custom_font_200 = "fonts/Roboto_Light.ttf";
    public static String custom_font_400 = "fonts/Roboto_Regular.ttf";


    public static Typeface tf_custom_font_caption;
    public static Typeface tf_custom_font;
    public static Typeface tf_custom_font_bold;
    public static Typeface tf_custom_font_app;
    public static Typeface tf_custom_font_100;
    public static Typeface tf_custom_font_200;
    public static Typeface tf_custom_font_400;


    public static String VIEW_MENU_TEXT                 = "TEXT";
    public static String VIEW_MENU_VERTICAL_ICON        = "VERTICAL_ICON";
    public static String VIEW_MENU_VERTICAL_IMAGE       = "VERTICAL_IMAGE";
    public static String VIEW_MENU_HORIZONTAL_ICON      = "HORIZONTAL_ICON";
    public static String VIEW_MENU_HORIZONTAL_IMAGE     = "HORIZONTAL_IMAGE";
    public static String VIEW_MENU_CELL_ICON            = "CELL_ICON";
    public static String VIEW_MENU_CELL_IMAGE_1         = "CELL_IMAGE_1";
    public static String VIEW_MENU_CELL_IMAGE_2         = "CELL_IMAGE_2";



    public static PublicData instance() {
        return sInstance;
    }


    public static void showDialog(Activity activity) {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
        if (dialog == null) {
            dialog = new Dialog(activity, android.R.style.Theme_Translucent);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_loading);
            dialog.setCancelable(false);
        }
        if (!dialog.isShowing())
            dialog.show();
    }

    public static void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    public static void  initialize(Context ctx) {
        // Double checked locking singleton, for thread safety VKSdk.initialize() calls
        if (sInstance == null) {
            synchronized (PublicData.class) {
                if (sInstance == null) {
                    sInstance = new PublicData();
                }
            }
        }
        mContext = ctx;

        if (CACHE_QUESTION) {
            Cache c = new Cache(mContext);
            c.deleteOldCache();
        }}

    /**
     *
     * Найти URL по наименованию.
     *
     * **/
    public static ObjURL getURL (String name) {
        if (PublicData.OPTIONS != null) {
            if (PublicData.OPTIONS.urls != null) {
                for (ObjURL obj : PublicData.OPTIONS.urls) {
                    if (obj.name.equals(name)) {
                        return obj;
                    }
                }
            }
        }
        return null;
    }



}
