package com.mobil2b.renatganiev.torfflowers.Class;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjMessage;
import com.mobil2b.renatganiev.torfflowers.R;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;



/**
 * Created by renatganiev on 02.04.16.
 * Вспомогательный класс, с полезными функциями, для которых сложно
 * подобрать отдельный класс
 */
public class Helps {
    public  static int VERTICAL = 1;
    public  static int HORIZONTAL = 2;



    private static volatile Helps sInstance;
    public static Helps instance() {
        return sInstance;
    }
    public static void  initialize() {
        // Double checked locking singleton, for thread safety VKSdk.initialize() calls
        if (sInstance == null) {
            synchronized (Helps.class) {
                if (sInstance == null) {
                    sInstance = new Helps();
                }
            }
        }
    }

    // md5
    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    // Определяет это планшет или нет.
    public static boolean isTablet(Context paramContext) {
        if (paramContext.getResources().getConfiguration().smallestScreenWidthDp >= 600)
            return true;
        else
            return false;
    }

    // В каком положении экран
    public static int orientation(Context paramContext) {
        if (paramContext.getResources().getConfiguration().orientation == 1) {
            return VERTICAL;
        } else {
            return HORIZONTAL;
        }
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    public static DisplayMetrics getDisplayWH(Activity active) {
        Display display = active.getWindowManager().getDefaultDisplay();
        DisplayMetrics metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);
        return metricsB;
    }

    public static String ebcodeBase64(String txt) {
        byte[] data = null;
        try {
            data = txt.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        // Закодировать
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }


    public static String decodeBase64(String base64) {
        // Receiving side
        byte[] data1 = Base64.decode(base64, Base64.DEFAULT);
        String text1 = null;
        try {
            text1 = new String(data1, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return text1;
    }

    /**
     * Принудительно позволяет замостить фон view.
     *
     * @param activity
     *            Родительское активити.
     * @param view
     *            Ресурс объекта для размещения фона.
     * @param drawable
     *            Ресурс фона, подлежащего повторению.
     */
    public static void setTiledBackground(Activity activity, View view, int drawable) {
        Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), drawable);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        view.setBackgroundDrawable(bitmapDrawable);
    }


    public static void errorTextEdit(View txt, Context mContext) {
        final Animation shakeanimation = AnimationUtils.loadAnimation(mContext, R.anim.shake);
        txt.startAnimation(shakeanimation);
    }


    public static String getPhoneFormat(String phone) {
        String new_phone = "8 (".concat(phone.substring(0,3).concat(") ".concat(phone.substring(3,6).concat("-").concat(phone.substring(6,8).concat("-".concat(phone.substring(8,10)))))));

        return new_phone;
    }

    /***********************************************************************************************
     * TODO: Показываем сообщение
     **********************************************************************************************/
    public static void showMessage(ObjMessage msg, Activity activity) {
        if (msg != null) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            LayoutInflater inflater = activity.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_message, null);
            dialogBuilder.setView(dialogView);

            Button btnCancel    = (Button)dialogView.findViewById(R.id.btn_ok);
            TextView txt_text   = (TextView)dialogView.findViewById(R.id.txt_text);
            TextView txt_title  = (TextView)dialogView.findViewById(R.id.txt_title);

            txt_title.setText(msg.title);
            txt_text.setText(msg.text);

            final AlertDialog alertDialog = dialogBuilder.create();

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.cancel();
                }
            });

            alertDialog.show();
        }
    }
    public static boolean isNotEmpty(String txt) {
        if ((txt != null) && (!txt.equals(""))) {
            return true;
        }
        return false;
    }
}
