package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.ControlView.MenuInfo.ClickListenerMenuInfo;
import com.mobil2b.renatganiev.torfflowers.ControlView.MenuInfo.MenuInfoView;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.MenuInfo.ObjMenuInfo;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.city_select;
import com.mobil2b.renatganiev.torfflowers.menu_info_item;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_info extends Fragment {

    public static final String TAG = "fragment_app_info";
    public static final int LAYOUT = R.layout.fragment_app_info;
    View rootView;
    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;
    DeliveryCityView deliveryCityView;

    LinearLayout layout_select_city, layout_menu_info, layout_mobil;
    Button btnCallOperator, btnSendEmail;

    Toolbar toolbar;

    public static fragment_app_info getInstance(Bundle args) {
        fragment_app_info fragment = new fragment_app_info();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        if (arg != null) {}
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        layout_select_city  = (LinearLayout) rootView.findViewById(R.id.layout_select_city);
        layout_menu_info    = (LinearLayout) rootView.findViewById(R.id.layout_menu_info);
        layout_mobil        = (LinearLayout) rootView.findViewById(R.id.layout_mobil);

        btnCallOperator     = (Button) rootView.findViewById(R.id.btnCallOperator);
        btnSendEmail        = (Button) rootView.findViewById(R.id.btnSendEmail);

        initToolbar();

        deliveryCityView = new DeliveryCityView(getContext(), PublicData.SETTINGS);
        deliveryCityView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), city_select.class);
                startActivityForResult(i, deliveryCityView.REZULT_REQUEST_SELECT_CITY);
            }
        });
        layout_select_city.removeAllViews();
        layout_select_city.addView(deliveryCityView);
        layout_select_city.setVisibility(View.GONE);

        if (PublicData.SETTINGS != null) {
            if (PublicData.SETTINGS.menu_info != null) {
                for (ObjMenuInfo objMenuInfo : PublicData.SETTINGS.menu_info) {
                    MenuInfoView menuInfoView = new MenuInfoView(getActivity(), objMenuInfo);
                    menuInfoView.setOnClickListener(new ClickListenerMenuInfo() {
                        @Override
                        public void onClick(View view, ObjMenuInfo menuInfo) {
                            if (menuInfo != null) {
                                Intent intent = new Intent(getActivity(), menu_info_item.class);
                                intent.putExtra("id", menuInfo.id);
                                startActivity(intent);
                            }
                        }
                    });
                    layout_menu_info.addView(menuInfoView);
                }
            }
        }

        layout_mobil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mobil2b.com/shop.html"));
                startActivity(browserIntent);
            }
        });

        btnCallOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callbackActionSuper != null) {
                    callbackActionSuper.onAction(TAG, PublicData.CALL_PHONE_NUMBER, new Bundle());
                }
            }
        });

        btnSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callbackActionSuper != null) {
                    callbackActionSuper.onAction(TAG, PublicData.CALL_SEND_EMAIL, new Bundle());
                }
            }
        });

        return rootView;
    }




    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        toolbar.setTitle("Информация");

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_menu));
            } else {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_menu));
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        deliveryCityView.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }
}
