package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterCompanyPointList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<ObjCompanyPoint> mList;
    ClickListenerCompanyPoint onClickListener;

    public AdapterCompanyPointList(Context context, ArrayList<ObjCompanyPoint> list) {
        this.mContext = context;
        this.mList = list;
    }

    public void setOnClickListener(ClickListenerCompanyPoint onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_company_info, parent, false);

        return new ViewHolderComment(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderComment viewHolder0 = (ViewHolderComment) holder;
        final ObjCompanyPoint obj = getItem(position);


        if (obj != null) {
            viewHolder0.txtName.setText(obj.name);

            viewHolder0.txtAddress.setVisibility(View.GONE);
            viewHolder0.txtPhone.setVisibility(View.GONE);
            if (obj.contacts != null) {
                if (obj.contacts.address != null) {
                    if (Helps.isNotEmpty(obj.contacts.address.city)) {
                        if (Helps.isNotEmpty(obj.contacts.address.street)) {
                            viewHolder0.txtAddress.setText(obj.contacts.address.street);
                            viewHolder0.txtAddress.setVisibility(View.VISIBLE);
                        }
                    }
                }
                if (Helps.isNotEmpty(obj.contacts.phone)) {
                    viewHolder0.txtPhone.setText(Helps.getPhoneFormat(obj.contacts.phone));
                    viewHolder0.txtPhone.setVisibility(View.VISIBLE);
                }
            }

            viewHolder0.setClickListener(onClickListener);
        }
    }


    private ObjCompanyPoint getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderComment extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtName, txtAddress, txtPhone;
        ClickListenerCompanyPoint clickListener;

        public ViewHolderComment(View itemView) {
            super(itemView);

            txtName         = (TextView)itemView.findViewById(R.id.txtName);
            txtAddress      = (TextView)itemView.findViewById(R.id.txtAddress);
            txtPhone        = (TextView)itemView.findViewById(R.id.txtPhone);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null){
                clickListener.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerCompanyPoint clickListener){
            this.clickListener = clickListener;
        }

    }
}