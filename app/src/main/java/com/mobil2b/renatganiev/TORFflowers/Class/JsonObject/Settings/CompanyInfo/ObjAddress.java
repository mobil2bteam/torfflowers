package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjAddress {
    public String city, street, comment;
    public ObjGeo geo;
    public int city_id;

    public ObjAddress() {
        init();
    }
    public ObjAddress(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjAddress(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("city")) {
                this.city = json.getString("city");
            }
            if (json.has("street")) {
                this.street = json.getString("street");
            }
            if (json.has("comment")) {
                this.comment = json.getString("comment");
            }
            if (json.has("geo")) {
                JSONObject geoJson = json.getJSONObject("geo");
                geo = new ObjGeo(geoJson);
            }
            if (json.has("city_id")) {
                this.city_id = json.getInt("city_id");
            }

        } catch (JSONException e) {
        }
    }
}
