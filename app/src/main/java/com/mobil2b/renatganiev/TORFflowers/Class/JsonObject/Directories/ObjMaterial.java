package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjMaterial extends ObjBaseDirectories {

    //public int      id;
    //public String   name = "";

    public ObjMaterial() {
        init();
    }
    public ObjMaterial(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjMaterial(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("material")) {
                JSONObject jsonMain = json.getJSONObject("material");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                super.id = json.getInt("id");
            }
            if (json.has("name")) {
                super.name = json.getString("name");
            }
        } catch (JSONException e) {
        }
    }

}
