package com.mobil2b.renatganiev.torfflowers;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Article.ObjArticleItems;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * TODO: Активити для отдельной статьи. Открываеться в отдельном окне.
 * */


public class article_item extends AppCompatActivity {

    Bundle arg;

    LinearLayout layout_add_text, layout_view;
    TextView txtName, txtDate, txtAddText, txtText;
    FloatingActionButton floatingShared;
    ImageView imgImage;
    Button btnOpenVideo;

    ObjArticleItems items;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_item);

        layout_add_text = (LinearLayout) findViewById(R.id.layout_add_text);
        layout_view     = (LinearLayout) findViewById(R.id.layout_view);

        txtName         = (TextView) findViewById(R.id.txtName);
        txtDate         = (TextView) findViewById(R.id.txtDate);
        txtAddText      = (TextView) findViewById(R.id.txtAddText);
        txtText         = (TextView) findViewById(R.id.txtText);

        imgImage        = (ImageView) findViewById(R.id.imgImage);

        floatingShared  = (FloatingActionButton) findViewById(R.id.floatingShared);
        btnOpenVideo    = (Button) findViewById(R.id.btnOpenVideo);


        layout_view.setVisibility(View.GONE);

        arg = getIntent().getExtras();
        if (arg != null) {
            if (arg.containsKey("id")) {
                int id = arg.getInt("id");
                if (id != 0) {
                    loadArticleItem(id);
                }
            }
        }
    }

    void loadArticleItem(int id) {
        ModuleObjects moduleObjects = new ModuleObjects(article_item.this, new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
                article_item.this.runOnUiThread(new Runnable() {
                    public void run() {
                        PublicData.showDialog(article_item.this);
                    }
                });
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {

                try {

                    JSONObject json = new JSONObject(result);
                    String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                    if (typeDate.equals(PublicData.QUESTION_TYPE_ARTICLE_ITEM)) {

                        if (json.has("articles_item")) {
                            JSONObject jsonHome = json.getJSONObject("articles_item");
                            items = new ObjArticleItems(jsonHome);
                            article_item.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    if (items != null) {
                                        updateView();
                                    }
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                }
            }

            @Override
            public void onError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    article_item.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(article_item.this, text, Toast.LENGTH_SHORT).show();
                            PublicData.closeDialog();
                        }
                    });
                } else {
                    article_item.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(article_item.this, getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                            PublicData.closeDialog();
                        }
                    });
                }

            }

            @Override
            public void onHttpError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    article_item.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(article_item.this, text, Toast.LENGTH_SHORT).show();
                            PublicData.closeDialog();
                        }
                    });
                } else {
                    article_item.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(article_item.this, getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                            PublicData.closeDialog();
                        }
                    });
                }
            }
        });

        moduleObjects.getArticleItem(false, id);
    }

    void updateView() {
        if (items != null) {

            // ДАТА
            if ((items.date != null) && (!items.date.equals(""))) {
                txtDate.setVisibility(View.VISIBLE);
                txtDate.setText(items.date);
            }

            // НАИМЕНОВАНИЕ
            txtName.setText(items.name);

            // ДОПОЛНИТЕЛЬНЫЙ ТЕКСТ
            if ((items.add_text != null) && (!items.add_text.equals(""))) {
                txtAddText.setText(Html.fromHtml(items.add_text));
                layout_add_text.setVisibility(View.VISIBLE);
            } else {
                layout_add_text.setVisibility(View.GONE);
            }

            // ОСНОВНОЙ ТЕКСТ
            if ((items.text != null) && (!items.text.equals(""))) {
                txtText.setText(Html.fromHtml(items.text));
                txtText.setVisibility(View.VISIBLE);
            } else {
                txtText.setVisibility(View.GONE);
            }


            // ИЗОБРАЖЕНИЕ
            imgImage.setVisibility(View.GONE);
            if (items.image_big != null) {
                if (items.image_big.url != null) {
                    if (!items.image_big.url.equals("")) {
                        Picasso.with(article_item.this).load(items.image_big.url).into(imgImage);
                        imgImage.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (items.image != null) {
                    if (items.image.url != null) {
                        if (!items.image.url.equals("")) {
                            Picasso.with(article_item.this).load(items.image.url).into(imgImage);
                            imgImage.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            // КНОПКА - ПОДЕЛИТЬСЯ
            if ((items.url != null) && (!items.url.equals(""))) {
                floatingShared.setVisibility(View.VISIBLE);
            } else {
                floatingShared.setVisibility(View.GONE);
            }

            // КНОПКА - ОТКРЫТЬ ВИДЕО
            if ((items.youtube != null) && (!items.youtube.equals(""))) {
                btnOpenVideo.setVisibility(View.VISIBLE);
            } else {
                btnOpenVideo.setVisibility(View.GONE);
            }

            layout_view.setVisibility(View.VISIBLE);
        }
        PublicData.closeDialog();
    }
}
