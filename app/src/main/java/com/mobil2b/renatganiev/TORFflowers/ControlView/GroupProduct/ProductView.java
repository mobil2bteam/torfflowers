package com.mobil2b.renatganiev.torfflowers.ControlView.GroupProduct;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.GroupView.ObjGroupView;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerLink;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterProductList;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerProductMin;
import com.mobil2b.renatganiev.torfflowers.R;


public class ProductView extends RelativeLayout {
    Context mContext;
    public ObjGroupView objGroupView;
    TextView title;
    Button moreBtn;
    RecyclerView recyclerView;
    View groupLayout;

    AdapterProductList listAdapter;
    private float density = getResources().getDisplayMetrics().density;
    private int dpPad = (int) (5*density + 0.5f);

    ClickListenerLink onClickListener;

    public ProductView(Context context, ObjGroupView objProductList) {
        super(context);

        this.mContext = context;
        objGroupView = objProductList;

        LayoutParams params = new LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        //setPadding(0,dpPad,0,dpPad);

        setLayoutParams(params);

        init();
    }

    public void loadingProductList(String list_id) {
    }


    private void init(){
        groupLayout = View.inflate(mContext, R.layout.group_view, null);
        addView(groupLayout);

        title           = (TextView) groupLayout.findViewById(R.id.title_text);
        moreBtn         = (Button) groupLayout.findViewById(R.id.more_btn);
        recyclerView    = (RecyclerView) groupLayout.findViewById(R.id.items_list);

        title.setText(objGroupView.name);
        if (objGroupView.button_more) {
            moreBtn.setVisibility(VISIBLE);
            if ((objGroupView.text_more != null) && (!objGroupView.text_more.equals(""))) {
                moreBtn.setText(objGroupView.text_more);
            }
            moreBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClickListener != null) {
                        onClickListener.onClick(view, 0, objGroupView.link, false);
                    }
                }
            });
        } else {
            moreBtn.setVisibility(INVISIBLE);
        }


        setProductList();
    }

    private void setProductList(){

        if ((objGroupView.view == null) || (objGroupView.view.equals(objGroupView.VIEW_HORIZONTAL))) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
            listAdapter = new AdapterProductList(mContext, objGroupView.items, true);
            if (objGroupView.count_items_display == 0f) {
                objGroupView.count_items_display = 2f;
            }
            listAdapter.count_item_displey = objGroupView.count_items_display;
        } else {
            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager((int)objGroupView.count_items_display, 1);
            recyclerView.setLayoutManager(staggeredGridLayoutManager);
            recyclerView.setNestedScrollingEnabled(false);
            listAdapter = new AdapterProductList(mContext, objGroupView.items, false);
        }

        recyclerView.setAdapter(listAdapter);

        if ((objGroupView.view == null) || (objGroupView.view.equals(objGroupView.VIEW_HORIZONTAL))) {
            ProductItemDecorator itemDecorator = new ProductItemDecorator(listAdapter.getItemCount());
            recyclerView.addItemDecoration(itemDecorator);
        }
    }

    public void setOnItemClickListener(ClickListenerLink onItemClickListener) {
        listAdapter.setOnClickListener(onItemClickListener);
        onClickListener = onItemClickListener;
    }

    public void setOnItemClickListenerProduct(ClickListenerProductMin onItemClickListener) {
        listAdapter.setOnClickListenerProduct(onItemClickListener);
    }
}
