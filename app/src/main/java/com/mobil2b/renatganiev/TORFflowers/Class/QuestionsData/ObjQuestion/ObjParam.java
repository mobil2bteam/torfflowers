package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjParam implements Serializable {
    public String name;
    public String value;

    public ObjParam() {}
    public ObjParam(String n, String v) {
        name = n;
        value = v;
    }
    public ObjParam(JSONObject json) {
        loadJSONObject(json);
    }
    public ObjParam(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("name")) {
                this.name = json.getString("name");
            }
            if (json.has("value")) {
                this.value = json.getString("value");
            }
        } catch (JSONException e) {
        }
    }
}
