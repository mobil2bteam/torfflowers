package com.mobil2b.renatganiev.torfflowers;

import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.MenuInfo.ObjMenuInfo;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.squareup.picasso.Picasso;

public class menu_info_item extends AppCompatActivity {

    WebView mWebView;
    NestedScrollView mNestedScrollView;
    ImageView imgImage;
    TextView txtDescription, txtDescriptionShort;
    Button btnPartner;

    ModuleObjects moduleObjects;
    ObjMenuInfo objMenuInfo;

    boolean partner = false;

    private class MyWebViewClient extends WebViewClient
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onLoadResource(WebView view, String url) {}

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_info_item);

        int id = getIntent().getIntExtra("id", 0);
        partner = getIntent().getBooleanExtra("partner", false);

        mWebView                = (WebView)             findViewById(R.id.webView);
        mNestedScrollView       = (NestedScrollView)    findViewById(R.id.mNestedScrollView);
        imgImage                = (ImageView)           findViewById(R.id.imgImage);
        txtDescription          = (TextView)            findViewById(R.id.txtDescription);
        txtDescriptionShort     = (TextView)            findViewById(R.id.txtDescriptionShort);
        btnPartner              = (Button)              findViewById(R.id.btnPartner);

        if (partner) {
            btnPartner.setVisibility(View.VISIBLE);

            btnPartner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    moduleObjects.setFeedPartne(true, User.getCurrentSession());
                }
            });

        } else {
            btnPartner.setVisibility(View.GONE);
        }

        txtDescription.setMovementMethod(LinkMovementMethod.getInstance());

        mWebView.setWebViewClient(new MyWebViewClient());

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        // ЗАПРЕЩАЕМ ВЫДЕЛЯТЬ ТЕКСТ
        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        mWebView.setLongClickable(false);

        mWebView.setVisibility(View.GONE);
        mNestedScrollView.setVisibility(View.GONE);

        moduleObjects = new ModuleObjects(menu_info_item.this, callBackSendDate);
        if (id != 0) {
            moduleObjects.getMenuInfo(false, id);
        }

        // указываем страницу загрузки
        //mWebView.loadUrl(url);
    }

    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            menu_info_item.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.showDialog(menu_info_item.this);
                }
            });
        }

        @Override
        public void onReturnAnswer(final String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            menu_info_item.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    String rezData = OpenBlocks.getOpenData(result);
                    if (rezData == null) return;

                    if (rezData.equals(PublicData.QUESTION_TYPE_INFORMATION)) {
                        objMenuInfo = new ObjMenuInfo(result);

                        initToolbar(objMenuInfo.name);

                        if (Helps.isNotEmpty(objMenuInfo.url)) {
                            mWebView.loadUrl(objMenuInfo.url);
                            mWebView.setVisibility(View.VISIBLE);
                        } else {
                            imgImage.setVisibility(View.GONE);
                            txtDescriptionShort.setVisibility(View.GONE);
                            if (objMenuInfo.image != null) {
                                if (objMenuInfo.image.url != null) {
                                    if (!objMenuInfo.image.url.equals("")) {
                                        Picasso.with(menu_info_item.this).load(objMenuInfo.image.url).into(imgImage);
                                        imgImage.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                            if (Helps.isNotEmpty(objMenuInfo.description_short)) {
                                txtDescriptionShort.setText(objMenuInfo.description_short);
                                txtDescriptionShort.setVisibility(View.VISIBLE);
                            }
                            txtDescription.setText(Html.fromHtml(objMenuInfo.description_long));
                            mNestedScrollView.setVisibility(View.VISIBLE);
                        }
                    }

                    if (rezData.equals(PublicData.QUESTION_TYPE_SUCCESS)) {
                        User.getCurrentSession().partner_feed = true;
                        setResult(RESULT_OK);
                        finish();
                    }
                }
            });
        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            menu_info_item.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    Toast.makeText(menu_info_item.this, text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            menu_info_item.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    Toast.makeText(menu_info_item.this, text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    @Override
    public void onBackPressed() {
        if(mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar(String caption) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle(caption);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(menu_info_item.this, R.drawable.ic_toolbar_icon_back_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
