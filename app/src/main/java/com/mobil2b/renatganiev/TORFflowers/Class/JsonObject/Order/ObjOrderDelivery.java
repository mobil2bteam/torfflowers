package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjOrderDelivery {

    public int deliveryId;
    public String addressPoint;
    public String deliveryText;
    public String deliveryDate;
    public String deliveryType;

    public ObjOrderDelivery() {
        init();
    }
    public ObjOrderDelivery(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjOrderDelivery(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("delivery")) {
                JSONObject jsonMain = json.getJSONObject("delivery");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {

    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("deliveryId")) {
                deliveryId = json.getInt("deliveryId");
            }
            if (json.has("addressPoint")) {
                addressPoint = json.getString("addressPoint");
            }
            if (json.has("deliveryText")) {
                deliveryText = json.getString("deliveryText");
            }
            if (json.has("deliveryDate")) {
                deliveryDate = json.getString("deliveryDate");
            }
            if (json.has("deliveryType")) {
                deliveryType = json.getString("deliveryType");
            }
        } catch (JSONException e) {
        }
    }

}
