package com.mobil2b.renatganiev.torfflowers.Fragments.User.Registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackUserExist;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_user_registration extends Fragment {

    public static final String TAG = "fragment_app_user_registration";
    public static final int LAYOUT = R.layout.fragment_app_user_registration;
    View rootView;
    public CallbackAction callbackActionSuper;

    LinearLayout layout_user_first_name, layout_user_second_name, layout_user_email, layout_user_password, layout_user_phone;
    ImageView imgCheckFirstName, imgCheckSecondName, imgCheckEmail, imgCheckPassword, imgCheckPhone;
    EditText editUserFirstName, editUserSecondName, editUserEmail, editUserPassword, editUserPhone;
    Button btnRegistration, btnLogin;
    ProgressBar progressBar;

    public static fragment_app_user_registration getInstance(Bundle args) {
        fragment_app_user_registration fragment = new fragment_app_user_registration();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        layout_user_first_name      = rootView.findViewById(R.id.layout_user_first_name);
        layout_user_second_name     = rootView.findViewById(R.id.layout_user_second_name);
        layout_user_email           = rootView.findViewById(R.id.layout_user_email);
        layout_user_password        = rootView.findViewById(R.id.layout_user_password);
        layout_user_phone           = rootView.findViewById(R.id.layout_user_phone);

        imgCheckFirstName           = rootView.findViewById(R.id.imgCheckFirstName);
        imgCheckSecondName          = rootView.findViewById(R.id.imgCheckSecondName);
        imgCheckEmail               = rootView.findViewById(R.id.imgCheckEmail);
        imgCheckPassword            = rootView.findViewById(R.id.imgCheckPassword);
        imgCheckPhone               = rootView.findViewById(R.id.imgCheckPhone);

        editUserFirstName           = rootView.findViewById(R.id.editUserFirstName);
        editUserSecondName          = rootView.findViewById(R.id.editUserSecondName);
        editUserEmail               = rootView.findViewById(R.id.editUserEmail);
        editUserPassword            = rootView.findViewById(R.id.editUserPassword);
        editUserPhone               = rootView.findViewById(R.id.editUserPhone);

        btnRegistration             = rootView.findViewById(R.id.btnRegistration);
        btnLogin                    = rootView.findViewById(R.id.btnLogin);
        progressBar                 = rootView.findViewById(R.id.progressBar);

        btnRegistration.setOnClickListener(view -> checkedEditText());

        btnLogin.setOnClickListener(view -> {
            if (callbackActionSuper != null) {
                callbackActionSuper.onAction(TAG, PublicData.USER_LOGIN_CLICK, new Bundle());
            }
        });

        return rootView;
    }


    void checkedEditText() {
        if (editUserSecondName.getText().toString().trim().length() == 0) {
            imageError(imgCheckSecondName, layout_user_second_name);
            return;
        } else {
            imageCheck(imgCheckSecondName);
        }
        if (editUserFirstName.getText().toString().trim().length() == 0) {
            imageError(imgCheckFirstName, layout_user_first_name);
            return;
        } else {
            imageCheck(imgCheckFirstName);
        }


        if (editUserEmail.getText().toString().trim().length() == 0) {
            imageError(imgCheckEmail, layout_user_email);
            return;
        } else {
            imageCheck(imgCheckEmail);
        }

        if (editUserPhone.getText().toString().trim().length() == 0) {
            imageError(imgCheckPhone, layout_user_phone);
            return;
        } else {
            imageCheck(imgCheckPhone);
        }



        if (editUserPassword.getText().toString().trim().length() == 0) {
            imageError(imgCheckPassword, layout_user_password);
            return;
        } else {
            imageCheck(imgCheckPassword);
        }

        User.onUserExist(editUserEmail.getText().toString(), new CallbackUserExist() {
            @Override
            public void onStart() {
                //btnRegistration.setVisibility(View.GONE);
                //progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(final boolean exist) {

                // here you check the value of getActivity() and break up if needed
                if(getActivity() == null) return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (exist) {
                            btnRegistration.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            imageError(imgCheckEmail, layout_user_email);
                            Toast.makeText(getContext(), "Пользователь уже зарегистрирован", Toast.LENGTH_SHORT).show();
                        } else {
                            imageCheck(imgCheckEmail);
                            Bundle bundle = new Bundle();
                            bundle.putString("userEmail",       editUserEmail.getText().toString());
                            bundle.putString("phone",           editUserPhone.getText().toString());
                            bundle.putString("userName",        editUserFirstName.getText().toString());
                            bundle.putString("userFirstName",   editUserFirstName.getText().toString());
                            bundle.putString("userSecondName",  editUserSecondName.getText().toString());
                            bundle.putString("userPassword",    editUserPassword.getText().toString());
                            callbackActionSuper.onAction(TAG, PublicData.USER_REGISTRATION_CLICK, bundle);
                        }
                    }
                });

            }

            @Override
            public void onError(final String message, int error_code) {
                // here you check the value of getActivity() and break up if needed
                if(getActivity() == null) return;
                getActivity().runOnUiThread(() -> {
                    btnRegistration.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                });
            }
        });
    }

    void imageError(ImageView imageView, View v) {
        imageView.setImageResource(R.drawable.ic_info_black_24dp);
        Helps.errorTextEdit(v, getActivity());
    }

    void imageCheck(ImageView imageView) {
        imageView.setImageResource(R.drawable.ic_check_circle_black_24dp);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


}
