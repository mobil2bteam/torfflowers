package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog.ObjCatalogItems;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerCatalog;
import com.mobil2b.renatganiev.torfflowers.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterCatalog extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private List<ObjCatalogItems> mList;
    ObjCatalogItems mCatalog;
    ClickListenerCatalog onClickListener;

    public AdapterCatalog(Context context, ObjCatalogItems catalog) {
        this.mContext = context;
        this.mList = catalog.items;
        this.mCatalog = catalog;
    }


    public void setOnClickListener(ClickListenerCatalog onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view3 = inflater.inflate(R.layout.item_catalog_icon_text_vertical, parent, false);

        if (mCatalog != null) {
            if (mCatalog.view != null) {
                if (    (mCatalog.view.equals(PublicData.VIEW_MENU_TEXT)) ||
                        (mCatalog.view.equals(PublicData.VIEW_MENU_VERTICAL_ICON))) {
                    view3 = inflater.inflate(R.layout.item_catalog_icon_text_vertical, parent, false);
                }
                if (    (mCatalog.view.equals(PublicData.VIEW_MENU_CELL_ICON))) {
                    view3 = inflater.inflate(R.layout.item_catalog_icon_text_cell, parent, false);
                }

                if (    (mCatalog.view.equals(PublicData.VIEW_MENU_VERTICAL_IMAGE))) {
                    view3 = inflater.inflate(R.layout.item_catalog_image_text_vertical, parent, false);
                }

                if (    (mCatalog.view.equals(PublicData.VIEW_MENU_CELL_IMAGE_1))) {
                    view3 = inflater.inflate(R.layout.item_catalog_image_text_cell_1, parent, false);
                }

                if (    (mCatalog.view.equals(PublicData.VIEW_MENU_CELL_IMAGE_2))) {
                    view3 = inflater.inflate(R.layout.item_catalog_image_text_cell_2, parent, false);
                }
            }
        }

        return new ViewHolderOrders(view3);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderOrders viewHolder0 = (ViewHolderOrders) holder;

        ObjCatalogItems obj = getItem(position);

        if (!obj.flag_not_show_name) {
            viewHolder0.txtName.setText(obj.name);

            if ((obj.comment != null) && (!obj.comment.equals(""))) {
                viewHolder0.txtDescription.setText(obj.comment);
                viewHolder0.txtDescription.setVisibility(View.VISIBLE);
            }
        } else {
            viewHolder0.txtName.setVisibility(View.GONE);
            viewHolder0.txtDescription.setVisibility(View.GONE);
        }

        if ((mCatalog != null) && (mCatalog.view != null)) {
            if (    (mCatalog.view.equals(PublicData.VIEW_MENU_TEXT)) ||
                    (mCatalog.view.equals(PublicData.VIEW_MENU_VERTICAL_ICON)) ||
                    (mCatalog.view.equals(PublicData.VIEW_MENU_CELL_ICON))) {

                // ИКОНКА

                if (!mCatalog.view.equals(PublicData.VIEW_MENU_TEXT)) {
                    if (obj.icon != null) {

                        if (getItem(position).icon.url != null) {
                            if (!getItem(position).icon.url.equals("")) {
                                Picasso.with(mContext).load(getItem(position).icon.url).into(viewHolder0.imgIcon);
                                viewHolder0.imgIcon.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            }

            if (    (mCatalog.view.equals(PublicData.VIEW_MENU_VERTICAL_IMAGE)) ||
                    (mCatalog.view.equals(PublicData.VIEW_MENU_CELL_IMAGE_1)) ||
                    (mCatalog.view.equals(PublicData.VIEW_MENU_CELL_IMAGE_2))) {


                // ИЗОБРАЖЕНИЕ
                //viewHolder0.txtDescription.setVisibility(View.GONE);

                if (obj.image != null) {
                    if (obj.image.url != null) {
                        if (!obj.image.url.equals("")) {

                            if (mCatalog.view.equals(PublicData.VIEW_MENU_VERTICAL_IMAGE)) {
                                DisplayMetrics metrics = Helps.getDisplayWH((Activity)mContext);
                                if (metrics != null) {
                                    if (metrics.widthPixels != 0) {
                                        float c = (float) obj.image.heigth / (float) obj.image.width;
                                        Picasso.with(mContext)
                                                .load(obj.image.url)
                                                .resize(metrics.widthPixels, (int)(((float)metrics.widthPixels) * c))
                                                .into(viewHolder0.imgIcon);
                                    }
                                }

                            } else {
                                Picasso.with(mContext).load(obj.image.url).into(viewHolder0.imgIcon);
                            }
                            viewHolder0.imgIcon.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }

        viewHolder0.setClickListener(onClickListener);
    }



    private ObjCatalogItems getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderOrders extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtName, txtDescription;
        ImageView imgIcon;
        ClickListenerCatalog clickListener;

        public ViewHolderOrders(View itemView) {
            super(itemView);

            txtName             = (TextView)    itemView.findViewById(R.id.txtName);
            txtDescription      = (TextView)    itemView.findViewById(R.id.txtDescription);
            imgIcon             = (ImageView)   itemView.findViewById(R.id.imgIcon);
            imgIcon.setVisibility(View.GONE);
            txtDescription.setVisibility(View.GONE);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null){
                clickListener.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerCatalog clickListener){
            this.clickListener = clickListener;
        }

    }

}