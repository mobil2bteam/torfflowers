package com.mobil2b.renatganiev.torfflowers.Fragments.Catalog;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterCatalog;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog.ObjCatalogItems;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjFilter;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerCatalog;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.R;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_catalog_list extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_catalog_list";
    public static final int LAYOUT = R.layout.fragment_app_catalog_list;
    View rootView;

    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;


    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    private AdapterCatalog mAdapterSearch;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    //TextView txtCaption;

    int catalog_id = 0;
    ObjCatalogItems objCatalogItems;

    Toolbar toolbar;

    public static fragment_app_catalog_list getInstance(Bundle args) {
        fragment_app_catalog_list fragment = new fragment_app_catalog_list();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.
            if (arg.containsKey("catalog_id")) {
                catalog_id = arg.getInt("catalog_id");
            }
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        //txtCaption              = (TextView)        rootView.findViewById(R.id.txtCaption);
        /*imgHead                 = (ImageView)       rootView.findViewById(R.id.imgHead);*/

        mSwipeRefreshLayout     = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        initToolbar();

        if (catalog_id != 0) {
            if (PublicData.DIRECTORIES != null) {
                if (PublicData.DIRECTORIES.catalog != null) {
                    if (PublicData.DIRECTORIES.catalog.items != null) {
                        objCatalogItems =  PublicData.DIRECTORIES.catalog.getCatalogItems(catalog_id, PublicData.DIRECTORIES.catalog.items);
                        if (objCatalogItems != null) {
                            refrashAllDate();
                        } else {
                            loadCatalog(catalog_id);
                        }
                    } else {
                        loadCatalog(catalog_id);
                    }
                } else {
                    loadCatalog(catalog_id);
                }
            } else {
                loadCatalog(catalog_id);
            }
        } else {
            loadCatalog(0);
        }

        return rootView;
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_menu));
            } else {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_menu));
            }
        }
        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                setResult(RESULT_OK);
                finish();

            }
        });*/
    }


    // Инициализация контейнера.
    void intiRecyclerView(String view) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);

        if ((view == null) || (view.equals(""))) {
            // В виде списка
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
        } else {
            if (    (view.equals(PublicData.VIEW_MENU_TEXT)) ||
                    (view.equals(PublicData.VIEW_MENU_VERTICAL_IMAGE)) ||
                    (view.equals(PublicData.VIEW_MENU_VERTICAL_ICON))) {
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
            } else {
                // В виде ячеек
                staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
                mRecyclerView.setLayoutManager(staggeredGridLayoutManager);
            }
        }

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void refrashAllDate() {

        if (objCatalogItems != null) {

            intiRecyclerView(objCatalogItems.view);

            //txtCaption.setText(objCatalogItems.name);
            if (toolbar != null) {
                toolbar.setTitle(objCatalogItems.name);
                String subText = "";
                for (ObjCatalogItems catalogItems : PublicData.DIRECTORIES.catalog.items) {
                    subText = PublicData.DIRECTORIES.catalog.getParentName(objCatalogItems.id, catalogItems);
                    if (!subText.equals("")) {
                        break;
                    }
                }
                toolbar.setSubtitle(subText);
            }


            mAdapterSearch = new AdapterCatalog(getActivity(), objCatalogItems);
            mRecyclerView.setAdapter(mAdapterSearch);
            mAdapterSearch.notifyDataSetChanged();
            mRecyclerView.invalidate();

            mAdapterSearch.setOnClickListener(new ClickListenerCatalog() {
                @Override
                public void onClick(View view, ObjCatalogItems objCatalogItems) {
                    //Toast.makeText(getContext(), objCatalogItems.name, Toast.LENGTH_SHORT).show();
                    if (callbackActionSuper != null) {
                        Bundle bundle = new Bundle();
                        if (objCatalogItems.search) {
                            /***
                             * ПОКАЗЫВАЕМ ТОВАР
                             */
                            ObjFilter objFilter = new ObjFilter();
                            objFilter.catalog_id = objCatalogItems.id;
                            bundle.putSerializable("filter", objFilter);
                            callbackActionSuper.onAction(TAG, PublicData.SELECT_FILTER, bundle);
                        } else {
                            if (objCatalogItems.items != null) {
                                if (objCatalogItems.items.size() > 0) {
                                    /***
                                     * ЗАГРУЖАЕМ СЛЕДУЮЩИЙ УРОВЕНЬ!
                                     */
                                    bundle.putInt("catalog_id", objCatalogItems.id);
                                    bundle.putInt("items", objCatalogItems.items.size());
                                    callbackActionSuper.onAction(TAG, PublicData.SELECT_CATALOG, bundle);
                                } else {
                                    /***
                                     * ПОКАЗЫВАЕМ ТОВАР
                                     */
                                    ObjFilter objFilter = new ObjFilter();
                                    objFilter.catalog_id = objCatalogItems.id;
                                    bundle.putSerializable("filter", objFilter);
                                    callbackActionSuper.onAction(TAG, PublicData.SELECT_FILTER, bundle);
                                }
                            } else {
                                /***
                                 * ПОКАЗЫВАЕМ ТОВАР
                                 */
                                ObjFilter objFilter = new ObjFilter();
                                objFilter.catalog_id = objCatalogItems.id;
                                bundle.putSerializable("filter", objFilter);
                                callbackActionSuper.onAction(TAG, PublicData.SELECT_FILTER, bundle);
                            }
                        }

                    }
                }
            });
        }
    }


    void loadCatalog(int id) {
        ModuleObjects moduleObjects = new ModuleObjects(getActivity(), new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
                mSwipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {

                try {

                    JSONObject json = new JSONObject(result);
                    String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                    if (typeDate.equals(PublicData.QUESTION_TYPE_CATALOG)) {

                        if (json.has("catalog")) {
                            JSONObject jsonHome = json.getJSONObject("catalog");
                            objCatalogItems = new ObjCatalogItems(jsonHome);
                            // here you check the value of getActivity() and break up if needed
                            if(getActivity() == null) return;
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    if (objCatalogItems != null) {
                                        mSwipeRefreshLayout.setRefreshing(false);
                                        refrashAllDate();
                                    }
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                }
            }

            @Override
            public void onError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }

            }

            @Override
            public void onHttpError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            }
        });

        moduleObjects.getCatalog(true, catalog_id);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRefresh() {
        loadCatalog(catalog_id);
    }


}
