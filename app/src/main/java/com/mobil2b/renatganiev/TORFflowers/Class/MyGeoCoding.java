package com.mobil2b.renatganiev.torfflowers.Class;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;

/**
 * Created by renatganiev on 01.08.17.
 */

public class MyGeoCoding {

    private static Context mContext;

    public MyGeoCoding(Context context) {
        mContext = context;
    }

    // ПОИСК АДРЕСА ПО ВХОДЯЩЕЙ СТРОКЕ
    public static Address searchAddress(String strAddress) {
        try {
            Geocoder geocoder = new Geocoder(mContext);
            List<Address> lst = geocoder.getFromLocationName(strAddress, 1);
            if (lst.size() > 0) {
                return lst.get(0);
            } else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
