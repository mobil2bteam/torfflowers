package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.DeliveryAddress.ObjDeliveryAddress;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerDeliveryAddress {
    void onClick(View view, ObjDeliveryAddress objDelivery);
    void onRemove(View view, ObjDeliveryAddress objDelivery);
}
