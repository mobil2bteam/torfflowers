package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Home;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.GroupView.ObjGroupView;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Menu.ObjMenu;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Slider.ObjSlider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ObjHome {

    public boolean flag_show_city_delivery;
    public boolean flag_show_social_link;
    public boolean flag_show_watched;
    public boolean flag_show_search;

    public ObjSlider slider;
    public ArrayList<ObjGroupView> group_view;
    public ObjMenu menu;

    public ObjHome() {}
    public ObjHome(JSONObject json) {
        loadJSONObject(json);
    }
    public ObjHome(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            if (json.has("home")) {
                JSONObject jsonMain = json.getJSONObject("home");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    public void loadJSONObject(JSONObject json){
        try {
                if (json.has("flag_show_city_delivery")) {
                    flag_show_city_delivery = json.getBoolean("flag_show_city_delivery");
                }
                if (json.has("flag_show_social_link")) {
                    flag_show_social_link = json.getBoolean("flag_show_social_link");
                }
                if (json.has("flag_show_watched")) {
                    flag_show_watched = json.getBoolean("flag_show_watched");
                }
                if (json.has("flag_show_search")) {
                    flag_show_search = json.getBoolean("flag_show_search");
                }

                if(json.has("slider")){
                    slider = new ObjSlider(json.getJSONObject("slider"));
                }
                if(json.has("menu")){
                    menu = new ObjMenu(json.getJSONObject("menu"));
                }
                if(json.has("group_view")) {
                    group_view = new ArrayList<>();
                    if (json.has("group_view")) {
                        for (int i = 0; i < json.getJSONArray("group_view").length(); i++) {
                            ObjGroupView it = new ObjGroupView(json.getJSONArray("group_view").getJSONObject(i));
                            group_view.add(it);
                        }
                    }
                }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
