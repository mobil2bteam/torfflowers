package com.mobil2b.renatganiev.torfflowers.ControlView.ItemImageView;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.R;
import com.squareup.picasso.Picasso;




/**
 * Created by renatganiev on 10.10.15.
 */
public class ItemImageView extends RelativeLayout {

    public String TAG;

    Context mContext;
    ObjProductMin objProduct;

    ImageView imageView;

    public ItemImageView(Context context, ObjProductMin obj) {
        super(context);

        mContext        = context;
        objProduct     = obj;
        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_image_view, this);

        imageView         = (ImageView)    findViewById(R.id.imageView);
        updateFields();
    }

    private void updateFields() {
        if (objProduct != null) {
            if (objProduct.image != null) {
                if (Helps.isNotEmpty(objProduct.image.url)) {
                    Picasso.with(mContext).load(objProduct.image.url)
                        .into(imageView);
                    return;
                }
            }
        }
    }

    public ObjProductMin getObject() {
        return objProduct;
    }

}