package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate;


import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;

/**
 * Created by renatganiev on 20.07.14.
 */
public interface CallBackSendDate {
    // Начал отправлять
    public  void onSend(String txt, ObjLink linkURL);
    // Завершение отправки
    public  void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL);
    // Ошибка
    public  void onError(String text, int code_error, ObjLink linkURL);

    public  void onHttpError(String text, int code_error, ObjLink linkURL);
}
