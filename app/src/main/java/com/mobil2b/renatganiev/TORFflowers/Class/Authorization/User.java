package com.mobil2b.renatganiev.torfflowers.Class.Authorization;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.login.AuthorizationLogin;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.loginToServer.GetLoginToServer;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjURL;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order.ObjOrderUserList;

import java.util.ArrayList;


/**
 * Created by renatganiev on 01.04.16.
 */
public class User {

    public static int USER_TOKEN               = 100;
    public static int USER_PASSWORD            = 101;
    public static int USER_REGISTRATION        = 102;
    public static int USER_EDIT                = 103;
    public static int USER_CHANGE_PASSWORD     = 104;

    private static AuthorizationLogin loginApi;

    // Класс для проверки валидности данных пользователя на сервере
    private static Context mContext;
    private static DataSession currentSession;

    public static ArrayList<ObjOrderUserList> order_list_active = new ArrayList<>();
    //public static ArrayList<ObjOrder> order_list_arhive = new ArrayList<>();

    private static volatile User sInstance;

    // Ссылка для работы с пользователем на нашем сервере
    private static ObjURL url_exist;
    private static ObjURL url_login;
    private static ObjURL url_registration;
    private static ObjURL url_edit;
    private static ObjURL url_change_password;
    private static ObjURL url_send_new_password;

    // Класс по работе с пльзователем на нашем сервере
    private static GetLoginToServer loginToServer;

    public static CallbackAuthorization callbackAuthorization;

    // Список заказов пользовател

    /*
    * Конструкторв
    * */
    public User(Context ctx) {
        mContext    = ctx;
        loginApi    = new AuthorizationLogin(ctx);
        loginToServer = new GetLoginToServer(ctx);
    }

    public static User instance() {
        return sInstance;
    }


    // Инициализируем класс
    public static void init(Context ctx) {
        mContext    = ctx;
        loginApi    = new AuthorizationLogin(ctx);
        loginToServer = new GetLoginToServer(ctx);
        setContext(ctx);
    }


    /*
    * Меняем контекст при необходимости
    * */
    public static void setContext(Context ctx) {
        mContext    = ctx;
        if (loginApi    != null)  loginApi.setContext(ctx);
    }

    public static void setURLLogin(ObjURL url) {
        url_login = url;
    }

    public static void setURLRegistration(ObjURL url) {
        url_registration = url;
    }

    public static void setURLEdit(ObjURL url) {
        url_edit = url;
    }

    public static void setURLPasswordChange(ObjURL url) {
        url_change_password = url;
    }

    public static void setURLPasswordMemory(ObjURL url) {
        url_send_new_password = url;
    }
    public static void setURLExist(ObjURL url) {
        url_exist = url;
    }


    // ПРОВЕРИТЬ, АВТОРИЗОВАН ИЛИ НЕТ, ТОЛЬКО В КЭШЕ
    public static boolean isLoggedIn() {
        // ПЕРВЫМ ДЕЛОМ ПРОВЕРЯЕМ АВТОРИЗАЦИЯЮ ПО ЛОГИНУ
        if (loginApi.wakeUpSession()) {
            return true;
        }
        // Нет активной сессии.
        return false;
    }

    public static String getToken() {
        DataSession dataSession = getCurrentSession();
        if (dataSession == null) {
            if (loginApi.wakeUpSession()) {
                dataSession = loginApi.getSession();
            }
        }
        if (dataSession != null) {
            return dataSession.token;
        }
        return null;
    }

    // Возвращаем активную сессию
    public static DataSession getCurrentSession() {
        return currentSession;
    }

    // Заменяем активную сессию
    public static void setCurrentSession(DataSession newSession) {
        currentSession = newSession;
        loginApi.setSession(currentSession);
    }

    // ПРОВЕРИТЬ, АВТОРИЗОВАН ИЛИ НЕТ С УЧАСТИЕМ СЕРВЕРА
    public static void onLoginSession(Activity ctx, CallbackAuthorization callback) {
        setContext(ctx.getApplicationContext());
        // ПЕРВЫМ ДЕЛОМ ПРОВЕРЯЕМ АВТОРИЗАЦИЯЮ ПО ЛОГИНУ

        if (url_login != null) {
            if (loginApi.wakeUpSession()) {
                // Авторизация через логин в кэше присутсвует.
                // Пробуем получить данные для запроса на сервер.
                // Так как данные скорей всего верны, сразу отсылаем на сервер для проверки.
                DataSession dataSession = loginApi.getDateForLogin();

                currentSession = dataSession;
                // Сохраняем сессию
                loginApi.setSession(dataSession);
                // Отправили данные о том что у нас есть сессия
                callback.onReturnSession(dataSession);
                // Проверяем сессию на валидность на нашем сервере
                loginToServer.loginToServer(dataSession, url_login, callback);
                return;
            }
        }
        // Нет активной сессии.
        currentSession = null;
        loginApi.onLogout();
        callback.onDontSession("", 0);
    }

    public static void onLoginSession(Fragment ctx, CallbackAuthorization callback) {
        setContext(ctx.getContext());
        // ПЕРВЫМ ДЕЛОМ ПРОВЕРЯЕМ АВТОРИЗАЦИЯЮ ПО ЛОГИНУ
        if (url_login != null) {
            if (loginApi.wakeUpSession()) {
                // Авторизация через логин в кэше присутсвует.
                // Пробуем получить данные для запроса на сервер.
                // Так как данные скорей всего верны, сразу отсылаем на сервер для проверки.
                DataSession dataSession = loginApi.getDateForLogin();

                currentSession = dataSession;
                // Сохраняем сессию
                loginApi.setSession(dataSession);
                // Отправили данные о том что у нас есть сессия
                callback.onReturnSession(dataSession);
                // Проверяем сессию на валидность на нашем сервере
                loginToServer.loginToServer(dataSession, url_login, callback);
                return;
            }
        }
        // Нет активной сессии.
        currentSession = null;
        loginApi.onLogout();
        callback.onDontSession("", 0);
    }

    // АВТОРИЗАЦИЯ
    public static void onLogin(String login, String password, CallbackAuthorization callback) {
        if (url_login != null) {
            if (isLoggedIn()) {
                onLogout(callback);
            }
            loginToServer.loginToServer(login, password, url_login, callback);
        }
    }

    // АВТОРИЗАЦИЯ
    public static void onLogin(String login, String password) {
        if (url_login != null) {
            if (isLoggedIn()) {
                onLogout(callbackAuthorization);
            }
            loginToServer.loginToServer(login, password, url_login, callbackAuthorization);
        }
    }

    // РЕГИСТРАЦИЯ
    public static void onRegistrationUser(DataSession new_session, CallbackAuthorization call) {
        // Регистрируем пользователя
        if (url_registration != null) {
            ArrayList<ObjParam> paramList = new_session.getParamList();
            if (paramList.size() != 0) {
                loginToServer.registrationToServer(url_registration, paramList, call, new_session);
            }
        }
    }

    // РЕГИСТРАЦИЯ
    public static void onRegistrationUser(DataSession new_session) {
        // Регистрируем пользователя
        if (url_registration != null) {
            ArrayList<ObjParam> paramList = new_session.getParamList();
            if (paramList.size() != 0) {
                loginToServer.registrationToServer(url_registration, paramList, callbackAuthorization, new_session);
            }
        }
    }


    // ИЗМЕНИТЬ СВОИ ДАННЫЕ
    public static void onUpdateUserInfo(DataSession current_session, CallbackUserUpdate callbackUserUpdate) {
        loginToServer.editToServer(current_session, url_edit, callbackUserUpdate);
    }

    // СМЕНА ПАРОЛЯ
    public static void onPasswordChange(String oldPassword, String newPassword, CallbackPasswordChange callbackPasswordChange) {
        loginToServer.changePasswordToServer(User.getCurrentSession(), url_change_password, oldPassword, newPassword, callbackPasswordChange);
    }

    // ВСПОМНИТЬ ПАРОЛЬ
    public static void onPasswordMemory(String login, CallbackPasswordMemory callbackPasswordMemory) {
        loginToServer.sendPasswordMemoryToServer(url_send_new_password, login, callbackPasswordMemory);
    }

    // Проверяем, существует ли пользователь на серверер
    public static void onUserExist(String login, CallbackUserExist callbackUserExist) {
        //loginToServer.sendMyNewPasswordToServer(User.getCurrentSession(), url_send_new_password, call);
        if (url_exist != null) {
            loginToServer.userExistToServer(url_exist, login, callbackUserExist);
        }
    }

    // ВЫЙТИ
    public static void onLogout(CallbackAuthorization callback) {
        loginApi.onLogout();
        currentSession = null;
        if (callback != null) {
            callback.onExit();
        } else {
            if (callbackAuthorization != null) {
                callbackAuthorization.onExit();
            }
        }
    }


    /*public static String getIMEITelephone() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            return telephonyManager.getDeviceId().toString();
        }
        return "";
    }*/



}
