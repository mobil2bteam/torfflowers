package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBaseDirectories;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerBaseDirectories;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterBaseDirectories extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private ArrayList<? extends ObjBaseDirectories> mList;
    ClickListenerBaseDirectories onClickListener;

    public AdapterBaseDirectories(Context context, ArrayList<? extends ObjBaseDirectories> list) {
        this.mContext = context;
        this.mList = list;
    }


    public void setOnClickListener(ClickListenerBaseDirectories onClickListener){
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view3 = inflater.inflate(R.layout.item_base_directories, parent, false);
        return new ViewHolderOrders(view3);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderOrders viewHolder0 = (ViewHolderOrders) holder;

        final ObjBaseDirectories obj = getItem(position);

        viewHolder0.txtName.setText(obj.name);
        viewHolder0.checkBox.setChecked(obj.checkbox);
        viewHolder0.checkBox.setEnabled(false);
        viewHolder0.checkBox.setClickable(false);

        if (obj.checkbox) {
            viewHolder0.txtName.setTypeface(viewHolder0.txtName.getTypeface(), Typeface.BOLD);
        } else {
            viewHolder0.txtName.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
        }

        viewHolder0.setClickListener(onClickListener);
    }



    private ObjBaseDirectories getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderOrders extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtName;
        CheckBox checkBox;
        CardView cardView;

        ClickListenerBaseDirectories clickListener;

        public ViewHolderOrders(View itemView) {
            super(itemView);

            txtName             = (TextView)    itemView.findViewById(R.id.txtName);
            checkBox            = (CheckBox)    itemView.findViewById(R.id.checkBox);
            cardView            = (CardView)    itemView.findViewById(R.id.cardView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null){
                if (getItem(getAdapterPosition()).checkbox) {
                    getItem(getAdapterPosition()).checkbox = false;
                    txtName.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
                } else {
                    getItem(getAdapterPosition()).checkbox = true;
                    txtName.setTypeface(txtName.getTypeface(), Typeface.BOLD);
                }

                checkBox.setChecked(getItem(getAdapterPosition()).checkbox);
                clickListener.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerBaseDirectories clickListener){
            this.clickListener = clickListener;
        }

    }

}