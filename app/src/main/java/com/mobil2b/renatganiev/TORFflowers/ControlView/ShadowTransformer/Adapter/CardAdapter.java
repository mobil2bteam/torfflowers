package com.mobil2b.renatganiev.torfflowers.ControlView.ShadowTransformer.Adapter;

import android.support.v7.widget.CardView;

public interface CardAdapter {

    public final int MAX_ELEVATION_FACTOR = 8;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}