package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjPrice;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBBag;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerBagPlusMinusProductMin;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerLink;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerProductMin;
import com.mobil2b.renatganiev.torfflowers.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterCartList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private ArrayList<ObjProductMin> mList;

    private boolean complicated;

    ClickListenerLink clickListenerLink;
    ClickListenerProductMin clickListenerProductMin;
    ClickListenerBagPlusMinusProductMin clickListenerBagPlusMinusProductMin;

    public AdapterCartList(Context context, ArrayList<ObjProductMin> list, boolean compilat) {
        this.mContext = context;
        this.mList = list;
        this.complicated = compilat;
    }


    public void setOnClickListener(ClickListenerLink onClickListener){
        this.clickListenerLink = onClickListener;
    }

    public void setOnClickListenerProduct(ClickListenerProductMin onClickListener){
        this.clickListenerProductMin = onClickListener;
    }

    public void setOnClickListenerBagPlusMinusProductMin(ClickListenerBagPlusMinusProductMin onClickListener){
        this.clickListenerBagPlusMinusProductMin = onClickListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_product_cart, parent, false);

        return new ViewHolderProduct(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ViewHolderProduct viewHolder0 = (ViewHolderProduct) holder;
        final ObjProductMin obj = getItem(position);

        if (complicated) {
            viewHolder0.layout_plus_minus.setVisibility(View.GONE);
            viewHolder0.layout_total.setVisibility(View.VISIBLE);
        } else {
            viewHolder0.layout_plus_minus.setVisibility(View.VISIBLE);
            viewHolder0.layout_total.setVisibility(View.GONE);
        }

        // НАИМЕНОВАНИЕ
        viewHolder0.product_item_text.setText(obj.name);

        // ФОТОГРАФИЯ
        if (obj.image != null) {
            if (obj.image.url != null) {
                if (!obj.image.url.equals("")) {
                    Picasso.with(mContext).load(obj.image.url).into(viewHolder0.image);
                }
            }
        }

        // ЦЕНЫ
        if (obj.prices != null) {
            if (obj.prices.size() == 1) {
                ObjPrice price = obj.prices.get(0);
                setPrice(price, viewHolder0.txtPrice, viewHolder0.txtOldPrice, viewHolder0.layout_only_price);
                viewHolder0.txtCount.setText(String.valueOf(price.count));
                viewHolder0.txtTotalCount.setText("Кол-во: ".concat(String.valueOf(price.count)));
                if (price.total == 0) {
                    double totla = price.count * price.price;
                    viewHolder0.txtTotal.setText(String.valueOf(totla).concat(" ").concat(mContext.getResources().getString(R.string.price_rub)));
                } else {
                    viewHolder0.txtTotal.setText(String.valueOf(price.total).concat(" ").concat(mContext.getResources().getString(R.string.price_rub)));
                }
                if (price.sumSalePercent != 0) {
                    viewHolder0.txtDiscount.setText(String.valueOf((int)Math.round(price.sumSalePercent)).concat("%"));
                    viewHolder0.txtDiscount.setVisibility(View.VISIBLE);
                } else {
                    viewHolder0.txtDiscount.setVisibility(View.GONE);
                }
            }
        }

        viewHolder0.layout_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (obj.prices != null) {
                    if (obj.prices.size() >= 1) {
                        ObjPrice price = obj.prices.get(0);

                        DBBag bag = new DBBag(mContext);
                        price.count = bag.getCount(price.id);
                        price.count = price.count + 1;
                        bag.add(obj.id, price.id, price.count);
                        bag.close();

                        viewHolder0.txtCount.setText(String.valueOf(price.count));

                        clickListenerBagPlusMinusProductMin.onClickPlus(view, obj, price);

                    }
                }
            }
        });

        viewHolder0.layout_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (obj.prices != null) {
                    if (obj.prices.size() >= 1) {
                        ObjPrice price = obj.prices.get(0);

                        if (price.count >= 1) {
                            DBBag bag = new DBBag(mContext);
                            price.count = bag.getCount(price.id);
                            price.count = price.count - 1;
                            if (price.count <= 0) {
                                bag.deleteOffer(price.id);
                                bag.close();
                                clickListenerBagPlusMinusProductMin.onRemovePrice(view, obj, price);
                            } else {
                                bag.add(obj.id, price.id, price.count);
                                bag.close();
                                clickListenerBagPlusMinusProductMin.onClickMinus(view, obj, price);
                                viewHolder0.txtCount.setText(String.valueOf(price.count));
                            }
                        }
                    }
                }
            }
        });


        viewHolder0.setClickListener(clickListenerProductMin);
        viewHolder0.setClickListenerBagPlusMinusProductMin(clickListenerBagPlusMinusProductMin);
    }

    void setPrice(ObjPrice price, TextView txtPrice, TextView txtOldPrice, LinearLayout layout_price) {
        if (price.oldPrice != 0) {
            String oldPriceText = "";
            oldPriceText = String.valueOf(price.oldPrice);//.concat(" ").concat(mContext.getResources().getString(R.string.price_rub));

            txtOldPrice.setText(oldPriceText);
            txtOldPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            txtOldPrice.setVisibility(View.VISIBLE);

            String PriceText = String.valueOf(price.price).concat(" ").concat(mContext.getResources().getString(R.string.price_rub));
            txtPrice.setText(PriceText);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                txtPrice.setTextColor(mContext.getResources().getColorStateList(R.color.colorTextDiscountPrice));
            } else {
                txtPrice.setTextColor(ResourcesCompat.getColorStateList(mContext.getResources(), R.color.colorTextDiscountPrice, null));
            }
        } else {
            txtOldPrice.setVisibility(View.GONE);
            String PriceText = String.valueOf(price.price).concat(" ").concat(mContext.getResources().getString(R.string.price_rub));
            txtPrice.setText(PriceText);
        }

        layout_price.setVisibility(View.VISIBLE);
    }



    private ObjProductMin getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderProduct extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image;
        TextView product_item_text, txtPrice, txtOldPrice, txtCount, txtDiscount;
        LinearLayout layout_only_price;
        LinearLayout layout_minus, layout_plus;
        LinearLayout layout_plus_minus, layout_total;
        TextView txtTotalCount, txtTotal;

        ClickListenerProductMin clickListenerProductMin;
        ClickListenerBagPlusMinusProductMin clickListenerBagPlusMinusProductMin;

        public ViewHolderProduct(View itemView) {
            super(itemView);
            image                       = (ImageView)       itemView.findViewById(R.id.imgImage);
            product_item_text           = (TextView)        itemView.findViewById(R.id.product_item_text);
            txtPrice                    = (TextView)        itemView.findViewById(R.id.txtPrice);
            txtOldPrice                 = (TextView)        itemView.findViewById(R.id.txtOldPrice);
            txtCount                    = (TextView)        itemView.findViewById(R.id.txtCount);
            txtTotalCount               = (TextView)        itemView.findViewById(R.id.txtTotalCount);
            txtTotal                    = (TextView)        itemView.findViewById(R.id.txtTotal);
            txtDiscount                 = (TextView)        itemView.findViewById(R.id.txtDiscount);
            layout_only_price           = (LinearLayout)    itemView.findViewById(R.id.layout_only_price);
            layout_minus                = (LinearLayout)    itemView.findViewById(R.id.layout_minus);
            layout_plus                 = (LinearLayout)    itemView.findViewById(R.id.layout_plus);
            layout_plus_minus           = (LinearLayout)    itemView.findViewById(R.id.layout_plus_minus);
            layout_total                = (LinearLayout)    itemView.findViewById(R.id.layout_total);

            txtOldPrice.setVisibility(View.GONE);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListenerProductMin != null) {
                clickListenerProductMin.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerProductMin clickListener){
            this.clickListenerProductMin = clickListener;
        }
        public void setClickListenerBagPlusMinusProductMin(ClickListenerBagPlusMinusProductMin clickListener) {
            this.clickListenerBagPlusMinusProductMin = clickListener;
        }

    }
}