package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.DeliveryAddress.ObjDeliveryAddress;

import java.util.ArrayList;


/**
 * Created by renatganiev on 14.10.15.
 */
public class DBAddressHistory {

    private ContentValues cv;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private Context mContext;
    private String TABLE = "address_history";

    public DBAddressHistory(Context context) {
        // создаем объект для данных
        cv          = new ContentValues();
        // создаем объект для создания и управления версиями БД
        dbHelper    = new DBHelper(context);
        // подключаемся к БД
        db          = dbHelper.getWritableDatabase();
        mContext    = context;
    }

    // ДОБАВЛЯЕМ
    public long add(ObjDeliveryAddress address) {

        if (exist(address)) return 0;

        cv          = new ContentValues();
        cv.put("city_id",   address.city_id);
        cv.put("zip_code",  address.zip_code);
        cv.put("street",    address.street);
        cv.put("home",      address.home);
        cv.put("office",    address.office);
        cv.put("comment",   address.comment);

        long rowID = db.insert(TABLE, null, cv);
        return rowID;
    }

    // ПОЛУЧАЕМ
    public ArrayList<ObjDeliveryAddress> getList(int city_id) {
        String query    = "";
        if (city_id == 0) {
            query    = "SELECT * FROM "+TABLE+" ORDER BY id DESC";
        } else {
            query    = "SELECT * FROM "+TABLE+" WHERE city_id = "+String.valueOf(city_id)+" ORDER BY id DESC";
        }
        Cursor cursor1  = db.rawQuery(query, null);

        ArrayList<ObjDeliveryAddress> listResult = new ArrayList<>();
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {

                ObjDeliveryAddress objDeliveryAddress = new ObjDeliveryAddress();
                objDeliveryAddress.id       = cursor1.getInt(cursor1.getColumnIndex("id"));
                objDeliveryAddress.city_id  = cursor1.getInt(cursor1.getColumnIndex("city_id"));
                objDeliveryAddress.zip_code = cursor1.getString(cursor1.getColumnIndex("zip_code"));
                objDeliveryAddress.street   = cursor1.getString(cursor1.getColumnIndex("street"));
                objDeliveryAddress.home     = cursor1.getString(cursor1.getColumnIndex("home"));
                objDeliveryAddress.office   = cursor1.getString(cursor1.getColumnIndex("office"));
                objDeliveryAddress.comment  = cursor1.getString(cursor1.getColumnIndex("comment"));
                listResult.add(objDeliveryAddress);
            }
        }
        return listResult;
    }


    // ПОЛУЧАЕМ
    public ObjDeliveryAddress getAddress(int address_id) {
        String query    = "SELECT * FROM "+TABLE+" WHERE id = "+String.valueOf(address_id)+" ORDER BY id DESC";

        Cursor cursor1  = db.rawQuery(query, null);

        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {

                ObjDeliveryAddress objDeliveryAddress = new ObjDeliveryAddress();
                objDeliveryAddress.id       = cursor1.getInt(cursor1.getColumnIndex("id"));
                objDeliveryAddress.city_id  = cursor1.getInt(cursor1.getColumnIndex("city_id"));
                objDeliveryAddress.zip_code = cursor1.getString(cursor1.getColumnIndex("zip_code"));
                objDeliveryAddress.street   = cursor1.getString(cursor1.getColumnIndex("street"));
                objDeliveryAddress.home     = cursor1.getString(cursor1.getColumnIndex("home"));
                objDeliveryAddress.office   = cursor1.getString(cursor1.getColumnIndex("office"));
                objDeliveryAddress.comment  = cursor1.getString(cursor1.getColumnIndex("comment"));

                return objDeliveryAddress;
            }
        }
        return null;
    }

    // ПРОВЕРЯЕМ
    public boolean exist(ObjDeliveryAddress value) {
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE (city_id = "+String.valueOf(value.city_id)+" AND street = '"+value.street+"' AND home = '"+value.home+"' AND office = '"+value.office+"')");
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // ПОЛУЧАЕМ ДАННЫЕ
    public ObjDeliveryAddress getAddressToObj(ObjDeliveryAddress value) {
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE city_id = "+String.valueOf(value.city_id)+" AND street = '"+value.street+"' AND home = '"+value.home+"' AND office = '"+value.office+"'");
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {

                ObjDeliveryAddress objDeliveryAddress = new ObjDeliveryAddress();
                objDeliveryAddress.id       = cursor1.getInt(cursor1.getColumnIndex("id"));
                objDeliveryAddress.city_id  = cursor1.getInt(cursor1.getColumnIndex("city_id"));
                objDeliveryAddress.zip_code = cursor1.getString(cursor1.getColumnIndex("zip_code"));
                objDeliveryAddress.street   = cursor1.getString(cursor1.getColumnIndex("street"));
                objDeliveryAddress.home     = cursor1.getString(cursor1.getColumnIndex("home"));
                objDeliveryAddress.office   = cursor1.getString(cursor1.getColumnIndex("office"));
                objDeliveryAddress.comment  = cursor1.getString(cursor1.getColumnIndex("comment"));

                return objDeliveryAddress;
            }
        }
        return null;
    }

    // УДАЛЯЕМ
    public boolean delete(ObjDeliveryAddress value) {
        int clearCount = db.delete(TABLE, "id = '".concat(String.valueOf(value.id)).concat("'"), null);
        if (clearCount > 0) {
            return true;
        } else {
            return false;
        }
    }


    // ОЧИСТИТЬ
    public int clear() {
        int clearCount = db.delete(TABLE, null, null);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE + "'");
        return clearCount;
    }

    public void close() {
        db.close();
    }

}
