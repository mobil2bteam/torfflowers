package com.mobil2b.renatganiev.torfflowers.Fragments.User.Info;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.GroupView.ObjGroupView;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBWatch;
import com.mobil2b.renatganiev.torfflowers.user_order_list;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjCards.ObjCards;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.ControlView.GroupProduct.ProductView;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.city_select;
import com.mobil2b.renatganiev.torfflowers.menu_info_item;
import com.mobil2b.renatganiev.torfflowers.user_update;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_user_info extends Fragment {

    public static final String TAG = "fragment_app_user_info";
    public static final int LAYOUT = R.layout.fragment_app_user_info;
    View rootView;
    public CallbackAction callbackActionSuper;

    LinearLayout layout_select_city, layout_group_view_watched, layout_order_history,
                    layout_information, layout_user_ball, layout_user_partner, layout_partner_info;
    TextView txtUserName, txtUserEmail, txtOrderHistory, txtBall, txtPartner, txtInformation, txtFlagPartner,
            txtUserPartnerPromo, txtPromocodeInfo;
    ImageView imgLogo, imgVerify;
    Button btnSettingsProfil, btnExit, btnShared;
    DeliveryCityView deliveryCityView;

    public static fragment_app_user_info getInstance(Bundle args) {
        fragment_app_user_info fragment = new fragment_app_user_info();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        btnSettingsProfil       = rootView.findViewById(R.id.btnSettingsProfil);
        btnExit                 = rootView.findViewById(R.id.btnExit);
        btnShared               = rootView.findViewById(R.id.btnShared);

        txtUserName             = rootView.findViewById(R.id.txtUserName);
        txtUserEmail            = rootView.findViewById(R.id.txtUserEmail);
        txtOrderHistory         = rootView.findViewById(R.id.txtOrderHistory);
        txtBall                 = rootView.findViewById(R.id.txtBall);
        txtPartner              = rootView.findViewById(R.id.txtPartner);
        txtInformation          = rootView.findViewById(R.id.txtInformation);
        txtFlagPartner          = rootView.findViewById(R.id.txtFlagPartner);
        txtUserPartnerPromo     = rootView.findViewById(R.id.txtUserPartnerPromo);
        txtPromocodeInfo        = rootView.findViewById(R.id.txtPromocodeInfo);

        imgLogo                 = rootView.findViewById(R.id.imgLogo);
        imgVerify               = rootView.findViewById(R.id.imgVerify);

        layout_select_city      = rootView.findViewById(R.id.layout_select_city);
        layout_group_view_watched= rootView.findViewById(R.id.layout_group_view_watched);
        layout_order_history    = rootView.findViewById(R.id.layout_order_history);
        layout_information      = rootView.findViewById(R.id.layout_information);
        layout_user_ball        = rootView.findViewById(R.id.layout_user_ball);
        layout_user_partner     = rootView.findViewById(R.id.layout_user_partner);
        layout_partner_info     = rootView.findViewById(R.id.layout_partner_info);


        updateView();

        btnSettingsProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), user_update.class);
                startActivity(i);
            }
        });


        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                User.onLogout(null);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Выйти из аккаунта?").setPositiveButton("Да", dialogClickListener)
                        .setNegativeButton("Нет", dialogClickListener).show();

            }
        });


        layout_order_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if (callbackActionSuper != null) {
                //    callbackActionSuper.onAction(TAG, PublicData.USER_ORDER_HISTORY, new Bundle());
                //}
                Intent intent = new Intent(getActivity(), user_order_list.class);
                startActivity(intent);
            }
        });

        btnShared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (User.getCurrentSession().promocode != null) {
                    if (Helps.isNotEmpty(User.getCurrentSession().promocode.code)) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, "Дарю тебе скидку на всю продукцию компании BeautyGroup по промо-коду: ".concat(User.getCurrentSession().promocode.code));
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent,"Поделиться"));
                    }
                }
            }
        });


        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        deliveryCityView.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PublicData.REZULT_REQUEST_REFRESH) {
            if (resultCode == RESULT_OK) {
                updateView();
            }
        }
    }

    void updateView() {
        if (User.getCurrentSession() != null) {
            txtUserName.setText(User.getCurrentSession().user_name);
            txtUserEmail.setText(User.getCurrentSession().email);
            if (User.getCurrentSession().verify) {
                imgVerify.setVisibility(View.VISIBLE);
                layout_information.setVisibility(View.GONE);
                layout_partner_info.setVisibility(View.GONE);
                layout_order_history.setVisibility(View.VISIBLE);
                layout_user_ball.setVisibility(View.VISIBLE);
                txtFlagPartner.setVisibility(View.GONE);
                txtPromocodeInfo.setVisibility(View.GONE);

                if ((!User.getCurrentSession().partner) && (!User.getCurrentSession().partner_feed)) {
                    // Заявки о партнерстве еще нет и пользователь не партнер
                    if (PublicData.SETTINGS != null) {
                        if (PublicData.SETTINGS.partner_info != null) {
                            if (Helps.isNotEmpty(PublicData.SETTINGS.partner_info.name)) {
                                txtPartner.setText(PublicData.SETTINGS.partner_info.name);
                                layout_user_partner.setVisibility(View.VISIBLE);

                                layout_user_partner.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getActivity(), menu_info_item.class);
                                        intent.putExtra("id", PublicData.SETTINGS.partner_info.id);
                                        intent.putExtra("partner", true);
                                        startActivityForResult(intent, PublicData.REZULT_REQUEST_REFRESH);
                                    }
                                });

                            }
                        }
                    }
                } else {
                    if ((!User.getCurrentSession().partner) && (User.getCurrentSession().partner_feed)) {
                        // Заявки о партнерстве есть но пользователь не партнер
                        layout_user_partner.setVisibility(View.GONE);

                        txtInformation.setText(getString(R.string.str_information_for_partner));
                        layout_information.setVisibility(View.VISIBLE);
                        layout_partner_info.setVisibility(View.GONE);
                    } else {
                        if (User.getCurrentSession().partner) {
                            // Пользователь партнер
                            txtFlagPartner.setVisibility(View.VISIBLE);
                            imgVerify.setVisibility(View.GONE);

                            if (User.getCurrentSession().promocode != null) {
                                txtPartner.setText("Ваш промо-код партнера:");
                                layout_partner_info.setVisibility(View.VISIBLE);
                                txtUserPartnerPromo.setText(User.getCurrentSession().promocode.code);

                                layout_user_partner.setVisibility(View.VISIBLE);

                                txtPromocodeInfo.setText("Размер скидки по промо-коду: ".concat(String.valueOf(User.getCurrentSession().promocode.percent).concat("%")));
                                txtPromocodeInfo.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                }
            } else {
                imgVerify.setVisibility(View.GONE);
                txtFlagPartner.setVisibility(View.GONE);

                txtInformation.setText(getString(R.string.str_information));
                layout_information.setVisibility(View.VISIBLE);

                layout_order_history.setVisibility(View.GONE);
                layout_user_ball.setVisibility(View.GONE);
                layout_user_partner.setVisibility(View.GONE);
                layout_partner_info.setVisibility(View.GONE);
            }
        }

        layout_user_ball.setVisibility(View.GONE);
        if (User.getCurrentSession() != null) {
            if (User.getCurrentSession().cards != null) {
                if (User.getCurrentSession().cards.size() > 0) {
                    for (ObjCards objCards : User.getCurrentSession().cards) {
                        if (objCards.type.equals(ObjCards.TYPE_CARD_BALL)) {
                            txtBall.setText("Ваши баллы: ".concat(String.valueOf(objCards.balance)));
                            layout_user_ball.setVisibility(View.VISIBLE);
                            break;
                        }
                    }
                }
            }
        }

        if (PublicData.HOME != null) {
            if (PublicData.HOME.flag_show_city_delivery) {
                deliveryCityView = new DeliveryCityView(getContext(), PublicData.SETTINGS);
                deliveryCityView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getActivity(), city_select.class);
                        startActivityForResult(i, deliveryCityView.REZULT_REQUEST_SELECT_CITY);
                    }
                });
                layout_select_city.removeAllViews();
                layout_select_city.addView(deliveryCityView);
                layout_select_city.setVisibility(View.VISIBLE);
            }

            if (PublicData.HOME.flag_show_watched) {
                DBWatch watch = new DBWatch(getContext());
                ArrayList<ObjProductMin> list_product = watch.getList();
                watch.close();
                if (list_product.size() > 0) {
                    //moduleObjects.getUpdateListProduct(false, list_id);

                    if (list_product.size() != 0) {
                        ObjGroupView objGroupView = new ObjGroupView();
                        objGroupView.button_more = false;
                        objGroupView.link = null;
                        objGroupView.name = "Вы недавно смотрели";
                        objGroupView.count_items_display = 2f;
                        objGroupView.items.addAll(list_product);


                        ProductView productView = new ProductView(getContext(), objGroupView);
                        layout_group_view_watched.addView(productView);
                    }

                }
            }
        }

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    //moduleObjects

}
