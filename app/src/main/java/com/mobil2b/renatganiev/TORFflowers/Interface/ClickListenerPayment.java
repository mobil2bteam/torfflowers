package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Payment.ObjPayment;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerPayment {
    void onClick(View view, ObjPayment objPayment);
}
