package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;


/**
 * Created by renatganiev on 25.04.16.
 * TODO: Класс возвращает тип данных который пришел, чтобы не городить в каждом окне.
 *
 */

public class OpenBlocks {

    private static volatile OpenBlocks sInstance;

    public static Context mContext;
    public static OpenBlocks instance() {
        return sInstance;
    }

    public static void  initialize(Context ctx) {
        // Double checked locking singleton, for thread safety VKSdk.initialize() calls
        if (sInstance == null) {
            synchronized (PublicData.class) {
                if (sInstance == null) {
                    sInstance = new OpenBlocks();
                }
            }
        }
        mContext = ctx;
    }

    public static String getOpenData(String jsonObj) {
        try {
            JSONObject json = new JSONObject(jsonObj);
            return getOpenData(json);
        } catch (JSONException e) {
            // Произошла какая то хуйня.
        }
        // Ошибка или неизвестный ответ.
        return null;
    }

    public static boolean getExistData(String jsonObj, String URL_TYPE) {
        try {
            JSONObject json = new JSONObject(jsonObj);
            return getExistData(json, URL_TYPE);
        } catch (JSONException e) {
            // Произошла какая то хуйня.
        }
        // Ошибка или неизвестный ответ.
        return false;
    }

    public static String getOpenData(JSONObject jsonObj) {
        if (jsonObj.has("options")) {
            // Это главная страница
            return PublicData.QUESTION_TYPE_URL;
        }
        if (jsonObj.has("catalog")) {
            // Это главная страница
            return PublicData.QUESTION_TYPE_CATALOG;
        }
        if (jsonObj.has("settings")) {
            // Это каталог
            return PublicData.QUESTION_TYPE_SETTINGS;
        }
        if (jsonObj.has("home")) {
            // Это каталог
            return PublicData.QUESTION_TYPE_HOME;
        }
        if (jsonObj.has("user")) {
            // Это каталог
            return PublicData.QUESTION_TYPE_USER_LOGIN;
        }
        if (jsonObj.has("products")) {
            // Это каталог
            return PublicData.QUESTION_TYPE_PRODUCTS;
        }
        if (jsonObj.has("product")) {
            // Это каталог
            return PublicData.QUESTION_TYPE_PRODUCT;
        }
        if (jsonObj.has("order")) {
            return PublicData.QUESTION_TYPE_ORDER;
        }
        if (jsonObj.has("orders")) {
            return PublicData.QUESTION_TYPE_USER_ORDER_LIST;
        }
        if (jsonObj.has("group_news")) {
            return PublicData.QUESTION_TYPE_NEWS_LIST;
        }
        if (jsonObj.has("group_articles")) {
            return PublicData.QUESTION_TYPE_ARTICLE_LIST;
        }
        if (jsonObj.has("news_item")) {
            return PublicData.QUESTION_TYPE_NEWS_ITEM;
        }
        if (jsonObj.has("articles_item")) {
            return PublicData.QUESTION_TYPE_ARTICLE_ITEM;
        }
        if (jsonObj.has("comments")) {
            return PublicData.QUESTION_TYPE_COMMENT_LIST;
        }
        if (jsonObj.has("cart")) {
            return PublicData.QUESTION_TYPE_GET_BAG;
        }
        if (jsonObj.has("menu_info")) {
            return PublicData.QUESTION_TYPE_INFORMATION;
        }
        if (jsonObj.has("success")) {
            return PublicData.QUESTION_TYPE_SUCCESS;
        }
        // Пришел неизвестный ответ.
        return null;
    }

    public static boolean getExistData(JSONObject jsonObj, String URL_TYPE) {
        if (URL_TYPE.equals(PublicData.QUESTION_TYPE_URL)) {
            if (jsonObj.has("options")) {
                // Это главная страница
                return true;
            }
        }
        if (URL_TYPE.equals(PublicData.QUESTION_TYPE_CATALOG)) {
            if (jsonObj.has("catalog")) {
                return true;
            }
        }
        if (URL_TYPE.equals(PublicData.QUESTION_TYPE_SETTINGS)) {
            if (jsonObj.has("settings")) {
                return true;
            }
        }
        if (URL_TYPE.equals(PublicData.QUESTION_TYPE_HOME)) {
            if (jsonObj.has("home")) {
                return true;
            }
        }
        if (URL_TYPE.equals(PublicData.QUESTION_TYPE_USER_LOGIN)) {
            if (jsonObj.has("user")) {
                return true;
            }
        }
        if (URL_TYPE.equals(PublicData.QUESTION_TYPE_PRODUCTS)) {
            if (jsonObj.has("products")) {
                return true;
            }
        }
        if (URL_TYPE.equals(PublicData.QUESTION_TYPE_PRODUCT)) {
            if (jsonObj.has("product")) {
                return true;
            }
        }
        if (URL_TYPE.equals(PublicData.QUESTION_TYPE_ORDER)) {
            if (jsonObj.has("order")) {
                return true;
            }
        }

        // Пришел неизвестный ответ.
        return false;
    }


    public static String getLinkURL(ObjLink link) {
        String result = "";

        if (link != null) {
            if (link.url != null) {
                if (link.url.name != null) {
                    return link.url.name;
                }
            }
        }

        return result;
    }


}
