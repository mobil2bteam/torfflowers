package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjBrand extends ObjBaseDirectories implements Serializable {

    //public int      id;
    //public String   name = "";
    public ObjImage image;

    public ObjBrand() {
        init();
    }
    public ObjBrand(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjBrand(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("brand")) {
                JSONObject jsonMain = json.getJSONObject("brand");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                super.id = json.getInt("id");
            }
            if (json.has("name")) {
                super.name = json.getString("name");
            }
            if (json.has("image")) {
                image = new ObjImage(json.getJSONObject("image"));
            }
        } catch (JSONException e) {
        }
    }

}
