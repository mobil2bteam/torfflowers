package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjImage implements Serializable {

    public String   url = "";
    public int      width, heigth;


    public ObjImage() {
        init();
    }
    public ObjImage(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjImage(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("age")) {
                JSONObject jsonMain = json.getJSONObject("age");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("width")) {
                width = json.getInt("width");
            }
            if (json.has("heigth")) {
                heigth = json.getInt("heigth");
            }
            if (json.has("url")) {
                url = json.getString("url");
            }
        } catch (JSONException e) {
        }
    }

}
