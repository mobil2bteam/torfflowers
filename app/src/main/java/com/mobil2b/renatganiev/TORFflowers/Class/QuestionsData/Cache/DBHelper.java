package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by renatganiev on 14.10.15.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, "DBCache", null, 10);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("DBHelper", "--- onCreate database ---");
        // создаем таблицу с полями
        db.execSQL("create table cache ("
                + "id integer primary key autoincrement,"
                + "cache_key    TEXT,"
                + "cache_date   LONG, "
                + "cache_value  TEXT);");

        db.execSQL("create table favorites ("
                + "id integer primary key autoincrement,"
                + "product_id int);");

        db.execSQL("create table watch ("
                + "id integer primary key autoincrement,"
                + "name         TEXT,"
                + "image        TEXT,"
                + "product_id int);");

        db.execSQL("create table order_list ("
                + "id integer primary key autoincrement,"
                + "order_date text);");

        db.execSQL("create table search_history ("
                + "id integer primary key autoincrement,"
                + "value_text   TEXT);");

        db.execSQL("create table bag ("
                + "id integer primary key autoincrement,"
                + "product_id int,"
                + "offer_id int,"
                + "offer_count int);");

        db.execSQL("create table address_history ("
                + "id integer primary key autoincrement,"
                + "city_id int,"
                + "zip_code TEXT,"
                + "street TEXT,"
                + "home TEXT,"
                + "office TEXT,"
                + "comment   TEXT);");


        db.execSQL("create table recipient_history ("
                + "id integer primary key autoincrement,"
                + "phone TEXT,"
                + "name  TEXT);");


        /*db.execSQL("create table promocode ("
                + "id integer primary key autoincrement,"
                + "id_user int,"
                + "promo_code text);");

        db.execSQL("create table history_orders ("
                + "id integer primary key autoincrement,"
                + "client_id text,"
                + "location_id text,"
                + "country text,"
                + "region text,"
                + "city text,"
                + "zip_code text,"
                + "address text);");*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Запишем в журнал
        Log.w("SQLite", "Обновляемся с версии " + oldVersion + " на версию " + newVersion);

        // Удаляем старую таблицу и создаём новую
        db.execSQL("DROP TABLE IF EXISTS cache");
        db.execSQL("DROP TABLE IF EXISTS favorites");
        db.execSQL("DROP TABLE IF EXISTS watch");
        db.execSQL("DROP TABLE IF EXISTS bag");
        db.execSQL("DROP TABLE IF EXISTS order_list");
        db.execSQL("DROP TABLE IF EXISTS search_history");
        db.execSQL("DROP TABLE IF EXISTS address_history");
        db.execSQL("DROP TABLE IF EXISTS recipient_history");

        /*db.execSQL("DROP TABLE IF EXISTS history_orders");
        db.execSQL("DROP TABLE IF EXISTS promocode");*/
        // Создаём новую таблицу
        onCreate(db);
    }
}
