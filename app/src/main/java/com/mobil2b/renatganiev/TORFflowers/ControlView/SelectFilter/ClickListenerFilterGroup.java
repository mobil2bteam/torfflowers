package com.mobil2b.renatganiev.torfflowers.ControlView.SelectFilter;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBaseDirectories;

import java.util.ArrayList;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerFilterGroup {
    void onClick(View view, ArrayList<? extends ObjBaseDirectories> objBaseDirectories, int request_code, String name);
}
