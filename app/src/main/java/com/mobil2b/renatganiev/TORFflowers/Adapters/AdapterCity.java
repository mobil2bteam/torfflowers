package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;



/**
 * Created by renatganiev on 05.04.16.
 */
public class AdapterCity extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    ArrayList<ObjCity> objects;

    int LAYOUT_ITEM = R.layout.item_city;

    public AdapterCity(Context context, ArrayList<ObjCity> city) {
        ctx = context;
        objects = city;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // кол-во элементов
    @Override
    public int getCount() {
        return objects.size();
    }

    // элемент по позиции
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position) {
        return position;
    }

    // пункт списка
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // используем созданные, но не используемые view
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(LAYOUT_ITEM, parent, false);
        }
        ObjCity p = getProduct(position);
        //((TextView) view.findViewById(R.id.txtName)).setTypeface(PublicData.tf_custom_font);
        ((TextView) view.findViewById(R.id.txtCity)).setText(p.name);
        if (p.country == null) {
            ((TextView) view.findViewById(R.id.txtCountry)).setVisibility(View.GONE);
        } else {
            if (p.country.name != null) {
                ((TextView) view.findViewById(R.id.txtCountry)).setText(p.country.name);
                ((TextView) view.findViewById(R.id.txtCountry)).setVisibility(View.VISIBLE);
            } else {
                ((TextView) view.findViewById(R.id.txtCountry)).setVisibility(View.GONE);
            }
        }
        return view;
    }

    // товар по позиции
    ObjCity getProduct(int position) {
        return ((ObjCity) getItem(position));
    }

    // обработчик для чекбоксов
    CompoundButton.OnCheckedChangeListener myCheckChangList = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            // меняем данные товара (в корзине или нет)
            //getProduct((Integer) buttonView.getTag()).box = isChecked;
        }
    };
}

