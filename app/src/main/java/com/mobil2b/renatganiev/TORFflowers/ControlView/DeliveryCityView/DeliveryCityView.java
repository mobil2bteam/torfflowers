package com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.ObjSettings;
import com.mobil2b.renatganiev.torfflowers.R;

import static android.app.Activity.RESULT_OK;


/**
 * Created by renatganiev on 10.10.15.
 */
public class DeliveryCityView extends RelativeLayout implements View.OnClickListener {

    public static final int REZULT_REQUEST_SELECT_CITY                        = 600;
    Context             mContext;
    ObjSettings         objSettings;
    TextView txtCitySelect, txtChange;
    LinearLayout layout_change_city;


    OnClickListener _wrappedOnClickListener;

    public DeliveryCityView(Context context, ObjSettings obj) {
        super(context);
        super.setOnClickListener(this);

        mContext    = context;
        objSettings = obj;


        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_delivery_city, this);

        txtCitySelect               = (TextView) findViewById(R.id.txtCitySelect);
        txtChange                   = (TextView) findViewById(R.id.txtChange);
        layout_change_city          = (LinearLayout) findViewById(R.id.layout_change_city);

        layout_change_city.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_wrappedOnClickListener != null) {

                    if (PublicData.SETTINGS != null) {
                        if (PublicData.SETTINGS.change_city) {
                            _wrappedOnClickListener.onClick(view);
                        } else {
                            Toast.makeText(mContext, mContext.getString(R.string.str_message_city), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

        txtChange.setVisibility(GONE);
        if (PublicData.SETTINGS != null) {
            if (PublicData.SETTINGS.change_city) {
                txtChange.setVisibility(VISIBLE);
            }
        }


        updateCityName();
    }

    void updateCityName() {
        ObjCity city = getCity();
        if (city != null) {
            txtCitySelect.setText("Ваш город: ".concat(city.name));
        }
    }


    public ObjCity getCity() {
        int city_id = getCitySelect();

        if (city_id != 0) {
            if (PublicData.DIRECTORIES != null) {
                if (PublicData.DIRECTORIES.city != null) {
                    for (ObjCity objCity : PublicData.DIRECTORIES.city) {
                        if (objCity.id == city_id) {
                            return objCity;
                        }
                    }
                }
            }
        }
        return null;
    }

    public void setCitySelect(int city_id) {
        SharedPreferences getSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor e = getSharedPreferences.edit();
        e.putInt("city_id", city_id);
        e.apply();
        updateCityName();
    }

    int getCitySelect() {
        SharedPreferences getSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int city = getSharedPreferences.getInt("city_id", 0);
        if (city == 0) {
            if (objSettings != null) {
                city = objSettings.default_city;
            }
        }
        return city;
    }

    @Override
    public void onClick(View view) {
        if (_wrappedOnClickListener != null)
            if (PublicData.SETTINGS != null) {
                if (PublicData.SETTINGS.change_city) {
                    _wrappedOnClickListener.onClick(view);
                } else {
                    Toast.makeText(mContext, mContext.getString(R.string.str_message_city), Toast.LENGTH_LONG).show();
                }
            }
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        _wrappedOnClickListener = l;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REZULT_REQUEST_SELECT_CITY) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle.containsKey("city_id")) {
                        int selectIdCity = bundle.getInt("city_id", 0);
                        if (selectIdCity != 0) {
                            setCitySelect(selectIdCity);
                        }
                    }
                }
            }
        }
    }
}