package com.mobil2b.renatganiev.torfflowers.Class.MarkerManager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobil2b.renatganiev.torfflowers.Class.BitmapMaster;
import com.mobil2b.renatganiev.torfflowers.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;

/**
 * Created by renatganiev on 24.12.17.
 */

public class MarkerManager {

    LatLng mPosition;
    Bitmap  mBackground;
    Bitmap  mPoint;
    Rect    mRect;
    String  mUrlImagePoint;
    CallbackListenerMarker callbackListenerMarker;
    GoogleMap mMap;
    Context mContext;
    int objId;

    public static HashMap<Marker, Integer> mHashMap        = new HashMap<Marker, Integer>();

    public MarkerManager(Context context) {
        mContext                = context;
    }

    public void createMarker(LatLng position, int id, Bitmap background, Rect rect, GoogleMap map) {
        mPosition               = position;
        mBackground             = background.copy(Bitmap.Config.ARGB_8888, true);
        mPoint                  = null;
        mRect                   = rect;
        mMap                    = map;
        objId                   = id;

        createMarker();
    }

    public void createMarker(LatLng position, int id, Bitmap background, Bitmap point, Rect rect, GoogleMap map) {
        mPosition               = position;
        mBackground             = background.copy(Bitmap.Config.ARGB_8888, true);
        if (point != null) {
            mPoint                  = point.copy(Bitmap.Config.ARGB_8888, true);
        }
        mRect                   = rect;
        mMap                    = map;
        objId                   = id;

        createMarker();
    }


    public void createMarker(LatLng position, int id, Bitmap background, String urlImagePoint, Rect rect, GoogleMap map) {
        mPosition               = position;
        mBackground             = background.copy(Bitmap.Config.ARGB_8888, true);
        mUrlImagePoint          = urlImagePoint;
        mRect                   = rect;
        mMap                    = map;
        objId                   = id;

        loadingPoint();
    }


    private void createMarker() {

        Canvas canvas1;
        if (mBackground != null) {
            mBackground = BitmapMaster.resize(mBackground, 100, 134);
            canvas1 = new Canvas(mBackground);
            if (mPoint != null) {
                if (mRect != null) {
                    mPoint = BitmapMaster.getRectBitmap(mPoint, mRect.width());
                    canvas1.drawBitmap(mPoint, mRect.left, mRect.top, null);
                } else {
                    canvas1.drawBitmap(mPoint, 0, 0, null);
                }
            }
        } else {
            if (mPoint != null) {
                if (mRect != null) {
                    mPoint = BitmapMaster.getRectBitmap(mPoint, mRect.width());
                }
            }
        }

        MarkerOptions markerOptions;
        if (mBackground != null) {
            markerOptions = new MarkerOptions()
                    .position(mPosition)
                    .icon(BitmapDescriptorFactory.fromBitmap(mBackground))
                    .anchor(0.5f, 1);
        } else {
            if (mPoint != null) {
                markerOptions = new MarkerOptions()
                        .position(mPosition)
                        .icon(BitmapDescriptorFactory.fromBitmap(mPoint))
                        .anchor(0.5f, 1);
            } else {
                markerOptions = new MarkerOptions()
                        .position(mPosition)
                        .anchor(0.5f, 1);
            }
        }

        Marker marker =  mMap.addMarker(markerOptions);
        marker.setTag(objId);
        mHashMap.put(marker, objId);
    }

    public int getMarkerId(Marker m) {
        return mHashMap.get(m);
    }

    private void loadingPoint() {
        Picasso.with(mContext)
                .load(mUrlImagePoint)
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round)
                .into(target);
    }

    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            mPoint = bitmap;
            createMarker();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

}
