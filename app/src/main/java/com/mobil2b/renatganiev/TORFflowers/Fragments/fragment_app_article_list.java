package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterArticle;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Article.ObjArticleItems;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Article.ObjGroupArticles;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerArticle;
import com.mobil2b.renatganiev.torfflowers.article_item;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.R;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_article_list extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_article_list";
    public static final int LAYOUT = R.layout.fragment_app_article_list;
    View rootView;

    public CallbackAction callbackActionSuper;


    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    LinearLayoutManager staggeredGridLayoutManager;
    private AdapterArticle mAdapterSearch;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public static fragment_app_article_list getInstance(Bundle args) {
        fragment_app_article_list fragment = new fragment_app_article_list();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.

        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        /*txtCaption              = (TextView)        rootView.findViewById(R.id.txtCaption);
        imgHead                 = (ImageView)       rootView.findViewById(R.id.imgHead);*/

        mSwipeRefreshLayout     = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        //initHead();
        intiRecyclerView();
        if (PublicData.NEWS != null) {
            if (PublicData.NEWS.items != null) {
                if (PublicData.NEWS.items.size() != 0) {
                    refrashAllDate();
                }
            }
        }

        loadArticle(false);

        return rootView;
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // В виде списка
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void refrashAllDate() {
        mAdapterSearch = new AdapterArticle(getActivity(), PublicData.ARTICLES.items);
        mRecyclerView.setAdapter(mAdapterSearch);
        mAdapterSearch.notifyDataSetChanged();

        mAdapterSearch.setOnClickListener(new ClickListenerArticle() {
            @Override
            public void onClick(View view, ObjArticleItems objArticleItems) {
                Intent intent = new Intent(getActivity(), article_item.class);
                intent.putExtra("id", objArticleItems.id);
                startActivity(intent);
            }
        });
    }


    void loadArticle(boolean cache) {
        ModuleObjects moduleObjects = new ModuleObjects(getActivity(), new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
                mSwipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {

                try {

                    JSONObject json = new JSONObject(result);
                    String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                    if (typeDate.equals(PublicData.QUESTION_TYPE_ARTICLE_LIST)) {

                        if (json.has("group_articles")) {
                            JSONObject jsonHome = json.getJSONObject("group_articles");
                            PublicData.ARTICLES         = new ObjGroupArticles(jsonHome);
                            // here you check the value of getActivity() and break up if needed
                            if(getActivity() == null) return;
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    if (PublicData.ARTICLES != null) {
                                        mSwipeRefreshLayout.setRefreshing(false);
                                        refrashAllDate();
                                    }
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }


            }

            @Override
            public void onError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }

            }

            @Override
            public void onHttpError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            }
        });

        moduleObjects.getArticleList(cache);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRefresh() {
        loadArticle(true);
    }


}
