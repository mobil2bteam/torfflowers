package com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.DeliveryAddress.ObjDeliveryAddress;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBAddressHistory;
import com.mobil2b.renatganiev.torfflowers.R;

import static android.app.Activity.RESULT_OK;


/**
 * Created by renatganiev on 10.10.15.
 */
public class DeliveryAddressView extends RelativeLayout implements View.OnClickListener {

    public int REZULT_REQUEST_SELECT_ADDRESS                        = 660;
    Context             mContext;
    TextView txtAddress, txtComment;
    ImageView imgClear;
    LinearLayout layout_change_city;

    OnClickListener _wrappedOnClickListener;

    public DeliveryAddressView(Context context) {
        super(context);
        super.setOnClickListener(this);

        mContext    = context;
        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_delivery_address_view, this);

        txtAddress              = (TextView) findViewById(R.id.txtAddress);
        txtComment              = (TextView) findViewById(R.id.txtComment);
        imgClear                = (ImageView) findViewById(R.id.imgClear);
        layout_change_city      = (LinearLayout) findViewById(R.id.layout_change_city);

        imgClear.setVisibility(GONE);

        layout_change_city.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_wrappedOnClickListener != null) {
                    _wrappedOnClickListener.onClick(view);
                }
            }
        });


        imgClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                clear();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Очистить?").setPositiveButton("Да", dialogClickListener)
                        .setNegativeButton("Нет", dialogClickListener).show();
            }
        });

        updateCityName();
    }

    void updateCityName() {
        ObjDeliveryAddress address = getDeliveryAddress();
        if (address != null) {
            txtAddress.setText(address.getAddress());
            txtAddress.setVisibility(VISIBLE);
            imgClear.setVisibility(VISIBLE);
            if (Helps.isNotEmpty(address.comment)) {
                txtComment.setText(address.comment);
                txtComment.setVisibility(VISIBLE);
            } else {
                txtComment.setVisibility(GONE);
            }
        } else {
            txtAddress.setVisibility(GONE);
            txtComment.setVisibility(GONE);
            imgClear.setVisibility(GONE);
        }
    }


    public ObjDeliveryAddress getDeliveryAddress() {
        int address_id = getAddressSelect();
        DBAddressHistory addressHistory = new DBAddressHistory(mContext);
        ObjDeliveryAddress objDeliveryAddress = addressHistory.getAddress(address_id);
        return objDeliveryAddress;
    }

    public void setAddressSelect(int address_id) {
        SharedPreferences getSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor e = getSharedPreferences.edit();
        e.putInt("address_id", address_id);
        e.apply();
        updateCityName();
    }

    public int getAddressSelect() {
        SharedPreferences getSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int address_id = getSharedPreferences.getInt("address_id", 0);
        return address_id;
    }

    public void clear() {
        SharedPreferences getSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor e = getSharedPreferences.edit();
        e.remove("address_id");
        e.apply();
        updateCityName();
    }

    @Override
    public void onClick(View view) {
        if (_wrappedOnClickListener != null)
            _wrappedOnClickListener.onClick(view);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        _wrappedOnClickListener = l;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REZULT_REQUEST_SELECT_ADDRESS) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle.containsKey("address_id")) {
                        int selectIdCity = bundle.getInt("address_id", 0);
                        if (selectIdCity != 0) {
                            setAddressSelect(selectIdCity);
                        }
                    }
                }
            }
        }
    }

    public void onResume() {
        updateCityName();
    }
}