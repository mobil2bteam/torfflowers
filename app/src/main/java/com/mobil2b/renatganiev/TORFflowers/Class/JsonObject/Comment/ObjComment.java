package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Comment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ObjComment {
    public int id;
    public String name;
    public String rating;
    public ObjCommentItem user_comment;
    public List<ObjCommentItem> all_comments = new ArrayList<>();

    public ObjComment() {

    }
    public ObjComment(JSONObject json) {
        loadJsonObject(json);
    }
    public ObjComment(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            json = json.getJSONObject("comments");
            loadJsonObject(json);
        } catch (JSONException e) {
        }
    }

    public void loadJsonObject(JSONObject json){
        try {

            if(json.has("id")){
                this.id = json.getInt("id");
            }

            if(json.has("name")){
                this.name = json.getString("name");
            }

            if(json.has("rating")){
                this.rating = json.getString("rating");
            }

            if (json.has("user_comment")) {
                user_comment = new ObjCommentItem(json.getJSONObject("user_comment"));
            }

            if(json.has("all_comments")){
                for (int i =0;i<json.getJSONArray("all_comments").length();i++){
                    ObjCommentItem menuItem = new ObjCommentItem();
                    menuItem.loadJsonObject(json.getJSONArray("all_comments").getJSONObject(i));
                    all_comments.add(menuItem);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
