package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


/**
 * Created by renatganiev on 14.10.15.
 */
public class DBSearchHistory {

    private ContentValues cv;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private Context mContext;
    private String TABLE = "search_history";

    public DBSearchHistory(Context context) {
        // создаем объект для данных
        cv          = new ContentValues();
        // создаем объект для создания и управления версиями БД
        dbHelper    = new DBHelper(context);
        // подключаемся к БД
        db          = dbHelper.getWritableDatabase();
        mContext    = context;
    }

    // ДОБАВЛЯЕМ
    public long add(String search_text) {

        if (exist(search_text)) return 0;

        cv          = new ContentValues();
        cv.put("value_text",         search_text);
        long rowID = db.insert(TABLE, null, cv);
        return rowID;
    }

    // ПОЛУЧАЕМ
    public ArrayList<String> getList() {
        cv          = new ContentValues();
        String query    = "SELECT * FROM "+TABLE+" ORDER BY id DESC";
        Cursor cursor1  = db.rawQuery(query, null);

        ArrayList<String> listResult = new ArrayList<>();
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {
                listResult.add(cursor1.getString(cursor1.getColumnIndex("value_text")));
            }
        }
        return listResult;
    }


    // ПРОВЕРЯЕМ
    public boolean exist(String value) {
        cv          = new ContentValues();
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE value_text = '").concat(value.trim()).concat("'");
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            return true;
        }
        return false;
    }

    // УДАЛЯЕМ
    public boolean delete(String value) {
        int clearCount = db.delete(TABLE, "value_text = '".concat(value).concat("'"), null);
        if (clearCount > 0) {
            return true;
        } else {
            return false;
        }
    }


    // ОЧИСТИТЬ
    public int clear() {
        int clearCount = db.delete(TABLE, null, null);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE + "'");
        return clearCount;
    }

    public void close() {
        db.close();
    }

}
