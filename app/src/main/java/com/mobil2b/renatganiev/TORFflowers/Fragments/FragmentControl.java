package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;

import com.mobil2b.renatganiev.torfflowers.Fragments.Bag.fragment_app_bag;
import com.mobil2b.renatganiev.torfflowers.Fragments.Order.fragment_app_delivery_select;
import com.mobil2b.renatganiev.torfflowers.Fragments.Order.fragment_app_input_date;
import com.mobil2b.renatganiev.torfflowers.Fragments.User.Info.fragment_app_order_list;
import com.mobil2b.renatganiev.torfflowers.Fragments.User.fragment_app_user_start_fragment;
import com.mobil2b.renatganiev.torfflowers.Fragments.Catalog.fragment_app_catalog_list;
import com.mobil2b.renatganiev.torfflowers.Fragments.Order.fragment_app_payment_select;
import com.mobil2b.renatganiev.torfflowers.Fragments.Products.fragment_app_products;
import com.mobil2b.renatganiev.torfflowers.Fragments.User.Info.fragment_app_user_info;
import com.mobil2b.renatganiev.torfflowers.Fragments.User.Login.fragment_app_user_login;
import com.mobil2b.renatganiev.torfflowers.Fragments.User.Registration.fragment_app_user_registration;
import com.mobil2b.renatganiev.torfflowers.Fragments.User.Welcome.fragment_app_user_welcome;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;


/**
 * Created by renatganiev on 11.04.17.
 */

public class FragmentControl {

    private FragmentManager fragmentmanager;
    private FragmentTransaction fragmenttransaction;
    public CallbackAction callbackAction;
    public DrawerLayout drawer;

    public FragmentControl(FragmentManager fm) {
        fragmentmanager     = fm;
    }

    /**
     * TODO: Открываем страницу выбора города
     * **/
    public void showFragmentCityList(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_city_list fragment = new fragment_app_city_list();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем домашнюю страницу
     * **/
    public void showFragmentHome(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_home fragment = new fragment_app_home();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем список новостей
     * **/
    public void showFragmentNewsList(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_news_list fragment = new fragment_app_news_list();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }


    /**
     * TODO: Открываем список новостей
     * **/
    public void showFragmentArticleList(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_article_list fragment = new fragment_app_article_list();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем фрагмент для управления фрагментами личного кабинета, если работаем через фрагменты
     * **/
    public void showFragmentUserStart(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_user_start_fragment fragment = new fragment_app_user_start_fragment();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }



    /**
     * TODO: Открываем фрагмент преветсвия пользователя и призыв зарегистрироваться
     * **/
    public void showFragmentUserWelcome(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_user_welcome fragment = new fragment_app_user_welcome();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем фрагмент регистрации пользователя
     * **/
    public void showFragmentUserRegistration(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_user_registration fragment = new fragment_app_user_registration();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }


    /**
     * TODO: Открываем фрагмент личный кабинет пользователя
     * **/
    public void showFragmentUserInfo(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_user_info fragment = new fragment_app_user_info();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем фрагмент личный кабинет пользователя
     * **/
    public void showFragmentUserLogin(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_user_login fragment = new fragment_app_user_login();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }


    /**
     * TODO: Открываем фрагмент преветсвия пользователя и призыв зарегистрироваться
     * **/
    public void showFragmentCatalog(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_catalog_list fragment = new fragment_app_catalog_list();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем фрагмент преветсвия пользователя и призыв зарегистрироваться
     * **/
    public void showFragmentProductList(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_products fragment = new fragment_app_products();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем фрагмент избранное
     * **/
    public void showFragmentFavorite(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_favorites fragment = new fragment_app_favorites();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем фрагмент избранное
     * **/
    public void showFragmentCompanyComment(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_company_comment fragment = new fragment_app_company_comment();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем фрагмент избранное
     * **/
    public void showFragmentInfo(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_info fragment = new fragment_app_info();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }


    /**
     * TODO: Открываем корзину
     * **/
    public void showFragmentBag(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_bag fragment = new fragment_app_bag();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }


    /**
     * TODO: Открываем корзину
     * **/
    public void showFragmentDelivery(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_delivery_select fragment = new fragment_app_delivery_select();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }


    /**
     * TODO: Открываем корзину
     * **/
    public void showFragmentPayment(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_payment_select fragment = new fragment_app_payment_select();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем корзину
     * **/
    public void showFragmentInputDate(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_input_date fragment = new fragment_app_input_date();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }

    /**
     * TODO: Открываем корзину
     * **/
    public void showFragmentOrderUserList(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_order_list fragment = new fragment_app_order_list();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }


    /**
     * TODO: Точки торговые
     * **/
    public void showFragmentMapList(boolean isFirst, FragmentAnimation animation, int layout, Bundle bundle) {
        fragment_app_map_list fragment = new fragment_app_map_list();
        fragment.callbackActionSuper = this.callbackAction;
        fragment.drawer = this.drawer;
        fragment.setArguments(bundle);

        fragmenttransaction = fragmentmanager.beginTransaction();
        if (animation != null) {
            fragmenttransaction.setCustomAnimations(animation.enter, animation.exit, animation.popEnter, animation.popExit);
        }
        fragmenttransaction.replace(layout, fragment, fragment.TAG);
        if (!isFirst) {
            fragmenttransaction.addToBackStack(fragment.TAG);
        }
        fragmenttransaction.commit();
    }
}
