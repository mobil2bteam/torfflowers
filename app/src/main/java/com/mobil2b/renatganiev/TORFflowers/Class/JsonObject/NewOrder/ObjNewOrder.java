package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.NewOrder;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Payment.ObjPayment;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Delivery.ObjDelivery;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.DeliveryAddress.ObjDeliveryAddress;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjCards.ObjCards;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjRecipient.ObjRecipient;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.PromoCode.ObjPromoCode;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjNewOrder {

    public ObjCity city_id;
    public ObjDelivery deliveryId;
    public ObjPayment paymentId;
    public ObjCompanyPoint addressPoint;
    public ObjRecipient contact;
    public ObjDeliveryAddress address;
    public ObjPromoCode promocode;
    public String comment;
    public String date;
    public String cart;
    public String cart_str;
    public ObjCards cards;
    public boolean pre;

    public ObjNewOrder() {
    }

    public ArrayList<ObjParam> getParamList() {
        ArrayList<ObjParam> rez = new ArrayList<>();

        if (this.city_id != null) {
            rez.add(new ObjParam("cityId", String.valueOf(this.city_id.id)));
        }
        if (this.deliveryId != null) {
            rez.add(new ObjParam("deliveryId", String.valueOf(this.deliveryId.id)));
        }
        if (this.paymentId != null) {
            rez.add(new ObjParam("paymentId", String.valueOf(this.paymentId.id)));
        }
        if (this.addressPoint != null) {
            rez.add(new ObjParam("addressPoint", String.valueOf(this.addressPoint.id)));
        }
        if (this.contact != null) {
            if (Helps.isNotEmpty(this.contact.name)) rez.add(new ObjParam("contact_name", String.valueOf(this.contact.name)));
            if (Helps.isNotEmpty(this.contact.phone)) rez.add(new ObjParam("contact_phone", String.valueOf(this.contact.phone)));
        }
        if (this.address != null) {
            rez.add(new ObjParam("address", String.valueOf(this.address.getAddressNoZipCode())));
            if (Helps.isNotEmpty(this.address.zip_code)) rez.add(new ObjParam("zipCode", String.valueOf(this.address.zip_code)));
        }
        if (this.promocode != null) {
            rez.add(new ObjParam("promocode", String.valueOf(this.promocode.code)));
        }
        if (Helps.isNotEmpty(comment)) {
            rez.add(new ObjParam("comment", String.valueOf(this.comment)));
        }
        if ((Helps.isNotEmpty(date)) && (!date.equals("01.01.1970"))) {
            rez.add(new ObjParam("date", String.valueOf(this.date)));
        }
        if (Helps.isNotEmpty(cart)) {
            rez.add(new ObjParam("cart", String.valueOf(this.cart)));
        }
        if (Helps.isNotEmpty(cart_str)) {
            rez.add(new ObjParam("cart_str", String.valueOf(this.cart_str)));
        }
        if (cards != null) {
            rez.add(new ObjParam("cardId", String.valueOf(cards.id)));
            if (cards.taken_balls > 0) {
                rez.add(new ObjParam("ball", String.valueOf(cards.taken_balls)));
            }
        }

        if (pre) {
            rez.add(new ObjParam("pre", "1"));
        }
        return rez;
    }

}
