package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.NewOrder.ObjNewOrder;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjCards.ObjCards;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjFilter;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjMessage;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjOptions;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjURL;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Questions.Question;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjDirectories;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Home.ObjHome;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.ObjSettings;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;

import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.Cache;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.R;

/**
 * Created by renatganiev on 18.04.16.
 */
public class ModuleObjects {

    public CallBackSendDate callBackSendDateSuper;

    private Question question;
    private Context mContext;
    private Picasso mPicasso;
    private String key, url_suite, version, versionRelease;
    private Cache chache;

    private int count_error = 0;

    private ArrayList<ObjLink> stack;


    public ModuleObjects(Context ctx) {
        mContext                = ctx;
        mPicasso                = new Picasso.Builder(mContext).executor(Executors.newSingleThreadExecutor()).build();
        question                = new Question(mContext);
        stack                   = new ArrayList<>();

        initDefaultParamInfo();
    }

    public ModuleObjects(Context ctx, Picasso mPic) {
        mContext                = ctx;
        mPicasso                = mPic;
        question                = new Question(mContext);
        stack                   = new ArrayList<>();

        initDefaultParamInfo();
    }

    public ModuleObjects(Context ctx, CallBackSendDate callBack) {
        mContext                = ctx;
        mPicasso                = new Picasso.Builder(mContext).executor(Executors.newSingleThreadExecutor()).build();
        callBackSendDateSuper   = callBack;
        question                = new Question(mContext);
        stack                   = new ArrayList<>();

        initDefaultParamInfo();
    }

    public ModuleObjects(Context ctx, Picasso mPic, CallBackSendDate callBack) {
        mContext                = ctx;
        mPicasso                = mPic;
        callBackSendDateSuper   = callBack;
        question                = new Question(mContext);
        stack                   = new ArrayList<>();

        initDefaultParamInfo();
    }


    private void initDefaultParamInfo() {
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
            version         = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version         = "0";
        }

        key             = mContext.getResources().getString(R.string.mobil2b_key);
        url_suite       = mContext.getResources().getString(R.string.url);
        versionRelease  = Build.VERSION.RELEASE;
    }



    /***********************************************************************************************
     * TODO: Первый запрос, для получения ссылок
     **********************************************************************************************/
    public void getFirstQuestions(boolean ignorCache, String paramUrls, String token) {

        ObjLink linkURL = new ObjLink();
        linkURL.url         = new ObjURL(PublicData.QUESTION_TYPE_URL, url_suite);
        linkURL.param       = getDefaultParam();

        if ((paramUrls != null) && (!paramUrls.equals(""))) {
            linkURL.param.add(new ObjParam("urls", paramUrls));
        }
        if ((token != null) && (!token.equals(""))) {
            linkURL.param.add(new ObjParam("token", token));
        }

        question.SendQuestionGet(linkURL, new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
                if (callBackSendDateSuper != null) {
                    callBackSendDateSuper.onSend(txt, linkURL);
                }
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
                try {

                    JSONObject json = new JSONObject(result);
                    String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                    if (typeDate.equals(PublicData.QUESTION_TYPE_URL)) {

                        if (json.has("options")) {
                            JSONObject jsonOptions = json.getJSONObject("options");
                            PublicData.OPTIONS      = new ObjOptions(jsonOptions);
                        }
                        if (json.has("directories")) {
                            JSONObject jsonDirectories = json.getJSONObject("directories");
                            PublicData.DIRECTORIES  = new ObjDirectories(jsonDirectories);
                        }
                        if (json.has("settings")) {
                            JSONObject jsonSettings = json.getJSONObject("settings");
                            PublicData.SETTINGS     = new ObjSettings(jsonSettings, mContext);
                        }
                        if (json.has("home")) {
                            JSONObject jsonHome = json.getJSONObject("home");
                            PublicData.HOME         = new ObjHome(jsonHome);
                        }

                        // Есть кэш.
                        if (!cache) {
                            addToCache(result, linkURL);
                        }

                        callBackSendDateSuper.onReturnAnswer(result, cache, cache_datetime, linkURL);
                    }
                } catch (JSONException e) {
                }
                //
            }

            @Override
            public void onError(String text, int code_error, ObjLink linkURL) {
                // Если тут произошла ошибка, то пиздец.
                Log.d("URL_RESULT_ERROR", text);
                try {
                    JSONObject json = new JSONObject(text);
                    if (json.has("text")) {
                        text = json.getString("text");
                    }
                } catch (JSONException e) {}
                callBackSendDateSuper.onHttpError(text, code_error, linkURL);
            }

            @Override
            public void onHttpError(String text, int code_error, ObjLink linkURL) {
                Log.d("URL_RESULT_ERROR_HTTP", text);
                // Если тут произошла ошибка, то пиздец. Ничего поделать не можем.
                // Говорим пользователю что все плохо.
                callBackSendDateSuper.onHttpError(text, code_error, linkURL);
            }
        }, ignorCache);
    }



    public boolean isEmptyURL() {
        if (PublicData.OPTIONS != null) {
            if (PublicData.OPTIONS.urls != null) {
                if (PublicData.OPTIONS.urls.size() == 0) {
                    return true;
                }
            } else {return true;}
        } else {return true;}
        return false;
    }

    /***********************************************************************************************
     * TODO: Запрашиваем главную страницу
     **********************************************************************************************/
    public void getHome(boolean ignorCache) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_HOME);

        SendQuestion(ignorCache, linkURL);
    }



    /***********************************************************************************************
     * TODO: Запрашиваем продукты
     **********************************************************************************************/
    public void getProducts(boolean ignorCache, ObjFilter filter, DataSession dataSession) {

        if (isEmptyURL()) return;

        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_PRODUCTS);

        if (filter != null) {
            linkURL.addParam(filter.getParamList());
        }

        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }

        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем новости
     **********************************************************************************************/
    public void getNewsList(boolean ignorCache) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_NEWS_LIST);
        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем конкретную новость
     **********************************************************************************************/
    public void getNewsItem(boolean ignorCache, int id) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_NEWS_ITEM);

        ObjParam news_id   = new ObjParam("news_id", String.valueOf(id));
        linkURL.param.add(news_id);

        SendQuestion(ignorCache, linkURL);
    }


    /***********************************************************************************************
     * TODO: Запрашиваем новости
     **********************************************************************************************/
    public void getArticleList(boolean ignorCache) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_ARTICLE_LIST);
        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем конкретную новость
     **********************************************************************************************/
    public void getArticleItem(boolean ignorCache, int id) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_ARTICLE_ITEM);

        ObjParam articles_id   = new ObjParam("article_id", String.valueOf(id));
        linkURL.param.add(articles_id);

        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем каталог
     **********************************************************************************************/
    public void getCatalog(boolean ignorCache, int id) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_CATALOG);

        ObjParam catalog_id   = new ObjParam("catalog_id", String.valueOf(id));
        linkURL.param.add(catalog_id);

        SendQuestion(ignorCache, linkURL);
    }


    /***********************************************************************************************
     * TODO: Запрашиваем обновленный список товаров
     **********************************************************************************************/
    public void getUpdateListProduct(boolean ignorCache, String list_id) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_GET_PRODUCTS);

        ObjParam catalog_id   = new ObjParam("cart", list_id);
        linkURL.param.add(catalog_id);

        SendQuestion(ignorCache, linkURL);
    }


    /***********************************************************************************************
     * TODO: Запрашиваем новости
     **********************************************************************************************/
    public void getMetroList(boolean ignorCache, int city_id) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_METRO_LIST);

        ObjParam pcity_id   = new ObjParam("city_id", String.valueOf(city_id));
        linkURL.param.add(pcity_id);

        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем настройки проекта
     **********************************************************************************************/
    public void getProjectSettings(boolean ignorCache) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_SETTINGS);

        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем список отзывов о компании
     **********************************************************************************************/
    public void getCompanyCommentList(boolean ignorCache, DataSession dataSession) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_COMPANY_COMMENT_LIST);

        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }

        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем список отзывов о компании
     **********************************************************************************************/
    public void setCompanyCommentAdd(boolean ignorCache, String text, float rating, DataSession dataSession) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_COMPANY_COMMENT_ADD);

        linkURL.param.add(new ObjParam("text", text));
        linkURL.param.add(new ObjParam("rating", String.valueOf(rating)));
        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }

        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем корзину
     **********************************************************************************************/
    public void getBagList(boolean ignorCache, String cart, String promocode, ObjCards objCards, int ball, int city_id, DataSession dataSession) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_GET_BAG_DELIVERY);

        linkURL.param.add(new ObjParam("cart",      cart));
        linkURL.param.add(new ObjParam("promocode", promocode));
        linkURL.param.add(new ObjParam("city_id",   String.valueOf(city_id)));
        if (objCards != null) {
            linkURL.param.add(new ObjParam("cardId",   String.valueOf(objCards.id)));
            if (ball != 0) {
                linkURL.param.add(new ObjParam("ball",   String.valueOf(ball)));
            }
        }

        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }

        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем корзину
     **********************************************************************************************/
    public void getMenuInfo(boolean ignorCache, int id) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_INFORMATION);

        linkURL.param.add(new ObjParam("id",   String.valueOf(id)));
        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Оформление заказа
     **********************************************************************************************/
    public void newOrder(boolean ignorCache, ObjNewOrder objNewOrder, DataSession dataSession) {
        if (isEmptyURL()) return;
        // Строим линк
        if (objNewOrder != null) {
            ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_ORDER);
            linkURL.param.addAll(objNewOrder.getParamList());
            if (dataSession != null) {
                linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
            }
            SendQuestion(ignorCache, linkURL);
        }
    }

    /***********************************************************************************************
     * TODO: Запрашиваем список заказов пользователя
     **********************************************************************************************/
    public void getUserOrderList(boolean ignorCache, DataSession dataSession) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_USER_ORDER_LIST);
        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }

        SendQuestion(ignorCache, linkURL);
    }

    /***********************************************************************************************
     * TODO: Запрашиваем конкретный товар
     **********************************************************************************************/
    public void getUserOrderItem(boolean ignorCache, int order_id, DataSession dataSession) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_USER_ORDER_ITEM);
        linkURL.param.add(new ObjParam("order_id", String.valueOf(order_id)));

        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }

        SendQuestion(ignorCache, linkURL);
    }


    /***********************************************************************************************
     * TODO: Помечаем что заказ оплачен
     **********************************************************************************************/
    public void setPaymentStatusForOrder(boolean ignorCache, DataSession dateSession, int orderId) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_ORDER_PAYMENT);
        linkURL.param.add(new ObjParam("orderId", String.valueOf(orderId)));
        linkURL.param.add(new ObjParam("isPayment", "1"));

        if (dateSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dateSession.token)));
        }


        SendQuestion(ignorCache, linkURL);
    }


    /***********************************************************************************************
     * TODO: Запрашиваем конкретный товар
     **********************************************************************************************/
    public void getProduct(boolean ignorCache, int id, DataSession dataSession) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_PRODUCT);
        linkURL.param.add(new ObjParam("product_id", String.valueOf(id)));
        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }

        SendQuestion(ignorCache, linkURL);
    }


    /***********************************************************************************************
     * TODO: Оставляем заявку на партнерство
     **********************************************************************************************/
    public void setFeedPartne(boolean ignorCache, DataSession dataSession) {
        if (isEmptyURL()) return;
        // Строим линк
        ObjLink linkURL = getDefaultLink(PublicData.QUESTION_TYPE_FEED_PARTNER);
        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }
        SendQuestion(ignorCache, linkURL);
    }



    /***********************************************************************************************
     * TODO: Запрашиваем неизвестную ссылку
     **********************************************************************************************/
    public void getDataToLink(boolean ignorCache, ObjLink linkURL, DataSession dataSession) {

        if (isEmptyURL()) return;

        if (linkURL.url == null) {
            linkURL.url = PublicData.getURL(linkURL.name);
        }
        if (dataSession != null) {
            linkURL.param.add(new ObjParam("token", String.valueOf(dataSession.token)));
        }
        linkURL.param.addAll(getDefaultParam());

        SendQuestion(ignorCache, linkURL);
    }


    public void SendQuestion(boolean ignorCache, ObjLink linkURL) {
        if (linkURL.url.metod.equals("POST")) {
            question.SendQuestionPost(linkURL, callBackSendDate, ignorCache);
        } else {
            question.SendQuestionGet(linkURL, callBackSendDate, ignorCache);
        }
    }



    /***********************************************************************************************
     * TODO: Проверяем на наличие контралируемых ошибок
     **********************************************************************************************/
    public ObjMessage isMessage(String answer, ObjLink linkURL) {
        try {
            JSONObject jObject = new JSONObject(answer);
            if (jObject.has("message")) {
                return new ObjMessage(jObject);
            }
        } catch (JSONException e) {
        }
        return null;
    }


    /**
     * Список обязательных параметров
     * */
    public ArrayList<ObjParam> getDefaultParam() {
        ArrayList<ObjParam> param = new ArrayList<>();
        ObjParam app_version    = new ObjParam("app_version",   version);
        ObjParam device         = new ObjParam("device",        "Android");
        ObjParam os_version     = new ObjParam("os_version",    versionRelease);
        ObjParam app_key        = new ObjParam("key",           key);

        param.add(app_key);
        param.add(app_version);
        param.add(device);
        param.add(os_version);
        return param;
    }

    /**
     *
     * Строим стандартный линк
     *
     * */

    public ObjLink getDefaultLink(String question_type) {
        ObjURL url = PublicData.getURL(question_type);

        ObjLink linkURL = new ObjLink();
        linkURL.url         = url;
        linkURL.param       = getDefaultParam();

        if (url.param != null) {
            linkURL.addParam(url.param);
        }

        return linkURL;
    }


    /***********************************************************************************************
     * TODO: Обработка всех ответов с сервера
     **********************************************************************************************/
    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            callBackSendDateSuper.onSend(txt, linkURL);
        }

        @Override
        public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            // Удаляем данные из стека, чтобы не дать повторный запрос.
            deleteStack(linkURL);
            count_error = 0;

            // Добавление в кеш, если это необходимо
            if (!cache) {
                cache_datetime = addToCache(result, linkURL);
            }

            // Если небыло ошибок, то отдаем ответ дальше на разбор.
            if ((!isError(result, linkURL)) && (cache_datetime != -1)) {

                // Если это загрузка справочника флагов то загружаем данные прямо тут.
                if (linkURL != null)
                    if (linkURL.url != null)
                        if (linkURL.url.name != null)
                            if (!linkURL.url.name.equals("")) {

                                /*if (linkURL.url.name.equals(PublicData.QUESTION_TYPE_CATALOG)) {
                                    PublicData.CATALOG = new ObjCatalog(result);
                                }

                                if (linkURL.url.name.equals(PublicData.QUESTION_TYPE_NEWS)) {
                                    PublicData.NEWS = new ObjNews(result);
                                }

                                if (linkURL.url.name.equals(PublicData.QUESTION_TYPE_SETTINGS)) {
                                    // Обновились все URL
                                    PublicData.SETTINGS = new ObjSettings(result);
                                }*/
                            }

                // Передаем ответ выше.
                callBackSendDateSuper.onReturnAnswer(result, cache, cache_datetime, linkURL);
            }

            // Запсукаем далее по стеку очереди, если это необходимо.
            startStack();
        }

        @Override
        public void onError(String text, int code_error, ObjLink linkURL) {

            Log.d("URL_RESULT_ERROR", text);

            // Отсылаем пользователю информацию о контролируемой ошибке
            // Скорей всего нет товара.
            try {
                JSONObject json = new JSONObject(text);
                if (json.has("message")) {
                    text = json.getString("message");
                }
                if (json.has("text")) {
                    text = json.getString("text");
                }
            } catch (JSONException e) {}
            callBackSendDateSuper.onError(text, code_error, linkURL);
        }

        @Override
        public void onHttpError(String text, int code_error, ObjLink linkURL) {

            Log.d("URL_RESULT_ERROR_HTTP", text);

            count_error++; // Счетчик ошибок
            if (count_error >= 2) {
                // Ошибка валиться даже после обновления всех ссылок.
                // Чтобы не зациклить процесс просто говорим пользователю, что все плохо.
                clearStack();
                callBackSendDateSuper.onHttpError(text, code_error, linkURL);
            } else {
                // Добавим запрос в стек.
                stack.add(linkURL);
                // Обновили список всех ссылок без кеша.
                getFirstQuestions(true, "", "");
            }
        }
    };



    /***********************************************************************************************
     * TODO: Проверяем наличие кэша в ответе и добавляем ответ в кеш, если это необходимо
     **********************************************************************************************/
    private long addToCache(String answer, ObjLink linkURL) {
        try {
            JSONObject jObject = new JSONObject(answer);

            // Если нет ошибок
            if (!jObject.has("code")) {
                // Если нет сообщений
                if (!jObject.has("message")) {
                    // Если есть структура кэша
                    if (jObject.has("cache")) {
                        // Если кэш разрешен в приложении.
                        if ((PublicData.CACHE_QUESTION)) {

                            long time = jObject.getLong("cache");
                            if (time != 0) {

                                // Так как ответ приходит в секундах, а все считаеться в миллисекундах, то переводим в миллисекунды.
                                time = new Date().getTime() + (time * 1000);

                                // Ключем для кэша являеться URL, для этого получаем его с параметрами
                                String full_url = linkURL.url.url;
                                if (linkURL.param != null) {
                                    full_url = question.getParamToGet(full_url, linkURL);
                                }

                                // Сохранили в кэш
                                chache = new Cache(mContext);
                                chache.AddCache(full_url, time, answer);
                                return time;
                            }

                        }
                    }
                }
            }
        } catch (JSONException e) {
            callBackSendDateSuper.onHttpError(mContext.getResources().getString(R.string.error_text_data), 0, null);
            return -1;
        }
        return 0;
    }


    /***********************************************************************************************
     * TODO: Проверяем на наличие контралируемых ошибок
     **********************************************************************************************/
    private boolean isError(String answer, ObjLink linkURL) {
        try {
            if (answer.equals("[]")) {
                return false;
            }
            JSONObject jObject = new JSONObject(answer);

            if (jObject.has("code")) {

                int error_code  = jObject.getInt("code");
                ObjMessage msg = isMessage(answer, linkURL);
                if (msg != null) {
                    callBackSendDateSuper.onError(msg.text, error_code, linkURL);
                } else {
                    callBackSendDateSuper.onError("", error_code, linkURL);
                }

                return true;
            } else {

                if (jObject.has("error")) {

                    String error  = jObject.getString("error");

                    try {
                        JSONObject json = new JSONObject(error);
                        if (json.has("text")) {
                            error = json.getString("text");
                        }
                        if (json.has("message")) {
                            error = json.getString("message");
                        }
                    } catch (JSONException e) {}

                    callBackSendDateSuper.onError(error, 0, linkURL);

                    return true;
                } else {
                    return false;
                }


            }
        } catch (JSONException e) {
            callBackSendDateSuper.onHttpError(mContext.getResources().getString(R.string.error_text_data), 0, null);
        }
        return true;
    }

    /***********************************************************************************************
     * TODO: Останавливаем все потоки
     **********************************************************************************************/
    public void cancelAllRequest() {
        question.cancelAllRequest();
    }


    /***********************************************************************************************
     * TODO: Запуск стека
     **********************************************************************************************/
    private void startStack() {
        if (stack.size() > 0) {
            getDataToLink(false, stack.get(0), null);
        }
    }

    /***********************************************************************************************
     * TODO: Удаляем из стека
     **********************************************************************************************/
    private void deleteStack(ObjLink link) {
        int idLink = stack.indexOf(link);
        if (idLink != -1) {
            stack.remove(link);
        }
    }

    /***********************************************************************************************
     * TODO: Очистка стека
     **********************************************************************************************/
    private void clearStack() {
        stack.clear();
    }




}
