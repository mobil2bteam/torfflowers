package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.location.Address;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterCompanyPointList;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.MarkerManager.MarkerManager;
import com.mobil2b.renatganiev.torfflowers.Class.MyGeoCoding;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.about_company_point_item;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


public class fragment_app_map_list extends Fragment implements OnMapReadyCallback {

    public static final String TAG = "fragment_app_map_list";
    public static final int LAYOUT = R.layout.fragment_app_map_list;
    View rootView;

    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;

    MenuItem menuItemList, menuItemMap;

    RecyclerView mRecyclerView;
    LinearLayoutManager manager;
    SupportMapFragment mapFragment;
    GoogleMap mMap;
    MarkerManager markerManager;
    AdapterCompanyPointList mAdapterSearch;

    int     city_id;
    boolean flag_delivery_point;
    boolean select_type;

    ArrayList<ObjCompanyPoint> objCompanyPointArrayList = new ArrayList<>();

    private LatLng POINT;


    static public fragment_app_map_list getInstance(Bundle args) {
        fragment_app_map_list fragment = new fragment_app_map_list();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        Bundle arg = getArguments();

        if (arg != null) {
            if (arg.containsKey("city_id")) {
                city_id = arg.getInt("city_id");
            }
            if (arg.containsKey("flag_delivery_point")) {
                flag_delivery_point = arg.getBoolean("flag_delivery_point");
            } else {
                flag_delivery_point = false;
            }
            if (arg.containsKey("select_type")) {
                select_type = arg.getBoolean("select_type");
            } else {
                select_type = false;
            }

        }

        objCompanyPointArrayList = new ArrayList<>();

        if (city_id != 0) {
            if (PublicData.SETTINGS != null) {
                if (PublicData.SETTINGS.company_info != null) {
                    if (PublicData.SETTINGS.company_info.points != null) {
                        for (ObjCompanyPoint objCompanyPoint : PublicData.SETTINGS.company_info.points) {
                            if (objCompanyPoint.contacts != null) {
                                if (objCompanyPoint.contacts.address != null) {
                                    if (objCompanyPoint.contacts != null) {
                                        if (objCompanyPoint.contacts.address != null) {
                                            if (objCompanyPoint.contacts.address.city_id == city_id) {
                                                if (flag_delivery_point) {
                                                    if (objCompanyPoint.check_point_pickup) {
                                                        objCompanyPointArrayList.add(objCompanyPoint);
                                                    }
                                                } else {
                                                    if (((objCompanyPoint.check_point_office) || (objCompanyPoint.check_point_sale))) {
                                                        objCompanyPointArrayList.add(objCompanyPoint);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (PublicData.DIRECTORIES != null) {
                if (PublicData.DIRECTORIES.city != null) {
                    for (ObjCity objCity : PublicData.DIRECTORIES.city) {
                        if (objCity.id == city_id) {
                            searchGeoLocationForAddress(objCity.name);
                            break;
                        }
                    }
                }
            }

        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //rootView = inflater.inflate(LAYOUT, container, false);

        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(LAYOUT, container, false);
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }

        markerManager = new MarkerManager(getActivity());

        setUpMapIfNeeded();
        intiRecyclerView();
        initToolbar();
        refrashAllDate();

        setHasOptionsMenu(true);

        return rootView;
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle("Дополнительные адреса");

        if (!select_type) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            activity.setSupportActionBar(toolbar);


            if (drawer != null) {
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
                drawer.addDrawerListener(toggle);
                toggle.syncState();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_back_24dp));
                } else {
                    toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_24dp));
                }

                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().finish();
                    }
                });
            }
        } else {
            toolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_maps, menu);
        super.onCreateOptionsMenu(menu,inflater);

        menuItemList    = menu.findItem(R.id.menu_list);
        menuItemMap     = menu.findItem(R.id.menu_maps);

        menuItemList.setVisible(true);
        menuItemMap.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_list) {
            menuItemMap.setVisible(true);
            menuItemList.setVisible(false);
            mRecyclerView.setVisibility(View.VISIBLE);
            mapFragment.getView().setVisibility(View.INVISIBLE);
            mRecyclerView.invalidate();
        }
        if (id == R.id.menu_maps) {
            menuItemMap.setVisible(false);
            menuItemList.setVisible(true);
            mRecyclerView.setVisibility(View.INVISIBLE);
            mapFragment.getView().setVisibility(View.VISIBLE);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (select_type) {
            if (requestCode == PublicData.REZULT_REQUEST_SELECT_POINT) {
                if (resultCode == RESULT_OK) {
                    //callbackActionSuper
                    if (data != null) {
                        Bundle bundle = data.getExtras();
                        if (bundle != null) {
                            if (bundle.containsKey("point_id")) {
                                int point_id = bundle.getInt("point_id");
                                if (PublicData.SETTINGS != null) {
                                    if (PublicData.SETTINGS.company_info != null) {
                                        if (PublicData.SETTINGS.company_info.points != null) {
                                            for (ObjCompanyPoint obj : PublicData.SETTINGS.company_info.points) {
                                                if (obj.id == point_id) {
                                                    PublicData.objCompanyPoint = obj;
                                                    callbackActionSuper.onAction(TAG, PublicData.SELECT_DELIVERY_POINT, bundle);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private void refrashAllDate() {
        if (objCompanyPointArrayList != null) {
            mAdapterSearch = new AdapterCompanyPointList(getActivity(), objCompanyPointArrayList);
            mRecyclerView.setAdapter(mAdapterSearch);
            mAdapterSearch.notifyDataSetChanged();

            mAdapterSearch.setOnClickListener(new ClickListenerCompanyPoint() {
                    @Override
                    public void onClick(View view, ObjCompanyPoint objCompanyPoint) {
                        Intent intent = new Intent(getActivity(), about_company_point_item.class);
                        intent.putExtra("point_id",     objCompanyPoint.id);
                        intent.putExtra("select_type", select_type);
                        if (select_type) {
                            startActivityForResult(intent, PublicData.REZULT_REQUEST_SELECT_POINT);
                        } else {
                            startActivity(intent);
                        }
                    }
            });
        }
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapFragment.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mapFragment.onDestroy();
        } catch (Exception e) {

        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();
    }


    /***********************************************************************************************
     *      Функции для работы с ГЕО определением и картой
     *********************************************************************************************/
    private void setUpMapIfNeeded() {
        mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        //mapFragment.getView().setVisibility(View.INVISIBLE);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Check if we were successful in obtaining the map.
        if (googleMap != null) {
            mMap = googleMap;

            mMap.setMinZoomPreference(5);
            mMap.getUiSettings().setZoomControlsEnabled(true);

            // Move the camera instantly to Sydney with a zoom of 15.
            //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SURGUT, 15));

            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(POINT)      // Sets the center of the map to Mountain View
                    .zoom(10)                   // Sets the zoom
                    .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            //mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


            mMap.setOnMarkerClickListener(marker -> {
                Intent intent = new Intent(getActivity(), about_company_point_item.class);
                int id = markerManager.getMarkerId(marker);
                intent.putExtra("point_id", id);
                intent.putExtra("select_type", select_type);
                if (select_type) {
                    startActivityForResult(intent, PublicData.REZULT_REQUEST_SELECT_POINT);
                } else {
                    startActivity(intent);
                }
                return false;
            });

            if (objCompanyPointArrayList != null) {
                for (ObjCompanyPoint club : objCompanyPointArrayList) {
                    createMarketClub(club);
                }
            }

        }
    }

    private void searchGeoLocationForAddress(String address) {
        MyGeoCoding myGeoCoding = new MyGeoCoding(getActivity());
        Address location = myGeoCoding.searchAddress(address);

        if (location != null) {
            POINT = new LatLng(location.getLatitude(), location.getLongitude());
        }
    }

    private void createMarketClub(ObjCompanyPoint objListPoint) {

        LatLng MELBOURNE = null;
        if (objListPoint.contacts != null) {
            if (objListPoint.contacts.address != null) {
                if (objListPoint.contacts.address.geo != null) {
                    MELBOURNE = new LatLng(objListPoint.contacts.address.geo.lat, objListPoint.contacts.address.geo.lng);
                }
            }
        }

        if (MELBOURNE != null) {

            markerManager.createMarker(MELBOURNE,
                                        objListPoint.id,
                                        BitmapFactory.decodeResource(getResources(), R.drawable.geo_point),
                                        new Rect(20, 20, 79, 79),
                                        mMap);



        }
    }


}

