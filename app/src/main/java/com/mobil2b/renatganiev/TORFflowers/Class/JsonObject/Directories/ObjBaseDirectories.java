package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import java.io.Serializable;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjBaseDirectories implements Serializable {

    public int      id;
    public String   name = "";
    public boolean  checkbox = false;

}
