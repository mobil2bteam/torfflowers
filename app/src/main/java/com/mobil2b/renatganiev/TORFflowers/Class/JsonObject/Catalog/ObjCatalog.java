package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCatalog {

    public int      id;
    public int      parent_id;
    public int      children_count;
    public String   name;
    public ObjImage image;
    public ObjImage icon;
    public String   comment;
    public String   view;
    public ArrayList<ObjCatalogItems> items;
    public boolean  flag_not_show_name;

    public ObjCatalog() {
        init();
    }
    public ObjCatalog(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjCatalog(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("catalog")) {
                JSONObject jsonMain = json.getJSONObject("catalog");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        items = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {

            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("parent_id")) {
                parent_id = json.getInt("parent_id");
            }
            if (json.has("children_count")) {
                children_count = json.getInt("children_count");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("image")) {
                image = new ObjImage(json.getJSONObject("image"));
            }
            if (json.has("icon")) {
                icon = new ObjImage(json.getJSONObject("icon"));
            }
            if (json.has("comment")) {
                comment = json.getString("comment");
            }
            if (json.has("view")) {
                view = json.getString("view");
            }
            if (json.has("flag_not_show_name")) {
                flag_not_show_name = json.getBoolean("flag_not_show_name");
            }


            if (json.has("items")) {
                for (int i =0;i<json.getJSONArray("items").length();i++){
                    ObjCatalogItems it = new ObjCatalogItems(json.getJSONArray("items").getJSONObject(i));
                    items.add(it);
                }
            }
        } catch (JSONException e) {
        }
    }

    public ObjCatalogItems getCatalogItems(int parent_id, ArrayList<ObjCatalogItems> itemsParent) {
        ObjCatalogItems result = null;

        if (itemsParent != null) {
            for (ObjCatalogItems objList : itemsParent) {
                if (objList.id == parent_id) {
                    return objList;
                } else {
                    if (objList.items != null) {
                        if (objList.items.size() != 0) {
                            result = getCatalogItems(parent_id, objList.items);
                            if (result != null) {
                                return result;
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    public String getParentName(int parent_id, ObjCatalogItems itemsParent) {
        String result = "";

        if (itemsParent != null) {
            for (ObjCatalogItems objList : itemsParent.items) {
                if (objList.id == parent_id) {
                    return itemsParent.name;
                } else {
                    if (objList.items != null) {
                        if (objList.items.size() != 0) {
                            result = getParentName(parent_id, objList);
                            if (!result.equals("")) {
                                return result;
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

}
