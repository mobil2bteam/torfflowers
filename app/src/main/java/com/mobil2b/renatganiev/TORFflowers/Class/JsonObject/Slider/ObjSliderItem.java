package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Slider;


import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;

import org.json.JSONException;
import org.json.JSONObject;

public class ObjSliderItem {
    public ObjImage image;
    public String text;
    public String addText;
    public ObjLink link;
    public void loadJsonObject(JSONObject json){
        try {
            if(json.has("image")){
                image = new ObjImage(json.getJSONObject("image"));
            }
            if(json.has("text")){
                this.text = json.getString("text");
            }
            if(json.has("addText")){
                this.addText = json.getString("addText");
            }
            if(json.has("link")){
                link = new ObjLink(json.getJSONObject("link"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
