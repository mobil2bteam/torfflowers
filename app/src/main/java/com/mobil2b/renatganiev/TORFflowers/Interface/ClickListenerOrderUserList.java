package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order.ObjOrderUserList;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerOrderUserList {
    void onClick(View view, ObjOrderUserList objOrderUserList);
}
