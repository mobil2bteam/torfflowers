package com.mobil2b.renatganiev.torfflowers.Fragments.Order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mobil2b.renatganiev.torfflowers.delivery_comment;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryAddressView;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCommentView.DeliveryCommentView;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryDateView.DeliveryDateView;
import com.mobil2b.renatganiev.torfflowers.ControlView.RecipientView.RecipientView;
import com.mobil2b.renatganiev.torfflowers.Fragments.Bag.fragment_app_bag;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.delivery_address_select;
import com.mobil2b.renatganiev.torfflowers.delivery_date_select;
import com.mobil2b.renatganiev.torfflowers.delivery_recipient_select;

import java.util.Date;

/**
 * Created by renatganiev on 20.01.18.
 */

public class fragment_app_input_date extends Fragment {

    public static final String TAG = "fragment_app_input_date";
    public static final int LAYOUT = R.layout.fragment_app_input_date;
    View rootView;
    public CallbackAction callbackActionSuper;

    LinearLayout address_delivery, layout_recipient, layout_date, layout_comment;
    Button btnNext;

    DeliveryAddressView deliveryAddressView;
    RecipientView recipientView;
    DeliveryDateView deliveryDateView;
    DeliveryCommentView deliveryCommentView;

    public static fragment_app_bag getInstance(Bundle args) {
        fragment_app_bag fragment = new fragment_app_bag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        address_delivery    = (LinearLayout) rootView.findViewById(R.id.address_delivery);
        layout_recipient    = (LinearLayout) rootView.findViewById(R.id.layout_recipient);
        layout_date         = (LinearLayout) rootView.findViewById(R.id.layout_date);
        layout_comment      = (LinearLayout) rootView.findViewById(R.id.layout_comment);
        btnNext             = (Button) rootView.findViewById(R.id.btnNext);

        // Адрес доставки
        deliveryAddressView = new DeliveryAddressView(getContext());
        deliveryAddressView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), delivery_address_select.class);
                startActivityForResult(i, deliveryAddressView.REZULT_REQUEST_SELECT_ADDRESS);
            }
        });
        address_delivery.removeAllViews();
        address_delivery.addView(deliveryAddressView);
        address_delivery.setVisibility(View.VISIBLE);

        // Получатель
        recipientView = new RecipientView(getContext());
        recipientView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), delivery_recipient_select.class);
                startActivityForResult(i, recipientView.REZULT_REQUEST_SELECT_ADDRESS);
            }
        });
        layout_recipient.removeAllViews();
        layout_recipient.addView(recipientView);
        layout_recipient.setVisibility(View.VISIBLE);

        // Дата доставки
        deliveryDateView = new DeliveryDateView(getContext());
        deliveryDateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), delivery_date_select.class);
                long datetime = deliveryDateView.getValue();
                if (datetime != 0) {
                    i.putExtra("value", datetime);
                }
                startActivityForResult(i, deliveryDateView.REZULT_REQUEST_SELECT_ADDRESS);
            }
        });
        layout_date.removeAllViews();
        layout_date.addView(deliveryDateView);
        layout_date.setVisibility(View.VISIBLE);

        //Комментарий к заказу
        deliveryCommentView = new DeliveryCommentView(getContext());
        deliveryCommentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), delivery_comment.class);
                i.putExtra("value", deliveryCommentView.getValue());
                startActivityForResult(i, deliveryCommentView.REZULT_REQUEST_SELECT_ADDRESS);
            }
        });
        layout_comment.removeAllViews();
        layout_comment.addView(deliveryCommentView);
        layout_comment.setVisibility(View.VISIBLE);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callbackActionSuper != null) {
                    if (deliveryAddressView.getDeliveryAddress() == null) {
                        Helps.errorTextEdit(deliveryAddressView, getContext());
                        return;
                    }

                    PublicData.objDeliveryAddress   = deliveryAddressView.getDeliveryAddress();
                    PublicData.objRecipient         = recipientView.getRecipient();
                    PublicData.objDeliveryDate      = new Date(deliveryDateView.getValue());
                    PublicData.objComment           = deliveryCommentView.getValue();

                    callbackActionSuper.onAction(TAG, PublicData.SELECT_INPUT_DATE, new Bundle());
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        deliveryAddressView.onActivityResult(requestCode, resultCode, data);
        recipientView.onActivityResult(requestCode, resultCode, data);
        deliveryDateView.onActivityResult(requestCode, resultCode, data);
        deliveryCommentView.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        deliveryAddressView.onResume();
        recipientView.onResume();
        deliveryDateView.onResume();
        deliveryCommentView.onResume();
    }
}
