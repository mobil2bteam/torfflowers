package com.mobil2b.renatganiev.torfflowers;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackPasswordChange;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackUserUpdate;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;

public class user_update extends AppCompatActivity {

    private Toolbar toolbar;

    EditText edit_second_name, edit_first_name, edit_last_name, edit_email, edit_phone, edit_password_old, edit_password_new;
    RadioButton radioButtonMan, radioButtonWoman;
    Button btnChangePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_update);

        edit_second_name    = (EditText) findViewById(R.id.edit_second_name);
        edit_first_name     = (EditText) findViewById(R.id.edit_first_name);
        edit_last_name      = (EditText) findViewById(R.id.edit_last_name);
        edit_email          = (EditText) findViewById(R.id.edit_email);
        edit_phone          = (EditText) findViewById(R.id.edit_phone);
        edit_password_old   = (EditText) findViewById(R.id.edit_password_old);
        edit_password_new   = (EditText) findViewById(R.id.edit_password_new);
        btnChangePassword   = (Button)   findViewById(R.id.btnChangePassword);

        radioButtonMan      = (RadioButton) findViewById(R.id.radioButtonMan);
        radioButtonWoman    = (RadioButton) findViewById(R.id.radioButtonWoman);

        initToolbar();
        updateView();

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePassword();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_update_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_save) {
            updateUserData();
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle(getString(R.string.str_caption_user_update));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_toolbar_icon_back_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                setResult(RESULT_OK);
                finish();

            }
        });
    }

    void updateView() {
        if (User.getCurrentSession() != null) {
            edit_first_name.setText(User.getCurrentSession().first_name);
            edit_second_name.setText(User.getCurrentSession().second_name);
            edit_last_name.setText(User.getCurrentSession().last_name);

            edit_email.setText(User.getCurrentSession().email);
            edit_phone.setText(User.getCurrentSession().phone);

            if (User.getCurrentSession().sex == 1) {
                radioButtonMan.setChecked(true);
            }
            if (User.getCurrentSession().sex == 2) {
                radioButtonWoman.setChecked(true);
            }
        }
    }


    void updateUserData() {
        if (edit_first_name.getText().length() == 0) {
            Helps.errorTextEdit(edit_first_name, user_update.this);
            return;
        }
        if (edit_email.getText().length() == 0) {
            Helps.errorTextEdit(edit_email, user_update.this);
            return;
        }

        if (User.getCurrentSession() != null) {
            User.getCurrentSession().user_name      = edit_first_name.getText().toString();
            User.getCurrentSession().first_name     = edit_first_name.getText().toString();
            User.getCurrentSession().second_name    = edit_second_name.getText().toString();
            User.getCurrentSession().last_name      = edit_last_name.getText().toString();
            User.getCurrentSession().email          = edit_email.getText().toString();
            User.getCurrentSession().login          = edit_email.getText().toString();
            User.getCurrentSession().phone          = edit_phone.getText().toString();
            if (radioButtonMan.isChecked()) {
                User.getCurrentSession().sex = 1;
            } else {
                User.getCurrentSession().sex = 2;
            }

            User.onUpdateUserInfo(User.getCurrentSession(), new CallbackUserUpdate() {
                @Override
                public void onStart() {
                    PublicData.showDialog(user_update.this);
                }

                @Override
                public void onSuccess(final DataSession result) {
                    user_update.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PublicData.closeDialog();
                            if (result != null) {
                                Toast.makeText(user_update.this, getString(R.string.text_user_update_success), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

                @Override
                public void onError(final String message, int error_code) {
                    user_update.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PublicData.closeDialog();
                            Toast.makeText(user_update.this, message, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });

        }
    }


    void updatePassword() {
        if (edit_password_old.getText().length() == 0) {
            Helps.errorTextEdit(edit_password_old, user_update.this);
            return;
        }
        if (edit_password_new.getText().length() == 0) {
            Helps.errorTextEdit(edit_password_new, user_update.this);
            return;
        }
        User.onPasswordChange(edit_password_old.getText().toString(), edit_password_new.getText().toString(), new CallbackPasswordChange() {
            @Override
            public void onStart() {
                PublicData.showDialog(user_update.this);
            }

            @Override
            public void onSuccess(final DataSession result) {
                user_update.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        PublicData.closeDialog();
                        if (result != null) {
                            Toast.makeText(user_update.this, getString(R.string.text_password_change_success), Toast.LENGTH_SHORT).show();
                            edit_password_old.setText("");
                            edit_password_new.setText("");
                        }
                    }
                });
            }

            @Override
            public void onError(final String message, int error_code) {
                user_update.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        PublicData.closeDialog();
                        Toast.makeText(user_update.this, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
