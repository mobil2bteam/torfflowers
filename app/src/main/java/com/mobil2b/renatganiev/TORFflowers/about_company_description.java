package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class about_company_description extends AppCompatActivity {

    String description, name;
    TextView txtDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_company_description);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle.containsKey("description")) {
            description = bundle.getString("description");
        }
        if (bundle.containsKey("name")) {
            name = bundle.getString("name");
        }

        txtDescription = (TextView) findViewById(R.id.txtDescription);
        txtDescription.setText(Html.fromHtml(description));

        initToolbar(name);
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar(String title) {
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.main_toolbar);
        toolbarTop.setTitle(title);

        toolbarTop.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        Drawable drawable = ContextCompat.getDrawable(about_company_description.this, R.drawable.ic_toolbar_icon_back_24dp);
        toolbarTop.setNavigationIcon(drawable);
        //setSupportActionBar(toolbarTop);
        setSupportActionBar(toolbarTop);

        //setHasOptionsMenu(true);

        toolbarTop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
