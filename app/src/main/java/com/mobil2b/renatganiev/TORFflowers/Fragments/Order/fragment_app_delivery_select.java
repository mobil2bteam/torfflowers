package com.mobil2b.renatganiev.torfflowers.Fragments.Order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterDeliveryList;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Cart.ObjCart;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Delivery.ObjDelivery;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjCards.ObjCards;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBBag;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.Fragments.Bag.fragment_app_bag;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerDelivery;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.city_select;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by renatganiev on 20.01.18.
 */

public class fragment_app_delivery_select  extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_delivery_select";
    public static final int LAYOUT = R.layout.fragment_app_delivery_select;
    View rootView;
    public CallbackAction callbackActionSuper;

    RecyclerView mRecyclerView;
    LinearLayoutManager manager;
    AdapterDeliveryList mAdapterSearch;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    DeliveryCityView deliveryCityView;
    LinearLayout layout_select_city;

    ModuleObjects moduleObjects;
    ArrayList<ObjDelivery> arrayListDelivery = new ArrayList<>();

    public static fragment_app_bag getInstance(Bundle args) {
        fragment_app_bag fragment = new fragment_app_bag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        layout_select_city      = rootView.findViewById(R.id.layout_select_city);
        mSwipeRefreshLayout     = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        deliveryCityView = new DeliveryCityView(getContext(), PublicData.SETTINGS);
        deliveryCityView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), city_select.class);
                startActivityForResult(i, deliveryCityView.REZULT_REQUEST_SELECT_CITY);
            }
        });
        layout_select_city.removeAllViews();
        layout_select_city.addView(deliveryCityView);
        layout_select_city.setVisibility(View.VISIBLE);


        moduleObjects = new ModuleObjects(getActivity(), callBackSendDate);

        intiRecyclerView();
        refrashAllDate();

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        deliveryCityView.onActivityResult(requestCode, resultCode, data);

        if (requestCode == deliveryCityView.REZULT_REQUEST_SELECT_CITY) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    getBagList();
                }
            }
        }
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(true);
        manager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void refrashAllDate() {

        if (PublicData.objCart != null) {
            if (PublicData.objCart.delivery != null) {

                if (PublicData.objCart.delivery.size() > 0) {

                    arrayListDelivery = new ArrayList<>();
                    for (ObjDelivery objDelivery : PublicData.objCart.delivery) {
                        if (objDelivery.type.equals(PublicData.DELIVERY_TYPE_PICKUP)) {
                            ObjCity objCity = deliveryCityView.getCity();
                            if (objCity != null) {
                                boolean delivery_pickup = false;
                                int price_min = Integer.MAX_VALUE;
                                if (PublicData.SETTINGS != null) {
                                    if (PublicData.SETTINGS.company_info != null) {
                                        if (PublicData.SETTINGS.company_info.points != null) {
                                            for (ObjCompanyPoint objCompanyPoint : PublicData.SETTINGS.company_info.points) {
                                                if (objCompanyPoint.check_point_pickup) {
                                                    if (objCompanyPoint.contacts != null) {
                                                        if (objCompanyPoint.contacts.address != null) {
                                                            if ((objCompanyPoint.contacts.address.city_id == objCity.id) ||
                                                                (objCompanyPoint.contacts.address.city.equals(objCity.name))) {
                                                                delivery_pickup = true;
                                                                if (price_min >= objCompanyPoint.summ_delivery) {
                                                                    price_min = (int)objCompanyPoint.summ_delivery;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                /**
                                 * ДОБАВИЛИ САМОВЫВОЗ. ЕСЛИ ЕСТЬ ХОТЬ ОДНА ТОЧКА САМОВЫВОЗА В ЭТОМ ГОРОДЕ
                                 * */
                                if (delivery_pickup) {
                                    if (price_min != 0) {
                                        objDelivery.price_str = "от ".concat(String.valueOf(price_min).concat(" р."));
                                    }
                                    arrayListDelivery.add(objDelivery);
                                }

                            }
                        } else {
                            arrayListDelivery.add(objDelivery);
                        }
                    }

                    mAdapterSearch = new AdapterDeliveryList(getActivity(), arrayListDelivery);
                    mRecyclerView.setAdapter(mAdapterSearch);
                    mAdapterSearch.notifyDataSetChanged();
                    /*
                    * КЛИК НА КАРТОЧКУ
                    * **/
                    mAdapterSearch.setOnClickListenerDelivery(new ClickListenerDelivery() {
                        @Override
                        public void onClick(View view, ObjDelivery objDelivery) {
                            //Toast.makeText(getContext(), objDelivery.name, Toast.LENGTH_SHORT).show();
                            if (callbackActionSuper != null) {
                                PublicData.objDelivery = objDelivery;
                                callbackActionSuper.onAction(TAG, PublicData.SELECT_DELIVERY, new Bundle());
                            }
                        }
                    });
                }
            }
        }
    }

    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }

        @Override
        public void onReturnAnswer(final String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    String rezData = OpenBlocks.getOpenData(result);
                    if (rezData.equals(PublicData.QUESTION_TYPE_GET_BAG)) {
                        PublicData.objCart = new ObjCart(result);
                        refrashAllDate();
                    }
                }
            });
        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    /**
     * Пересчет корзины
     * */
    void getBagList() {
        DBBag bag = new DBBag(getContext());
        String cart = bag.getOfferStringCount();
        bag.close();
        if (!cart.equals("")) {
            String promo = "";
            ObjCards objCards = null;
            int ball = 0;
            if (PublicData.objCart != null) {
                if (PublicData.objCart.promoCode != null) {
                    if (PublicData.objCart.promoCode.code != null) {
                        if (!PublicData.objCart.promoCode.code.equals("")) {
                            promo = PublicData.objCart.promoCode.code;
                        }
                    }
                }
                if (PublicData.objCart.card != null) {
                    objCards = PublicData.objCart.card;
                    ball = objCards.taken_balls;
                }
            }
            moduleObjects.getBagList(true, cart, promo, objCards, ball, deliveryCityView.getCity().id, User.getCurrentSession());
        } else {
            //emptyBag();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }


    @Override
    public void onRefresh() {
        getBagList();
    }


    @Override
    public void onPause() {
        super.onPause();
        moduleObjects.cancelAllRequest();
    }
}
