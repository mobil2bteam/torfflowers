package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.mobil2b.renatganiev.torfflowers.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjSocialLink {
    public String youtube, vk, facebook, instagram, web, twitter, ok;
    public Bitmap bmp_youtube, bmp_vk, bmp_facebook, bmp_instagram, bmp_web, bmp_twitter, bmp_ok;
    public Context mContext;

    public ArrayList<ObjSocialLinkItem> socialLinkItems = new ArrayList<>();

    public ObjSocialLink() {
        init();
    }
    public ObjSocialLink(JSONObject json, Context context) {
        mContext = context;
        init();
        loadJSONObject(json);
    }
    public ObjSocialLink(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    private void init() {

        if (mContext != null) {
            bmp_vk          = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_vk);
            bmp_facebook    = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_facebook);
            bmp_instagram   = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_instagram);
            bmp_youtube     = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_youtube);
            bmp_ok          = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_ok);
            bmp_twitter     = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_twitter);
        }

    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("vk")) {
                this.vk = json.getString("vk");
                socialLinkItems.add(new ObjSocialLinkItem(this.vk, bmp_vk));
            }
            if (json.has("facebook")) {
                this.facebook = json.getString("facebook");
                socialLinkItems.add(new ObjSocialLinkItem(this.facebook, bmp_facebook));
            }
            if (json.has("instagram")) {
                this.instagram = json.getString("instagram");
                socialLinkItems.add(new ObjSocialLinkItem(this.instagram, bmp_instagram));
            }
            if (json.has("ok")) {
                this.ok = json.getString("ok");
                socialLinkItems.add(new ObjSocialLinkItem(this.ok, bmp_ok));
            }
            if (json.has("youtube")) {
                this.youtube = json.getString("youtube");
                socialLinkItems.add(new ObjSocialLinkItem(this.youtube, bmp_youtube));
            }
            if (json.has("twitter")) {
                this.twitter = json.getString("twitter");
                socialLinkItems.add(new ObjSocialLinkItem(this.twitter, bmp_twitter));
            }
            if (json.has("web")) {
                this.web = json.getString("web");
            }

        } catch (JSONException e) {
        }
    }
}
