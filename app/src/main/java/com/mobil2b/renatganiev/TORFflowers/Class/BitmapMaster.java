package com.mobil2b.renatganiev.torfflowers.Class;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by renatganiev on 20.07.14.
 */
public class BitmapMaster {
    private static volatile BitmapMaster sInstance;

    public static int WIDTH_IMG_max  = 600;
    public static int WIDTH_IMG  = 500;
    public static int HEIGHT_IMG = 750;
    public static float COF      = (float)1.5;

    private BitmapMaster() {}

    public static BitmapMaster instance() {
        return sInstance;
    }

    public static Bitmap getRoundedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


    public static Bitmap getRoundRectBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, 6, 6, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


    public static Bitmap getCroppedBitmap(Bitmap source, int widthImg) {

        Bitmap output;
        output  = getRectBitmap(source, widthImg);
        output = getRoundedBitmap(output);

        return output;
    }

    public static Bitmap getRectBitmap(Bitmap source, int widthImg) {

        if (source == null) {
            return null;
        }

        final int sourceWidth = source.getWidth();
        final int sourceHeight = source.getHeight();

        int outputWandH;
        int left = 0, top = 0, bottom = 0, right = 0;

        if (sourceWidth >= sourceHeight) {
            outputWandH = sourceHeight;
            top = 0;
            bottom = outputWandH;
            left = ((sourceWidth - sourceHeight) / 2);
            right = left + outputWandH;
        }
        else {
            outputWandH = sourceWidth;
            top = ((sourceHeight - sourceWidth) / 2);
            bottom = top + outputWandH;
            left = 0;
            right = outputWandH;
        }

        Bitmap output = Bitmap.createBitmap(outputWandH, outputWandH, Bitmap.Config.ARGB_8888);

        final Rect dest = new Rect(0, 0, outputWandH, outputWandH);
        final Rect src = new Rect(left, top, right, bottom);

        Canvas canvas = new Canvas(output);
        canvas.drawBitmap(source, src, dest, new Paint());
        output = resize(output, widthImg, widthImg);

        return output;
    }

    public static Bitmap resizeProporcion(Bitmap source, int widthImg) {

        if (source == null) { return null; }

        final int sourceWidth   = source.getWidth();
        final int sourceHeight  = source.getHeight();
        float cof = ( (float) sourceHeight / (float) sourceWidth);

        source = resize(source, widthImg, ((int)(widthImg * cof)));

        return source;
    }

    public static Bitmap resizePortret(Bitmap source, int widthImg, int heightImg, float cof) {

        if (source == null) {
            return null;
        }

        final int sourceWidth = source.getWidth();
        final int sourceHeight = source.getHeight();

        // ПО ВЫСОТЕ НАДО ОПРЕДЕЛИТЬ БУДУЩУЮ ШИРИНУ.
        int left = 0, top = 0, bottom = 0, right = 0;

        int newWidth  = (int)(sourceHeight / cof);
        int newHeight = sourceHeight;

        if (sourceWidth < newWidth) {
            // НЕОБХОДИМО РАСЧИТАТЬ НОВУЮ ВЫСОТУ.
            newHeight   = (int)(sourceWidth * cof);
            newWidth    = sourceWidth;
        }

        left   = (sourceWidth - newWidth) / 2;
        top    = (sourceHeight - newHeight) / 2;
        right  = left + newWidth;
        bottom = top + newHeight;

        // ПО НОВОЙ ВЫСОТЕ И ШИРИНЕ НАДО ОТЦЕНТРИРОВАТЬ И ВЫРЕЗАТЬ ФОТОГРАФИЮ.
        Bitmap output = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);
        final Rect dest = new Rect(0, 0, newWidth, newHeight);
        final Rect src = new Rect(left, top, right, bottom);
        Canvas canvas = new Canvas(output);
        canvas.drawBitmap(source, src, dest, new Paint());

        // УЖИМАЕМ ДО РАЗМЕРОВ
        output = resize(output, widthImg, heightImg);

        return output;
    }


    public static Bitmap resize(Bitmap bmp, int width, int height)
    {
        //Matrix matrix = new Matrix();
        //matrix.postScale(width, height);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bmp, width, height, false);
        return resizedBitmap;
    }

    public static byte[] BitmapToByte(Bitmap bmp)
    {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap ByteToBitmap(byte[] bitmapdata)
    {
        Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
        return bitmap;
    }

    public static Bitmap decodeFile(String filePath) throws FileNotFoundException {

        FileInputStream fis;
        fis = new FileInputStream(filePath);

        BufferedInputStream bis = new BufferedInputStream(fis);
        Bitmap img = BitmapFactory.decodeStream(bis);

        return img;
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }


    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;//RGB_565;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }


    public static Rect getWidthImage(Resources res, int resId) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;//RGB_565;
        BitmapFactory.decodeResource(res, resId, options);

        Rect r = new Rect(0, 0, options.outWidth, options.outHeight);
        return r;
    }

    public static Bitmap eraseColor(Bitmap src, int color) {
        int width = src.getWidth();
        int height = src.getHeight();
        Bitmap b = src.copy(Bitmap.Config.ARGB_8888, true);
        b.setHasAlpha(true);

        int[] pixels = new int[width * height];
        src.getPixels(pixels, 0, width, 0, 0, width, height);

        for (int i = 0; i < width * height; i++) {
            if (pixels[i] == color) {
                pixels[i] = 0;
            }
        }

        b.setPixels(pixels, 0, width, 0, 0, width, height);

        return b;
    }
}
