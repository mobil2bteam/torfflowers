package com.mobil2b.renatganiev.torfflowers.Fragments.User.Info;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterOrderUserList;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterEmptyRecyclerView;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order.ObjOrderUserList;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerOrderUserList;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.order_item;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_order_list extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_order_list";
    public static final int LAYOUT = R.layout.fragment_app_order_list;
    View rootView;

    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;


    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    private AdapterOrderUserList mAdapterSearch;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public static fragment_app_order_list getInstance(Bundle args) {
        fragment_app_order_list fragment = new fragment_app_order_list();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.

        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        /*txtCaption              = (TextView)        rootView.findViewById(R.id.txtCaption);
        imgHead                 = (ImageView)       rootView.findViewById(R.id.imgHead);*/

        mSwipeRefreshLayout     = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        intiRecyclerView();
        loadOrders(false);

        return rootView;
    }


    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // В виде списка
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void refrashAllDate() {

        if (User.order_list_active != null) {
            if (User.order_list_active.size() > 0) {
                mAdapterSearch = new AdapterOrderUserList(getActivity(), User.order_list_active);
                mRecyclerView.setAdapter(mAdapterSearch);
                mAdapterSearch.notifyDataSetChanged();

                mAdapterSearch.setOnClickListener(new ClickListenerOrderUserList() {
                    @Override
                    public void onClick(View view, ObjOrderUserList objOrderUserList) {
                        if (getActivity() == null) return;
                        Intent intent = new Intent(getActivity(), order_item.class);
                        intent.putExtra("order_id", objOrderUserList.id);
                        startActivity(intent);
                    }
                });
            } else {
                AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_order), R.drawable.ic_icon_order_history_empty);
                mRecyclerView.setAdapter(mEmptyAdapter);
            }
        } else {
            AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_order), R.drawable.ic_icon_order_history_empty);
            mRecyclerView.setAdapter(mEmptyAdapter);
        }
    }


    void loadOrders(boolean cache) {
        ModuleObjects moduleObjects = new ModuleObjects(getActivity(), new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
                mSwipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {

                try {

                    JSONObject json = new JSONObject(result);
                    String typeDate = OpenBlocks.getOpenData(json); // Получаем то что реально пришло в ответ.

                    if (typeDate.equals(PublicData.QUESTION_TYPE_USER_ORDER_LIST)) {

                        if (json.has("orders")) {
                            JSONObject jsonOrders = json.getJSONObject("orders");

                            if (jsonOrders.has("actual")) {
                                if (User.order_list_active != null) {
                                    User.order_list_active.clear();
                                } else {
                                    User.order_list_active = new ArrayList<>();
                                }
                                for (int i =0;i<jsonOrders.getJSONArray("actual").length();i++){
                                    ObjOrderUserList it = new ObjOrderUserList(jsonOrders.getJSONArray("actual").getJSONObject(i));
                                    User.order_list_active.add(it);
                                }
                            }

                            // here you check the value of getActivity() and break up if needed
                            if(getActivity() == null) return;
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    if(getActivity() == null) return;
                                    mSwipeRefreshLayout.setRefreshing(false);
                                    refrashAllDate();
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            if(getActivity() == null) return;
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }

                if(getActivity() == null) return;
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        if(getActivity() == null) return;
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });

            }

            @Override
            public void onError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            if(getActivity() == null) return;
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            if(getActivity() == null) return;
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_data), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }

            }

            @Override
            public void onHttpError(final String text, int code_error, ObjLink linkURL) {
                if (!text.equals("")) {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            if(getActivity() == null) return;
                            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    // here you check the value of getActivity() and break up if needed
                    if(getActivity() == null) return;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            if(getActivity() == null) return;
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_text_fatal), Toast.LENGTH_SHORT).show();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            }
        });

        if (User.getCurrentSession() != null) {
            moduleObjects.getUserOrderList(cache, User.getCurrentSession());
        } else {
            AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_order), R.drawable.ic_icon_order_history_empty);
            mRecyclerView.setAdapter(mEmptyAdapter);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRefresh() {
        loadOrders(true);
    }


}
