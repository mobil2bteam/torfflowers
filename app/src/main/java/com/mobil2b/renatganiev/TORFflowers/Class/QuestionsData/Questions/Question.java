package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Questions;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.Cache;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.SendDate;

/**
 * Created by renatganiev on 18.04.16.
 */
public class Question {

    private Context mContext;
    private Cache cache;
    private SendDate send;

    public Question(Context ctx) {
        mContext    = ctx;
        send        = new SendDate();
    }

    /**
     * Посылаем запрос на сервер, с проверкой на кэш и прочее.
     * */
    public void SendQuestionGet(ObjLink objLink, CallBackSendDate callback, boolean ignorCache) {

        if (objLink != null)
            if (objLink.url != null)
                if (!objLink.url.url.equals("")) {
                    String url = objLink.url.url;
                    SendQuestionGet(url, callback, ignorCache, objLink);
                    return;
                }

    }

    private void SendQuestionGet(String url, CallBackSendDate callback, boolean ignorCache, ObjLink objLink) {

        String fullUrl = getParamToGet(url, objLink);
        cache       = new Cache(mContext);

        if ((PublicData.CACHE_QUESTION) && (!ignorCache)) {
            // Первым делом проверяем наличие кэша
            new getCacheAnsyc(callback, objLink, url, mContext).execute(fullUrl);
        } else {
            send.send("question", callback, url, send.SEND_METOD_GET, objLink);
        }

        //send.send("question", callback, url, send.SEND_METOD_GET, objLink);
    }


    public void SendQuestionPost(ObjLink objLink, CallBackSendDate callback, boolean ignorCache) {

        if (objLink != null)
            if (objLink.url != null)
                if (!objLink.url.url.equals("")) {
                    String url = objLink.url.url;
                    SendQuestionPost(url, objLink.param, callback, ignorCache, objLink);
                    return;
                }

    }


    private void SendQuestionPost(String url, ArrayList<ObjParam> objParamArray, CallBackSendDate callback, boolean ignorCache, ObjLink objLink) {
        String fullUrl = getParamToGet(url, objLink);
        cache       = new Cache(mContext);

        if ((PublicData.CACHE_QUESTION) && (!ignorCache)) {
            new getCacheAnsyc(callback, objLink, url, mContext).execute(fullUrl);
        } else {
            send.send("question", callback, url, send.SEND_METOD_POST, objLink);
        }
    }

    class getCacheAnsyc extends AsyncTask<String, String, Cache.CacheValue>
    {
        private CallBackSendDate listener;
        private ObjLink objLink;
        private Cache cache;
        private String url;

        public getCacheAnsyc(CallBackSendDate listener, ObjLink link, String urlString, Context context){
            this.listener   = listener;
            this.objLink    = link;
            this.url        = urlString;
            this.cache      = new Cache(context);
        }

        //@TargetApi(Build.VERSION_CODES.FROYO)
        @Override
        protected Cache.CacheValue doInBackground(String... params) {
            //listener.onSend("question", objLink);
            Cache.CacheValue cacheValue = cache.getCache(params[0]);
            return cacheValue;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Cache.CacheValue result) {
            if (result != null) {
                // Кэш живой, возвращаем.
                listener.onReturnAnswer(result.value, true, result.datetime, objLink);
            } else {
                send.send("question", listener, url, send.SEND_METOD_GET, objLink);
            }
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            listener.onSend("question", objLink);
            super.onPreExecute();
        }
    }

    public void cancelAllRequest() {
        send.cancelAllCall();
    }


    // ФОРМИРУЕМ ПАРАМЕТРЫ В ВИДЕ СТРОКИ ДЛЯ ОТПРАВКИ GET МЕТОДОМ
    public String getParamToGet(String url, ObjLink link) {

        if (link == null) {
            return "";
        }

        String start = "?";
        if (url.indexOf("?") != -1) {
            start = "&";
        }

        String result = "";
        for(ObjParam param : link.param) {

            String param_new = param.value.replace(" ", "%20");

            if (result.equals("")) {
                result = result.concat(start + param.name + "=" + param_new);
            } else {
                result = result.concat("&" + param.name + "=" + param_new);
            }
        }
        return url.concat(result);
    }

}
