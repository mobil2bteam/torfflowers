package com.mobil2b.renatganiev.torfflowers.Class.Authorization.loginToServer;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackPasswordMemory;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjMessage;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjURL;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Questions.Question;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackAuthorization;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackPasswordChange;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackUserExist;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackUserUpdate;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 02.04.16.
 */
public class GetLoginToServer {

    public Context mContext;
    private Question q;

    private CallbackAuthorization clabackAuthorization;
    private CallbackUserExist callbackUserExist;
    private CallbackPasswordChange callbackPasswordChange;
    private CallbackUserUpdate  callbackUserUpdate;
    private CallbackPasswordMemory callbackPasswordMemory;
    DataSession sessionUser;

    //private ObjURL objUrl;

    private String url_suite, key, version, versionRelease;

    public GetLoginToServer(Context ctx) {
        mContext = ctx;
        initDefaultParamInfo();
        q = new Question(mContext);
    }

    private void initDefaultParamInfo() {
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
            version         = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version         = "0";
        }

        url_suite       = mContext.getResources().getString(R.string.url);
        key             = mContext.getResources().getString(R.string.mobil2b_key);

        versionRelease  = Build.VERSION.RELEASE;
    }

    // Проверка сессии на валидность на сервере
    public void loginToServer(DataSession session, ObjURL url, CallbackAuthorization callback) {
        if (session != null) {

            clabackAuthorization = callback;
            sessionUser = session;
            // Запрос на сервере.

            ArrayList<ObjParam> arrayParam = new ArrayList<>();

            if (session.token == null)    session.token = "";
            if (session.email       == null)    session.email       = "";

            ObjLink link = new ObjLink();
            link.url = url;


            if (!session.token.equals("")) {
                arrayParam.add(new ObjParam("token", session.token));

                link.param = getDefaultParam();
                link.addParam(arrayParam);

                //q.SendQuestionPost(link, answerResult, true);
                if (link.url.metod.equals("POST")) {
                    q.SendQuestionPost(link, answerResult, true);
                } else {
                    q.SendQuestionGet(link, answerResult, true);
                }
            }
        }
    }

    // Проверка сессии на валидность на сервере (По прямому через пароль и логин)
    public void loginToServer(String login, String password, ObjURL url, CallbackAuthorization callback) {
        if ((!login.equals("")) &&  (!password.equals(""))) {
            //objUrl = url;

            clabackAuthorization = callback;
            ArrayList<ObjParam> arrayParam = new ArrayList<>();
            arrayParam.add(new ObjParam("login",        login));
            arrayParam.add(new ObjParam("password",      password));

            ObjLink link = new ObjLink();
            link.url = url;
            link.param = getDefaultParam();
            link.addParam(arrayParam);

            //q.SendQuestion(link, answerResult, true);
            if (link.url.metod.equals("POST")) {
                q.SendQuestionPost(link, answerResult, true);
            } else {
                q.SendQuestionGet(link, answerResult, true);
            }
        }
    }

    // Регистрация на сервере
    public void registrationToServer(ObjURL url, ArrayList<ObjParam> listParam, CallbackAuthorization callback, DataSession session) {

        if (session != null) {
            sessionUser = session;

            //objUrl = url;

            clabackAuthorization = callback;

            ObjLink link = new ObjLink();
            link.url = url;
            link.param = getDefaultParam();
            link.addParam(listParam);
            link.param.add(new ObjParam("registration", "1"));

            if (link.url.metod.equals("POST")) {
                q.SendQuestionPost(link, answerResult, true);
            } else {
                q.SendQuestionGet(link, answerResult, true);
            }
        }
    }

    //
    public void editToServer(DataSession session, ObjURL url, CallbackUserUpdate callback) {
        //objUrl = url;

        callbackUserUpdate = callback;

        ObjLink link = new ObjLink();
        link.url = url;
        link.param = getDefaultParam();
        link.addParam(session.getParamList());

        if (link.url.metod.equals("POST")) {
            q.SendQuestionPost(link, answerUserUpdate, true);
        } else {
            q.SendQuestionGet(link, answerUserUpdate, true);
        }
    }

    // Регистрация на сервере
    public void changePasswordToServer(DataSession session, ObjURL url, String oldPassword, String newPassword, CallbackPasswordChange callback) {

        callbackPasswordChange = callback;

        ObjLink link = new ObjLink();
        link.url    = url;
        link.param  = getDefaultParam();

        link.param.add(new ObjParam("token", session.token));
        link.param.add(new ObjParam("password_old", oldPassword));
        link.param.add(new ObjParam("password_new", newPassword));


        if (link.url.metod.equals("POST")) {
            q.SendQuestionPost(link, answerPasswordChange, true);
        } else {
            q.SendQuestionGet(link, answerPasswordChange, true);
        }

    }

    // Выслать новый пароль
    public void sendPasswordMemoryToServer(ObjURL url, String login, CallbackPasswordMemory callback) {
        callbackPasswordMemory = callback;
        ObjLink link = new ObjLink();
        link.url = url;
        link.param = getDefaultParam();
        link.param.add(new ObjParam("email", login));

        if (link.url.metod.equals("POST")) {
            q.SendQuestionPost(link, answerPasswordMemory, true);
        } else {
            q.SendQuestionGet(link, answerPasswordMemory, true);
        }
    }


    // Регистрация на сервере
    public void userExistToServer(ObjURL url, String login, CallbackUserExist callback) {

        callbackUserExist = callback;

        ObjLink link = new ObjLink();
        link.url = url;
        link.param = getDefaultParam();
        link.param.add(new ObjParam("login", login));

        if (link.url.metod.equals("POST")) {
            q.SendQuestionPost(link, answerResultExist, true);
        } else {
            q.SendQuestionGet(link, answerResultExist, true);
        }
    }

    CallBackSendDate answerResultExist = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            if (callbackUserExist != null) {
                callbackUserExist.onStart();
            }
        }
        @Override
        public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            if (callbackUserExist != null) {
                try {
                    //создали читателя json объектов и отдали ему строку - result
                    JSONObject json = new JSONObject(result);

                    if (json.has("result")) {
                        JSONObject resultJson = json.getJSONObject("result");
                        if (resultJson.has("success")) {
                            boolean exist = resultJson.getBoolean("success");
                            callbackUserExist.onSuccess(exist);
                        }
                    }

                } catch (JSONException e) {
                    callbackUserExist.onError(e.getMessage(), 0);
                }
            }
        }

        @Override
        public void onError(String text, int code_error, ObjLink linkURL) {
            if (callbackUserExist != null) {
                callbackUserExist.onError(text, code_error);
            }
        }

        @Override
        public void onHttpError(String text, int code_error, ObjLink linkURL) {
            if (callbackUserExist != null) {
                callbackUserExist.onError(text, code_error);
            }
        }
    };

    CallBackSendDate answerUserUpdate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            if (callbackUserUpdate != null) {
                callbackUserUpdate.onStart();
            }
        }
        @Override
        public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            if (callbackUserUpdate != null) {
                try {
                    //создали читателя json объектов и отдали ему строку - result
                    JSONObject json = new JSONObject(result);

                    if (json.has("user")) {

                        DataSession newSession = new DataSession(result, mContext);

                        if (sessionUser == null) {
                            sessionUser = new DataSession();
                        }
                        newSession.copySession(sessionUser);
                        newSession.loadJSONObject(json.getJSONObject("user"));
                        sessionUser.copySession(newSession);

                        User.setCurrentSession(newSession);
                        callbackUserUpdate.onSuccess(newSession);

                        return;
                    } else {
                        int code = 0;
                        String text_error = "";
                        if (json.has("code")) {
                            code = json.getInt("code");
                        }
                        if (json.has("error")) {
                            JSONObject error = json.getJSONObject("error");
                            if (error.has("code")) {
                                String strcode = error.getString("code");
                                code = Integer.parseInt(strcode);
                            }
                            if (error.has("text")) text_error = error.getString("text");
                            if (error.has("message")) text_error = error.getString("message");
                        }
                        callbackUserUpdate.onError(text_error, code);
                    }

                } catch (JSONException e) {
                    callbackUserUpdate.onError(e.getMessage(), 0);
                }
            }
        }

        @Override
        public void onError(String text, int code_error, ObjLink linkURL) {
            if (callbackUserUpdate != null) {
                callbackUserUpdate.onError(text, code_error);
            }
        }

        @Override
        public void onHttpError(String text, int code_error, ObjLink linkURL) {
            if (callbackUserUpdate != null) {
                callbackUserUpdate.onError(text, code_error);
            }
        }
    };



    CallBackSendDate answerPasswordChange = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            if (callbackPasswordChange != null) {
                callbackPasswordChange.onStart();
            }
        }
        @Override
        public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            if (callbackPasswordChange != null) {
                try {
                    //создали читателя json объектов и отдали ему строку - result
                    JSONObject json = new JSONObject(result);

                    if (json.has("user")) {

                        DataSession newSession = new DataSession(result, mContext);

                        if (sessionUser == null) {
                            sessionUser = new DataSession();
                        }
                        newSession.copySession(sessionUser);
                        newSession.loadJSONObject(json.getJSONObject("user"));
                        sessionUser.copySession(newSession);

                        User.setCurrentSession(newSession);
                        callbackPasswordChange.onSuccess(newSession);

                        return;
                    } else {
                        int code = 0;
                        String text_error = "";
                        if (json.has("code")) {
                            code = json.getInt("code");
                        }
                        if (json.has("error")) {
                            JSONObject error = json.getJSONObject("error");
                            if (error.has("code")) {
                                String strcode = error.getString("code");
                                code = Integer.parseInt(strcode);
                            }
                            if (error.has("text")) text_error = error.getString("text");
                            if (error.has("message")) text_error = error.getString("message");
                        }
                        callbackPasswordChange.onError(text_error, code);
                    }

                } catch (JSONException e) {
                    callbackPasswordChange.onError(e.getMessage(), 0);
                }
            }
        }

        @Override
        public void onError(String text, int code_error, ObjLink linkURL) {
            if (callbackPasswordChange != null) {
                callbackPasswordChange.onError(text, code_error);
            }
        }

        @Override
        public void onHttpError(String text, int code_error, ObjLink linkURL) {
            if (callbackPasswordChange != null) {
                callbackPasswordChange.onError(text, code_error);
            }
        }
    };


    CallBackSendDate answerPasswordMemory = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            if (callbackPasswordMemory != null) {
                callbackPasswordMemory.onStart();
            }
        }
        @Override
        public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            if (callbackPasswordMemory != null) {
                try {
                    //создали читателя json объектов и отдали ему строку - result
                    JSONObject json = new JSONObject(result);

                    if (json.has("message")) {
                        ObjMessage message = new ObjMessage(json);
                        callbackPasswordMemory.onSuccess(message);

                        return;
                    } else {
                        int code = 0;
                        String text_error = "";
                        if (json.has("code")) {
                            code = json.getInt("code");
                        }
                        if (json.has("error")) {
                            JSONObject error = json.getJSONObject("error");
                            if (error.has("code")) {
                                String strcode = error.getString("code");
                                code = Integer.parseInt(strcode);
                            }
                            if (error.has("text")) text_error = error.getString("text");
                            if (error.has("message")) text_error = error.getString("message");
                        }
                        callbackPasswordMemory.onError(text_error, code);
                    }

                } catch (JSONException e) {
                    callbackPasswordMemory.onError(e.getMessage(), 0);
                }
            }
        }

        @Override
        public void onError(String text, int code_error, ObjLink linkURL) {
            if (callbackPasswordMemory != null) {
                callbackPasswordMemory.onError(text, code_error);
            }
        }

        @Override
        public void onHttpError(String text, int code_error, ObjLink linkURL) {
            if (callbackPasswordMemory != null) {
                callbackPasswordMemory.onError(text, code_error);
            }
        }
    };




    CallBackSendDate answerResult = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            clabackAuthorization.onStart(txt);
        }

        @Override
        public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {

            boolean user_token = false, user_password = false, user_registration = false, user_edit = false, user_change_password = false;

            for (ObjParam p : linkURL.param) {
                if (p.name.equals("token")) {
                    user_token = true;
                }
                if (p.name.equals("password")) {
                    user_password = true;
                }
                if (p.name.equals("registration")) {
                    user_registration = true;
                }
                if (p.name.equals("changePassword")) {
                    user_change_password = true;
                }
                if (p.name.equals("edit")) {
                    user_edit = true;
                }
            }

            if (user_registration) {
                getReturnServerJSON(result, User.USER_REGISTRATION);
                return;
            }
            if (user_password) {
                getReturnServerJSON(result, User.USER_PASSWORD);
                return;
            }
            if (user_token) {
                getReturnServerJSON(result, User.USER_TOKEN);
                return;
            }
        }

        @Override
        public void onError(String text, int code_error, ObjLink linkURL) {
            for (ObjParam p : linkURL.param) {
                if (p.name.equals("token")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_TOKEN);
                    return;
                }
                if (p.name.equals("password")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_PASSWORD);
                    return;
                }
                if (p.name.equals("registration")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_REGISTRATION);
                    return;
                }
                if (p.name.equals("changePassword")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_CHANGE_PASSWORD);
                    return;
                }
                if (p.name.equals("edit")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_EDIT);
                    return;
                }
            }
        }

        @Override
        public void onHttpError(String text, int code_error, ObjLink linkURL) {
            for (ObjParam p : linkURL.param) {
                if (p.name.equals("token")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_TOKEN);
                    return;
                }
                if (p.name.equals("password")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_PASSWORD);
                    return;
                }
                if (p.name.equals("registration")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_REGISTRATION);
                    return;
                }
                if (p.name.equals("changePassword")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_CHANGE_PASSWORD);
                    return;
                }
                if (p.name.equals("edit")) {
                    clabackAuthorization.onError(sessionUser, text, code_error, User.USER_EDIT);
                    return;
                }
            }
        }
    };


    //ОБРАБОТКА ОТВЕТА ОТ САЙТА
    private void getReturnServerJSON(String text, int userTypeLogin) {
        try {
            //создали читателя json объектов и отдали ему строку - result
            JSONObject json = new JSONObject(text);

            if (json.has("message")) {
                JSONObject jsonMessage = json.getJSONObject("message");
                if (jsonMessage.has("text")) {
                    String msg = jsonMessage.getString("text");
                    Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                }
            }

            if (json.has("user")) {

                DataSession newSession = new DataSession(text, mContext);

                if (sessionUser == null) {
                    sessionUser = new DataSession();
                }
                newSession.copySession(sessionUser);
                newSession.loadJSONObject(json.getJSONObject("user"));
                sessionUser.copySession(newSession);

                User.setCurrentSession(newSession);
                clabackAuthorization.onSuccess(newSession, userTypeLogin);

                return;
            } else {
                if (json.has("code")) {
                    int code = json.getInt("code");
                    clabackAuthorization.onError(sessionUser, text, code, userTypeLogin);
                    return;
                }
                if (json.has("error")) {
                    JSONObject error = json.getJSONObject("error");
                    String text_error = "";
                    int code = 0;
                    if (error.has("code")) {
                        String strcode = error.getString("code");
                        code = Integer.parseInt(strcode);
                    }
                    if (error.has("text")) text_error = error.getString("text");
                    if (error.has("message")) text_error = error.getString("message");
                    //if (userTypeLogin == User.USER_REGISTRATION) {
                    clabackAuthorization.onError(sessionUser, text_error, code, userTypeLogin);
                    //}

                    return;
                }
            }
        } catch (JSONException e) {

        }
    }


    /**
     * Список обязательных параметров
     * */
    public ArrayList<ObjParam> getDefaultParam() {
        ArrayList<ObjParam> param = new ArrayList<>();
        ObjParam k              = new ObjParam("key",           key);
        ObjParam app_version    = new ObjParam("app_version",   version);
        ObjParam device         = new ObjParam("device",        "Android");
        ObjParam os_version     = new ObjParam("os_version",    versionRelease);

        param.add(k);
        param.add(app_version);
        param.add(device);
        param.add(os_version);
        return param;
    }


}
