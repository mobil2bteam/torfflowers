package com.mobil2b.renatganiev.torfflowers.Fragments.User.Welcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.GroupView.ObjGroupView;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.ControlView.GroupProduct.ProductView;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.city_select;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_user_welcome extends Fragment {

    public static final String TAG = "fragment_app_user_welcome";
    public static final int LAYOUT = R.layout.fragment_app_user_welcome;
    View rootView;
    public CallbackAction callbackActionSuper;

    LinearLayout layout_select_city, layout_group_view_watched;
    Button btnRegistration, btnLogin;
    DeliveryCityView deliveryCityView;

    public static fragment_app_user_welcome getInstance(Bundle args) {
        fragment_app_user_welcome fragment = new fragment_app_user_welcome();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        layout_select_city          = rootView.findViewById(R.id.layout_select_city);
        layout_group_view_watched   = rootView.findViewById(R.id.layout_group_view_watched);
        btnRegistration             = rootView.findViewById(R.id.btnRegistration);
        btnLogin                    = rootView.findViewById(R.id.btnLogin);

        if (PublicData.HOME.flag_show_city_delivery) {
            deliveryCityView = new DeliveryCityView(getContext(), PublicData.SETTINGS);
            deliveryCityView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), city_select.class);
                    startActivityForResult(i, deliveryCityView.REZULT_REQUEST_SELECT_CITY);
                }
            });
            layout_select_city.removeAllViews();
            layout_select_city.addView(deliveryCityView);
            layout_select_city.setVisibility(View.VISIBLE);
        }


        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callbackActionSuper != null) {
                    callbackActionSuper.onAction(TAG, PublicData.USER_REGISTRATION_CLICK, new Bundle());
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callbackActionSuper != null) {
                    callbackActionSuper.onAction(TAG, PublicData.USER_LOGIN_CLICK, new Bundle());
                }
            }
        });


        ModuleObjects moduleObjects = new ModuleObjects(getActivity(), new CallBackSendDate() {
            @Override
            public void onSend(String txt, ObjLink linkURL) {
            }

            @Override
            public void onReturnAnswer(String result, boolean cache, long cache_datetime, ObjLink linkURL) {

                String type = OpenBlocks.getOpenData(result);

                try {
                    if (type.equals(PublicData.QUESTION_TYPE_PRODUCTS)) {
                        JSONObject json = new JSONObject(result);
                        if (json.has("products")) {
                            final ArrayList<ObjProductMin> list = new ArrayList<>();
                            for (int i = 0; i < json.getJSONArray("products").length(); i++) {
                                ObjProductMin it = new ObjProductMin(json.getJSONArray("products").getJSONObject(i));
                                list.add(it);
                            }
                            // here you check the value of getActivity() and break up if needed
                            if(getActivity() == null) return;
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    if (list.size() != 0) {
                                        ObjGroupView objGroupView = new ObjGroupView();
                                        objGroupView.button_more = false;
                                        objGroupView.link = null;
                                        objGroupView.name = "Вы недавно смотрели";
                                        objGroupView.count_items_display = 3f;
                                        objGroupView.items.addAll(list);


                                        ProductView productView = new ProductView(getContext(), objGroupView);
                                        layout_group_view_watched.addView(productView);
                                    }
                                }
                            });
                        }
                    }
                } catch (JSONException e) {
                }

            }

            @Override
            public void onError(String text, int code_error, ObjLink linkURL) {
            }

            @Override
            public void onHttpError(String text, int code_error, ObjLink linkURL) {
            }
        });


        String list_id = "1-2-3-4";//favorites.getString();
        if (!list_id.equals("")) {
            moduleObjects.getUpdateListProduct(false, list_id);
        }

        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        deliveryCityView.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

}
