package com.mobil2b.renatganiev.torfflowers.Fragments.Products;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterEmptyRecyclerView;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBrand;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCatalogFastFilter;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjFilter;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerSort;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterProductList;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterSort;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog.ObjCatalogItems;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjSort;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProducts;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerProductMin;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.filter;
import com.mobil2b.renatganiev.torfflowers.product_item;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_products extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_products";
    public static final int LAYOUT = R.layout.fragment_app_products;
    View rootView;
    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;


    RecyclerView mRecyclerView, mRecyclerViewSort;
    StaggeredGridLayoutManager manager;
    LinearLayoutManager managerSort;
    AdapterProductList mAdapterSearch;
    AdapterSort mAdapterSort;


    LinearLayout layout_fast_filter;
    HorizontalScrollView horizontalScrollFastFilter;
    SlidingUpPanelLayout sliding_layout;
    LinearLayout btn_sort, btn_filter;
    TextView txtSortCaption;
    Button btnClearSort;

    ObjProducts objProducts;
    //ObjFilter objFilter = null;
    ObjLink objLink = null;

    ModuleObjects moduleObjects;
    boolean loading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    int REQUEST_OPEN_FILTER = 505;

    Toolbar toolbar;
    AppBarLayout app_bar;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    public static fragment_app_products getInstance(Bundle args) {
        fragment_app_products fragment = new fragment_app_products();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (objProducts == null) {
            objProducts = new ObjProducts();
        }

        if (arg != null) {
            // Если передали каталог в котором надо искать.
            if (arg.containsKey("filter")) {
                objProducts.filter = (ObjFilter)arg.getSerializable("filter");
            }
            if (arg.containsKey("link")) {
                objLink = (ObjLink) arg.getSerializable("link");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);


        mSwipeRefreshLayout         = (SwipeRefreshLayout)  rootView.findViewById(R.id.swipe_container);
        layout_fast_filter          = (LinearLayout) rootView.findViewById(R.id.layout_fast_filter);
        horizontalScrollFastFilter  = (HorizontalScrollView) rootView.findViewById(R.id.horizontalScrollFastFilter);
        sliding_layout              = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);
        btn_sort                    = (LinearLayout) rootView.findViewById(R.id.btn_sort);
        btn_filter                  = (LinearLayout) rootView.findViewById(R.id.btn_filter);
        txtSortCaption              = (TextView)    rootView.findViewById(R.id.txtSortCaption);
        btnClearSort                = (Button)      rootView.findViewById(R.id.btnClearSort);

        //sliding_layout.setAnchorPoint(0.05f); // slide up 50% then stop
        sliding_layout.addPanelSlideListener(panelSlideListener);
        sliding_layout.setSaveEnabled(false);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        initToolbar();

        btn_sort.setVisibility(View.GONE);
        if (PublicData.DIRECTORIES != null) {
            if (PublicData.DIRECTORIES.sort != null) {
                if (PublicData.DIRECTORIES.sort.size() > 0) {
                    btn_sort.setVisibility(View.VISIBLE);
                    intiRecyclerViewSort();

                    btnClearSort.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            for (ObjSort objSort : PublicData.DIRECTORIES.sort) {
                                objSort.check = false;
                            }
                            if (mAdapterSort != null) {
                                mAdapterSort.notifyDataSetChanged();
                            }

                            if (sliding_layout.getPanelState() != SlidingUpPanelLayout.PanelState.COLLAPSED) {
                                sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                            }
                            if (objProducts.filter != null) {
                                objProducts.filter.sort_id = 0;
                                txtSortCaption.setText("Сортировать");
                                moduleObjects.getProducts(false, objProducts.filter, User.getCurrentSession());
                            }
                        }
                    });

                }
            }
        }

        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (objProducts != null) {
                    if (objProducts.filter != null) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("filter", objProducts.filter);
                        Intent intent = new Intent(getActivity(), filter.class);
                        intent.putExtras(bundle);
                        startActivityForResult(intent, REQUEST_OPEN_FILTER);
                    }
                }
            }
        });

        btn_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (objProducts != null) {
                    if (objProducts.filter != null) {
                        if (objProducts.filter.sort_id != 0) {
                            if (PublicData.DIRECTORIES != null) {
                                if (PublicData.DIRECTORIES.sort != null) {
                                    for (ObjSort objSort : PublicData.DIRECTORIES.sort) {
                                        if (objSort.id == objProducts.filter.sort_id) {
                                            objSort.check = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
            }
        });


        moduleObjects = new ModuleObjects(getActivity(), callBackSendDate);

        // here you check the value of getActivity() and break up if needed
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapterSearch = null;
                    if ((objProducts.filter == null) && (objLink == null)) {
                        objProducts.filter = new ObjFilter();
                        objProducts.filter.catalog_id = 0;
                        moduleObjects.getProducts(false, objProducts.filter, User.getCurrentSession());
                    } else {
                        if (objProducts.filter != null) {
                            moduleObjects.getProducts(false, objProducts.filter, User.getCurrentSession());
                        } else {
                            if (objLink != null) {
                                moduleObjects.getDataToLink(false, objLink, User.getCurrentSession());
                            }
                        }
                    }

                }
            });
        }

        return rootView;
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        app_bar = (AppBarLayout) rootView.findViewById(R.id.app_bar);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(0));

            app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    //some other code here
                    ViewCompat.setElevation(appBarLayout, Helps.dpToPx(2));
                }
            });

        }

        toolbar.setTitle("");

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_menu));
            } else {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_menu));
            }
        }

        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                setResult(RESULT_OK);
                finish();

            }
        });*/
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_OPEN_FILTER) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle.containsKey("filter")) {
                        if (objProducts == null) {
                            objProducts = new ObjProducts();
                        }
                        objProducts.filter = (ObjFilter)bundle.getSerializable("filter");
                        objProducts.filter.page = 1;
                        moduleObjects.getProducts(false, objProducts.filter, User.getCurrentSession());
                    }
                }
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (sliding_layout.getPanelState() != SlidingUpPanelLayout.PanelState.COLLAPSED) {
                        sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });

    }


    SlidingUpPanelLayout.PanelSlideListener  panelSlideListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {

        }

        @Override
        public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
            if (newState.equals(SlidingUpPanelLayout.PanelState.ANCHORED)) {}
            if (newState.equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {}
            if (newState.equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {}
        }
    };


    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            mSwipeRefreshLayout.setRefreshing(true);
            //PublicData.showDialog(getActivity());
        }

        @Override
        public void onReturnAnswer(final String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    //PublicData.closeDialog();
                    String rezData = OpenBlocks.getOpenData(result);
                    if (rezData.equals(PublicData.QUESTION_TYPE_PRODUCTS)) {
                        if (objProducts == null) {
                            objProducts = new ObjProducts(result);
                            mAdapterSearch = null;
                        } else {
                            ObjProducts tempProduct = new ObjProducts(result);
                            if (tempProduct.filter != null) {
                                if (tempProduct.filter.page == 1) {
                                    objProducts = new ObjProducts(result);
                                    mAdapterSearch = null;
                                } else {
                                    objProducts.addNewResult(new ObjProducts(result));
                                }
                            }
                        }
                        if (mAdapterSearch != null) {
                            mAdapterSearch.notifyDataSetChanged();
                        } else {
                            refrashAllDate();
                        }
                        loading = false;
                    }
                }
            });
        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    //PublicData.closeDialog();
                    if(getActivity() == null) return;
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    PublicData.closeDialog();
                    if(getActivity() == null) return;
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };


    private void refrashAllDate() {
        intiRecyclerView();

        if (objProducts != null) {

            if (toolbar != null) {
                if (objProducts.filter != null) {
                    toolbar.setSubtitle("Найдено: ".concat(String.valueOf(objProducts.filter.result_count)));
                }
                if (objProducts.options != null) {
                    if (!objProducts.options.title.equals("")) {
                        toolbar.setTitle(objProducts.options.title);
                    } else {

                        String myTitle = "Товары";
                        boolean change = false;

                        if (objProducts.filter != null) {
                            if (!objProducts.filter.search.equals("")) {
                                myTitle = "Результат поиска";
                                change = true;
                            } else {
                                if (objProducts.filter.catalog_id != 0) {
                                    ObjCatalogItems catalogItems = PublicData.DIRECTORIES.catalog.getCatalogItems(objProducts.filter.catalog_id, PublicData.DIRECTORIES.catalog.items);
                                    if (catalogItems != null) {
                                        myTitle = catalogItems.name;
                                        change = true;
                                    }
                                }

                                if (!change) {
                                    if (objProducts.filter.brand_id != null) {
                                        if (objProducts.filter.brand_id.size() > 0) {
                                            if ((PublicData.DIRECTORIES != null) && (PublicData.DIRECTORIES.brand != null) && (PublicData.DIRECTORIES.brand.size() > 0)) {
                                                myTitle = "";
                                                for (ObjBrand objBrand : PublicData.DIRECTORIES.brand) {
                                                    if (objProducts.filter.brand_id.contains(objBrand.id)) {
                                                        if (myTitle.equals("")) {
                                                            myTitle = myTitle.concat(objBrand.name);
                                                        } else {
                                                            myTitle = myTitle.concat(",".concat(objBrand.name));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        toolbar.setTitle(myTitle);

                        if (!objProducts.options.filter_edit) {
                            btn_filter.setVisibility(View.GONE);
                        }
                    }
                }
            }

            if (objProducts.items != null) {

                if (objProducts.items.size() > 0) {
                    if (mAdapterSearch != null) {
                        mAdapterSearch.notifyDataSetChanged();
                    } else {
                        mAdapterSearch = new AdapterProductList(getActivity(), objProducts.items, false);
                        mRecyclerView.setAdapter(mAdapterSearch);
                        mRecyclerView.addOnScrollListener(scroll);
                        mAdapterSearch.notifyDataSetChanged();
                    }
                } else {
                    AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_product), R.drawable.ic_icon_search_empty);

                    LinearLayoutManager managerLinear = new LinearLayoutManager(getContext());
                    mRecyclerView.setLayoutManager(managerLinear);
                    mRecyclerView.setAdapter(mEmptyAdapter);

                }

                /*
                * БЫСТРЫЕ ФИЛЬТРЫ
                * */
                horizontalScrollFastFilter.setVisibility(View.GONE);
                if (objProducts.items.size() > 1) {
                    if (objProducts.filter != null) {
                        setFastFilter(objProducts.filter);
                    }
                }

                    /*
                    * СОРТИРОВКА
                    * */
                if (objProducts.filter != null) {
                    if (objProducts.filter.sort_id != 0) {
                        if (PublicData.DIRECTORIES != null) {
                            if (PublicData.DIRECTORIES.sort != null) {
                                for (ObjSort objSort : PublicData.DIRECTORIES.sort) {
                                    if (objSort.id == objProducts.filter.sort_id) {
                                        txtSortCaption.setText(objSort.name);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                /*
                   * КЛИК НА КАРТОЧКУ
                * **/
                if (mAdapterSearch != null) {
                    mAdapterSearch.setOnClickListenerProduct(new ClickListenerProductMin() {
                        @Override
                        public void onClick(View view, ObjProductMin objProductMin) {
                            if (objProductMin != null) {
                                if (getActivity() != null) {
                                    Intent intent = new Intent(getActivity(), product_item.class);
                                    intent.putExtra("id", objProductMin.id);
                                    startActivity(intent);
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView   = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // В виде карточек
        if (getActivity() == null) return;
        if (getResources().getConfiguration().orientation == 1) {
            if (Helps.isTablet(getActivity())) {
                manager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            } else {
                manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            }
        } else {
            if (Helps.isTablet(getActivity())) {
                manager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            } else {
                manager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            }
        }

        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    // Инициализация контейнера.
    void intiRecyclerViewSort() {
        mRecyclerViewSort   = (RecyclerView) rootView.findViewById(R.id.recycler_sort);
        mRecyclerViewSort.setHasFixedSize(true);
        managerSort = new LinearLayoutManager(getActivity());
        mRecyclerViewSort.setLayoutManager(managerSort);
        mRecyclerViewSort.setItemAnimator(new DefaultItemAnimator());

        if (PublicData.DIRECTORIES != null) {
            if (PublicData.DIRECTORIES.sort != null) {
                mAdapterSort = new AdapterSort(getActivity(), PublicData.DIRECTORIES.sort);
                mRecyclerViewSort.setAdapter(mAdapterSort);
                mAdapterSort.notifyDataSetChanged();

                mAdapterSort.setOnClickListener(new ClickListenerSort() {
                    @Override
                    public void onClick(View view, ObjSort objSort) {
                        if (sliding_layout.getPanelState() != SlidingUpPanelLayout.PanelState.COLLAPSED) {
                            sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        }
                        if (objProducts != null) {
                            if (objProducts.filter != null) {
                                objProducts.filter.sort_id = objSort.id;
                                txtSortCaption.setText(objSort.name);
                                moduleObjects.getProducts(false, objProducts.filter, User.getCurrentSession());
                            }
                        }
                    }
                });

            }
        }
    }


    RecyclerView.OnScrollListener scroll = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            visibleItemCount = mRecyclerView.getChildCount();
            totalItemCount = manager.getItemCount();
            int[] firstVisibleItems = null;
            pastVisiblesItems = manager.findFirstVisibleItemPositions(firstVisibleItems)[0];

            if (!loading) {
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    loading = true;

                    if (objProducts.filter != null) {
                        if (objProducts.filter.existNextPage()) {
                            objProducts.filter.getNextPage();
                            moduleObjects.getProducts(false, objProducts.filter, User.getCurrentSession());
                        }
                    } else {
                        //Toast.makeText(products.this, "Последняя страница", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    };

    void setFastFilter(ObjFilter objFilter) {
        layout_fast_filter.removeAllViews();
        if ((objFilter.catalog_list == null) && (objFilter.brand_list == null)) {
            horizontalScrollFastFilter.setVisibility(View.GONE);
            return;
        }
        if (objFilter.catalog_list != null) {
            if (objFilter.catalog_list.size() <= 0) {
                if (objFilter.brand_list != null) {
                    if (objFilter.brand_list.size() <= 0) {
                        horizontalScrollFastFilter.setVisibility(View.GONE);
                        return;
                    } else {
                        // СПИСОК ФИЛЬТРОВ ПО БРЕНДУ
                        if (PublicData.SETTINGS != null) {
                            if (PublicData.SETTINGS.filters != null) {
                                if (PublicData.SETTINGS.filters.brand) {
                                    createButtonBrand(objFilter.brand_list);
                                    horizontalScrollFastFilter.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                }
            } else {
                // СПИСОК ФИЛЬТРОВ ПО КАТАЛОГУ
                createButtonCatalog(objFilter.catalog_list);
                horizontalScrollFastFilter.setVisibility(View.VISIBLE);
            }
        }
    }


    void createButtonCatalog(ArrayList<ObjCatalogFastFilter> filter) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        param.setMargins(Helps.dpToPx(10), Helps.dpToPx(0), Helps.dpToPx(0), Helps.dpToPx(0));

        for (ObjCatalogFastFilter objMenu : filter) {
            Button newMenuButton = new Button(getActivity(), null, R.style.ButtonFasFilterPrimary);

            newMenuButton.setId(objMenu.id);
            newMenuButton.setText(objMenu.name);

            newMenuButton.setTextSize(12);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                newMenuButton.setBackground(getResources().getDrawable(R.drawable.button_fast_filter));
                newMenuButton.setTextColor(getResources().getColorStateList(R.color.colorButtonTextDefault));
            } else {
                newMenuButton.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_fast_filter, null));
                newMenuButton.setTextColor(ResourcesCompat.getColorStateList(getResources(), R.color.colorButtonTextDefault, null));
            }
            newMenuButton.setPadding(Helps.dpToPx(10),Helps.dpToPx(5),Helps.dpToPx(10),Helps.dpToPx(5));
            newMenuButton.setOnClickListener(clickListenerFastCatalog);
            layout_fast_filter.addView(newMenuButton, param);
        }
    }

    void createButtonBrand(ArrayList<ObjBrand> brand) {

        if (brand.size() < 2) return;

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        param.setMargins(Helps.dpToPx(10), Helps.dpToPx(0), Helps.dpToPx(0), Helps.dpToPx(0));

        for (ObjBrand objMenu : brand) {
            Button newMenuButton = new Button(getActivity(), null, R.style.ButtonFasFilterPrimary);

            newMenuButton.setId(objMenu.id);
            newMenuButton.setText(objMenu.name);
            //newMenuButton.setTypeface(PublicData.custom_font_2);
            newMenuButton.setTextSize(12);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                newMenuButton.setBackground(getResources().getDrawable(R.drawable.button_fast_filter));
                newMenuButton.setTextColor(getResources().getColorStateList(R.color.colorButtonTextDefault));
            } else {
                newMenuButton.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_fast_filter, null));
                newMenuButton.setTextColor(ResourcesCompat.getColorStateList(getResources(), R.color.colorButtonTextDefault, null));
            }
            newMenuButton.setPadding(Helps.dpToPx(10),Helps.dpToPx(5),Helps.dpToPx(10),Helps.dpToPx(5));
            newMenuButton.setOnClickListener(clickListenerFastBrand);
            layout_fast_filter.addView(newMenuButton, param);
        }
    }

    View.OnClickListener clickListenerFastCatalog = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (objProducts != null) {
                if (objProducts.filter != null) {
                    objProducts.filter.catalog_id = v.getId();
                    //openNewFilter(objProducts.filter);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("filter", objProducts.filter);
                    callbackActionSuper.onAction(TAG, PublicData.SELECT_FILTER, bundle);
                }
            }
        }
    };

    View.OnClickListener clickListenerFastBrand = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (objProducts != null) {
                if (objProducts.filter != null) {
                    if (objProducts.filter.brand_id != null) {
                        objProducts.filter.brand_id.clear();
                    } else {
                        objProducts.filter.brand_id = new ArrayList<>();
                    }
                    objProducts.filter.brand_id.add(v.getId());
                    //openNewFilter(objProducts.filter);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("filter", objProducts.filter);
                    callbackActionSuper.onAction(TAG, PublicData.SELECT_FILTER, bundle);
                }
            }
        }
    };

    @Override
    public void onRefresh() {
        if (objProducts != null) {
            if (objProducts.filter != null) {
                objProducts.filter.page = 1;
                moduleObjects.getProducts(false, objProducts.filter, User.getCurrentSession());
            }
        }

    }
}
