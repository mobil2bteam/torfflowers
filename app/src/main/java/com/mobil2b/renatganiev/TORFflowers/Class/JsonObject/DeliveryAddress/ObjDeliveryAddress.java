package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.DeliveryAddress;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjDeliveryAddress {

    public int id;
    public int city_id;
    public String city;
    public String zip_code;
    public String street;
    public String home;
    public String office;
    public String comment;

    public ObjDeliveryAddress() {
    }

    public String getAddress() {
        String result = "";
        if (Helps.isNotEmpty(zip_code)) {
            result = result.concat(zip_code).concat(", ");
        }
        if (Helps.isNotEmpty(city)) {
            result = result.concat(city).concat(" ");
        }
        if (Helps.isNotEmpty(street)) {
            result = result.concat(street).concat(" ");
        }
        if (Helps.isNotEmpty(home)) {
            result = result.concat(home).concat(" ");
        }
        if (Helps.isNotEmpty(office)) {
            result = result.concat(", оф./кв. ").concat(office);
        }

        return result;
    }

    public String getAddressNoZipCode() {
        String result = "";
        if (Helps.isNotEmpty(city)) {
            result = result.concat(city).concat(" ");
        }
        if (Helps.isNotEmpty(street)) {
            result = result.concat(street).concat(" ");
        }
        if (Helps.isNotEmpty(home)) {
            result = result.concat(home).concat(" ");
        }
        if (Helps.isNotEmpty(office)) {
            result = result.concat(", оф./кв. ").concat(office);
        }

        return result;
    }
}
