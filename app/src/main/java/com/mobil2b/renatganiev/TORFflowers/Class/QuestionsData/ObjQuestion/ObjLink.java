package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjLink  implements Serializable {

    public String name;
    public ObjURL url;
    public String url_link;
    public ArrayList<ObjParam> param;

    public ObjLink() {
        init();
    }
    public ObjLink(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjLink(String txt) {
        init();
        try {
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    public void addParam(ArrayList<ObjParam> list) {
        for (ObjParam p : list) {
            param.add(p);
        }
    }

    public void setParam(ArrayList<ObjParam> list) {
        param.clear();
        for (ObjParam p : list) {
            param.add(p);
        }
    }

    private void init() {
        param = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("name")) {
                String url_name = json.getString("name");
                this.name = url_name;
                this.url = PublicData.getURL(url_name);
            } else {
                if (json.has("url")) {
                    url_link = json.getString("url");
                    /*if (!url.equals("")) {
                        ObjURL my_url = new ObjURL("my_url", url);
                        this.url = my_url;
                    }*/
                }
            }
            if (json.has("params")) {
                JSONArray param = json.getJSONArray("params");
                loadParam(param);
            }
        } catch (JSONException e) {
        }
    }

    private void loadParam(JSONArray array) {
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject JSONItem = array.getJSONObject(i);
                ObjParam objParam = new ObjParam(JSONItem);
                param.add(objParam);
            }
        } catch (JSONException e) {
        }
    }


}
