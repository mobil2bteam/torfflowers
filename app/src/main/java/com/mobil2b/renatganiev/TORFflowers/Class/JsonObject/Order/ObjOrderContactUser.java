package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjOrderContactUser {

    public String   name;
    public String   phone;
    public String   cityId;
    public String   cityText;
    public String   address;
    public String   zipCode;
    public String   comment;

    public ObjOrderContactUser() {
        init();
    }
    public ObjOrderContactUser(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjOrderContactUser(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("payment")) {
                JSONObject jsonMain = json.getJSONObject("payment");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("phone")) {
                phone = json.getString("phone");
            }
            if (json.has("cityId")) {
                cityId = json.getString("cityId");
            }
            if (json.has("cityText")) {
                cityText = json.getString("cityText");
            }
            if (json.has("address")) {
                address = json.getString("address");
            }
            if (json.has("zipCode")) {
                zipCode = json.getString("zipCode");
            }
            if (json.has("comment")) {
                comment = json.getString("comment");
            }
            
        } catch (JSONException e) {
        }
    }

}
