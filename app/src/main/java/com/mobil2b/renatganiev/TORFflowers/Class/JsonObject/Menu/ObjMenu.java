package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Menu;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ObjMenu {
    public String view;
    public List<ObjMenuItem> items = new ArrayList<>();

    public ObjMenu() {

    }
    public ObjMenu(JSONObject json) {
        loadJsonObject(json);
    }
    public ObjMenu(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            loadJsonObject(json);
        } catch (JSONException e) {
        }
    }

    public void loadJsonObject(JSONObject json){
        try {
            if(json.has("view")){
                this.view = json.getString("view");
            }
            if(json.has("items")){
                for (int i =0;i<json.getJSONArray("items").length();i++){
                    ObjMenuItem menuItem = new ObjMenuItem();
                    menuItem.loadJsonObject(json.getJSONArray("items").getJSONObject(i));
                    items.add(menuItem);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
