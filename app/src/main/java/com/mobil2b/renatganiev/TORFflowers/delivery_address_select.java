package com.mobil2b.renatganiev.torfflowers;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerDeliveryAddress;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterAddressDeliveryList;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.DeliveryAddress.ObjDeliveryAddress;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBAddressHistory;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;

import java.util.ArrayList;

public class delivery_address_select extends AppCompatActivity {

    AppBarLayout mainAppbar;

    boolean open = true;

    TextInputEditText txtZipCode, txtStreet, txtHome, txtOffice, txtComment;
    RecyclerView mRecyclerView;
    LinearLayoutManager manager;
    AdapterAddressDeliveryList mAdapterSearch;

    DeliveryCityView deliveryCityView;
    ObjCity objCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_address_select);

        txtZipCode  = (TextInputEditText) findViewById(R.id.txtZipCode);
        txtStreet   = (TextInputEditText) findViewById(R.id.txtStreet);
        txtHome     = (TextInputEditText) findViewById(R.id.txtHome);
        txtOffice   = (TextInputEditText) findViewById(R.id.txtOffice);
        txtComment  = (TextInputEditText) findViewById(R.id.txtComment);

        mainAppbar  = (AppBarLayout)      findViewById(R.id.app_bar);
        mainAppbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    // Fully expanded
                    open = true;
                } else {
                    // Not fully expanded or collapsed
                    open = false;
                }
            }
        });
        if (PublicData.SETTINGS != null) {
            deliveryCityView = new DeliveryCityView(delivery_address_select.this, PublicData.SETTINGS);
            objCity = deliveryCityView.getCity();
        }

        initToolbar();
        intiRecyclerView();
        refrashAllDate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_add) {
            //updateUserData();
            if (!open) {
                mainAppbar.setExpanded(true, true);
            } else {
                addNewAddress();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        AppBarLayout app_bar = (AppBarLayout) findViewById(R.id.app_bar);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(0));

            app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    //some other code here
                    ViewCompat.setElevation(appBarLayout, Helps.dpToPx(2));
                }
            });

        }

        toolbar.setTitle("Адрес доставки");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(delivery_address_select.this, R.drawable.ic_toolbar_icon_close_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_close_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(true);
        manager = new LinearLayoutManager(delivery_address_select.this);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    private void refrashAllDate() {

        if (objCity != null) {

            DBAddressHistory addressHistory = new DBAddressHistory(delivery_address_select.this);
            ArrayList<ObjDeliveryAddress> list = addressHistory.getList(objCity.id);

            if (list != null) {
                mAdapterSearch = new AdapterAddressDeliveryList(delivery_address_select.this, list, objCity.name);
                mRecyclerView.setAdapter(mAdapterSearch);
                mAdapterSearch.notifyDataSetChanged();

                mAdapterSearch.setOnClickListenerDelivery(new ClickListenerDeliveryAddress() {
                    @Override
                    public void onClick(View view, ObjDeliveryAddress objDelivery) {
                        //Toast.makeText(delivery_address_select.this, objDelivery.getAddress(), Toast.LENGTH_SHORT).show();
                        selectAddress(objDelivery);
                    }

                    @Override
                    public void onRemove(View view, final ObjDeliveryAddress objDelivery) {
                        
                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case DialogInterface.BUTTON_POSITIVE:
                                        //Yes button clicked
                                        DBAddressHistory addressHistory = new DBAddressHistory(delivery_address_select.this);
                                        addressHistory.delete(objDelivery);
                                        addressHistory.close();
                                        refrashAllDate();
                                        break;

                                    case DialogInterface.BUTTON_NEGATIVE:
                                        //No button clicked
                                        break;
                                }
                            }
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(delivery_address_select.this);
                        builder.setMessage("Удалить адрес?").setPositiveButton("Да", dialogClickListener)
                                .setNegativeButton("Нет", dialogClickListener).show();

                    }
                });

                mRecyclerView.setVisibility(View.VISIBLE);
            }
        }
    }


    void addNewAddress() {
        if (txtStreet.getText().toString().trim().length() == 0) {
            Helps.errorTextEdit(txtStreet, delivery_address_select.this);
            return;
        }
        if (txtHome.getText().toString().trim().length() == 0) {
            Helps.errorTextEdit(txtHome, delivery_address_select.this);
            return;
        }
        if (txtOffice.getText().toString().trim().length() == 0) {
            Helps.errorTextEdit(txtOffice, delivery_address_select.this);
            return;
        }
        if (objCity != null) {
            ObjDeliveryAddress objDeliveryAddress = new ObjDeliveryAddress();
            objDeliveryAddress.city_id  = objCity.id;
            objDeliveryAddress.zip_code = txtZipCode.getText().toString().trim();
            objDeliveryAddress.street   = txtStreet.getText().toString().trim();
            objDeliveryAddress.home     = txtHome.getText().toString().trim();
            objDeliveryAddress.office   = txtOffice.getText().toString().trim();
            objDeliveryAddress.comment  = txtComment.getText().toString().trim();

            DBAddressHistory addressHistory = new DBAddressHistory(delivery_address_select.this);
            long id = 0;
            if (addressHistory.exist(objDeliveryAddress)) {
                objDeliveryAddress = addressHistory.getAddressToObj(objDeliveryAddress);
                id = objDeliveryAddress.id;
            } else {
                id = addressHistory.add(objDeliveryAddress);
                objDeliveryAddress.id = (int)id;
            }
            addressHistory.close();

            if (id != 0) {
                refrashAllDate();
                selectAddress(objDeliveryAddress);
            }
        }
    }

    void selectAddress(ObjDeliveryAddress objDeliveryAddress) {
        Intent intent = new Intent();
        intent.putExtra("address_id", objDeliveryAddress.id);
        setResult(RESULT_OK, intent);
        finish();
    }

}
