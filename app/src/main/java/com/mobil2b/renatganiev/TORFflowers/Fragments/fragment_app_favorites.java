package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterEmptyRecyclerView;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBFavorites;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterProductList;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_favorites extends Fragment  implements SwipeRefreshLayout.OnRefreshListener  {

    public static final String TAG = "fragment_app_favorites";
    public static final int LAYOUT = R.layout.fragment_app_favorites;
    View rootView;
    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;


    private SwipeRefreshLayout mSwipeRefreshLayout;

    RecyclerView mRecyclerView;
    StaggeredGridLayoutManager manager;
    AdapterProductList mAdapterSearch;

    ModuleObjects moduleObjects;
    Toolbar toolbar;

    ArrayList<ObjProductMin> products = new ArrayList<>();

    public static fragment_app_favorites getInstance(Bundle args) {
        fragment_app_favorites fragment = new fragment_app_favorites();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        if (arg != null) {}
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        mSwipeRefreshLayout     = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        moduleObjects = new ModuleObjects(getActivity(), callBackSendDate);

        initToolbar();
        intiRecyclerView();
        loadData();

        return rootView;
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        toolbar.setTitle("Избранное");

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_menu));
            } else {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_menu));
            }
        }
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView   = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // В виде карточек
        if (getResources().getConfiguration().orientation == 1) {
            if (Helps.isTablet(getActivity())) {
                manager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            } else {
                manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
            }
        } else {
            if (Helps.isTablet(getActivity())) {
                manager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            } else {
                manager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
            }
        }

        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    void loadData() {
        DBFavorites dbFavorites = new DBFavorites(getActivity());
        String list_id = dbFavorites.getString();
        if (!list_id.equals("")) {
            moduleObjects.getUpdateListProduct(false, list_id);
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
            AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_favorite), R.drawable.ic_icon_favorit_empty);

            LinearLayoutManager managerLinear = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(managerLinear);
            mRecyclerView.setAdapter(mEmptyAdapter);
        }
        dbFavorites.close();
    }

    void refreshAllData() {
        mAdapterSearch = new AdapterProductList(getActivity(), products, false);
        mRecyclerView.setAdapter(mAdapterSearch);
        mAdapterSearch.notifyDataSetChanged();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public void onReturnAnswer(final String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    String rezData = OpenBlocks.getOpenData(result);
                    if (rezData.equals(PublicData.QUESTION_TYPE_PRODUCTS)) {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.has("products")) {
                                products.clear();
                                for (int i = 0; i < json.getJSONArray("products").length(); i++) {
                                    ObjProductMin it = new ObjProductMin(json.getJSONArray("products").getJSONObject(i));
                                    products.add(it);
                                }
                                if (products.size() > 0) {
                                    if (mAdapterSearch != null) {
                                        mAdapterSearch.notifyDataSetChanged();
                                    } else {
                                        refreshAllData();
                                    }
                                } else {
                                    AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_favorite), R.drawable.ic_icon_favorit_empty);

                                    LinearLayoutManager managerLinear = new LinearLayoutManager(getContext());
                                    mRecyclerView.setLayoutManager(managerLinear);
                                    mRecyclerView.setAdapter(mEmptyAdapter);
                                }
                            }
                        } catch (JSONException e) {
                        }
                    }


                }
            });
        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    @Override
    public void onRefresh() {
        loadData();
    }
}
