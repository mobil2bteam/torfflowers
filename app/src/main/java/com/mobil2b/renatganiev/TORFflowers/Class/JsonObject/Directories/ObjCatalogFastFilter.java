package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCatalogFastFilter implements Serializable {

    public int      id;
    public String   name = "";

    public ObjCatalogFastFilter() {
        init();
    }
    public ObjCatalogFastFilter(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjCatalogFastFilter(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("catalog_list")) {
                JSONObject jsonMain = json.getJSONObject("catalog_list");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
        } catch (JSONException e) {
        }
    }

}
