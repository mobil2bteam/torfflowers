package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Article.ObjArticleItems;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerArticle {

    void onClick(View view, ObjArticleItems objArticleItems);

}
