package com.mobil2b.renatganiev.torfflowers;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackAuthorization;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentAnimation;

public class order_new extends AppCompatActivity {

    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;
    private FragmentAnimation fragmentAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_new);

        fragmentmanager = getSupportFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction = callbackAction;

        fragmentAnimation = new FragmentAnimation();
        fragmentAnimation.enter     = R.anim.slide_left_enter;
        fragmentAnimation.exit      = R.anim.slide_left_end;
        fragmentAnimation.popEnter  = R.anim.slide_right_enter;
        fragmentAnimation.popExit   = R.anim.slide_right_end;

        initToolbar();
        fragmentControl.showFragmentDelivery(true, null, R.id.layout_container, null);
        //fragmentControl.showFragmentInputDate(true, null, R.id.layout_container, null);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_close, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.menu_close) {

        }*/

        return super.onOptionsItemSelected(item);
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle("Новый заказ");
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(order_new.this, R.drawable.ic_toolbar_icon_back_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                finish();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(order_new.this);
                builder.setMessage("Прервать оформление заказа?").setPositiveButton("Да", dialogClickListener)
                        .setNegativeButton("Нет", dialogClickListener).show();
            }
        });
    }


    CallbackAction callbackAction = new CallbackAction() {
        @Override
        public void onAction(String tag, int type, ObjLink data) {
        }

        @Override
        public void onAction(String tag, int type, Bundle data) {
            if (type == PublicData.SELECT_DELIVERY) {
                if (PublicData.objDelivery != null) {
                    if (!PublicData.objDelivery.type.equals(PublicData.DELIVERY_TYPE_PICKUP)) {
                        fragmentControl.showFragmentInputDate(false, fragmentAnimation, R.id.layout_container, null);
                    } else {

                        if (PublicData.SETTINGS != null) {
                            DeliveryCityView deliveryCityView = new DeliveryCityView(order_new.this, PublicData.SETTINGS);
                            ObjCity objCity = deliveryCityView.getCity();
                            Bundle bundle = new Bundle();
                            bundle.putInt("city_id", objCity.id);
                            bundle.putBoolean("flag_delivery_point", true);
                            bundle.putBoolean("select_type", true);
                            fragmentControl.showFragmentMapList(false, null, R.id.layout_container, bundle);
                        }
                    }
                }
            }
            if (type == PublicData.SELECT_INPUT_DATE) {
                if (User.isLoggedIn()) {
                    onRefresh();
                } else {
                    Intent i = new Intent(order_new.this, user_registration.class);
                    startActivityForResult(i, PublicData.REZULT_REQUEST_USER_REGISTRATION);
                }
            }
            if (type == PublicData.SELECT_DELIVERY_POINT) {
                if (User.isLoggedIn()) {
                    onRefresh();
                } else {
                    Intent i = new Intent(order_new.this, user_registration.class);
                    startActivityForResult(i, PublicData.REZULT_REQUEST_USER_REGISTRATION);
                }
            }

        }
    };

    CallbackAuthorization callbackAuthorization = new CallbackAuthorization() {
        @Override
        public void onStart(String text) {
            PublicData.showDialog(order_new.this);
        }

        @Override
        public void onReturnSession(DataSession result) {

        }

        @Override
        public void onDontSession(final String text, int error_code) {
            order_new.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    Toast.makeText(order_new.this, text, Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(order_new.this, user_registration.class);
                    startActivityForResult(i, PublicData.REZULT_REQUEST_USER_REGISTRATION);
                }
            });
        }

        @Override
        public void onSuccess(DataSession result, int type) {
            PublicData.closeDialog();
            fragmentControl.showFragmentPayment(false, fragmentAnimation, R.id.layout_container, null);
        }

        @Override
        public void onError(DataSession result, final String message, int error_code, int type) {
            order_new.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    Toast.makeText(order_new.this, message, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onExit() {
            PublicData.closeDialog();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PublicData.REZULT_REQUEST_USER_REGISTRATION) {
            if (resultCode == RESULT_OK) {
                onRefresh();
            }
        }
        if (requestCode == PublicData.REZULT_REQUEST_SELECT_POINT) {
            if (resultCode == RESULT_OK) {
                //onRefresh();
            }
        }
    }


    public void onRefresh() {
        User.init(order_new.this);
        User.callbackAuthorization = callbackAuthorization;
        User.onLoginSession(order_new.this, callbackAuthorization);
    }

}
