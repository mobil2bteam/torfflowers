package com.mobil2b.renatganiev.torfflowers.ControlView.RecipientView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjRecipient.ObjRecipient;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBRecipientHistory;
import com.mobil2b.renatganiev.torfflowers.R;

import static android.app.Activity.RESULT_OK;


/**
 * Created by renatganiev on 10.10.15.
 */
public class RecipientView extends RelativeLayout implements View.OnClickListener {

    public int REZULT_REQUEST_SELECT_ADDRESS                        = 770;
    Context             mContext;
    TextView txtName, txtPhone, txtMessage;
    ImageView imgClear;
    LinearLayout layout_change_city;

    OnClickListener _wrappedOnClickListener;

    public RecipientView(Context context) {
        super(context);
        super.setOnClickListener(this);

        mContext    = context;
        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_recipient_view, this);

        txtName                 = (TextView) findViewById(R.id.txtName);
        txtPhone                = (TextView) findViewById(R.id.txtPhone);
        txtMessage              = (TextView) findViewById(R.id.txtMessage);
        layout_change_city      = (LinearLayout) findViewById(R.id.layout_change_city);

        imgClear                = (ImageView) findViewById(R.id.imgClear);
        imgClear.setVisibility(GONE);

        layout_change_city.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_wrappedOnClickListener != null) {
                    _wrappedOnClickListener.onClick(view);
                }
            }
        });

        imgClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                clear();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Очистить?").setPositiveButton("Да", dialogClickListener)
                        .setNegativeButton("Нет", dialogClickListener).show();
            }
        });


        updateRecipientName();
    }

    void updateRecipientName() {
        ObjRecipient address = getRecipient();
        if (address != null) {
            txtName.setText(address.name);
            txtName.setVisibility(VISIBLE);
            imgClear.setVisibility(VISIBLE);
            txtMessage.setVisibility(GONE);
            if (Helps.isNotEmpty(address.phone)) {
                txtPhone.setText(address.phone);
                txtPhone.setVisibility(VISIBLE);
            } else {
                txtPhone.setVisibility(GONE);
            }
        } else {
            txtName.setVisibility(GONE);
            txtPhone.setVisibility(GONE);
            txtMessage.setVisibility(VISIBLE);
            imgClear.setVisibility(GONE);
        }
    }


    public ObjRecipient getRecipient() {
        int address_id = getRecipientSelect();
        DBRecipientHistory addressHistory = new DBRecipientHistory(mContext);
        ObjRecipient objDeliveryAddress = addressHistory.getRecipient(address_id);
        return objDeliveryAddress;
    }

    public void setRecipientSelect(int address_id) {
        SharedPreferences getSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor e = getSharedPreferences.edit();
        e.putInt("recipient_id", address_id);
        e.apply();
        updateRecipientName();
    }

    int getRecipientSelect() {
        SharedPreferences getSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int address_id = getSharedPreferences.getInt("recipient_id", 0);
        if (address_id == 0) {
            return 0;
        }
        return address_id;
    }

    public void clear() {
        SharedPreferences getSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor e = getSharedPreferences.edit();
        e.remove("recipient_id");
        e.apply();
        updateRecipientName();
    }

    @Override
    public void onClick(View view) {
        if (_wrappedOnClickListener != null)
            _wrappedOnClickListener.onClick(view);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        _wrappedOnClickListener = l;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REZULT_REQUEST_SELECT_ADDRESS) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle.containsKey("recipient_id")) {
                        int selectIdCity = bundle.getInt("recipient_id", 0);
                        if (selectIdCity != 0) {
                            setRecipientSelect(selectIdCity);
                        }
                    }
                }
            }
        }
    }

    public void onResume() {
        updateRecipientName();
    }
}