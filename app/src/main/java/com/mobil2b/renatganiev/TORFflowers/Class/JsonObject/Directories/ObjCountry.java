package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCountry {

    public int      id;
    public String   name = "";
    public ObjImage image;

    public ObjCountry() {
        init();
    }
    public ObjCountry(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjCountry(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("country")) {
                JSONObject jsonMain = json.getJSONObject("country");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("image")) {
                image = new ObjImage(json.getJSONObject("image"));
            }
        } catch (JSONException e) {
        }
    }

}
