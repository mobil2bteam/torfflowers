package com.mobil2b.renatganiev.torfflowers.Fragments.Bag;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjPrice;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterCartList;
import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterEmptyRecyclerView;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Cart.ObjCart;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjCards.ObjCards;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBBag;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ModuleObjects;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.OpenBlocks;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.SendDate.CallBackSendDate;
import com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryCityView.DeliveryCityView;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerBagPlusMinusProductMin;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerProductMin;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.order_new;
import com.mobil2b.renatganiev.torfflowers.product_item;

import static android.app.Activity.RESULT_OK;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_bag extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_bag";
    public static final int LAYOUT = R.layout.fragment_app_bag;
    View rootView;
    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;

    RecyclerView mRecyclerView;
    LinearLayoutManager manager;
    AdapterCartList mAdapterSearch;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    LinearLayout layout_fast_filter, layout_promo, layout_total, layout_order_caption, layout_card;
    HorizontalScrollView horizontalScrollFastFilter;
    TextView txtSortCaption, txtTotalNotSale, txtSumSale, txtTotal;
    TextView txtPromoComment, txtBalance;
    Button btnClearSort, btnAppPromo, btnCalculation, btnOk;
    EditText editPromo;
    SeekBar seekBar;

    ModuleObjects moduleObjects;

    DeliveryCityView deliveryCityView;
    ObjCity objCity;
    ObjCards objCard;

    boolean promo = true;
    boolean new_order = false;

    public static fragment_app_bag getInstance(Bundle args) {
        fragment_app_bag fragment = new fragment_app_bag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        mRecyclerView               = rootView.findViewById(R.id.recycler_view);
        layout_fast_filter          = rootView.findViewById(R.id.layout_fast_filter);
        layout_promo                = rootView.findViewById(R.id.layout_promo);
        layout_total                = rootView.findViewById(R.id.layout_total);
        layout_order_caption        = rootView.findViewById(R.id.layout_order_caption);
        layout_card                 = rootView.findViewById(R.id.layout_card);
        horizontalScrollFastFilter  = rootView.findViewById(R.id.horizontalScrollFastFilter);
        txtSortCaption              = rootView.findViewById(R.id.txtSortCaption);
        btnClearSort                = rootView.findViewById(R.id.btnClearSort);
        btnAppPromo                 = rootView.findViewById(R.id.btnAppPromo);
        btnCalculation              = rootView.findViewById(R.id.btnCalculation);
        btnOk                       = rootView.findViewById(R.id.btnOk);
        txtTotalNotSale             = rootView.findViewById(R.id.txtTotalNotSale);
        txtSumSale                  = rootView.findViewById(R.id.txtSumSale);
        txtTotal                    = rootView.findViewById(R.id.txtTotal);
        txtPromoComment             = rootView.findViewById(R.id.txtPromoComment);
        txtBalance                  = rootView.findViewById(R.id.txtBalance);
        editPromo                   = rootView.findViewById(R.id.editPromo);
        seekBar                     = rootView.findViewById(R.id.seekBar);



        mSwipeRefreshLayout     = rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        if (PublicData.SETTINGS != null) {
            deliveryCityView = new DeliveryCityView(getContext(), PublicData.SETTINGS);
            objCity = deliveryCityView.getCity();
        }
        //initToolbar();
        intiRecyclerView();

        moduleObjects = new ModuleObjects(getActivity(), callBackSendDate);
        // here you check the value of getActivity() and break up if needed


        btnAppPromo.setOnClickListener(view -> getBagList(false));


        btnCalculation.setOnClickListener(view -> getBagList(false));

        layout_promo.setVisibility(View.GONE);
        layout_card.setVisibility(View.GONE);
        layout_total.setVisibility(View.GONE);
        layout_order_caption.setVisibility(View.GONE);

        btnOk.setOnClickListener(view -> getBagList(true));


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (objCard != null) {
                    txtBalance.setText(String.valueOf(i).concat(" / ".concat(String.valueOf(objCard.balance).concat(" ball"))));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                getBagList(false);
            }
        });


        return rootView;
    }


    /**
     * TODO: Инициализация toolbar.
     * */
    /*private void initToolbar() {
        toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle("Корзина");
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_back_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_black_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((AppCompatActivity)getActivity()).finish();
                ((AppCompatActivity)getActivity()).onBackPressed();
            }
        });
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();

        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onRefresh();
                }
            });
        }
    }


    CallBackSendDate callBackSendDate = new CallBackSendDate() {
        @Override
        public void onSend(String txt, ObjLink linkURL) {
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!new_order) {
                        mSwipeRefreshLayout.setRefreshing(true);
                    } else {
                        PublicData.showDialog(getActivity());
                    }
                    btnOk.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onReturnAnswer(final String result, boolean cache, long cache_datetime, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    PublicData.closeDialog();
                    String rezData = OpenBlocks.getOpenData(result);
                    if (rezData.equals(PublicData.QUESTION_TYPE_GET_BAG)) {
                        PublicData.objCart = new ObjCart(result);
                        refrashAllDate();
                    }
                }
            });
        }

        @Override
        public void onError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    PublicData.closeDialog();
                    if(getActivity() == null) return;
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onHttpError(final String text, int code_error, ObjLink linkURL) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                    PublicData.closeDialog();
                    if(getActivity() == null) return;
                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };


    private void refrashAllDate() {
        layout_total.setVisibility(View.GONE);
        layout_card.setVisibility(View.GONE);

        if (PublicData.objCart != null) {
            if (PublicData.objCart.products != null) {
                if (PublicData.objCart.products.size() > 0) {
                    layout_order_caption.setVisibility(View.VISIBLE);
                    mAdapterSearch = new AdapterCartList(getActivity(), PublicData.objCart.products, false);
                    mRecyclerView.setAdapter(mAdapterSearch);
                    mAdapterSearch.notifyDataSetChanged();

                    /*
                    * КЛИК НА КАРТОЧКУ
                    * **/
                    mAdapterSearch.setOnClickListenerProduct(new ClickListenerProductMin() {
                        @Override
                        public void onClick(View view, ObjProductMin objProductMin) {
                            if (objProductMin != null) {
                                if (getActivity() != null) {
                                    Intent intent = new Intent(getActivity(), product_item.class);
                                    intent.putExtra("id", objProductMin.id);
                                    startActivity(intent);
                                }
                            }
                        }
                    });

                    mAdapterSearch.setOnClickListenerBagPlusMinusProductMin(new ClickListenerBagPlusMinusProductMin() {
                        @Override
                        public void onClickPlus(View view, ObjProductMin objProductMin, ObjPrice objPrice) {
                            //Toast.makeText(getContext(), "Plus", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onClickMinus(View view, ObjProductMin objProductMin, ObjPrice objPrice) {
                            //Toast.makeText(getContext(), "Minus", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onRemovePrice(View view, ObjProductMin objProductMin, ObjPrice objPrice) {
                            //Toast.makeText(getContext(), "Minus", Toast.LENGTH_SHORT).show();
                            if (objPrice != null) {
                                if (objPrice.count <= 0) {
                                    getBagList(false);
                                }
                            }
                        }
                    });


                    txtTotalNotSale.setText(String.valueOf(PublicData.objCart.totalNotSale).concat(" р."));
                    txtSumSale.setText(String.valueOf(PublicData.objCart.totalSumSale).concat(" р."));
                    txtTotal.setText(String.valueOf(PublicData.objCart.total).concat(" р."));

                    if (PublicData.objCart.products.size() > 0) {
                        layout_total.setVisibility(View.VISIBLE);
                        btnOk.setVisibility(View.VISIBLE);
                    }

                    if (promo) {
                        if (PublicData.objCart.promoCode != null) {
                            if (PublicData.objCart.promoCode.percent == 0) {
                                txtPromoComment.setText("Промо-код не существует или просрочен");
                            } else {
                                txtPromoComment.setText("Данный промо-код дарит вам скидну на ".concat(String.valueOf(PublicData.objCart.promoCode.percent).concat("%")));
                            }
                            txtPromoComment.setVisibility(View.VISIBLE);
                        } else {
                            txtPromoComment.setVisibility(View.GONE);
                        }
                        layout_promo.setVisibility(View.VISIBLE);
                    }


                    if (User.getCurrentSession() != null) {
                        if (User.getCurrentSession().cards != null) {
                            for (ObjCards obj : User.getCurrentSession().cards) {
                                if (obj.type.equals(ObjCards.TYPE_CARD_BALL)) {
                                    objCard = obj;
                                    break;
                                }
                            }
                            if (objCard != null) {
                                if (objCard.balance > 0) {
                                    seekBar.setMax(objCard.balance);
                                    layout_card.setVisibility(View.VISIBLE);

                                    int ball = 0;
                                    if (PublicData.objCart.card != null) {
                                        ball = PublicData.objCart.card.taken_balls;
                                    }
                                    seekBar.setProgress(ball);
                                    txtBalance.setText(String.valueOf(ball).concat(" / ".concat(String.valueOf(objCard.balance).concat(" ball"))));
                                }
                            }
                        }
                    }

                    DBBag bag = new DBBag(getActivity());
                    bag.clear();
                    for (ObjProductMin objProductMin : PublicData.objCart.products) {
                        if (objProductMin.prices != null) {
                            if (objProductMin.prices.size() > 0) {
                                ObjPrice objPrice = objProductMin.prices.get(0);
                                bag.add(objProductMin.id, objPrice.id, objPrice.count);
                            }
                        }
                    }
                    bag.close();

                    if (new_order) {
                        new_order = false;
                        PublicData.closeDialog();
                        Intent intent = new Intent(getActivity(), order_new.class);
                        startActivity(intent);
                    }

                } else {
                    emptyBag();
                }
            } else {
                emptyBag();
            }
        } else {
            emptyBag();
        }
    }

    // Пусая корзина
    void emptyBag() {
        if (getActivity() != null) {
            DBBag bag = new DBBag(getActivity());
            bag.clear();
            bag.close();
        }
        layout_total.setVisibility(View.GONE);
        layout_promo.setVisibility(View.GONE);
        layout_order_caption.setVisibility(View.GONE);
        layout_card.setVisibility(View.GONE);
        if (mAdapterSearch != null) {
            mAdapterSearch.notifyDataSetChanged();
        }
        AdapterEmptyRecyclerView mEmptyAdapter = new AdapterEmptyRecyclerView(getString(R.string.str_empty_bag), R.drawable.ic_icon_bag_empty);
        mRecyclerView.setAdapter(mEmptyAdapter);

        btnOk.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
        PublicData.closeDialog();

    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(true);
        manager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    /**
     * Пересчет корзины
     * */
    void getBagList(boolean order) {
        DBBag bag = new DBBag(getContext());
        String cart = bag.getOfferStringCount();
        bag.close();
        if (!cart.equals("")) {
            String promo = editPromo.getText().toString();
            if (objCity == null) {
                if (deliveryCityView != null) {
                    objCity = deliveryCityView.getCity();
                }
            }
            if (objCity != null) {
                new_order = order;
                moduleObjects.getBagList(true, cart, promo, objCard, seekBar.getProgress(), objCity.id, User.getCurrentSession());
            }
        } else {
            emptyBag();
        }
    }


    @Override
    public void onRefresh() {
        getBagList(false);
    }


    @Override
    public void onPause() {
        super.onPause();
        moduleObjects.cancelAllRequest();
    }
}
