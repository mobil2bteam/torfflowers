package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterBaseDirectories;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBaseDirectories;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerBaseDirectories;

import java.util.ArrayList;

/**
 * TODO: Активити с отдельными значениями для конкретного фильтра
 * */


public class filter_list extends AppCompatActivity {

    ArrayList<? extends ObjBaseDirectories> objBaseDirectories;
    String name;

    Toolbar toolbar;
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    AdapterBaseDirectories mAdapterSearch;

    Button btnFilterOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_list);

        btnFilterOk = (Button) findViewById(R.id.btnFilterOk);

        Intent data = getIntent();
        if (data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                if (bundle.containsKey("filterList")) {
                    objBaseDirectories = (ArrayList<? extends ObjBaseDirectories>)bundle.getSerializable("filterList");
                }
                if (bundle.containsKey("name")) {
                    name = bundle.getString("name");
                }
            }
        }
        initToolbar();
        if (objBaseDirectories != null) {
            intiRecyclerView();
            refrashAllDate();

            btnFilterOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (objBaseDirectories != null) {

                        ArrayList<Integer> listSelectId = new ArrayList<>();

                        for (ObjBaseDirectories obj : objBaseDirectories) {
                            if (obj.checkbox) {
                                listSelectId.add(obj.id);
                            }
                        }

                        Intent intentResult = new Intent();
                        intentResult.putIntegerArrayListExtra("listId", listSelectId);
                        setResult(RESULT_OK, intentResult);
                        finish();
                        //mAdapterSearch.notifyDataSetChanged();
                    }
                }
            });
        } else {
            btnFilterOk.setVisibility(View.GONE);
        }
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle(name);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_toolbar_icon_back_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    // Инициализация контейнера.
    void intiRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        // В виде списка
        mLayoutManager = new LinearLayoutManager(filter_list.this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void refrashAllDate() {
        mAdapterSearch = new AdapterBaseDirectories(filter_list.this, objBaseDirectories);
        mRecyclerView.setAdapter(mAdapterSearch);
        mAdapterSearch.notifyDataSetChanged();

        mAdapterSearch.setOnClickListener(new ClickListenerBaseDirectories() {
            @Override
            public void onClick(View view, ObjBaseDirectories objBaseDirectories) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.filter_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_clear) {

            //updateUserData();

            if (objBaseDirectories != null) {
                for (ObjBaseDirectories obj : objBaseDirectories) {
                    obj.checkbox = false;
                }
                mAdapterSearch.notifyDataSetChanged();
            }

        }

        return super.onOptionsItemSelected(item);
    }

}
