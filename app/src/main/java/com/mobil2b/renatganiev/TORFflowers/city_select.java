package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Fragments.fragment_app_city_list;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;


/**
 * TODO: Активити для выбора города. Подгружает для этого отдельный фрагмент с полным функционалом
 *          поиска города.
 * */

public class city_select extends AppCompatActivity {


    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_select);

        Bundle bundle = null;
        Intent intent = getIntent();
        if (intent != null) {
            bundle = intent.getExtras();
        }

        fragmentmanager = getSupportFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction          = new CallbackAction() {
            @Override
            public void onAction(String tag, int type, ObjLink data) {
            }
            @Override
            public void onAction(String tag, int type, Bundle data) {
                if (tag.equals(fragment_app_city_list.TAG)) {
                    if (type == PublicData.SELECT_CITY) {
                        if (data.containsKey("city_id")) {
                            int city_id = data.getInt("city_id");
                            Intent intent = new Intent();
                            intent.putExtra("city_id", city_id);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                }
            }
        };

        fragmentControl.showFragmentCityList(true, null, R.id.layout_container, bundle);
    }
}
