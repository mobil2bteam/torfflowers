package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerProductMin {
    void onClick(View view, ObjProductMin objProductMin);
}
