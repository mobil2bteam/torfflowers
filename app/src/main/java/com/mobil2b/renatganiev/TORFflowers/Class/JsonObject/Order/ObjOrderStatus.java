package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjOrderStatus {

    public int      statusId;
    public String   status;
    public String   statusColor;
    public String   statusColorText;

    public ObjOrderStatus() {
        init();
    }
    public ObjOrderStatus(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjOrderStatus(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("payment")) {
                JSONObject jsonMain = json.getJSONObject("payment");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("statusId")) {
                statusId = json.getInt("statusId");
            }
            if (json.has("status")) {
                status = json.getString("status");
            }
            if (json.has("statusColor")) {
                statusColor = json.getString("statusColor");
            }
            if (json.has("statusColorText")) {
                statusColorText = json.getString("statusColorText");
            }
            
        } catch (JSONException e) {
        }
    }

}
