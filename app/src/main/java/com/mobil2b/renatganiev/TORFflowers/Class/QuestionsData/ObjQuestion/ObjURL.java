package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjURL  implements Serializable {

    public String name;
    public String url;
    public String metod;
    public ArrayList<ObjParam> param;


    public ObjURL() {init();}
    public ObjURL(String n, String u) {
        name = n;
        url = u;
        init();
    }
    public ObjURL(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjURL(String txt) {
        init();
        try {
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    public void addParam(ArrayList<ObjParam> list) {
        for (ObjParam p : list) {
            param.add(p);
        }
    }

    public void setParam(ArrayList<ObjParam> list) {
        param.clear();
        for (ObjParam p : list) {
            param.add(p);
        }
    }

    private void init() {
        param = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("name")) {
                this.name = json.getString("name");
            }
            if (json.has("url")) {
                this.url = json.getString("url");
            }
            if (json.has("metod")) {
                this.metod = json.getString("metod");
            }
            if (json.has("param")) {
                JSONArray param = json.getJSONArray("param");
                loadParam(param);
            }
        } catch (JSONException e) {
        }
    }

    private void loadParam(JSONArray array) {
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject JSONItem = array.getJSONObject(i);
                ObjParam objParam = new ObjParam(JSONItem);
                param.add(objParam);
            }
        } catch (JSONException e) {
        }
    }
}
