package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjRecipient.ObjRecipient;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerRecipient {
    void onClick(View view, ObjRecipient objDelivery);
    void onRemove(View view, ObjRecipient objDelivery);
}
