package com.mobil2b.renatganiev.torfflowers;

import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;

public class bag extends AppCompatActivity {

    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bag);

        fragmentmanager = getSupportFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction = callbackAction;

        initToolbar();

        fragmentControl.showFragmentBag(true, null, R.id.layout_container, null);
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle("Корзина");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_toolbar_icon_back_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_back_24dp));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
    }

    CallbackAction callbackAction = new CallbackAction() {
        @Override
        public void onAction(String tag, int type, ObjLink data) {

        }

        @Override
        public void onAction(String tag, int type, Bundle data) {
        }
    };
}
