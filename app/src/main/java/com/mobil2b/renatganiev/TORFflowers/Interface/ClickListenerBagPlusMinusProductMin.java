package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjPrice;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerBagPlusMinusProductMin {
    void onClickPlus(View view, ObjProductMin objProductMin, ObjPrice objPrice);
    void onClickMinus(View view, ObjProductMin objProductMin, ObjPrice objPrice);
    void onRemovePrice(View view, ObjProductMin objProductMin, ObjPrice objPrice);
}
