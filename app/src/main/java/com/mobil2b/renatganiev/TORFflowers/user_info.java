package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackAuthorization;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;

public class user_info extends AppCompatActivity {

    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        fragmentmanager = getSupportFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction = callbackAction;

        //fragmentControl.showFragmentUserWelcome(true, null, R.id.layout_container, null);

        User.init(user_info.this);
        if (User.isLoggedIn()) {
            // Авторизован
            //User.onLogout(null);
            fragmentControl.showFragmentUserInfo(true, null, R.id.layout_container, null);
        } else {
            // Не авторизован
            fragmentControl.showFragmentUserWelcome(true, null, R.id.layout_container, null);
        }

        User.callbackAuthorization = callbackAuthorization;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (requestCode == PublicData.REZULT_REQUEST_SELECT_CITY) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle.containsKey("city_id")) {
                        int selectIdCity = bundle.getInt("city_id", 0);
                        if (selectIdCity != 0) {

                        }
                    }
                }
            }
        }*/

    }


    CallbackAction callbackAction = new CallbackAction() {
        @Override
        public void onAction(String tag, int type, ObjLink data) {

        }

        @Override
        public void onAction(String tag, int type, Bundle data) {
            if (type == PublicData.USER_LOGIN_CLICK) {
                fragmentControl.showFragmentUserLogin(false, null, R.id.layout_container, null);
            }
        }
    };


    CallbackAuthorization callbackAuthorization = new CallbackAuthorization() {
        @Override
        public void onStart(String text) {

        }

        @Override
        public void onReturnSession(DataSession result) {

        }

        @Override
        public void onDontSession(String text, int error_code) {
            fragmentControl.showFragmentUserWelcome(true, null, R.id.layout_container, null);
        }

        @Override
        public void onSuccess(DataSession result, int type) {
            fragmentControl.showFragmentUserInfo(true, null, R.id.layout_container, null);
        }

        @Override
        public void onError(DataSession result, final String message, int error_code, int type) {
            user_info.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(user_info.this, message, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onExit() {
            fragmentControl.showFragmentUserWelcome(true, null, R.id.layout_container, null);
        }
    };

}
