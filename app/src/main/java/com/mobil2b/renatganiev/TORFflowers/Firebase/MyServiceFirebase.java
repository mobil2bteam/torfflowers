package com.mobil2b.renatganiev.torfflowers.Firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mobil2b.renatganiev.torfflowers.loading;
import com.mobil2b.renatganiev.torfflowers.R;


public class MyServiceFirebase extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("Фаербейс", "FromBase: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            //Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
                sendNotification(remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void scheduleJob() {
    }

    private void handleNow() {

    }

    private void sendNotification(String messageBody) {

        Intent notificationIntent = new Intent(this, loading.class).
                setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //notificationIntent.putExtra("NOTIFICATION_MESSAGE", messageBody);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(loading.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // only for gingerbread and newer versions
            MyNotificationHelper notificationHelper = new MyNotificationHelper(this);
            Notification.Builder notificationBuilder = notificationHelper.getNotification1(pendingIntent, "Текст", messageBody);

            if (notificationBuilder != null) {
                notificationHelper.notify(1, notificationBuilder);
            }

        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

            Notification notification = builder.setContentTitle("Текс")
                    .setContentText(messageBody)
                    .setTicker("Привет")
                    .setSmallIcon(R.drawable.logo_loading)
                    .setContentIntent(pendingIntent)
                    .setSound(soundUri)
                    .setAutoCancel(true)
                    .build();

            NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        }

    }

}
