package com.mobil2b.renatganiev.torfflowers.ControlView.SocialLinkView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjSocialLinkItem;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 10.10.15.
 */
public class SocialItemView extends LinearLayout implements View.OnClickListener {

    Context                 mContext;

    TextView                txtName;
    ImageView               imgIcon;
    LinearLayout            layout_back;
    ClickListenerSocialLink clickListenerMenu;
    ObjSocialLinkItem objSocialLinkItem;

    public SocialItemView(Context context, ObjSocialLinkItem obj, ClickListenerSocialLink clickListener) {
        super(context);

        mContext            = context;
        objSocialLinkItem   = obj;
        clickListenerMenu   = clickListener;

        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_menu_icon_social_link, this);

        txtName             = (TextView) findViewById(R.id.txtName);
        layout_back         = (LinearLayout) findViewById(R.id.layout_back);
        txtName.setVisibility(GONE);

        layout_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListenerMenu != null){
                    clickListenerMenu.onClick(view, objSocialLinkItem);
                }
            }
        });

        imgIcon             = (ImageView)   findViewById(R.id.imgIcon);
        if (objSocialLinkItem.icon_bmp != null) {
            imgIcon.setImageBitmap(objSocialLinkItem.icon_bmp);
        }
        imgIcon.setVisibility(VISIBLE);
    }

    public ObjSocialLinkItem getObject() {
        return objSocialLinkItem;
    }

    @Override
    public void onClick(View v) {
        if (clickListenerMenu != null){
            clickListenerMenu.onClick(v, objSocialLinkItem);
        }
    }

    public void setOnClickListener(ClickListenerSocialLink listener) {
        clickListenerMenu = listener;
    }

}