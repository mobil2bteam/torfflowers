package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Cart;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Delivery.ObjDelivery;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjCards.ObjCards;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.PromoCode.ObjPromoCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCart {

    public  ArrayList<ObjProductMin> products;
    public double totalNotSale;
    public double sumSale;
    public double percentSale;
    public double totalSale;
    public double totalSumSale;
    public double totalSumPercent;
    public double totalCart;
    public double sumDelivery;
    public double total;

    public ObjPromoCode promoCode;
    public ArrayList<ObjDelivery> delivery;
    public ObjCards card;

    public ObjCart() {
        init();
    }
    public ObjCart(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjCart(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("cart")) {
                JSONObject jsonMain = json.getJSONObject("cart");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        products                = new ArrayList<>();
        delivery                = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("totalNotSale")) {
                totalNotSale = json.getDouble("totalNotSale");
            }
            if (json.has("sumSale")) {
                sumSale = json.getDouble("sumSale");
            }
            if (json.has("percentSale")) {
                percentSale = json.getDouble("percentSale");
            }
            if (json.has("totalSale")) {
                totalSale = json.getDouble("totalSale");
            }
            if (json.has("totalSumSale")) {
                totalSumSale = json.getDouble("totalSumSale");
            }
            if (json.has("totalSumPercent")) {
                totalSumPercent = json.getDouble("totalSumPercent");
            }
            if (json.has("totalCart")) {
                totalCart = json.getDouble("totalCart");
            }
            if (json.has("sumDelivery")) {
                sumDelivery = json.getDouble("sumDelivery");
            }
            if (json.has("totalSale")) {
                total = json.getDouble("total");
            }
            if (json.has("promoCode")) {
                promoCode = new ObjPromoCode(json.getJSONObject("promoCode"));
            }
            if (json.has("delivery")) {
                for (int i = 0; i < json.getJSONArray("delivery").length(); i++) {
                    ObjDelivery it = new ObjDelivery(json.getJSONArray("delivery").getJSONObject(i));
                    delivery.add(it);
                }
            }
            if (json.has("products")) {
                for (int i = 0; i < json.getJSONArray("products").length(); i++) {
                    ObjProductMin it = new ObjProductMin(json.getJSONArray("products").getJSONObject(i));
                    products.add(it);
                }
            }

            if (json.has("card")) {
                card = new ObjCards(json.getJSONObject("card"));
            }
        } catch (JSONException e) {
        }
    }

}
