package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by renatganiev on 18.04.16.
 */
public class Cache {

    private ContentValues cv;
    SQLiteDatabase db;
    DBHelper dbHelper;
    Context mContext;

    public class CacheValue {
        public String value;
        public long datetime;
    }

    public Cache(Context context) {

        // создаем объект для данных
        cv = new ContentValues();
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(context);
        // подключаемся к БД
        db = dbHelper.getWritableDatabase();
        //dbHelper.onUpgrade(db, 0, 1);

        mContext = context;
    }

    // ДОБАВЛЯЕМ В КОРЗИНУ ТОВАР
    public void AddCache(String key, long date, String value) {
        key = md5(key);
        String query = "SELECT * FROM cache WHERE cache_key = '".concat(key.concat("'"));
        Cursor cursor1 = db.rawQuery(query, null);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        cv.put("cache_key",     key);
        //cv.put("cache_date",    dateFormat.format(date));
        cv.put("cache_date",    date);
        cv.put("cache_value",   value);

        // ЕСЛИ УЖЕ ЕСТЬ, ТО НАДО ПРИБАВИТЬ КОЛИЧЕСТВО К СУЩЕСТВУЮЩЕЙ
        int updCount;
        long rowID;
        if (cursor1.getCount() > 0) {
            updCount    = db.update("cache", cv, "cache_key = ?", new String[]{key});
        } else {
            // вставляем запись и получаем ее ID
            rowID      = db.insert("cache", null, cv);
        }
        cursor1.close();
        db.close();
    }

    // ДОБАВЛЯЕМ В КОРЗИНУ ТОВАР
    public CacheValue getCache(String key) {
        key = md5(key);
        // ДЛЯ НАЧАЛА ПРОВЕРЯЕМ, ЕСТЬ ЛИ ТАКАЯ ЗАПИСЬ В НАШЕЙ КОРЗИНЕ
        Date date = new Date();
        String query = "SELECT * FROM cache WHERE cache_key = '".concat(key.concat("'").concat(" AND (cache_date > ".concat(String.valueOf(date.getTime())).concat(")")));
        Cursor cursor1 = db.rawQuery(query, null);

        // Кэш живой, вернем его значение
        if (cursor1.getCount() > 0) {
            if (cursor1.moveToFirst()) {
                // определяем номера столбцов по имени в выборке
                int vIndex = cursor1.getColumnIndex("cache_value");
                int dIndex = cursor1.getColumnIndex("cache_date");
                String value;
                long dateval;

                do {
                    value = cursor1.getString(vIndex);
                    dateval =  cursor1.getLong(dIndex);
                    break;
                } while (cursor1.moveToNext());
                cursor1.close();
                // Вернули значение кэша
                CacheValue val = new CacheValue();
                val.value       = value;
                val.datetime    = dateval;
                return val;
            }
        }
        // Кэш уже умер
        return null;
    }

    public int clear() {
        int clearCount = db.delete("cache", null, null);
        db.close();
        return clearCount;
    }

    // ДОБАВЛЯЕМ В КОРЗИНУ ТОВАР
    public void deleteOldCache() {

        // ДЛЯ НАЧАЛА ПРОВЕРЯЕМ, ЕСТЬ ЛИ ТАКАЯ ЗАПИСЬ В НАШЕЙ КОРЗИНЕ
        Date date = new Date();
        //String query = "DELETE FROM cache WHERE cache_date < ";
        int clearCount = db.delete("cache", "cache_date < ".concat(String.valueOf(date.getTime())), null);

        //Cursor cursor1 = db.rawQuery(query, null);
        db.close();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }



    private static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
