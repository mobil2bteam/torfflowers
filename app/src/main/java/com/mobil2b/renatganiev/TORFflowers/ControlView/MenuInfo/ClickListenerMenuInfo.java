package com.mobil2b.renatganiev.torfflowers.ControlView.MenuInfo;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.MenuInfo.ObjMenuInfo;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerMenuInfo {
    void onClick(View view, ObjMenuInfo menuInfo);
}
