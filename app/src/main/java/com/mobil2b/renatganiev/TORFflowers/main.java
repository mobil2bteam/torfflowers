package com.mobil2b.renatganiev.torfflowers;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBBag;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Fragments.fragment_app_info;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;

public class main extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;

    DrawerLayout drawer;
    NavigationView navigationView;

    MenuItem menuItemSearch;

    int badgeCount = 0;
    MenuItem itemBag;

    int PERMISSION_REQUEST_CODE = 500;
    String NOTIFICATION_MESSAGE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        fragmentmanager = getSupportFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction = callbackAction;
        fragmentControl.drawer = drawer;

        navigationView.setNavigationItemSelectedListener(this);


        Intent arg = getIntent();
        Bundle bundle = null;
        if (arg != null) {
            bundle = arg.getExtras();
            if (bundle != null) {
                if (bundle.containsKey("filter")) {
                    fragmentControl.showFragmentProductList(true, null, R.id.layout_container, bundle);
                    return;
                }
                if (bundle.containsKey("NOTIFICATION_MESSAGE")) {
                    NOTIFICATION_MESSAGE = bundle.getString("NOTIFICATION_MESSAGE");
                }
            }
        }
        fragmentControl.showFragmentHome(true, null, R.id.layout_container, bundle);


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.item_main) {
            // Handle the camera action
            fragmentControl.showFragmentHome(false, null, R.id.layout_container, null);
        }

        if (id == R.id.item_catalog) {
            if (PublicData.SETTINGS != null) {
                Bundle bundle = new Bundle();
                bundle.putInt("catalog_id", PublicData.SETTINGS.default_catalog);
                fragmentControl.showFragmentCatalog(false, null, R.id.layout_container, bundle);
            }
        }

        if (id == R.id.item_user_info) {
            fragmentControl.showFragmentUserStart(false, null, R.id.layout_container, null);
        }

        if (id == R.id.item_news) {
            fragmentControl.showFragmentNewsList(false, null, R.id.layout_container, null);
        }

        if (id == R.id.item_favorite) {
            fragmentControl.showFragmentFavorite(false, null, R.id.layout_container, null);
        }

        if (id == R.id.item_comment) {
            fragmentControl.showFragmentCompanyComment(false, null, R.id.layout_container, null);
        }

        if (id == R.id.item_about) {
            Intent intent = new Intent(main.this, about_company.class);
            startActivity(intent);
        }

        if (id == R.id.item_info) {
            fragmentControl.showFragmentInfo(false, null, R.id.layout_container, null);
        }

        if (id == R.id.item_bag) {
            //fragmentControl.showFragmentBag(false, null, R.id.layout_container, null);
            Intent intent = new Intent(main.this, bag.class);
            startActivity(intent);
        }

        if (id == R.id.item_order_history) {
            //fragmentControl.showFragmentOrderUserList(false, null, R.id.layout_container, null);
            Intent intent = new Intent(main.this, user_order_list.class);
            startActivity(intent);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_default, menu);

        menuItemSearch = menu.findItem(R.id.menu_search);

        if (PublicData.HOME != null) {
            menuItemSearch.setVisible(PublicData.HOME.flag_show_search);
        }

        DBBag b = new DBBag(main.this);
        badgeCount = b.getOfferList().size();
        b.close();

        //if (badgeCount > 0) {
        Drawable icon = ContextCompat.getDrawable(main.this, R.drawable.ic_toolbar_icon_bag);
        ActionItemBadge.update(this, menu.findItem(R.id.menu_bag), icon, ActionItemBadge.BadgeStyles.RED, badgeCount);
        itemBag = menu.findItem(R.id.menu_bag);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_bag) {
            //fragmentControl.showFragmentBag(false, null, R.id.layout_container, null);
            Intent intent = new Intent(main.this, bag.class);
            startActivity(intent);
        }

        if (id == R.id.menu_search) {
            //updateUserData();
            Intent intent = new Intent(main.this, search.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCount();
    }

    void updateCount() {
        if (itemBag != null) {
            DBBag b = new DBBag(main.this);
            badgeCount = b.getOfferList().size();
            ActionItemBadge.update(itemBag, badgeCount);
            b.close();
        }
    }


    CallbackAction callbackAction = new CallbackAction() {
        @Override
        public void onAction(String tag, int type, ObjLink data) {

        }

        @Override
        public void onAction(String tag, int type, Bundle data) {
            if (type == PublicData.SELECT_CATALOG) {
                if (data.containsKey("catalog_id")) {
                    fragmentControl.showFragmentCatalog(false, null, R.id.layout_container, data);
                }
            }
            if (type == PublicData.SELECT_FILTER) {
                fragmentControl.showFragmentProductList(false, null, R.id.layout_container, data);
            }
            if (tag.equals(fragment_app_info.TAG)) {
                if (type == PublicData.CALL_PHONE_NUMBER) {
                    if (PublicData.SETTINGS != null) {
                        if (PublicData.SETTINGS.company_info != null) {
                            if (PublicData.SETTINGS.company_info.contacts != null) {
                                if (Helps.isNotEmpty(PublicData.SETTINGS.company_info.contacts.phone)) {
                                    PublicData.callPhoneStr = PublicData.SETTINGS.company_info.contacts.phone;
                                    callPhone(PublicData.SETTINGS.company_info.contacts.phone);
                                }
                            }
                        }
                    }
                }
            }
            if (tag.equals(fragment_app_info.TAG)) {
                if (type == PublicData.CALL_SEND_EMAIL) {
                    openEmail();
                }
            }
        }
    };

    void openEmail() {
        if (PublicData.SETTINGS != null) {
            if (PublicData.SETTINGS.company_info != null) {
                if (PublicData.SETTINGS.company_info.contacts != null) {
                    if (PublicData.SETTINGS.company_info.contacts.email != null) {


                        String mailto = "mailto:" +PublicData.SETTINGS.company_info.contacts.email;

                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);

                        //emailIntent.putExtra(Intent.EXTRA_EMAIL, PublicData.SETTINGS.company_info.contacts.email);
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Письмо с мобильного приложения");
                        emailIntent.putExtra(Intent.EXTRA_TEXT, "");

                        emailIntent.setData(Uri.parse(mailto));

                        try {
                            startActivity(emailIntent);
                        } catch (ActivityNotFoundException e) {
                            //TODO: Handle case where no email app is available
                        }


                    }
                }
            }
        }
    }


    void callPhone(String callPhoneStr) {
        if (ActivityCompat.checkSelfPermission(main.this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent myIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + callPhoneStr));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(myIntent);
            return;
        } else {
            PublicData.callPhoneStr = callPhoneStr;
            ActivityCompat.requestPermissions(main.this, new String[] { android.Manifest.permission.CALL_PHONE }, PublicData.CALL_PHONE_NUMBER);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callPhone(PublicData.callPhoneStr);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
