package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjSort {

    public int      id;
    public String   name = "";
    public boolean  check;

    public ObjSort() {
        init();
    }
    public ObjSort(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjSort(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("sort")) {
                JSONObject jsonMain = json.getJSONObject("sort");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
        } catch (JSONException e) {
        }
    }

}
