package com.mobil2b.renatganiev.torfflowers.Fragments.User.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackPasswordMemory;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjMessage;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_user_login extends Fragment {

    public static final String TAG = "fragment_app_user_login";
    public static final int LAYOUT = R.layout.fragment_app_user_login;
    View rootView;
    public CallbackAction callbackActionSuper;

    LinearLayout layout_user_email, layout_user_password;
    ImageView imgCheckEmail, imgCheckPassword;
    EditText editUserEmail, editUserPassword;
    Button btnLogin, btnPasswordMemory;
    ProgressBar progressBar;

    public static fragment_app_user_login getInstance(Bundle args) {
        fragment_app_user_login fragment = new fragment_app_user_login();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);


        layout_user_email           = rootView.findViewById(R.id.layout_user_email);
        layout_user_password        = rootView.findViewById(R.id.layout_user_password);

        imgCheckEmail               = rootView.findViewById(R.id.imgCheckEmail);
        imgCheckPassword            = rootView.findViewById(R.id.imgCheckPassword);

        editUserEmail               = rootView.findViewById(R.id.editUserEmail);
        editUserPassword            = rootView.findViewById(R.id.editUserPassword);

        btnLogin                    = rootView.findViewById(R.id.btnLogin);
        btnPasswordMemory           = rootView.findViewById(R.id.btnPasswordMemory);
        progressBar                 = rootView.findViewById(R.id.progressBar);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkedEditText();
            }
        });

        btnPasswordMemory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialogMemoryPassword();
            }
        });

        return rootView;
    }


    void checkedEditText() {

        if (editUserEmail.getText().toString().trim().length() == 0) {
            imageError(imgCheckEmail, layout_user_email);
            return;
        } else {
            imageCheck(imgCheckEmail);
        }
        if (editUserPassword.getText().toString().trim().length() == 0) {
            imageError(imgCheckPassword, layout_user_password);
            return;
        } else {
            imageCheck(imgCheckPassword);
        }

        User.onLogin(editUserEmail.getText().toString(), editUserPassword.getText().toString());

    }

    void imageError(ImageView imageView, View v) {
        imageView.setImageResource(R.drawable.ic_info_black_24dp);
        Helps.errorTextEdit(v, getActivity());
    }

    void imageCheck(ImageView imageView) {
        imageView.setImageResource(R.drawable.ic_check_circle_black_24dp);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    private void showAlertDialogMemoryPassword() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.msg_memory_password, null);
        dialogBuilder.setView(dialogView);

        Button btnOk        = (Button)dialogView.findViewById(R.id.btnOk);
        Button btnCancel    = (Button)dialogView.findViewById(R.id.btnCancel);
        final EditText edit_login = (EditText)dialogView.findViewById(R.id.edit_login);

        final AlertDialog alertDialog = dialogBuilder.create();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_login.getText().toString().trim().length() == 0) {
                    Helps.errorTextEdit(edit_login, getActivity());
                    return;
                }
                User.onPasswordMemory(edit_login.getText().toString(), callbackPasswordMemory);
                alertDialog.cancel();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });

        alertDialog.show();
    }


    CallbackPasswordMemory callbackPasswordMemory = new CallbackPasswordMemory() {
        @Override
        public void onStart() {
            PublicData.showDialog(getActivity());
        }

        @Override
        public void onSuccess(final ObjMessage msg) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    if (msg != null) {
                        Helps.showMessage(msg, getActivity());
                    }
                }
            });
        }

        @Override
        public void onError(final String message, int error_code) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PublicData.closeDialog();
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

}
