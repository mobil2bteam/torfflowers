package com.mobil2b.renatganiev.torfflowers.ControlView.GroupProduct;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.IntRange;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class ProductItemDecorator extends RecyclerView.ItemDecoration {
    private final int columns;
    private int countOfItems;

    /**
     * constructor
     *
     * @param columns number of columns of the RecyclerView
     */
    public ProductItemDecorator(@IntRange(from = 0) int columns) {
        this.columns = columns;
        this.countOfItems = 0;
    }
    public ProductItemDecorator(@IntRange(from = 1) int countOfItems, @IntRange(from = 0) int columns) {
        this.columns = columns;
        this.countOfItems = countOfItems;
    }

    /**
     * Set different margins for the items inside the recyclerView: no top margin for the first row
     * and no left margin for the first column.
     */
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }
    public static float getDensity() {
        return Resources.getSystem().getDisplayMetrics().density;
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        int margin;
        if(countOfItems != 0){
            view.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            int width=view.getMeasuredWidth();
            margin = (getScreenWidth() - width*countOfItems)/countOfItems;
            view.measure(0,0);
        }else{
            margin = (int) (getDensity()*5);
        }

        Log.e("ItemDecorator",""+margin);
        Log.e("ItemDecorator 5dp = ",""+getDensity()*5);

        int position = parent.getChildLayoutPosition(view);
        //set right margin to all
        if(position!=columns-1){
            outRect.right = margin;
        }

        //set bottom margin to all

        //add left margin only to the first column

    }
}