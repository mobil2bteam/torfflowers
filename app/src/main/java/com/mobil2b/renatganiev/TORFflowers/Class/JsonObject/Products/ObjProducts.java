package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjProducts {

    public  ArrayList<ObjProductMin>              items;
    public  ObjFilter                             filter;
    public  ObjProductsOptions                    options;

    public ObjProducts() {
        init();
    }
    public ObjProducts(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjProducts(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("products")) {
                JSONObject jsonMain = json.getJSONObject("products");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    public void addNewResult(ObjProducts newProducts) {
        if (newProducts != null) {
            this.filter = newProducts.filter;
            this.options = newProducts.options;
            if (newProducts.items != null) {
                this.items.addAll(newProducts.items);
            }
        }
    }

    private void init() {
        items                = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {

            if (json.has("filter")) {
                filter = new ObjFilter(json.getJSONObject("filter"));
            }
            if (json.has("options")) {
                options = new ObjProductsOptions(json.getJSONObject("options"));
            }
            if (json.has("items")) {
                for (int i = 0; i < json.getJSONArray("items").length(); i++){
                    ObjProductMin it = new ObjProductMin(json.getJSONArray("items").getJSONObject(i));
                    items.add(it);
                }
            }

        } catch (JSONException e) {
        }
    }

}
