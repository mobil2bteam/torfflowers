package com.mobil2b.renatganiev.torfflowers.ControlView.DeliveryDateView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.R;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;


/**
 * Created by renatganiev on 10.10.15.
 */
public class DeliveryDateView extends RelativeLayout implements View.OnClickListener {

    public int REZULT_REQUEST_SELECT_ADDRESS                        = 870;
    Context             mContext;
    TextView txtDate;
    ImageView imgClear;
    LinearLayout layout_change_city;
    OnClickListener _wrappedOnClickListener;

    Date selectDate;

    public DeliveryDateView(Context context) {
        super(context);
        super.setOnClickListener(this);

        mContext    = context;
        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_delivery_date_view, this);

        txtDate                 = (TextView) findViewById(R.id.txtDate);
        layout_change_city      = (LinearLayout) findViewById(R.id.layout_change_city);
        imgClear                = (ImageView) findViewById(R.id.imgClear);
        imgClear.setVisibility(GONE);

        layout_change_city.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_wrappedOnClickListener != null) {
                    _wrappedOnClickListener.onClick(view);
                }
            }
        });

        imgClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                clear();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Очистить?").setPositiveButton("Да", dialogClickListener)
                        .setNegativeButton("Нет", dialogClickListener).show();
            }
        });

        updateRecipientName();
    }

    void updateRecipientName() {
        if (selectDate != null) {
            SimpleDateFormat simpleDateFormat =
                    new SimpleDateFormat("dd MMMM yyyy (E)");
            String timestamp = simpleDateFormat.format(selectDate);
            txtDate.setText(timestamp);
            imgClear.setVisibility(VISIBLE);
        } else {
            imgClear.setVisibility(GONE);
            txtDate.setText("");
        }
    }

    public long getValue() {
        if (selectDate != null) {
            return selectDate.getTime();
        }
        return 0;
    }

    public void clear() {
        selectDate = null;
        updateRecipientName();
    }

    @Override
    public void onClick(View view) {
        if (_wrappedOnClickListener != null)
            _wrappedOnClickListener.onClick(view);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        _wrappedOnClickListener = l;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REZULT_REQUEST_SELECT_ADDRESS) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle.containsKey("date")) {
                        long selectIdCity = bundle.getLong("date", 0);
                        selectDate = new Date(selectIdCity);
                        updateRecipientName();
                    }
                }
            }
        }
    }

    public void onResume() {
        updateRecipientName();
    }
}