package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCatalogItems {

    public int      id;
    public int      parent_id;
    public int      children_count;
    public String   name;
    public ObjImage image;
    public ObjImage icon;
    public String   comment;
    public String   view;
    public boolean  flag_not_show_name;
    public boolean  search;
    public ArrayList<ObjCatalogItems> items;

    public ObjCatalogItems() {
        init();
    }
    public ObjCatalogItems(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjCatalogItems(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("settings")) {
                JSONObject jsonMain = json.getJSONObject("settings");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        items = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("parent_id")) {
                parent_id = json.getInt("parent_id");
            }
            if (json.has("children_count")) {
                children_count = json.getInt("children_count");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("image")) {
                image = new ObjImage(json.getJSONObject("image"));
            }
            if (json.has("icon")) {
                icon = new ObjImage(json.getJSONObject("icon"));
            }
            if (json.has("comment")) {
                comment = json.getString("comment");
            }
            if (json.has("view")) {
                view = json.getString("view");
            }
            if (json.has("flag_not_show_name")) {
                flag_not_show_name = json.getBoolean("flag_not_show_name");
            }
            if (json.has("search")) {
                search = json.getBoolean("search");
            }

            for (int i =0;i<json.getJSONArray("items").length();i++){
                ObjCatalogItems it = new ObjCatalogItems(json.getJSONArray("items").getJSONObject(i));
                items.add(it);
            }
        } catch (JSONException e) {
        }
    }

}
