package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjSettingsFilter {

    public boolean          brand;
    public boolean          size;
    public boolean          color;
    public boolean          material;
    public boolean          season;
    public boolean          age;
    public boolean          flags;
    public boolean          price;
    public boolean          sex;
    public boolean          country;
    public boolean          properties;


    public ObjSettingsFilter() {
        init();
    }
    public ObjSettingsFilter(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjSettingsFilter(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("filters")) {
                JSONObject jsonMain = json.getJSONObject("filters");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {

    }

    public void loadJSONObject(JSONObject json) {
        try {

            if (json.has("brand")) {
                brand = json.getBoolean("brand");
            }
            if (json.has("size")) {
                size = json.getBoolean("size");
            }
            if (json.has("color")) {
                color = json.getBoolean("color");
            }
            if (json.has("material")) {
                material = json.getBoolean("material");
            }
            if (json.has("season")) {
                season = json.getBoolean("season");
            }
            if (json.has("age")) {
                age = json.getBoolean("age");
            }
            if (json.has("flags")) {
                flags = json.getBoolean("flags");
            }
            if (json.has("price")) {
                price = json.getBoolean("price");
            }
            if (json.has("sex")) {
                sex = json.getBoolean("sex");
            }
            if (json.has("country")) {
                country = json.getBoolean("country");
            }
            if (json.has("properties")) {
                properties = json.getBoolean("properties");
            }

        } catch (JSONException e) {
        }
    }

}
