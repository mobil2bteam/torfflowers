package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjProductsOptions implements Serializable {


    public boolean              filter_edit;
    public String               title;

    public ObjProductsOptions() {
        init();
    }
    public ObjProductsOptions(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjProductsOptions(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("options")) {
                JSONObject jsonMain = json.getJSONObject("options");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("filter_edit")) {
                filter_edit = json.getBoolean("filter_edit");
            }
            if (json.has("title")) {
                title = json.getString("title");
            }
        } catch (JSONException e) {
        }
    }
}
