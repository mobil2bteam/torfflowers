package com.mobil2b.renatganiev.torfflowers.Class.Authorization.session;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjCards.ObjCards;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjTypePrice.ObjTypePrice;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.PromoCode.ObjPromoCode;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjParam;


/**
 * Created by renatganiev on 02.04.16.
 */


public class DataSession {

    public boolean authorization;

    public int      id;             // Ид
    public boolean  verify;         // Прошел верификацию
    public boolean  active;         // Активен
    public boolean  partner;        // Партнер
    public boolean  partner_feed;   // Заявка на партнерство подана
    public String   token;          // Токен доступа
    public String   email;          // Почта
    public String   login;          // Логин
    public String   password;       // Пароль
    public String   phone;          // Телефон
    public String   user_name;      // Псевдоним
    public String   first_name;     // Имя
    public String   second_name;    // Фамилия
    public String   last_name;      // Очество
    public int      sex;            // Пол
    public int      city_id;        // Ид Города
    public String   city_name;      // Название города
    public int      orders_count;   // Количество заказов

    public ArrayList<ObjCards> cards;
    public ObjTypePrice typePrice;
    public ObjPromoCode promocode;

    Context mContext;

    public void copySession(DataSession session) {
        if (session != null) {
            this.authorization      = session.authorization;
            this.id                 = session.id;
            this.active             = session.active;
            this.partner            = session.partner;
            this.partner_feed       = session.partner_feed;
            this.token              = session.token;
            this.verify             = session.verify;
            this.email              = session.email;
            this.phone              = session.phone;
            this.user_name          = session.user_name;
            this.first_name         = session.first_name;
            this.second_name        = session.second_name;
            this.last_name          = session.last_name;
            this.sex                = session.sex;
            this.city_id            = session.city_id;
            this.city_name          = session.city_name;
            this.orders_count       = session.orders_count;

        }
    }

    public DataSession() {
        init();
    }

    public DataSession(JSONObject json) {
        init();
        loadJSONObject(json);
    }

    public DataSession(String txt, Context mCtx) {

        mContext = mCtx;

        init();
        try {
            JSONObject json = new JSONObject(txt);
            if (json.has("user")) {
                json = json.getJSONObject("user");
                loadJSONObject(json);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        cards = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {

            if (json.has("info")) {
                JSONObject jsonInfo = json.getJSONObject("info");

                authorization = true;

                if (jsonInfo.has("token"))          token           = jsonInfo.getString("token");
                if (jsonInfo.has("verify"))         verify          = jsonInfo.getBoolean("verify");
                if (jsonInfo.has("partner"))        partner         = jsonInfo.getBoolean("partner");
                if (jsonInfo.has("partner_feed"))   partner_feed    = jsonInfo.getBoolean("partner_feed");
                if (jsonInfo.has("active"))         active          = jsonInfo.getBoolean("active");

                if (jsonInfo.has("id"))             id              = jsonInfo.getInt("id");
                if (jsonInfo.has("sex"))            sex             = jsonInfo.getInt("sex");
                if (jsonInfo.has("city_id"))        city_id         = jsonInfo.getInt("city_id");
                if (jsonInfo.has("orders_count"))   orders_count    = jsonInfo.getInt("orders_count");


                if (jsonInfo.has("email"))          email           = jsonInfo.getString("email");
                if (jsonInfo.has("phone"))          phone           = jsonInfo.getString("phone");

                if (jsonInfo.has("user_name"))      user_name       = jsonInfo.getString("user_name");
                if (jsonInfo.has("first_name"))     first_name      = jsonInfo.getString("first_name");
                if (jsonInfo.has("second_name"))    second_name     = jsonInfo.getString("second_name");
                if (jsonInfo.has("last_name"))      last_name       = jsonInfo.getString("last_name");
                if (jsonInfo.has("city_name"))      city_name       = jsonInfo.getString("city_name");
            }

            if (json.has("cards")) {
                for (int i =0;i<json.getJSONArray("cards").length();i++){
                    ObjCards it = new ObjCards(json.getJSONArray("cards").getJSONObject(i));
                    cards.add(it);
                }
            }

            if (json.has("price_type")) {
                typePrice = new ObjTypePrice(json.getJSONObject("price_type"));
            }

            if (json.has("promocode")) {
                promocode = new ObjPromoCode(json.getJSONObject("promocode"));
            }

        } catch (JSONException e) {
        }
    }

    public ArrayList<ObjParam> getParamList() {
        ArrayList<ObjParam> rez = new ArrayList<>();

        if ((this.token != null) && (!this.token.equals(""))) {
            rez.add(new ObjParam("token", this.token));
        }
        if ((this.email != null) && (!this.email.equals(""))) {
            rez.add(new ObjParam("email", this.email));
        }
        if ((this.phone != null) && (!this.phone.equals(""))) {
            rez.add(new ObjParam("phone", this.phone));
        }
        if ((this.user_name != null) && (!this.user_name.equals(""))) {
            rez.add(new ObjParam("user_name", this.user_name));
        }
        if ((this.first_name != null) && (!this.first_name.equals(""))) {
            rez.add(new ObjParam("first_name", this.first_name));
        }
        if ((this.second_name != null) && (!this.second_name.equals(""))) {
            rez.add(new ObjParam("second_name", this.second_name));
        }
        if ((this.last_name != null) && (!this.last_name.equals(""))) {
            rez.add(new ObjParam("last_name", this.last_name));
        }
        if ((this.city_name != null) && (!this.city_name.equals(""))) {
            rez.add(new ObjParam("city_name", this.city_name));
        }
        if (this.sex != 0) {
            rez.add(new ObjParam("sex", String.valueOf(this.sex)));
        }
        if (this.city_id != 0) {
            rez.add(new ObjParam("city_id", String.valueOf(this.city_id)));
        }
        if ((this.login != null) && (!this.login.equals(""))) {
            rez.add(new ObjParam("login", this.login));
        }
        if ((this.password != null) && (!this.password.equals(""))) {
            rez.add(new ObjParam("password", this.password));
        }
        return rez;
    }

}
