package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.PromoCode.ObjPromoCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjOrder {

    public int id;
    public int number;
    public String createdAt;
    public int userId;
    public double totalNotSale;
    public double sumSale;
    public double percentSale;
    public double totalSale;
    public double totalSumSale;
    public double totalSumPercent;
    public double totalCart;
    public double sumDelivery;
    public double promoPercent;
    public double balls;
    public double cartSale;
    public double total;
    public boolean isPayment;

    public  ArrayList<ObjProductMin> products;
    public ObjOrderDelivery delivery;
    public ObjOrderPayment payment;
    public ObjOrderStatus status;
    public ObjOrderContactUser contact_user;
    public ObjPromoCode promoCode;


    public ObjOrder() {
        init();
    }
    public ObjOrder(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjOrder(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("order")) {
                JSONObject jsonMain = json.getJSONObject("order");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        products                = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("number")) {
                number = json.getInt("number");
            }
            if (json.has("userId")) {
                userId = json.getInt("userId");
            }
            if (json.has("createdAt")) {
                createdAt = json.getString("createdAt");
            }
            if (json.has("balls")) {
                balls = json.getDouble("balls");
            }
            if (json.has("cartSale")) {
                cartSale = json.getDouble("cartSale");
            }
            if (json.has("isPayment")) {
                isPayment = json.getBoolean("isPayment");
            }
            if (json.has("promoPercent")) {
                promoPercent = json.getDouble("promoPercent");
            }
            if (json.has("totalNotSale")) {
                totalNotSale = json.getDouble("totalNotSale");
            }
            if (json.has("sumSale")) {
                sumSale = json.getDouble("sumSale");
            }
            if (json.has("percentSale")) {
                percentSale = json.getDouble("percentSale");
            }
            if (json.has("totalSale")) {
                totalSale = json.getDouble("totalSale");
            }
            if (json.has("totalSumSale")) {
                totalSumSale = json.getDouble("totalSumSale");
            }
            if (json.has("totalSumPercent")) {
                totalSumPercent = json.getDouble("totalSumPercent");
            }
            if (json.has("totalCart")) {
                totalCart = json.getDouble("totalCart");
            }
            if (json.has("sumDelivery")) {
                sumDelivery = json.getDouble("sumDelivery");
            }
            if (json.has("total")) {
                total = json.getDouble("total");
            }
            if (json.has("delivery")) {
                delivery = new ObjOrderDelivery(json.getJSONObject("delivery"));
            }
            if (json.has("payment")) {
                payment = new ObjOrderPayment(json.getJSONObject("payment"));
            }
            if (json.has("status")) {
                status = new ObjOrderStatus(json.getJSONObject("status"));
            }
            if (json.has("contact_user")) {
                contact_user = new ObjOrderContactUser(json.getJSONObject("contact_user"));
            }
            if (json.has("promoCode")) {
                promoCode = new ObjPromoCode(json.getJSONObject("promoCode"));
            }
            if (json.has("products")) {
                for (int i = 0; i < json.getJSONArray("products").length(); i++) {
                    ObjProductMin it = new ObjProductMin(json.getJSONArray("products").getJSONObject(i));
                    products.add(it);
                }
            }
        } catch (JSONException e) {
        }
    }

}
