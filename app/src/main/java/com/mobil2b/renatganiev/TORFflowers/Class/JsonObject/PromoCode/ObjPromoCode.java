package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.PromoCode;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjPromoCode {

    public int      id;
    public String   code;
    public double   percent;
    public double   totalPromoCode;

    public ObjPromoCode() {
        init();
    }
    public ObjPromoCode(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjPromoCode(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("promoCode")) {
                JSONObject jsonMain = json.getJSONObject("promoCode");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("code")) {
                code = json.getString("code");
            }
            if (json.has("percent")) {
                percent = json.getDouble("percent");
            }
            if (json.has("totalPromoCode")) {
                totalPromoCode = json.getDouble("totalPromoCode");
            }
        } catch (JSONException e) {
        }
    }

}
