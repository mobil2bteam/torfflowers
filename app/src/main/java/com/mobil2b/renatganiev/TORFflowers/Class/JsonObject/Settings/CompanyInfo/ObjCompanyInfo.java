package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import android.content.Context;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Slider.ObjSlider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCompanyInfo {
    public int id;
    public String name, description_short, description_long;
    public ObjSlider slider;
    public ObjSocialLink social_link;
    public ObjContacts contacts;
    public ArrayList<ObjWorkingHours> working_hours;
    public ArrayList<ObjCompanyPoint> points;
    public Context mContext;


    public ObjCompanyInfo() {
        init();
    }
    public ObjCompanyInfo(JSONObject json, Context context) {
        mContext = context;
        init();
        loadJSONObject(json);
    }
    public ObjCompanyInfo(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    private void init() {
        working_hours   = new ArrayList<>();
        points          = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                this.id = json.getInt("id");
            }
            if (json.has("name")) {
                this.name = json.getString("name");
            }
            if (json.has("description_short")) {
                this.description_short = json.getString("description_short");
            }
            if (json.has("description_long")) {
                this.description_long = json.getString("description_long");
            }
            if (json.has("slider")) {
                slider = new ObjSlider(json.getJSONObject("slider"));
            }
            if (json.has("social_links")) {
                JSONObject social_linkJson = json.getJSONObject("social_links");
                social_link = new ObjSocialLink(social_linkJson, mContext);
            }
            if (json.has("contacts")) {
                JSONObject contactsJson = json.getJSONObject("contacts");
                contacts = new ObjContacts(contactsJson);
            }
            if(json.has("working_house")){
                for (int i =0;i<json.getJSONArray("working_house").length();i++){
                    ObjWorkingHours sliderItem = new ObjWorkingHours(json.getJSONArray("working_house").getJSONObject(i));
                    working_hours.add(sliderItem);
                }
            }
            if(json.has("points")){
                for (int i =0;i<json.getJSONArray("points").length();i++){
                    ObjCompanyPoint sliderItem = new ObjCompanyPoint(json.getJSONArray("points").getJSONObject(i), mContext);
                    points.add(sliderItem);
                }
            }

        } catch (JSONException e) {
        }
    }

    public ArrayList<String> getListCity() {

        ArrayList<String> listCity = new ArrayList<>();

        for (ObjCompanyPoint companyPoint : points) {

            if (companyPoint.contacts != null) {
                if (companyPoint.contacts.address != null) {
                    if (companyPoint.contacts.address.city != null) {
                        if (!companyPoint.contacts.address.city.equals("")) {
                            if (listCity.indexOf(companyPoint.contacts.address.city.trim().toUpperCase()) == -1) {
                                listCity.add(companyPoint.contacts.address.city.trim().toUpperCase());
                            }
                        }
                    }
                }
            }
        }

        return listCity;
    }
}
