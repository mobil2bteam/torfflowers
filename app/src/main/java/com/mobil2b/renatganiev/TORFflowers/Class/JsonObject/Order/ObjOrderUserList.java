package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Order;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjOrderUserList {

    public int id;
    public int number;
    public String createdAt;
    public String address;
    public String addressPoint;
    public String cityText;
    public String deliveryText;
    public String paymentText;
    public String paymentType;
    public String status;
    public double total;
    public boolean isPayment;

    public  ArrayList<ObjImage> products;


    public ObjOrderUserList() {
        init();
    }
    public ObjOrderUserList(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjOrderUserList(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("order")) {
                JSONObject jsonMain = json.getJSONObject("order");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        products                = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("number")) {
                number = json.getInt("number");
            }
            if (json.has("createdAt")) {
                createdAt = json.getString("createdAt");
            }
            if (json.has("isPayment")) {
                isPayment = json.getBoolean("isPayment");
            }
            if (json.has("total")) {
                total = json.getDouble("total");
            }
            if (json.has("address")) {
                address = json.getString("address");
            }
            if (json.has("addressPoint")) {
                addressPoint = json.getString("addressPoint");
            }
            if (json.has("cityText")) {
                cityText = json.getString("cityText");
            }
            if (json.has("deliveryText")) {
                deliveryText = json.getString("deliveryText");
            }
            if (json.has("paymentText")) {
                paymentText = json.getString("paymentText");
            }
            if (json.has("paymentType")) {
                paymentType = json.getString("paymentType");
            }
            if (json.has("status")) {
                status = json.getString("status");
            }
            if (json.has("products")) {
                for (int i = 0; i < json.getJSONArray("products").length(); i++) {
                    ObjImage it = new ObjImage(json.getJSONArray("products").getJSONObject(i));
                    products.add(it);
                }
            }
        } catch (JSONException e) {
        }
    }

}
