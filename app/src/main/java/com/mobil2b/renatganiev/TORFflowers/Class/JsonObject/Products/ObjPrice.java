package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjPrice {

    public int      id;
    public int      oldPrice;
    public int      price;
    public double   sumSale;
    public double   sumSalePercent;
    public double   total;
    public String   unit;
    public int      scale;
    public boolean  stock;
    public int      value_int;
    public int      count;
    public String   value_str;
    public String   weight;

    public ObjPrice() {
        init();
    }
    public ObjPrice(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjPrice(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("age")) {
                JSONObject jsonMain = json.getJSONObject("age");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("old_price")) {
                oldPrice = json.getInt("old_price");
            }
            if (json.has("oldPrice")) {
                oldPrice = json.getInt("oldPrice");
            }
            if (json.has("price")) {
                price = json.getInt("price");
            }
            if (json.has("sumSale")) {
                sumSale = json.getDouble("sumSale");
            }
            if (json.has("sumSalePercent")) {
                sumSalePercent = json.getDouble("sumSalePercent");
            }
            if (json.has("total")) {
                total = json.getDouble("total");
            }
            if (json.has("scale")) {
                scale = json.getInt("scale");
            }
            if (json.has("count")) {
                count = json.getInt("count");
            }
            if (json.has("value_int")) {
                value_int = json.getInt("value_int");
            }
            if (json.has("stock")) {
                stock = json.getBoolean("stock");
            }
            if (json.has("unit")) {
                unit = json.getString("unit");
            }
            if (json.has("value_str")) {
                value_str = json.getString("value_str");
            }
            if (json.has("weight")) {
                weight = json.getString("weight");
            }
        } catch (JSONException e) {
        }
    }

}
