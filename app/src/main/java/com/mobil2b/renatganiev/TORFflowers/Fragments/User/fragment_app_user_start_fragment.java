package com.mobil2b.renatganiev.torfflowers.Fragments.User;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;
import com.mobil2b.renatganiev.torfflowers.user_registration;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.CallbackAuthorization;
import com.mobil2b.renatganiev.torfflowers.Class.Authorization.User;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;
import com.mobil2b.renatganiev.torfflowers.Fragments.FragmentControl;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.R;

import static android.app.Activity.RESULT_OK;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_user_start_fragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "fragment_app_user_start_fragment";
    public static final int LAYOUT = R.layout.fragment_app_user_start_fragment;
    View rootView;
    public CallbackAction callbackActionSuper;
    public DrawerLayout drawer;


    private FragmentManager fragmentmanager;
    private FragmentControl fragmentControl;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    public static fragment_app_user_start_fragment getInstance(Bundle args) {
        fragment_app_user_start_fragment fragment = new fragment_app_user_start_fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.
        }

        fragmentmanager = getChildFragmentManager();
        fragmentControl = new FragmentControl(fragmentmanager);
        fragmentControl.callbackAction = callbackAction;

        User.init(getActivity());
        User.callbackAuthorization = callbackAuthorization;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        initToolbar();

        mSwipeRefreshLayout     = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        if (User.isLoggedIn()) {
            // Авторизован
            fragmentControl.showFragmentUserInfo(true, null, R.id.layout_container, null);
        } else {
            // Не авторизован
            fragmentControl.showFragmentUserWelcome(true, null, R.id.layout_container, null);
        }

        return rootView;
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.my_toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(Helps.dpToPx(2));
        }

        toolbar.setTitle("Личный кабинет");
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        if (drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.str_open_video, R.string.app_name);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_toolbar_icon_menu));
            } else {
                toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_toolbar_icon_menu));
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PublicData.REZULT_REQUEST_USER_REGISTRATION) {
            if (resultCode == RESULT_OK) {
                onRefresh();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (fragmentmanager.getBackStackEntryCount() > 0) {
                        fragmentmanager.popBackStack();
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });

        User.init(getActivity());
        User.callbackAuthorization = callbackAuthorization;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRefresh() {
        User.init(getActivity());
        User.callbackAuthorization = callbackAuthorization;
        User.onLoginSession(getActivity(), callbackAuthorization);
    }

    CallbackAction callbackAction = new CallbackAction() {
        @Override
        public void onAction(String tag, int type, ObjLink data) {
            callbackActionSuper.onAction(tag, type, data);
        }

        @Override
        public void onAction(String tag, int type, Bundle data) {
            if (type == PublicData.USER_LOGIN_CLICK) {
                fragmentControl.showFragmentUserLogin(false, null, R.id.layout_container, null);
            }
            if (type == PublicData.USER_REGISTRATION_CLICK) {
                Intent i = new Intent(getActivity(), user_registration.class);
                startActivityForResult(i, PublicData.REZULT_REQUEST_USER_REGISTRATION);
            }
            if (type == PublicData.USER_ORDER_HISTORY) {
                //fragmentControl.showFragmentOrderUserList(false, null, R.id.layout_container, null);
            }
            callbackActionSuper.onAction(tag, type, data);
        }
    };


    CallbackAuthorization callbackAuthorization = new CallbackAuthorization() {
        @Override
        public void onStart(String text) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }

        @Override
        public void onReturnSession(DataSession result) {

        }

        @Override
        public void onDontSession(String text, int error_code) {
            fragmentControl.showFragmentUserWelcome(true, null, R.id.layout_container, null);
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }

        @Override
        public void onSuccess(DataSession result, int type) {
            fragmentControl.showFragmentUserInfo(true, null, R.id.layout_container, null);
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }

        @Override
        public void onError(DataSession result, final String message, int error_code, int type) {
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

        }

        @Override
        public void onExit() {
            fragmentControl.showFragmentUserWelcome(true, null, R.id.layout_container, null);
            // here you check the value of getActivity() and break up if needed
            if(getActivity() == null) return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    };


}
