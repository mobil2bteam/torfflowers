package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjFlag;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjProductMin {

    public int      id;
    public int      rating;
    public int      count_comment;
    public String   name = "";
    public ObjImage image;
    public ArrayList<ObjFlag> flags;
    public ArrayList<ObjPrice> prices;
    public boolean flag_favorites;
    public boolean flag_bag;

    public ObjProductMin() {
        init();
    }
    public ObjProductMin(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjProductMin(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("age")) {
                JSONObject jsonMain = json.getJSONObject("age");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        flags = new ArrayList<>();
        prices = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("rating")) {
                rating = json.getInt("rating");
            }
            if (json.has("count_comment")) {
                count_comment = json.getInt("count_comment");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("image")) {
                image = new ObjImage(json.getJSONObject("image"));
            }
            /*if (json.has("images")) {
                for (int i = 0; i < json.getJSONArray("images").length(); i++) {
                    image = json.getJSONArray("images").getString(i);
                }
            }*/
            if (json.has("prices")) {
                for (int i = 0; i < json.getJSONArray("prices").length(); i++) {
                    ObjPrice it = new ObjPrice(json.getJSONArray("prices").getJSONObject(i));
                    prices.add(it);
                }
            }
            if (json.has("flags")) {
                for (int i = 0; i < json.getJSONArray("flags").length(); i++) {
                    ObjFlag it = new ObjFlag(json.getJSONArray("flags").getJSONObject(i));
                    flags.add(it);
                }
            }
        } catch (JSONException e) {
        }
    }

}
