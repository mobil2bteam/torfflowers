package com.mobil2b.renatganiev.torfflowers;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.WorkDayView.WordDayView;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjWorkingHours;

public class about_company_work extends AppCompatActivity {

    LinearLayout layout_main;

    ObjCompanyPoint objCompanyPoint;
    int point_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_company_work);

        layout_main = (LinearLayout) findViewById(R.id.layout_main);

        initToolbar(PublicData.SETTINGS.company_info.name);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("point_id")) {
                point_id = bundle.getInt("point_id");

                if (PublicData.SETTINGS != null) {
                    if (PublicData.SETTINGS.company_info != null) {
                        if (PublicData.SETTINGS.company_info.points != null) {
                            for (ObjCompanyPoint objPoint : PublicData.SETTINGS.company_info.points) {
                                if (objPoint.id == point_id) {
                                    objCompanyPoint = objPoint;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (objCompanyPoint == null) {
            if (PublicData.SETTINGS.company_info.working_hours != null) {
                for (ObjWorkingHours objWork : PublicData.SETTINGS.company_info.working_hours) {
                    WordDayView wordDayView = new WordDayView(about_company_work.this, objWork);
                    layout_main.addView(wordDayView);
                }
            }
        } else {
            if (objCompanyPoint.working_hours != null) {
                for (ObjWorkingHours objWork : objCompanyPoint.working_hours) {
                    WordDayView wordDayView = new WordDayView(about_company_work.this, objWork);
                    layout_main.addView(wordDayView);
                }
            }
        }
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar(String title) {
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.main_toolbar);
        toolbarTop.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        Drawable drawable = ContextCompat.getDrawable(about_company_work.this, R.drawable.ic_toolbar_icon_back_24dp);
        toolbarTop.setNavigationIcon(drawable);
        //setSupportActionBar(toolbarTop);
        setSupportActionBar(toolbarTop);

        //setHasOptionsMenu(true);

        toolbarTop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
