package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Slider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ObjSlider {
    public int id;
    public boolean status;
    public int speed;
    public boolean auto;
    public boolean manual;
    public List<ObjSliderItem> items = new ArrayList<>();

    public ObjSlider() {}
    public ObjSlider(JSONObject json) {
        loadJsonObject(json);
    }

    public void loadJsonObject(JSONObject json){
        try {
            if(json.has("id")){
                this.id = json.getInt("id");
            }
            if(json.has("status")){
                this.status = json.getBoolean("status");
            }
            if(json.has("speed")){
                this.speed = json.getInt("speed");
            }
            if(json.has("auto")){
                this.auto = json.getBoolean("auto");
            }
            if(json.has("manual")){
                this.manual = json.getBoolean("manual");
            }
            if(json.has("items")){
                for (int i =0;i<json.getJSONArray("items").length();i++){
                    ObjSliderItem sliderItem = new ObjSliderItem();
                    sliderItem.loadJsonObject(json.getJSONArray("items").getJSONObject(i));
                    items.add(sliderItem);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
