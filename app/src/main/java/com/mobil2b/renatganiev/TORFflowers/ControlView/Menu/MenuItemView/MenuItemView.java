package com.mobil2b.renatganiev.torfflowers.ControlView.Menu.MenuItemView;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Menu.ObjMenuItem;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.Menu.ClickListenerMenu;
import com.mobil2b.renatganiev.torfflowers.R;
import com.squareup.picasso.Picasso;


/**
 * Created by renatganiev on 10.10.15.
 */
public class MenuItemView extends LinearLayout implements View.OnClickListener {

    public String view_menu;

    Context             mContext;

    TextView txtName;
    TextView txtDescription;
    ImageView imgIcon;
    LinearLayout layout_back;

    ObjMenuItem objMenuItem;

    ClickListenerMenu clickListenerMenu;

    public MenuItemView(Context context, ObjMenuItem obj, String v, ClickListenerMenu clickListener) {
        super(context);

        mContext    = context;
        objMenuItem     = obj;
        view_menu = v;
        clickListenerMenu = clickListener;

        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        boolean text        = false;
        boolean image       = false;
        boolean icon        = false;
        boolean description = false;

        if (view_menu.equals(PublicData.VIEW_MENU_TEXT)) {

            System.out.println("VIEW_MENU_TEXT");

            inflater.inflate(R.layout.item_menu_icon_text_vertical, this);
            image = false;
            icon = false;
            text = true;
            description = true;
        } else {
            if (view_menu.equals(PublicData.VIEW_MENU_VERTICAL_ICON)) {

                System.out.println("VIEW_MENU_VERTICAL_ICON");

                inflater.inflate(R.layout.item_menu_icon_text_vertical, this);
                image = false;
                icon = true;
                text = true;
                description = true;
            } else {
                if (view_menu.equals(PublicData.VIEW_MENU_HORIZONTAL_ICON)) {

                    System.out.println("VIEW_MENU_HORIZONTAL_ICON");


                    inflater.inflate(R.layout.item_menu_icon_text_horizontal, this);
                    image = false;
                    icon = true;
                    text = true;
                    description = false;
                } else {
                    if (view_menu.equals(PublicData.VIEW_MENU_HORIZONTAL_IMAGE)) {

                        System.out.println("VIEW_MENU_HORIZONTAL_IMAGE");
                        inflater.inflate(R.layout.item_menu_image_text_horizontal, this);
                        image = true;
                        icon = false;
                        text = true;
                        description = false;

                    } else {

                        if (view_menu.equals(PublicData.VIEW_MENU_VERTICAL_IMAGE)) {

                            System.out.println("VIEW_MENU_VERTICAL_IMAGE");

                            inflater.inflate(R.layout.item_menu_image_text_vertical, this);
                            image = true;
                            icon = false;
                            text = true;
                            description = false;
                        } else {

                            if (view_menu.equals(PublicData.VIEW_MENU_CELL_ICON)) {

                                System.out.println("VIEW_MENU_CELL_ICON");

                                inflater.inflate(R.layout.item_menu_icon_text_horizontal, this);
                                image = false;
                                icon = true;
                                text = true;
                                description = false;
                            } else {


                                if (view_menu.equals(PublicData.VIEW_MENU_CELL_IMAGE_1)) {

                                    System.out.println("VIEW_MENU_CELL_IMAGE_1");

                                    inflater.inflate(R.layout.item_menu_image_text_horizontal, this);
                                    image = true;
                                    icon = false;
                                    text = true;
                                    description = false;
                                } else {
                                    if (view_menu.equals(PublicData.VIEW_MENU_CELL_IMAGE_2)) {

                                        System.out.println("VIEW_MENU_CELL_IMAGE_2");

                                        inflater.inflate(R.layout.item_menu_image_text_horizontal, this);
                                        image = true;
                                        icon = false;
                                        text = true;
                                        description = false;
                                    } else {
                                        inflater.inflate(R.layout.item_menu_icon_text_vertical, this);
                                        image       = false;
                                        icon        = false;
                                        text        = true;
                                        description = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        if (objMenuItem.flag_not_show_name) {
            text = false;
        }

        //newMenuButton.setBackground(getResources().getDrawable(R.drawable.button_menu_selector));
        txtName             = (TextView) findViewById(R.id.txtName);
        layout_back         = (LinearLayout) findViewById(R.id.layout_back);

        layout_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListenerMenu != null){
                    clickListenerMenu.onClick(view, objMenuItem);
                }
            }
        });

        if (text) {
            if (objMenuItem.name != null) {
                txtName.setText(objMenuItem.name);
                txtName.setVisibility(VISIBLE);

                if ((objMenuItem.text_color != null) && (!objMenuItem.text_color.equals(""))) {
                    txtName.setTextColor(Color.parseColor(objMenuItem.text_color));
                }
            } else {
                txtName.setVisibility(GONE);
            }
        } else {
            txtName.setVisibility(GONE);
        }

        if (description) {
            txtDescription      = (TextView)    findViewById(R.id.txtDescription);
            if ((objMenuItem.desctiption != null) && (!objMenuItem.desctiption.equals(""))) {
                txtDescription.setText(objMenuItem.desctiption);
                txtDescription.setVisibility(VISIBLE);
            } else {
                txtDescription.setVisibility(GONE);
            }
        }

        imgIcon             = (ImageView)   findViewById(R.id.imgIcon);
        if ((icon) || (image)) {

            if (icon) {
                if (objMenuItem.icon != null) {
                    if (objMenuItem.icon.url != null) {
                        if (!objMenuItem.icon.url.equals("")) {
                            Picasso.with(mContext).load(objMenuItem.icon.url).into(imgIcon);
                        }
                    }
                }
            } else {
                if (objMenuItem.image != null) {
                    if (objMenuItem.image.url != null) {
                        if (!objMenuItem.image.url.equals("")) {

                            if (view_menu.equals(PublicData.VIEW_MENU_VERTICAL_IMAGE)) {
                                DisplayMetrics metrics = Helps.getDisplayWH((Activity)mContext);
                                if (metrics != null) {
                                    if (metrics.widthPixels != 0) {
                                        float c = (float) objMenuItem.image.heigth / (float) objMenuItem.image.width;
                                        Picasso.with(mContext)
                                                .load(objMenuItem.image.url)
                                                .resize(metrics.widthPixels, (int)(((float)metrics.widthPixels) * c))
                                                .into(imgIcon);
                                    }
                                }
                            } else {
                                Picasso.with(mContext).load(objMenuItem.image.url).resize(400, 400).into(imgIcon);
                            }
                        }
                    }
                }
            }

            imgIcon.setVisibility(VISIBLE);

        } else {
            imgIcon.setVisibility(GONE);
        }
    }


    public ObjMenuItem getObject() {
        return objMenuItem;
    }

    @Override
    public void onClick(View v) {
        if (clickListenerMenu != null){
            clickListenerMenu.onClick(v, objMenuItem);
        }
    }

    public void setOnClickListener(ClickListenerMenu listener) {
        clickListenerMenu = listener;
    }

}