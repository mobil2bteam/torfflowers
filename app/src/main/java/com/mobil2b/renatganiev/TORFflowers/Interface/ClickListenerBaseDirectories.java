package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBaseDirectories;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerBaseDirectories {
    void onClick(View view, ObjBaseDirectories objBaseDirectories);
}
