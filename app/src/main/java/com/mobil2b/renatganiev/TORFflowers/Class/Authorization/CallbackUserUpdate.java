package com.mobil2b.renatganiev.torfflowers.Class.Authorization;


import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;

/**
 * Created by renatganiev on 20.07.14.
 */
public interface CallbackUserUpdate {
    // Старт регистрации, авторизации и т.д.
    public  void onStart();

    // Пользователь авторизовался
    public  void onSuccess(DataSession result);

    // Произошла ошибка
    public  void onError(String message, int error_code);
}
