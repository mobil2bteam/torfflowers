package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog.ObjCatalogItems;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerCatalog {
    void onClick(View view, ObjCatalogItems objCatalogItems);
}
