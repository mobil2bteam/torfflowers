package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjOptions {

    public ArrayList<ObjURL> urls;
    public ArrayList<ObjURL> first_questions;

    public long cache;

    public ObjOptions() {
        init();
    }
    public ObjOptions(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjOptions(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("options")) {

                if (json.has("cache")) {
                    cache = json.getLong("cache");
                } else {
                    cache = 0;
                }

                JSONObject jsonOptions = json.getJSONObject("options");
                loadJSONObject(jsonOptions);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        urls            = new ArrayList<>();
        first_questions = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
                JSONArray array;

                if (json.has("urls")) {
                    array = json.getJSONArray("urls");
                    loadUrls(array);
                }
                if (json.has("first_questions")) {
                    array = json.getJSONArray("first_questions");
                    loadFirstQuestions(array);
                }


        } catch (JSONException e) {
        }
    }

    private void loadUrls(JSONArray array) {
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject JSONItem = array.getJSONObject(i);

                ObjURL obj = new ObjURL(JSONItem);
                urls.add(obj);
            }
        } catch (JSONException e) {
        }
    }

    private void loadFirstQuestions(JSONArray array) {
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject JSONItem = array.getJSONObject(i);

                ObjURL obj = new ObjURL(JSONItem);
                first_questions.add(obj);
            }
        } catch (JSONException e) {
        }
    }

}
