package com.mobil2b.renatganiev.torfflowers.ControlView.Menu;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Menu.ObjMenu;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Menu.ObjMenuItem;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.Menu.MenuItemView.MenuItemView;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by renatganiev on 10.10.15.
 */
public class MenuView extends RelativeLayout {

    Context             mContext;

    LinearLayout layout_body;
    HorizontalScrollView horizontalScroll;

    ObjMenu objMenu;

    ClickListenerMenu clickListenerMenu;
    DisplayMetrics metrics;

    public MenuView(Activity context, ObjMenu obj, ClickListenerMenu listener) {
        super(context);

        mContext    = context;
        objMenu     = obj;
        metrics     = Helps.getDisplayWH(context);
        clickListenerMenu  = listener;
        initComponent();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_menu, this);

        horizontalScroll = findViewById(R.id.horizontalScroll);

        if ((objMenu.view.equals(PublicData.VIEW_MENU_TEXT))                || (objMenu.view.equals(PublicData.VIEW_MENU_VERTICAL_ICON))
            || (objMenu.view.equals(PublicData.VIEW_MENU_VERTICAL_IMAGE))   || (objMenu.view.equals(PublicData.VIEW_MENU_CELL_ICON))
            || (objMenu.view.equals(PublicData.VIEW_MENU_CELL_IMAGE_1))     || (objMenu.view.equals(PublicData.VIEW_MENU_CELL_IMAGE_2))) {

            horizontalScroll.setVisibility(GONE);
            layout_body = findViewById(R.id.layout_vertical);

        } else {

            horizontalScroll.setVisibility(VISIBLE);
            layout_body = findViewById(R.id.layout_horizontal);

        }


        //param.setMargins(Helps.dpToPx(5), Helps.dpToPx(5), Helps.dpToPx(5), Helps.dpToPx(5));


        LinearLayout linearLayout = null;


        int i = 0;
        int count = 0;
        for (ObjMenuItem objMenuItem : objMenu.items) {


            MenuItemView menuItemView = new MenuItemView(mContext, objMenuItem, objMenu.view, clickListenerMenu);


            if ((objMenu.view.equals(PublicData.VIEW_MENU_TEXT)) || (objMenu.view.equals(PublicData.VIEW_MENU_VERTICAL_ICON))
                    || (objMenu.view.equals(PublicData.VIEW_MENU_VERTICAL_IMAGE))) {
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layout_body.addView(menuItemView, param);
            }


            if ((objMenu.view.equals(PublicData.VIEW_MENU_CELL_ICON)) || (objMenu.view.equals(PublicData.VIEW_MENU_CELL_IMAGE_1))) {
                LinearLayout.LayoutParams param         = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
                LinearLayout.LayoutParams layout_param  = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

                i++;
                count++;
                if (i == 1) {
                    linearLayout = new LinearLayout(mContext);
                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                }

                //menuItemView.setWeightSum(1f);
                linearLayout.addView(menuItemView, param);

                if ((i%3 == 0)) {
                    if (linearLayout != null) {
                        layout_body.addView(linearLayout, layout_param);
                    }
                    i = 0;
                } else {
                    if (count == objMenu.items.size())  {
                        if (linearLayout != null) {
                            layout_body.addView(linearLayout, layout_param);
                        }
                    }
                }
            }

            if ((objMenu.view.equals(PublicData.VIEW_MENU_HORIZONTAL_ICON)) || (objMenu.view.equals(PublicData.VIEW_MENU_HORIZONTAL_IMAGE))) {
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                layout_body.addView(menuItemView, param);
            }

        }

        layout_body.setVisibility(VISIBLE);

    }


    public void setOnClickListener(ClickListenerMenu listener) {
        clickListenerMenu = listener;
    }

}