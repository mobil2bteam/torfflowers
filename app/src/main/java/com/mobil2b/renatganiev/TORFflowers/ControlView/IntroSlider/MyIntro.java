package com.mobil2b.renatganiev.torfflowers.ControlView.IntroSlider;

/**
 * Created by renatganiev on 15.07.17.
 */


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;

import com.github.paolorotolo.appintro.AppIntro;
import com.mobil2b.renatganiev.torfflowers.R;


/**
 * Created by HP on 10/23/2016.
 */
public class MyIntro extends AppIntro {
    // Please DO NOT override onCreate. Use init
    @Override
    public void init(Bundle savedInstanceState) {

        //adding the three slides for introduction app you can ad as many you needed
        addSlide(AppIntroSampleSlider.newInstance(R.layout.app_intro1));
        addSlide(AppIntroSampleSlider.newInstance(R.layout.app_intro2));
        addSlide(AppIntroSampleSlider.newInstance(R.layout.app_intro3));
        addSlide(AppIntroSampleSlider.newInstance(R.layout.app_intro4));

        // Show and Hide Skip and Done buttons
        showStatusBar(false);
        showSkipButton(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setColorDoneText(ContextCompat.getColor(this, R.color.colorPrimary));
            setIndicatorColor(ContextCompat.getColor(this, R.color.colorCircleActive), ContextCompat.getColor(this, R.color.colorCircleInActive));
            setColorSkipButton(ContextCompat.getColor(this, R.color.colorTextSecond));
            setSeparatorColor(ContextCompat.getColor(this, R.color.colorTextSecond));
            //setImageNextButton(ContextCompat.getDrawable(this, R.drawable.ic_chevron_right_black_24dp));
        }
        else {
            setColorDoneText(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null));
            setIndicatorColor(ResourcesCompat.getColor(getResources(), R.color.colorCircleActive, null), ResourcesCompat.getColor(getResources(), R.color.colorCircleInActive, null));
            setColorSkipButton(ResourcesCompat.getColor(getResources(), R.color.colorTextSecond, null));
            setSeparatorColor(ResourcesCompat.getColor(getResources(), R.color.colorTextSecond, null));
            //setImageNextButton(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_chevron_right_black_24dp, null));
        }

        setDoneText(getString(R.string.app_intro_done));

        // Turn vibration on and set intensity
        // You will need to add VIBRATE permission in Manifest file
        setVibrate(true);
        setVibrateIntensity(30);

        //Add animation to the intro slider
        setDepthAnimation();
    }

    @Override
    public void onSkipPressed() {
        // Do something here when users click or tap on Skip button.
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onNextPressed() {
        // Do something here when users click or tap on Next button.
    }

    @Override
    public void onDonePressed() {
        // Do something here when users click or tap tap on Done button.
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onSlideChanged() {
        // Do something here when slide is changed
    }
}
