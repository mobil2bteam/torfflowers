package com.mobil2b.renatganiev.torfflowers.Interface;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjSort;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerSort {
    void onClick(View view, ObjSort objSort);
}
