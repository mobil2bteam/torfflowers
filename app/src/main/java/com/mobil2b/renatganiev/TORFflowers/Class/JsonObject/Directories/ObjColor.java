package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjColor extends ObjBaseDirectories {

    //public int      id;
    //public String   name = "";
    public String   value = "";
    public boolean  colorful = false;

    public ObjColor() {
        init();
    }
    public ObjColor(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjColor(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("color")) {
                JSONObject jsonMain = json.getJSONObject("color");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                super.id = json.getInt("id");
            }
            if (json.has("name")) {
                super.name = json.getString("name");
            }
            if (json.has("value")) {
                value = json.getString("value");
            }
            if (json.has("colorful")) {
                colorful = json.getBoolean("colorful");
            }
        } catch (JSONException e) {
        }
    }

}
