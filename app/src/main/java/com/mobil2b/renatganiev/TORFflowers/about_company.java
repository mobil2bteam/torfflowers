package com.mobil2b.renatganiev.torfflowers;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjSocialLinkItem;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.ControlView.Slider.SliderView;
import com.mobil2b.renatganiev.torfflowers.ControlView.SocialLinkView.ClickListenerSocialLink;
import com.mobil2b.renatganiev.torfflowers.ControlView.SocialLinkView.SocialLinkView;

import java.util.ArrayList;

public class about_company extends AppCompatActivity {

    ImageView imgClubLogo;
    TextView txtClubName, txtClubAddress, txtDescriptionShort,
            txtDescriptionCaption, txtWorksCaption;
    Button btnWeb, btnPhone;

    NestedScrollView scrollView;
    AppBarLayout mainAppbar;
    CoordinatorLayout coordinatorLayout;

    LinearLayout layout_slider, layout_social_link, layout_description,
            layout_works, layout_point;

    int PERMISSION_REQUEST_CODE = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_company);

        imgClubLogo             = (ImageView)   findViewById(R.id.imgClubLogo);

        txtClubName             = (TextView)    findViewById(R.id.txtClubName);
        txtClubAddress          = (TextView)    findViewById(R.id.txtClubAddress);
        txtDescriptionCaption   = (TextView)    findViewById(R.id.txtDescriptionCaption);
        txtWorksCaption         = (TextView)    findViewById(R.id.txtWorksCaption);

        scrollView              = (NestedScrollView) findViewById(R.id.scrollView);
        mainAppbar              = (AppBarLayout) findViewById(R.id.mainAppbar);
        coordinatorLayout       = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        layout_slider           = (LinearLayout) findViewById(R.id.layout_slider);
        layout_social_link      = (LinearLayout) findViewById(R.id.layout_social_link);
        layout_description      = (LinearLayout) findViewById(R.id.layout_description);
        layout_works            = (LinearLayout) findViewById(R.id.layout_works);
        layout_point            = (LinearLayout) findViewById(R.id.layout_point);

        btnWeb                  = (Button) findViewById(R.id.btnWeb);
        btnPhone                = (Button) findViewById(R.id.btnPhone);


        initToolbar(getString(R.string.menu_item_about));
        updateView();
    }

    /**
     * TODO: Инициализация toolbar.
     * */
    private void initToolbar(String title) {
        Toolbar toolbarTop = (Toolbar) findViewById(R.id.main_toolbar);
        //TextView mTitle = (TextView) toolbarTop.findViewById(R.id.toolbar_title);
        //mTitle.setText(title);

        toolbarTop.setTitle("");

        toolbarTop.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });

        Drawable drawable = ContextCompat.getDrawable(about_company.this, R.drawable.ic_toolbar_icon_back_black_24dp);
        toolbarTop.setNavigationIcon(drawable);
        //setSupportActionBar(toolbarTop);
        setSupportActionBar(toolbarTop);

        //setHasOptionsMenu(true);

        toolbarTop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void updateView() {
        if ((PublicData.SETTINGS != null) && (PublicData.SETTINGS.company_info != null)) {

            if (PublicData.SETTINGS.company_info.slider != null) {
                SliderView sliderView      = new SliderView(about_company.this, PublicData.SETTINGS.company_info.slider);
                sliderView.setCircleFillColor(ContextCompat.getColor(about_company.this, R.color.colorAccent), Color.WHITE);
                sliderView.setCircleRadius(3);
                sliderView.setCircleStrokeColor(Color.WHITE);
                sliderView.setCircleStrokeWidth(1);
                sliderView.setPagerMargin(0, 0, 0, 0);

                layout_slider.removeAllViews();
                layout_slider.addView(sliderView);
                layout_slider.setVisibility(View.VISIBLE);
            }



            txtClubName.setText(PublicData.SETTINGS.company_info.name);

            txtClubAddress.setText("");
            btnPhone.setVisibility(View.GONE);
            btnWeb.setVisibility(View.GONE);
            if (PublicData.SETTINGS.company_info.contacts != null) {
                if (PublicData.SETTINGS.company_info.contacts.address != null) {
                    txtClubAddress.setText(PublicData.SETTINGS.company_info.contacts.address.city.concat(" ").concat(PublicData.SETTINGS.company_info.contacts.address.street));
                }

                if (PublicData.SETTINGS.company_info.contacts.phone != null) {
                    btnPhone.setVisibility(View.VISIBLE);
                    btnPhone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callPhone(PublicData.SETTINGS.company_info.contacts.phone);
                        }
                    });
                }
            }

            if (PublicData.SETTINGS.company_info.social_link != null) {
                if (Helps.isNotEmpty(PublicData.SETTINGS.company_info.social_link.web)) {
                    btnWeb.setVisibility(View.VISIBLE);
                    btnWeb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PublicData.SETTINGS.company_info.social_link.web));
                            startActivity(browserIntent);
                        }
                    });
                }
            }

            if (Helps.isNotEmpty(PublicData.SETTINGS.company_info.description_long)) {
                layout_description.setVisibility(View.VISIBLE);

                layout_description.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(about_company.this, about_company_description.class);
                        intent.putExtra("description", PublicData.SETTINGS.company_info.description_long);
                        intent.putExtra("name", PublicData.SETTINGS.company_info.name);
                        startActivity(intent);
                    }
                });

            } else {
                layout_description.setVisibility(View.GONE);
            }

            if (PublicData.SETTINGS.company_info.working_hours != null) {
                layout_works.setVisibility(View.VISIBLE);

                layout_works.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(about_company.this, about_company_work.class);
                        startActivity(intent);
                    }
                });

            } else {
                layout_works.setVisibility(View.GONE);
            }


            ArrayList<Integer> city_id = new ArrayList<>();
            if (PublicData.SETTINGS != null) {
                if (PublicData.SETTINGS.company_info != null) {
                    if (PublicData.SETTINGS.company_info.points != null) {
                        for (ObjCompanyPoint objCompanyPoint : PublicData.SETTINGS.company_info.points) {
                            if (objCompanyPoint.contacts != null) {
                                if (objCompanyPoint.contacts.address != null) {
                                    if (((objCompanyPoint.check_point_sale) || (objCompanyPoint.check_point_office))) {
                                                if (!city_id.contains(objCompanyPoint.contacts.address.city_id)) {
                                                    city_id.add(objCompanyPoint.contacts.address.city_id);
                                                }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (city_id.size() > 0) {
                layout_point.setVisibility(View.VISIBLE);

                layout_point.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(about_company.this, about_company_point.class);
                        startActivity(intent);
                    }
                });

            } else {
                layout_point.setVisibility(View.GONE);
            }


            if (PublicData.HOME.flag_show_social_link) {

                int count_social_link = 0;
                if (PublicData.SETTINGS != null) {
                    if (PublicData.SETTINGS.company_info != null) {
                        if (PublicData.SETTINGS.company_info.social_link != null) {

                            for (ObjSocialLinkItem objSocialLinkItem : PublicData.SETTINGS.company_info.social_link.socialLinkItems) {
                                if (Helps.isNotEmpty(objSocialLinkItem.url)) {
                                    count_social_link++;
                                }
                            }

                            int count_line = 3;
                            if (count_social_link == 2) count_line = 2;
                            if (count_social_link == 3) count_line = 3;
                            if (count_social_link == 4) count_line = 2;
                            if (count_social_link == 5) count_line = 3;
                            if (count_social_link == 6) count_line = 3;

                            SocialLinkView socialLinkView = new SocialLinkView(about_company.this, PublicData.SETTINGS.company_info.social_link, count_line,
                                    new ClickListenerSocialLink() {
                                        @Override
                                        public void onClick(View view, ObjSocialLinkItem objSocialLink) {
                                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(objSocialLink.url));
                                            startActivity(browserIntent);
                                        }
                                    });

                            layout_social_link.removeAllViews();
                            layout_social_link.addView(socialLinkView);
                        }
                    }
                }

                if (count_social_link > 0) {
                    layout_social_link.setVisibility(View.VISIBLE);
                }
            }


                mainAppbar.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.VISIBLE);

        }
    }


    void callPhone(String callPhoneStr) {
        if (ActivityCompat.checkSelfPermission(about_company.this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent myIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + callPhoneStr));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(myIntent);
            return;
        } else {
            PublicData.callPhoneStr = callPhoneStr;
            ActivityCompat.requestPermissions(about_company.this, new String[] { android.Manifest.permission.CALL_PHONE }, PublicData.CALL_PHONE_NUMBER);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callPhone(PublicData.callPhoneStr);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
