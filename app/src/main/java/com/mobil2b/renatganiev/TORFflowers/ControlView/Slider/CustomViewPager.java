package com.mobil2b.renatganiev.torfflowers.ControlView.Slider;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Slider.ObjSliderItem;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerLink;

import java.util.List;


public class CustomViewPager extends ViewPager {

    private float               density             =   getResources().getDisplayMetrics().density;
    private int                 indicatorIcon       =   0;
    private boolean             isPagingEnabled     =   false;
    private CustomPagerAdapter  pagerAdapter;
    private int                 itemsOnTheScreen    =   1;
    private ClickListenerLink   onItemClickListener;

    public CustomViewPager(Context context, List<ObjSliderItem> itemList, int itemsOnTheScreen) {
        super(context);
        this.itemsOnTheScreen = itemsOnTheScreen;
        pagerAdapter = new CustomPagerAdapter(context,itemList, this.itemsOnTheScreen);
        pagerAdapter.setOnItemClickLictener(onItemClickListener);
        setAdapter(pagerAdapter);
        setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
    }
    @Override
    public void addOnPageChangeListener(OnPageChangeListener listener) {
        super.addOnPageChangeListener(listener);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event);
    }

    public void setSize(int height, int width){
        getLayoutParams().height = toDp(height);
        getLayoutParams().width = toDp(width);
    }

    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = 0;
        for(int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight()/itemsOnTheScreen;
            if(h > height) height = h;
        }
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    public void setIcon(int icon){
        indicatorIcon = icon;
        pagerAdapter.setIcon(indicatorIcon);
    }

    public void setOnItemClickListener(ClickListenerLink onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        if (pagerAdapter != null) {
            pagerAdapter.setOnItemClickLictener(onItemClickListener);
        }
    }

    private int toDp(int i){
        return  (int) (i*density + 0.5f);
    }
}
