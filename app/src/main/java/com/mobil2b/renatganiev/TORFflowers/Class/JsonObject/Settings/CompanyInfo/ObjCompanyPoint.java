package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo;

import android.content.Context;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Slider.ObjSlider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjCompanyPoint {
    public int id;
    public String name, description_short, description_long;
    public ObjSlider slider;
    public ObjSocialLink social_link;
    public ObjContacts contacts;
    public ArrayList<ObjWorkingHours> working_hours;
    public boolean check_point_sale, check_point_office, check_point_pickup, check_point_cache, check_point_dressing;
    public double summ_delivery, summ_free_delivery;
    public Context mContext;


    public ObjCompanyPoint() {
        init();
    }
    public ObjCompanyPoint(JSONObject json, Context context) {
        mContext = context;
        init();
        loadJSONObject(json);
    }
    public ObjCompanyPoint(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            loadJSONObject(json);
        } catch (JSONException e) {
        }
    }

    private void init() {
        working_hours = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                this.id = json.getInt("id");
            }

            if (json.has("name")) {
                this.name = json.getString("name");
            }
            if (json.has("description_short")) {
                this.description_short = json.getString("description_short");
            }
            if (json.has("description_long")) {
                this.description_long = json.getString("description_long");
            }
            if (json.has("slider")) {
                slider = new ObjSlider(json.getJSONObject("slider"));
            }
            if (json.has("social_link")) {
                JSONObject social_linkJson = json.getJSONObject("social_link");
                social_link = new ObjSocialLink(social_linkJson, mContext);
            }
            if (json.has("contacts")) {
                JSONObject contactsJson = json.getJSONObject("contacts");
                contacts = new ObjContacts(contactsJson);
            }
            if(json.has("working_house")){
                for (int i =0;i<json.getJSONArray("working_house").length();i++){
                    ObjWorkingHours sliderItem = new ObjWorkingHours(json.getJSONArray("working_house").getJSONObject(i));
                    working_hours.add(sliderItem);
                }
            }
            if (json.has("check_point_sale")) {
                check_point_sale = json.getBoolean("check_point_sale");
            }
            if (json.has("check_point_office")) {
                check_point_office = json.getBoolean("check_point_office");
            }
            if (json.has("check_point_pickup")) {
                check_point_pickup = json.getBoolean("check_point_pickup");
            }
            if (json.has("check_point_cache")) {
                check_point_cache = json.getBoolean("check_point_cache");
            }
            if (json.has("check_point_dressing")) {
                check_point_dressing = json.getBoolean("check_point_dressing");
            }
            if (json.has("summ_delivery")) {
                summ_delivery = json.getDouble("summ_delivery");
            }
            if (json.has("summ_free_delivery")) {
                summ_free_delivery = json.getDouble("summ_free_delivery");
            }

        } catch (JSONException e) {
        }
    }
}
