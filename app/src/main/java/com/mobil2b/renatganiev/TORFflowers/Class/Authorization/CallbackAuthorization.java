package com.mobil2b.renatganiev.torfflowers.Class.Authorization;


import com.mobil2b.renatganiev.torfflowers.Class.Authorization.session.DataSession;

/**
 * Created by renatganiev on 20.07.14.
 */
public interface CallbackAuthorization {
    // Старт регистрации, авторизации и т.д.
    public  void onStart(String text);

    // Вернули структуру для авторизации на сервере сессия жива
    public  void onReturnSession(DataSession result);

    // Сессия пустая, сессии нет на мобильном телефона
    public  void onDontSession(String text, int error_code);

    // Пользователь авторизовался
    public  void onSuccess(DataSession result, int type);

    // Произошла ошибка
    public  void onError(DataSession result, String message, int error_code, int type);

    // Пользователь вышел
    public  void onExit();
}
