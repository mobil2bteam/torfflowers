package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjSize extends ObjBaseDirectories {

    //public int      id;
    //public String   name = "";

    public ObjSize() {
        init();
    }
    public ObjSize(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjSize(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("size")) {
                JSONObject jsonMain = json.getJSONObject("size");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                super.id = json.getInt("id");
            }
            if (json.has("name")) {
                super.name = json.getString("name");
            }
        } catch (JSONException e) {
        }
    }

}
