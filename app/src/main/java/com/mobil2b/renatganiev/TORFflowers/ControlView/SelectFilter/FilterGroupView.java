package com.mobil2b.renatganiev.torfflowers.ControlView.SelectFilter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjBaseDirectories;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;


/**
 * Created by renatganiev on 10.10.15.
 */
public class FilterGroupView extends RelativeLayout {

    Context             mContext;

    TextView txtName, txtSelect, txtCount;
    CardView cardView;

    ArrayList<? extends ObjBaseDirectories> objBaseDirectories;
    ArrayList<Integer> selectId;
    ClickListenerFilterGroup clickListenerMenu;
    int FILTER_REQUEST_CODE;
    String filtername = "";

    public FilterGroupView(Activity context, String name, int REQUEST_CODE, ClickListenerFilterGroup listener) {
        super(context);

        mContext                = context;
        filtername              = name;
        clickListenerMenu       = listener;
        FILTER_REQUEST_CODE     = REQUEST_CODE;
    }

    public void setData(ArrayList<? extends ObjBaseDirectories> obj, ArrayList<Integer> select) {
        objBaseDirectories      = obj;
        selectId                = select;
        initComponent();
        updateView();
    }

    private void initComponent() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_filter_base_directories, this);

        txtName     = (TextView) findViewById(R.id.txtName);
        txtCount    = (TextView) findViewById(R.id.txtCount);
        txtSelect   = (TextView) findViewById(R.id.txtSelect);
        cardView    = (CardView) findViewById(R.id.cardView);



        cardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListenerMenu != null) {
                    clickListenerMenu.onClick(view, objBaseDirectories, FILTER_REQUEST_CODE, filtername);
                }
            }
        });

    }

    public void setSelectArrayId(ArrayList<Integer> select) {
        selectId                = select;
        updateView();
    }

    public ArrayList<Integer> getSelectId() {
        return selectId;
    }

    void updateView() {
        String filter_str = "Все значения";
        if (objBaseDirectories != null) {
            if ((selectId != null) && (selectId.size() > 0)) {
                filter_str = "";
                for (ObjBaseDirectories objItem : objBaseDirectories) {
                    objItem.checkbox = false;

                    if (selectId.contains(objItem.id)) {
                        objItem.checkbox = true;
                        if (filter_str.equals("")) {
                            filter_str = filter_str.concat(objItem.name);
                        } else {
                            filter_str = filter_str.concat(", ".concat(objItem.name));
                        }
                    }
                }
            } else {
                for (ObjBaseDirectories objItem : objBaseDirectories) {
                    objItem.checkbox = false;
                }
            }

            txtCount.setText("(".concat(String.valueOf(objBaseDirectories.size())).concat(")"));
        }

        if ((selectId != null) && (selectId.size() > 0)) {
            txtName.setTypeface(txtName.getTypeface(), Typeface.BOLD);
        } else {
            txtName.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
        }
        txtName.setText(filtername);
        txtSelect.setText(filter_str);
    }


    public void setOnClickListener(ClickListenerFilterGroup listener) {
        clickListenerMenu = listener;
    }

}