package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.R;

/**
 * Created by renatganiev on 03.02.18.
 */

public class AdapterEmptyRecyclerView extends RecyclerView.Adapter<AdapterEmptyRecyclerView.ViewHolder> {

    private String  mMessage;
    private int     drawableId;

    public AdapterEmptyRecyclerView(){}

    public AdapterEmptyRecyclerView(String message, int drawableResourceId){
        mMessage = message;
        drawableId = drawableResourceId;
    }

    @Override
    public AdapterEmptyRecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        if(mMessage != null){
            viewHolder.mMessageView.setText(mMessage);
        }
        if (drawableId != 0) {
            viewHolder.imageView.setImageResource(drawableId);
            viewHolder.imageView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imageView.setVisibility(View.GONE);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterEmptyRecyclerView.ViewHolder holder, int position) {}

    @Override
    public int getItemCount() {
        return 1;//must return one otherwise none item is shown
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mMessageView;
        public final ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mMessageView = (TextView) view.findViewById(R.id.empty_item_message);
            imageView   = (ImageView) view.findViewById(R.id.imageView);
        }
    }
}
