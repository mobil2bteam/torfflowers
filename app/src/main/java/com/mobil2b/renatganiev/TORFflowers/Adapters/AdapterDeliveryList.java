package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Payment.ObjPayment;
import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Delivery.ObjDelivery;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerDelivery;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterDeliveryList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //несколко вьютайпов, для всех

    private Context mContext;
    private ArrayList<ObjDelivery> mList;

    ClickListenerDelivery clickListenerDelivery;

    public AdapterDeliveryList(Context context, ArrayList<ObjDelivery> list) {
        this.mContext = context;
        this.mList = list;
    }

    public void setOnClickListenerDelivery(ClickListenerDelivery onClickListener){
        this.clickListenerDelivery = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_delivery, parent, false);

        return new ViewHolderProduct(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ViewHolderProduct viewHolder0 = (ViewHolderProduct) holder;
        final ObjDelivery obj = getItem(position);

        // НАИМЕНОВАНИЕ
        viewHolder0.txtName.setText(obj.name);

        if (obj.price != 0) {
            viewHolder0.txtPrice.setText(String.valueOf(obj.price).concat(" р."));
        } else {
            if (Helps.isNotEmpty(obj.price_str)) {
                viewHolder0.txtPrice.setText(obj.price_str);
            } else {
                viewHolder0.txtPrice.setText("Бесплатно");
            }
        }

        viewHolder0.txtDescription.setVisibility(View.GONE);
        if (obj.add_text != null) {
            if (!obj.add_text.equals("")) {
                viewHolder0.txtDescription.setText(obj.add_text);
                viewHolder0.txtDescription.setVisibility(View.VISIBLE);
            }
        }

        viewHolder0.txtPayment.setVisibility(View.GONE);
        if (obj.payment_list != null) {
            if (obj.payment_list.size() > 0) {
                String txt_payment = "";
                for (ObjPayment payment : obj.payment_list) {
                    if (txt_payment.equals("")) {
                        txt_payment = txt_payment.concat(payment.name);
                    } else {
                        txt_payment = txt_payment.concat(", ".concat(payment.name));
                    }
                }
                if (!txt_payment.equals("")) {
                    viewHolder0.txtPayment.setText(txt_payment);
                    viewHolder0.txtPayment.setVisibility(View.VISIBLE);
                }
            }
        }

        viewHolder0.setClickListener(clickListenerDelivery);
    }


    private ObjDelivery getItem(int position){
        return mList.get(position);
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderProduct extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtName, txtDescription, txtPrice, txtPayment;

        ClickListenerDelivery clickListenerDelivery;

        public ViewHolderProduct(View itemView) {
            super(itemView);

            txtName           = (TextView) itemView.findViewById(R.id.txtName);
            txtDescription    = (TextView) itemView.findViewById(R.id.txtDescription);
            txtPrice          = (TextView) itemView.findViewById(R.id.txtPrice);
            txtPayment        = (TextView) itemView.findViewById(R.id.txtPayment);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListenerDelivery != null) {
                clickListenerDelivery.onClick(v, getItem(getAdapterPosition()));
            }
        }
        public void setClickListener(ClickListenerDelivery clickListener){
            this.clickListenerDelivery = clickListener;
        }
    }
}