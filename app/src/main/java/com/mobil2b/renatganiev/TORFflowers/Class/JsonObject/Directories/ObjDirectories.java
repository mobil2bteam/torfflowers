package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Catalog.ObjCatalog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjDirectories {

    public  ArrayList<ObjAge>                     age;
    public  ArrayList<ObjBrand>                   brand;
    public  ArrayList<ObjCity>                    city;
    public  ArrayList<ObjColor>                   color;
    public  ArrayList<ObjCountry>                 country;
    public  ArrayList<ObjCountryManufacture>      country_manufacture;
    public  ArrayList<ObjFlag>                    flags;
    public  ArrayList<ObjMaterial>                material;
    public  ArrayList<ObjPropertiesType>          properties_type;
    public  ArrayList<ObjProperties>              properties;
    public  ArrayList<ObjSeason>                  season;
    public  ArrayList<ObjSex>                     sex;
    public  ArrayList<ObjSize>                    size;
    public  ArrayList<ObjSort>                    sort;
    public ObjCatalog catalog;

    public ObjDirectories() {
        init();
    }
    public ObjDirectories(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjDirectories(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("directories")) {
                JSONObject jsonMain = json.getJSONObject("directories");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
        age                     = new ArrayList<>();
        brand                   = new ArrayList<>();
        city                    = new ArrayList<>();
        color                   = new ArrayList<>();
        country                 = new ArrayList<>();
        country_manufacture     = new ArrayList<>();
        flags                   = new ArrayList<>();
        material                = new ArrayList<>();
        properties              = new ArrayList<>();
        properties_type         = new ArrayList<>();
        season                  = new ArrayList<>();
        sex                     = new ArrayList<>();
        size                    = new ArrayList<>();
        sort                    = new ArrayList<>();
    }

    public void loadJSONObject(JSONObject json) {
        try {

            if (json.has("country")) {
                for (int i = 0; i < json.getJSONArray("country").length(); i++){
                    ObjCountry it = new ObjCountry(json.getJSONArray("country").getJSONObject(i));
                    country.add(it);
                }
            }
            if (json.has("city")) {
                for (int i = 0; i < json.getJSONArray("city").length(); i++) {
                    ObjCity it = new ObjCity(json.getJSONArray("city").getJSONObject(i));

                    if (it.country_id != 0) {
                        for (ObjCountry objCountry : country) {
                            if (it.country_id == objCountry.id) {
                                it.country = objCountry;
                                break;
                            }
                        }
                    }

                    city.add(it);
                }
            }
            if (json.has("brand")) {
                for (int i = 0; i < json.getJSONArray("brand").length(); i++) {
                    ObjBrand it = new ObjBrand(json.getJSONArray("brand").getJSONObject(i));
                    brand.add(it);
                }
            }
            if (json.has("size")) {
                for (int i = 0; i < json.getJSONArray("size").length(); i++){
                    ObjSize it = new ObjSize(json.getJSONArray("size").getJSONObject(i));
                    size.add(it);
                }
            }
            if (json.has("color")) {
                for (int i = 0; i < json.getJSONArray("color").length(); i++){
                    ObjColor it = new ObjColor(json.getJSONArray("color").getJSONObject(i));
                    color.add(it);
                }
            }
            if (json.has("material")) {
                for (int i = 0; i < json.getJSONArray("material").length(); i++){
                    ObjMaterial it = new ObjMaterial(json.getJSONArray("material").getJSONObject(i));
                    material.add(it);
                }
            }
            if (json.has("season")) {
                for (int i = 0; i < json.getJSONArray("season").length(); i++){
                    ObjSeason it = new ObjSeason(json.getJSONArray("season").getJSONObject(i));
                    season.add(it);
                }
            }
            if (json.has("age")) {
                for (int i = 0; i < json.getJSONArray("age").length(); i++){
                    ObjAge it = new ObjAge(json.getJSONArray("age").getJSONObject(i));
                    age.add(it);
                }
            }
            if (json.has("flags")) {
                for (int i = 0; i < json.getJSONArray("flags").length(); i++){
                    ObjFlag it = new ObjFlag(json.getJSONArray("flags").getJSONObject(i));
                    flags.add(it);
                }
            }
            if (json.has("sex")) {
                for (int i = 0; i < json.getJSONArray("sex").length(); i++){
                    ObjSex it = new ObjSex(json.getJSONArray("sex").getJSONObject(i));
                    sex.add(it);
                }
            }
            if (json.has("country_manufacture")) {
                for (int i = 0; i < json.getJSONArray("country_manufacture").length(); i++){
                    ObjCountryManufacture it = new ObjCountryManufacture(json.getJSONArray("country_manufacture").getJSONObject(i));
                    country_manufacture.add(it);
                }
            }
            if (json.has("properties_type")) {
                for (int i = 0; i < json.getJSONArray("properties_type").length(); i++) {
                    ObjPropertiesType it = new ObjPropertiesType(json.getJSONArray("properties_type").getJSONObject(i));
                    properties_type.add(it);
                }
            }
            if (json.has("properties")) {
                for (int i = 0; i < json.getJSONArray("properties").length(); i++){
                    ObjProperties it = new ObjProperties(json.getJSONArray("properties").getJSONObject(i));
                    properties.add(it);
                }
            }
            if (json.has("sort")) {
                for (int i = 0; i < json.getJSONArray("sort").length(); i++){
                    ObjSort it = new ObjSort(json.getJSONArray("sort").getJSONObject(i));
                    sort.add(it);
                }
            }
            if (json.has("catalog")) {
                catalog = new ObjCatalog(json.getJSONObject("catalog"));
            }
        } catch (JSONException e) {
        }
    }

}
