package com.mobil2b.renatganiev.torfflowers.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mobil2b.renatganiev.torfflowers.Adapters.AdapterCity;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Settings.CompanyInfo.ObjCompanyPoint;
import com.mobil2b.renatganiev.torfflowers.Class.PublicData.PublicData;
import com.mobil2b.renatganiev.torfflowers.Interface.CallbackAction;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjCity;
import com.mobil2b.renatganiev.torfflowers.R;

import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by renatganiev on 03.04.16.
 *
 * Фрагмент с каталогом
 *
 */
public class fragment_app_city_list extends Fragment {

    public static final String TAG = "fragment_app_city_list";
    public static final int LAYOUT = R.layout.fragment_app_city_list;
    View rootView;

    public CallbackAction callbackActionSuper;

    AdapterCity adapter;
    public ArrayList<ObjCity> myDataset;
    public ArrayList<ObjCity> myDatasetList;

    ListView list_city;
    EditText txt_city_name;
    ImageButton btnClear;

    boolean select_city_company_point = false;

    public static fragment_app_city_list getInstance(Bundle args) {
        fragment_app_city_list fragment = new fragment_app_city_list();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();

        if (arg != null) {
            // Если передали каталог в котором надо искать.
            if (arg.containsKey("select_city_company_point")) {
                select_city_company_point = arg.getBoolean("select_city_company_point");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(LAYOUT, container, false);

        list_city       = (ListView)    rootView.findViewById(R.id.list_city);
        txt_city_name   = (EditText)    rootView.findViewById(R.id.txt_city_name);
        btnClear        = (ImageButton) rootView.findViewById(R.id.btnClear);

        txt_city_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                myDataset.clear();
                myDataset.addAll(myDatasetList);

                if (s.toString().trim().length()==0) {

                } else {
                    String charText = s.toString().toLowerCase(Locale.getDefault());
                    for(ObjCity city : myDatasetList) {
                        if (!city.name.toLowerCase(Locale.getDefault()).contains(charText)) {
                            myDataset.remove(city);
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_city_name.setText("");
            }
        });

        initCityList();

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    void initCityList() {

        if (myDataset != null) myDataset.clear();
        else myDataset = new ArrayList();

        if (myDatasetList != null) myDatasetList.clear();
        else myDatasetList = new ArrayList();


        if (!select_city_company_point) {
            if (PublicData.DIRECTORIES != null) {
                if (PublicData.DIRECTORIES.city != null) {

                    for (ObjCity l : PublicData.DIRECTORIES.city) {
                        myDataset.add(l);
                        myDatasetList.add(l);
                    }

                }
            }
        } else {

            ArrayList<Integer> city_id = new ArrayList<>();
            if (PublicData.SETTINGS != null) {
                if (PublicData.SETTINGS.company_info != null) {
                    if (PublicData.SETTINGS.company_info.points != null) {
                        for (ObjCompanyPoint objCompanyPoint : PublicData.SETTINGS.company_info.points) {
                            if (objCompanyPoint.contacts != null) {
                                if (objCompanyPoint.contacts.address != null) {
                                    if (((objCompanyPoint.check_point_office) || (objCompanyPoint.check_point_sale))) {
                                            if (!city_id.contains(objCompanyPoint.contacts.address.city_id)) {
                                                city_id.add(objCompanyPoint.contacts.address.city_id);
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (PublicData.DIRECTORIES != null) {
                if (PublicData.DIRECTORIES.city != null) {
                    for (Integer c_id : city_id) {
                        for (ObjCity l : PublicData.DIRECTORIES.city) {
                            if (l.id == c_id) {
                                myDataset.add(l);
                                myDatasetList.add(l);
                            }
                        }
                    }
                }
            }

        }

        adapter = new AdapterCity(getActivity(), myDataset);
        list_city.setAdapter(adapter);
        list_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {

                if (callbackActionSuper != null) {
                    ObjCity selectedmovie = myDataset.get(position);
                    //PublicData.cityItems = selectedmovie;
                    Bundle bundle = new Bundle();
                    bundle.putInt("city_id", selectedmovie.id);
                    callbackActionSuper.onAction(TAG, PublicData.SELECT_CITY, bundle);
                }

            }
        });

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }


}
