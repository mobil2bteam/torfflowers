package com.mobil2b.renatganiev.torfflowers.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobil2b.renatganiev.torfflowers.Class.Helps;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Directories.ObjFlag;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjPrice;
import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Products.ObjProductMin;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache.DBFavorites;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerLink;
import com.mobil2b.renatganiev.torfflowers.Interface.ClickListenerProductMin;
import com.mobil2b.renatganiev.torfflowers.R;
import com.mobil2b.renatganiev.torfflowers.product_item;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ruslan on 07.04.2017.
 */
public class AdapterProductList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context mContext;
    private List<ObjProductMin> mList;
    public float count_item_displey = 2f;
    private float itemWidth;
    private boolean mCountCart;

    ClickListenerLink clickListenerLink;
    ClickListenerProductMin clickListenerProductMin;

    public AdapterProductList(Context context, List<ObjProductMin> list, boolean countCart) {
        this.mContext = context;
        this.mList = list;
        mCountCart = countCart;
    }


    public void setOnClickListener(ClickListenerLink onClickListener){
        this.clickListenerLink = onClickListener;
    }

    public void setOnClickListenerProduct(ClickListenerProductMin onClickListener){
        this.clickListenerProductMin = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // return view holder for your normal list item
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_product, parent, false);

        if (mCountCart) {
            itemWidth = parent.getMeasuredWidth() / count_item_displey;
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.width = (int)itemWidth;
            view.setLayoutParams(layoutParams);
        }

        return new ViewHolderProduct(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderProduct viewHolder0 = (ViewHolderProduct) holder;
        final ObjProductMin obj = getItem(position);

        // НАИМЕНОВАНИЕ
        viewHolder0.text.setText(obj.name);


        // ФОТОГРАФИЯ
        if (obj.image != null) {
            if (obj.image.url != null) {
                if (!obj.image.url.equals("")) {
                    if ((int) itemWidth != 0) {
                        float c = (float) obj.image.heigth / (float) obj.image.width;
                        Picasso.with(mContext)
                                .load(obj.image.url)
                                .resize((int) itemWidth, (int) (itemWidth * c))
                                .into(viewHolder0.image);
                    } else {
                        Picasso.with(mContext).load(obj.image.url).into(viewHolder0.image);
                    }
                }
            }
        }


        // ФЛАГИ
        if (obj.flags != null) {
            if (obj.flags.size() != 0) {
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                param.setMargins(Helps.dpToPx(5), Helps.dpToPx(3), Helps.dpToPx(5), Helps.dpToPx(3));

                viewHolder0.layout_flags.removeAllViews();
                for (ObjFlag f : obj.flags) {
                    TextView txtFlag = new TextView(mContext);
                    txtFlag.setLayoutParams(param);
                    txtFlag.setPadding(Helps.dpToPx(5), Helps.dpToPx(3), Helps.dpToPx(5), Helps.dpToPx(3));
                    txtFlag.setTextSize(10);
                    txtFlag.setBackgroundResource(R.drawable.flag_product);
                    GradientDrawable drawable = (GradientDrawable) txtFlag.getBackground();
                    drawable.setColor(Color.parseColor(f.color));
                    txtFlag.setTextColor(Color.parseColor(f.text_color));
                    txtFlag.setText(f.name);

                    viewHolder0.layout_flags.addView(txtFlag);
                }
            }
        }

        // ЦЕНЫ
        viewHolder0.layout_only_price.setVisibility(View.GONE);
        viewHolder0.layout_price_1.setVisibility(View.GONE);
        viewHolder0.layout_price_2.setVisibility(View.GONE);
        viewHolder0.layout_price_3.setVisibility(View.GONE);

        viewHolder0.txtDiscountProcent.setVisibility(View.GONE);

        if (obj.prices != null) {
            if (obj.prices.size() == 1) {
                ObjPrice price = obj.prices.get(0);
                if (Helps.isNotEmpty(price.weight)) {
                    viewHolder0.txtValuePrice.setText(price.weight);
                    viewHolder0.txtValuePrice.setVisibility(View.VISIBLE);

                    setPrice(price, viewHolder0.txtPrice, viewHolder0.txtOldPrice, null, viewHolder0.txtDiscountProcent, viewHolder0.layout_only_price);
                } else {
                    viewHolder0.txtValuePrice.setVisibility(View.GONE);
                    setPrice(price, viewHolder0.txtPrice, viewHolder0.txtOldPrice, null, viewHolder0.txtDiscountProcent, viewHolder0.layout_only_price);
                }

            } else if (obj.prices.size() > 1) {

                ObjPrice price1 = obj.prices.get(0);
                ObjPrice price2 = obj.prices.get(1);
                ObjPrice price3 = null;
                if (obj.prices.size() > 2) {
                    price3 = obj.prices.get(2);
                }
                setPrice(price1, viewHolder0.txtPrice1, viewHolder0.txtOldPrice1, viewHolder0.txtValuePrice1, viewHolder0.txtDiscountProcent, viewHolder0.layout_price_1);
                setPrice(price2, viewHolder0.txtPrice2, viewHolder0.txtOldPrice2, viewHolder0.txtValuePrice2, null, viewHolder0.layout_price_2);
                if (price3 != null) {
                    setPrice(price3, viewHolder0.txtPrice3, viewHolder0.txtOldPrice3, viewHolder0.txtValuePrice3, null, viewHolder0.layout_price_3);
                }
            }
        }


        DBFavorites favorites = new DBFavorites(mContext);
        if (favorites.exist(obj.id)) {
            viewHolder0.checkBoxAddFavorit.setChecked(true);
        } else {
            viewHolder0.checkBoxAddFavorit.setChecked(false);
        }
        favorites.close();

        // ДОБАВИТЬ В ИЗБРАННОЕ
        viewHolder0.checkBoxAddFavorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBFavorites favorites = new DBFavorites(mContext);
                if (favorites.exist(obj.id)) {
                    favorites.delete(obj.id);
                } else {
                    favorites.add(obj.id);
                }
                favorites.close();
            }
        });

        viewHolder0.setClickListener(clickListenerProductMin);
    }

    void setPrice(ObjPrice price, TextView txtPrice, TextView txtOldPrice, TextView txtValuePrice, TextView txtDiscount, LinearLayout layout_price) {
        if (price.oldPrice != 0) {
            String oldPriceText = "";
            oldPriceText = String.valueOf(price.oldPrice);//.concat(" ").concat(mContext.getResources().getString(R.string.price_rub));

            txtOldPrice.setText(oldPriceText);
            txtOldPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            txtOldPrice.setVisibility(View.VISIBLE);

            String PriceText = String.valueOf(price.price).concat(" ").concat(mContext.getResources().getString(R.string.price_rub));
            txtPrice.setText(PriceText);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                txtPrice.setTextColor(mContext.getResources().getColorStateList(R.color.colorTextDiscountPrice));
            } else {
                txtPrice.setTextColor(ResourcesCompat.getColorStateList(mContext.getResources(), R.color.colorTextDiscountPrice, null));
            }

            //a > b
            //((a-b)/a) * 100
            if (txtDiscount != null) {
                price.sumSalePercent = ((float)(((float)price.price-(float)price.oldPrice)/(float)price.price) * 100f);
                if (price.sumSalePercent != 0) {
                    txtDiscount.setText(String.valueOf((int)Math.round(price.sumSalePercent)).concat("%"));
                    txtDiscount.setVisibility(View.VISIBLE);
                } else {
                    txtDiscount.setVisibility(View.GONE);
                }
            }

        } else {
            txtOldPrice.setVisibility(View.GONE);
            String PriceText = String.valueOf(price.price).concat(" ").concat(mContext.getResources().getString(R.string.price_rub));
            txtPrice.setText(PriceText);
        }
        if (txtValuePrice != null) {
            if (price.weight != null) {
                if (!price.weight.equals("")) {
                    txtValuePrice.setText(price.weight);
                }
            }
        }
        layout_price.setVisibility(View.VISIBLE);
    }



    private ObjProductMin getItem(int position){
        return mList.get(position);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderProduct extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image;
        TextView text, txtPrice, txtOldPrice, txtDiscountProcent, txtValuePrice;
        TextView txtValuePrice1, txtValuePrice2, txtValuePrice3;
        TextView txtPrice1, txtPrice2, txtPrice3;
        TextView txtOldPrice1, txtOldPrice2, txtOldPrice3;
        CheckBox checkBoxAddFavorit;
        LinearLayout layout_flags, layout_only_price, layout_price_1, layout_price_2, layout_price_3;

        ClickListenerProductMin clickListenerProductMin;

        public ViewHolderProduct(View itemView) {
            super(itemView);
            image                       = (ImageView)itemView.findViewById(R.id.imgImage);
            text                        = (TextView)itemView.findViewById(R.id.product_item_text);
            txtPrice                    = (TextView)itemView.findViewById(R.id.txtPrice);
            txtOldPrice                 = (TextView)itemView.findViewById(R.id.txtOldPrice);
            text                        = (TextView)itemView.findViewById(R.id.product_item_text);
            txtDiscountProcent          = (TextView)itemView.findViewById(R.id.txtDiscountProcent);
            layout_flags                = (LinearLayout)itemView.findViewById(R.id.layout_flags);
            layout_only_price           = (LinearLayout)itemView.findViewById(R.id.layout_only_price);
            layout_price_1              = (LinearLayout)itemView.findViewById(R.id.layout_price_1);
            layout_price_2              = (LinearLayout)itemView.findViewById(R.id.layout_price_2);
            layout_price_3              = (LinearLayout)itemView.findViewById(R.id.layout_price_3);

            txtValuePrice               = (TextView)itemView.findViewById(R.id.txtValuePrice1);
            txtValuePrice1              = (TextView)itemView.findViewById(R.id.txtValuePrice1);
            txtValuePrice2              = (TextView)itemView.findViewById(R.id.txtValuePrice2);
            txtValuePrice3              = (TextView)itemView.findViewById(R.id.txtValuePrice3);

            txtPrice1                   = (TextView)itemView.findViewById(R.id.txtPrice1);
            txtPrice2                   = (TextView)itemView.findViewById(R.id.txtPrice2);
            txtPrice3                   = (TextView)itemView.findViewById(R.id.txtPrice3);

            txtOldPrice1                = (TextView)itemView.findViewById(R.id.txtOldPrice1);
            txtOldPrice2                = (TextView)itemView.findViewById(R.id.txtOldPrice2);
            txtOldPrice3                = (TextView)itemView.findViewById(R.id.txtOldPrice3);

            checkBoxAddFavorit          = (CheckBox)itemView.findViewById(R.id.checkBoxAddFavorit);

            txtOldPrice.setVisibility(View.GONE);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListenerProductMin != null) {
                clickListenerProductMin.onClick(v, getItem(getAdapterPosition()));
            } else {
                Intent intent = new Intent((Activity)mContext, product_item.class);
                intent.putExtra("id", getItem(getAdapterPosition()).id);
                ((Activity)mContext).startActivity(intent);
            }
        }
        public void setClickListener(ClickListenerProductMin clickListener){
            this.clickListenerProductMin = clickListener;
        }

    }
}