package com.mobil2b.renatganiev.torfflowers.ControlView.Menu;


import android.view.View;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Menu.ObjMenuItem;


/**
 * Created by Руслан on 09.04.2017.
 */

public interface ClickListenerMenu {
    void onClick(View view, ObjMenuItem objMenuItem);
}
