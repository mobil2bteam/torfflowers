package com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.Cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;


/**
 * Created by renatganiev on 14.10.15.
 */
public class DBFavorites {

    private ContentValues cv;
    private SQLiteDatabase db;
    private DBHelper dbHelper;
    private Context mContext;
    private String TABLE = "favorites";

    public DBFavorites(Context context) {
        // создаем объект для данных
        cv          = new ContentValues();
        // создаем объект для создания и управления версиями БД
        dbHelper    = new DBHelper(context);
        // подключаемся к БД
        db          = dbHelper.getWritableDatabase();
        mContext    = context;
    }

    // ДОБАВЛЯЕМ
    public long add(int product_id) {

        if (exist(product_id)) return 0;

        cv          = new ContentValues();
        cv.put("product_id",         product_id);
        long rowID = db.insert(TABLE, null, cv);
        return rowID;
    }

    // ПОЛУЧАЕМ
    public ArrayList<Integer> getList() {
        cv          = new ContentValues();
        String query    = "SELECT * FROM "+TABLE;
        Cursor cursor1  = db.rawQuery(query, null);

        ArrayList<Integer> listResult = new ArrayList<>();
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {
                listResult.add(cursor1.getInt(cursor1.getColumnIndex("product_id")));
            }
        }
        return listResult;
    }

    // ПОЛУЧАЕМ
    public String getString() {
        cv          = new ContentValues();
        String query    = "SELECT * FROM "+TABLE;
        Cursor cursor1  = db.rawQuery(query, null);

        String listResult = "";
        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {
                if (listResult.length() == 0) {
                    listResult = listResult.concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("product_id"))));
                } else {
                    listResult = listResult.concat("-").concat(String.valueOf(cursor1.getInt(cursor1.getColumnIndex("product_id"))));
                }
            }
        }
        return listResult;
    }

    // ПРОВЕРЯЕМ
    public boolean exist(int id) {
        cv          = new ContentValues();
        String query    = "SELECT * FROM ".concat(TABLE).concat(" WHERE product_id = ").concat(String.valueOf(id));
        Cursor cursor1  = db.rawQuery(query, null);
        if (cursor1.getCount() > 0) {
            return true;
        }
        return false;
    }

    // УДАЛЯЕМ
    public boolean delete(int id) {
        int clearCount = db.delete(TABLE, "product_id = ".concat(String.valueOf(id)), null);
        if (clearCount > 0) {
            return true;
        } else {
            return false;
        }
    }


    // ОЧИСТИТЬ
    public int clear() {
        int clearCount = db.delete(TABLE, null, null);
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE + "'");
        return clearCount;
    }

    public void close() {
        db.close();
    }

}
