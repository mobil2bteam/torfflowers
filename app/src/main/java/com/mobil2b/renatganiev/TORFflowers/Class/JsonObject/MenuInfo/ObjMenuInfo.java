package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.MenuInfo;


import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

public class ObjMenuInfo {
    public int id;
    public String name;
    public String description_short;
    public String description_long;
    public ObjImage image;
    public String url;


    public ObjMenuInfo() {}
    public ObjMenuInfo(JSONObject json) {
        loadJsonObject(json);
    }
    public ObjMenuInfo(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            json = json.getJSONObject("menu_info");
            loadJsonObject(json);
        } catch (JSONException e) {
        }
    }

    public void loadJsonObject(JSONObject json){
        try {
            if(json.has("id")){
                this.id = json.getInt("id");
            }
            if(json.has("name")){
                this.name = json.getString("name");
            }
            if(json.has("description_short")){
                this.description_short = json.getString("description_short");
            }
            if(json.has("description_long")){
                this.description_long = json.getString("description_long");
            }
            if(json.has("url")){
                this.url = json.getString("url");
            }
            if (json.has("image")) {
                image = new ObjImage(json.getJSONObject("image"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
