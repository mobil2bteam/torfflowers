package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjTypePrice;

import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by renatganiev on 17.04.16.
 */
public class ObjTypePrice {

    public int      id;
    public String   description_short, name;
    public ObjImage image;

    public ObjTypePrice() {
        init();
    }
    public ObjTypePrice(JSONObject json) {
        init();
        loadJSONObject(json);
    }
    public ObjTypePrice(String txt) {
        try {
            init();
            JSONObject json = new JSONObject(txt);
            if (json.has("price_type")) {
                JSONObject jsonMain = json.getJSONObject("price_type");
                loadJSONObject(jsonMain);
            }
        } catch (JSONException e) {
        }
    }

    private void init() {
    }

    public void loadJSONObject(JSONObject json) {
        try {
            if (json.has("id")) {
                id = json.getInt("id");
            }
            if (json.has("description_short")) {
                description_short = json.getString("description_short");
            }
            if (json.has("name")) {
                name = json.getString("name");
            }
            if (json.has("image")) {
                image = new ObjImage(json.getJSONObject("image"));
            }
        } catch (JSONException e) {
        }
    }

}
