package com.mobil2b.renatganiev.torfflowers.Class.JsonObject.Menu;


import com.mobil2b.renatganiev.torfflowers.Class.JsonObject.ObjImage.ObjImage;
import com.mobil2b.renatganiev.torfflowers.Class.QuestionsData.ObjQuestion.ObjLink;

import org.json.JSONException;
import org.json.JSONObject;

public class ObjMenuItem {
    public int id;
    public String name;
    public String desctiption;
    public ObjImage image;
    public ObjImage icon;
    public String background_color;
    public String text_color;
    public boolean  flag_not_show_name;
    public ObjLink link = new ObjLink();

    public ObjMenuItem() {}
    public ObjMenuItem(JSONObject json) {
        loadJsonObject(json);
    }
    public ObjMenuItem(String txt) {
        try {
            JSONObject json = new JSONObject(txt);
            loadJsonObject(json);
        } catch (JSONException e) {
        }
    }

    public void loadJsonObject(JSONObject json){
        try {
            if(json.has("name")){
                this.name = json.getString("name");
            }
            if(json.has("desctiption")){
                this.desctiption = json.getString("desctiption");
            }
            if(json.has("image")){
                image = new ObjImage(json.getJSONObject("image"));
            }
            if(json.has("icon")){
                icon = new ObjImage(json.getJSONObject("icon"));
            }
            if(json.has("background_color")){
                this.background_color = json.getString("background_color");
            }
            if(json.has("text_color")){
                this.text_color = json.getString("text_color");
            }
            if(json.has("link")){
                link = new ObjLink(json.getJSONObject("link"));
            }
            if (json.has("flag_not_show_name")) {
                flag_not_show_name = json.getBoolean("flag_not_show_name");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
